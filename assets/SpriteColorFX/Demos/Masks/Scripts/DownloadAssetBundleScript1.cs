﻿using UnityEngine;
using System.Collections;
using System;

public class DownloadAssetBundleScript1 : MonoBehaviour {

	public string BundleURL;
	public string AssetName;
	public int version;
	public string[] assets;
	
	void Start() {
		//StartCoroutine (DownloadAndCache());

		BundleURL = "https://academyassets.blob.core.windows.net/iosbundles/pruebamathlon.unity3d";
		AssetName = "DemoMathlon";
		version = 0;

		StartCoroutine (GetAssetBundle());
	}
	
	IEnumerator DownloadAndCache (){

		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;
		
		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using(WWW www = WWW.LoadFromCacheOrDownload (BundleURL, version)){
			yield return www;
			if (www.error != null)
				throw new Exception("WWW download had an error:" + www.error);
			AssetBundle bundle = www.assetBundle;
			assets = bundle.GetAllAssetNames();
			for(int i=0;i<assets.Length;i++){Debug.Log("este es uno de los assets: " + assets[i]);}
			if (AssetName == "")
				Instantiate(bundle.mainAsset);
			else
				Instantiate(bundle.LoadAsset(AssetName));
			// Unload the AssetBundles compressed contents to conserve memory
			bundle.Unload(false);
			
		} // memory is freed from the web stream (www.Dispose() gets called implicitly)


	}

	IEnumerator GetAssetBundle() {
		WWW download;
		
		//string url = "http://somehost/somepath/someassetbundle.assetbundle";
		
		download = WWW.LoadFromCacheOrDownload(BundleURL, 0);
		
		yield return download;
		
		AssetBundle assetBundle = download.assetBundle;
		Debug.Log ("Este es el nombre del bundle que descargue " + assetBundle.name);
		
		if (assetBundle != null) {
			Debug.Log("entro en el asset pero aun no lo instancie");
			// Alternatively you can also load an asset by name (assetBundle.Load("my asset name"))
			Instantiate(assetBundle.LoadAsset(AssetName));
			Debug.Log("se supone que ya lo instancie");
			/*Object go = assetBundle.mainAsset;
			
			if (go != null)
				Instantiate(go);
			else
				Debug.Log("Couldn't load resource");	*/
		} else {
			Debug.Log("Couldn't load resource");	
		}
	}
}
