﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// .
// Copyright (c) Ibuprogames. All rights reserved.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using UnityEditor;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class AssetbundleHelper
{
  [MenuItem("Assets/Asset Bundles Helper/Create from selection")]
  static void CreateAssetBundleFromSelection()
  {
    Caching.CleanCache();

    Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
    if (selection.Length > 0)
    {
      string[] assetNames = new string[selection.Length];
      int i = 0;
      foreach (Object obj in selection)
        assetNames[i++] = AssetDatabase.GetAssetPath(obj);

      AssetBundleBuild[] buildMap = new AssetBundleBuild[1];
      buildMap[0] = new AssetBundleBuild();
      buildMap[0].assetBundleName = "TestBundle";
      buildMap[0].assetNames = assetNames;

      if (BuildPipeline.BuildAssetBundles("Assets/StreamingAssets", buildMap, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget) != null)
      {
        AssetDatabase.Refresh();

        Debug.Log("Done!");
      }
      else
        Debug.LogWarning("Fail!");
    }
    else
      Debug.LogWarning("No assets :(");
  }
}
