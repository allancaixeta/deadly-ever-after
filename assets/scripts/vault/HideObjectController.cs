using UnityEngine;
using System.Collections;

public class HideObjectController : MonoBehaviour {
	
	Transform player;
	bool hidden = false;
	public Shader transparent, shadow;
	public float transparency = 0.35f;
	public float maxDistance = 50;
	float countToNormalMaterial = 0;
	GameObject [] childs;
	Color [][] childsDefaultColors;
	Color [][] childsTransparentColors;
	// Use this for initialization
	void Start () {                                       
		shadow = Shader.Find("Custom/ShadowOrtho");
		transparent = Shader.Find("Custom/Diffuse with Shadow");
		player = GameObject.FindGameObjectWithTag("Player").transform;	
		childs = new GameObject [transform.childCount];
		childsDefaultColors = new Color [transform.childCount][];
		childsTransparentColors = new Color [transform.childCount][];
		for (int i = 0; i < transform.childCount; i++){
			childs[i] = transform.GetChild(i).gameObject;
			childsTransparentColors[i] = new Color [childs[i].GetComponent<Renderer>().materials.Length];
			childsDefaultColors[i] = new Color [childs[i].GetComponent<Renderer>().materials.Length];
			for (int j = 0; j < childs[i].GetComponent<Renderer>().materials.Length; j++) {
				childsDefaultColors[i][j] = childs[i].GetComponent<Renderer>().materials[j].color;
				Color newColor = childsDefaultColors[i][j];
				newColor.a = transparency;
				childsTransparentColors[i][j] = newColor;
			}
		}
		AdjustMaxDistance();
	}
	
	void AdjustMaxDistance()
	{
		if(transform.lossyScale.x > transform.lossyScale.z)
		{
			if(transform.lossyScale.x > 100)
				maxDistance *= transform.lossyScale.x/100;
		}
		else
		{
			if(transform.lossyScale.z > 100)
				maxDistance *= transform.lossyScale.z/100;
		}
	}
	
	float cont = 0;
	// Update is called once per frame
	void Update () {
		cont+= GameController.deltaTime;
		if(cont>0.75f)
		{
			cont = 0;
			if(Vector2.Distance(player.position,transform.position) < maxDistance)
			{
				if(player.position.z > this.transform.position.z)
				{
					if(!hidden)
					{
						hidden = true;	
						countToNormalMaterial = 1;
					}
				}
				else
				{
					if(hidden)
					{
						hidden = false;	
						countToNormalMaterial = 1;
					}
				}
			}
		}
		if(countToNormalMaterial > 0)
		{
			countToNormalMaterial -= GameController.deltaTime/3;
			MakeTransparent();
			if(countToNormalMaterial < 0 && !hidden){
				for (int i = 0; i < transform.childCount; i++) {	
					for (int j = 0; j < childs[i].GetComponent<Renderer>().materials.Length; j++) {
						childs[i].GetComponent<Renderer>().materials[j].shader = shadow;
					}
				}
			}
		}
	}
	
	void MakeTransparent()
	{
		if(hidden)
		{
			for (int i = 0; i < transform.childCount; i++) {	
				for (int j = 0; j < childs[i].GetComponent<Renderer>().materials.Length; j++) {
					childs[i].GetComponent<Renderer>().materials[j].shader = transparent;
					childs[i].GetComponent<Renderer>().materials[j].color = Color.Lerp(childsTransparentColors[i][j],childs[i].GetComponent<Renderer>().materials[j].color,countToNormalMaterial);
				}
			}
		}
		else
		{
			for (int i = 0; i < transform.childCount; i++) {	
				for (int j = 0; j < childs[i].GetComponent<Renderer>().materials.Length; j++) {
					childs[i].GetComponent<Renderer>().materials[j].color = Color.Lerp(childsDefaultColors[i][j],childs[i].GetComponent<Renderer>().materials[j].color,countToNormalMaterial);
				}
			}
		}
	}
}
