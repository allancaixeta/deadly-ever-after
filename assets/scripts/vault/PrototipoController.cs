using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PrototipoController : MonoBehaviour {

	int monsterChosen;	
	
	public GameObject []weaponAOE = new GameObject[5];
	public int weaponChosenAOE = 0 ;
	
	public GameObject []weaponDefense = new GameObject[5];
	public int weaponChosenDefense = 0;
	
	public GameObject []weaponCrush = new GameObject[5];
	public int weaponChosenCrush = 0;

	bool detailVisualization = false;
	public GameObject detailsSkin;
	public GameObject detail;
	public Texture2D []weaponAOEDetail = new Texture2D[5];
	public Texture2D []weaponDefenseDetail = new Texture2D[5];
	public Texture2D []weaponCrushDetail = new Texture2D[5];

	public string [] CRUSHdescription;
	public string [] AOEdescription;
	public string [] DEFdescription;

	public Text crushText, AOEText, defText;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(detailVisualization)
		{
			if(Input.GetKeyDown(KeyCode.T))
			{
				detailVisualization = false;
				detail.SetActive (false);
			}
		}
		if(Input.GetKeyDown(KeyCode.T))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			if(Physics.Raycast(ray, out hit, 1000)){
				print (hit.collider.tag);
				switch(hit.collider.tag)
				{
				case "ProtWeapon":
					print ("2");
					string weapontype =	hit.transform.name;
					int n;
					GameController.categories cat;
					string temp = weapontype.Substring (0,2);
					if(temp == "cc")
					{
						cat = GameController.categories.aoe;
						n = int.Parse(weapontype.Substring(2));
					}
					else
					{
						if(temp == "cr")
						{
							cat = GameController.categories.crush;
							n = int.Parse(weapontype.Substring (5));
						}	
						else
						{
							cat = GameController.categories.defense;
							n = int.Parse(weapontype.Substring (5));
						}
					}
					VisualizeDetailsWeapon(n,cat);
					break;	
				}
			}
		}
	}

	public void StartGame()
	{
		detailVisualization = false;
		detail.SetActive (false);
	}

	public void SetAOEWeapon(int aoe)
	{
		for (int i = 0; i < weaponAOE.Length; i++) {
			if(i != aoe)
				weaponAOE[i].SendMessage ("TurnOff");
		}
		weaponChosenAOE = aoe;
		AOEText.text = AOEdescription [weaponChosenAOE];
	}

	public void SetDefenseWeapon(int c)
	{
		for (int i = 0; i < weaponDefense.Length; i++) {
			if(i != c)
				weaponDefense[i].SendMessage ("TurnOff");
		}
		weaponChosenDefense = c;
		defText.text = DEFdescription [weaponChosenDefense];
	}

	public void SetCrushWeapon(int c)
	{
		for (int i = 0; i < weaponCrush.Length; i++) {
			if(i != c)
				weaponCrush[i].SendMessage ("TurnOff");
		}
		weaponChosenCrush = c;
		crushText.text = CRUSHdescription [weaponChosenCrush];
	}
		
	public void VisualizeDetailsWeapon(int n, GameController.categories type)
	{
		detailVisualization = true;	
		detail.SetActive (true);
		switch(type)
		{
		case GameController.categories.aoe:
			detailsSkin.GetComponent<Renderer>().material.mainTexture = weaponAOEDetail[n];
			break;	
		case GameController.categories.defense:
			detailsSkin.GetComponent<Renderer>().material.mainTexture = weaponDefenseDetail[n];
			break;	
		case GameController.categories.crush:
			detailsSkin.GetComponent<Renderer>().material.mainTexture = weaponCrushDetail[n];
			break;
		}	
	}
}

