using UnityEngine;
using System.Collections;

public class FloorTiller : MonoBehaviour {
	
	public float tillDivisionRateX = 4;
	public float tillDivisionRateY = 4;
	// Use this for initialization
	void Awake () {
		Vector2 FloorTileMatrix;		
		if(transform.lossyScale.x > transform.lossyScale.y)
		{
			FloorTileMatrix.x = transform.lossyScale.x / tillDivisionRateX;
			if(tillDivisionRateY == 0)
				FloorTileMatrix.y = gameObject.GetComponent<Renderer>().material.mainTextureScale.y;
			else
				FloorTileMatrix.y = transform.lossyScale.y / tillDivisionRateY;
			gameObject.GetComponent<Renderer>().material.mainTextureScale = FloorTileMatrix;
		}
		else
		{
			FloorTileMatrix.x = transform.lossyScale.y / tillDivisionRateX;
			if(tillDivisionRateY == 0)
				FloorTileMatrix.y = gameObject.GetComponent<Renderer>().material.mainTextureScale.y;
			else
				FloorTileMatrix.y = transform.lossyScale.y / tillDivisionRateY;
			gameObject.GetComponent<Renderer>().material.mainTextureScale = FloorTileMatrix;
		}	
		this.enabled = false;
	}
}
