using UnityEngine;
using System.Collections;

public class GuideController : MonoBehaviour {
	
	public enum guideType{
		skill,	
		monster,
		weapon
	}
	
	public guideType type;
	
	TextMesh weaponDPS, weaponName;
	
	void Awake()
	{
		switch(type)
		{
		case guideType.weapon:
			weaponDPS = GameObject.Find("weaponDPS").GetComponent<TextMesh>();
			weaponName = GameObject.Find("weaponName").GetComponent<TextMesh>();
			break;
		}
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SetSkillData(SkillController item)
	{
		
	}

	public void SetWeaponData(WeaponController weapon)
	{
		weaponName.text = weapon.weaponName;
		weaponDPS.text = ((int)weapon.GetDPS()).ToString();
		GetComponent<Renderer>().material.mainTexture = weapon.weaponAppearenceUP;
	}
	
	public void SetMonsterData(MonsterGeneric monster)
	{
		
	}
}
