using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour {
	
	
	public enum PortalType
	{
		oneway,
		twoway,
		threeway
	}
	public PortalType portalType;
	public Transform end, thirdway;
	public string eventName;
	public GameObject eventTarget;
	bool secondTime = false;
	void OnTriggerEnter(Collider other)
	{
		print (other.name);
		switch(portalType)
		{
		case PortalType.oneway:
			if(other.tag == "Player")
			{
				other.transform.position = end.position;
				if(eventTarget != null)
					eventTarget.SendMessage (eventName);
				Destroy (this.gameObject);
			}
		break;
		case PortalType.twoway:
			if(other.tag == "Player")
			{
				other.transform.position = end.position;
				if(eventTarget != null)
					eventTarget.SendMessage (eventName);
			}
			break;
		case PortalType.threeway:
			if(other.tag == "Player")
			{
				if(secondTime)
				{
					other.transform.position = thirdway.position;
					secondTime = false;
				}
				else
				{
					secondTime = true;
					other.transform.position = end.position;
				}
				if(eventTarget != null)
					eventTarget.SendMessage (eventName);
			}
			break;
		}
	}
}
