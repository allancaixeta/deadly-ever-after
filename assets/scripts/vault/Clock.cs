﻿using UnityEngine;
using System.Collections;

public class Clock : MonoBehaviour {

	float cont;	
	// Update is called once per frame
	void Update () {
		cont += GameController.deltaTime;
		GetComponent<GUIText>().text = cont.ToString ("0.0");
		if(cont >= 60)
			cont = 0;
	}
}