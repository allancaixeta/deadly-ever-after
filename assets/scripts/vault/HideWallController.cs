using UnityEngine;
using System.Collections;

public class HideWallController : MonoBehaviour {
	
	public enum direction
	{
		left,
		right,
		up,
		down
	}
	
	public direction wallDirection;
	public float maxDistance = 50;
	public float transparency = 0.5f;
	Transform player;
	bool change = false;
	bool hidden = false;
	Material normal, transparent;
	float maxZ, maxX;
	Color defaultColor, transparentColor;
	bool returnToNormalMaterial;
	float countToNormalMaterial = 0;
	// Use this for initialization
	void Start () {
		//transparent = Shader.Find( "Transparent/Diffuse" );
		player = GameObject.FindGameObjectWithTag("Player").transform;
		normal = GetComponent<Renderer>().sharedMaterial;
		transparent = GetComponent<Renderer>().material;
		transparent.shader = Shader.Find ("Transparent/Specular");	
		defaultColor = transparent.color;
		Color newColor = transparent.color;
		newColor.a = transparency;
		transparentColor = newColor;					
		AdjustMaxDistance();
		GetComponent<Renderer>().material = normal;
		//gameObject.layer = 0;
	}
	
	void AdjustMaxDistance()
	{
		if(transform.lossyScale.x > transform.lossyScale.z)
		{
			if(transform.lossyScale.x > 100)
				maxDistance *= transform.lossyScale.x/100;
		}
		else
		{
			if(transform.lossyScale.z > 100)
				maxDistance *= transform.lossyScale.z/100;
		}
		maxZ = this.transform.position.z + transform.lossyScale.z / 2;
		maxX = this.transform.position.x + transform.lossyScale.x / 2;
	}
	float cont = 1;
	// Update is called once per frame
	void Update () {
		cont+= GameController.deltaTime;
		if(cont>0.75f)
		{
			cont = 0;
			if(Vector2.Distance(player.position,transform.position) < maxDistance)
			{
				switch(wallDirection)
				{
				case direction.up:		
					if(player.position.z > maxZ)
					{
						if(!hidden)
						{
							hidden = true;	
							change = true;
						}
					}
					else
					{
						if(hidden)
						{
							hidden = false;	
							change = true;
						}
					}
					break;
				case direction.down:
					if(player.position.z < maxZ)
					{
						if(!hidden)
						{
							hidden = true;	
							change = true;
						}
					}
					else
					{
						if(hidden)
						{
							hidden = false;	
							change = true;
						}
					}
					break;
				case direction.right:
					if(player.position.x > maxX)
					{
						if(!hidden)
						{
							hidden = true;	
							change = true;
						}
					}
					else
					{
						if(hidden)
						{
							hidden = false;	
							change = true;
						}
					}
					break;
				case direction.left:
					if(player.position.x < maxX)
					{
						if(!hidden)
						{
							hidden = true;	
							change = true;
						}
					}
					else
					{
						if(hidden)
						{
							hidden = false;	
							change = true;
						}
					}
					break;
				}				
				if(change)
				{
					change = false;
					if(hidden)
					{
						GetComponent<Renderer>().material = transparent;
						iTween.ColorTo(this.gameObject,transparentColor,1);
						//gameObject.layer = 10;
					}
					else
					{
						countToNormalMaterial = 1;
						iTween.ColorTo(this.gameObject,defaultColor,1);
						//gameObject.layer = 0;
					}
				}
			}
		}
		if(countToNormalMaterial > 0)
		{
			countToNormalMaterial -= GameController.deltaTime;
			if(countToNormalMaterial < 0)
				GetComponent<Renderer>().material = normal;
		}
	}
}