using UnityEngine;
using System.Collections;

public class SpaceDistortion : MonoBehaviour {

	bool insideSpaceDistortion = false;
	public float DistortionRate = 1;
	public float modifierFloorX, modifierFloorY, modifierObjX, modifierObjY;
	public GameObject floor;
	public GameObject [] objects;	
	Vector3 [] initialObjectsPos;
	Vector3 [] currentObjectsPos;
	Vector2 SpaceDistortionMatrix, ScaleDistortionMatrix, FloorTileMatrix, FloorInitialTileMatrix;
	Vector3 initialFloorScale;
	PlayerController player;
	Vector3 playerPosition;	
	// Use this for initialization
	void Start () {
		player = (PlayerController) FindObjectOfType(typeof(PlayerController));	
		initialObjectsPos = new Vector3 [objects.Length];
		currentObjectsPos = new Vector3 [objects.Length];
		for (int i = 0; i <  objects.Length; i++) {
			initialObjectsPos[i] = objects[i].transform.position;				
		}
	}
	Vector3 temp;
	public void UpdateSpaceDistortion()
	{
		if(insideSpaceDistortion)
		{
			/*floor.transform.localScale = new Vector3(initialFloorScale.x + (player.transform.position.x - playerPosition.x) * DistortionRate,
				initialFloorScale.y + (player.transform.position.z - playerPosition.z) * DistortionRate,
				initialFloorScale.z);*/
			temp = new Vector3(initialFloorScale.x + (playerPosition.x - player.transform.position.x) * DistortionRate * modifierFloorX,
				initialFloorScale.y + (playerPosition.z - player.transform.position.z) * DistortionRate * modifierFloorY,
				initialFloorScale.z);			
			FloorTileMatrix = floor.GetComponent<Renderer>().material.mainTextureOffset;				
			FloorTileMatrix.x = temp.x / initialFloorScale.x * FloorInitialTileMatrix.x;
			FloorTileMatrix.y = temp.y / initialFloorScale.y * FloorInitialTileMatrix.y;			
			floor.GetComponent<Renderer>().material.mainTextureOffset = FloorTileMatrix;
			//ScaleDistortionMatrix.x = (temp.x - initialFloorScale.x) * modifier * modifierX;
			//ScaleDistortionMatrix.y = (temp.y - initialFloorScale.y) * modifier * modifierY;		
			ScaleDistortionMatrix.x = (player.transform.position.x - playerPosition.x) * DistortionRate * modifierObjX;
			ScaleDistortionMatrix.y = (player.transform.position.z - playerPosition.z) * DistortionRate * modifierObjY;
			for (int i = 0; i < objects.Length; i++) {
				SpaceDistortionMatrix.x = initialObjectsPos[i].x - floor.transform.position.x;
				SpaceDistortionMatrix.y = initialObjectsPos[i].z - floor.transform.position.z;
				//SpaceDistortionMatrix.x = initialObjectsPos[i].x - floor.transform.position.x;
				//SpaceDistortionMatrix.y = initialObjectsPos[i].y - floor.transform.position.y;				
				objects[i].transform.position = new Vector3(currentObjectsPos[i].x + (ScaleDistortionMatrix.x) * Mathf.Abs(SpaceDistortionMatrix.x)
					,objects[i].transform.position.y,currentObjectsPos[i].z + (ScaleDistortionMatrix.y) * Mathf.Abs(SpaceDistortionMatrix.y));
			}
		}	
	}	
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Player")
			return;
		insideSpaceDistortion = true;
		EnterSpaceDistortion();
	}
	
	void OnTriggerStay(Collider other)
	{
		if(insideSpaceDistortion || other.tag != "Player")
			return;
		OnTriggerEnter(other);
	}
	
	void OnTriggerExit(Collider other)
	{
		if(other.tag != "Player")
			return;
		insideSpaceDistortion = false;
		ExitSpaceDistortion();
	}
	
	public void EnterSpaceDistortion()
	{
		playerPosition = player.transform.position;
		initialFloorScale = floor.transform.localScale;
		FloorInitialTileMatrix = floor.GetComponent<Renderer>().material.mainTextureOffset;	
		for (int i = 0; i <  objects.Length; i++) {
			currentObjectsPos[i] = objects[i].transform.position;				
		}
	}

	void ExitSpaceDistortion()
	{
		floor.transform.localScale = initialFloorScale;
		floor.GetComponent<Renderer>().material.mainTextureOffset = FloorInitialTileMatrix;	
		for (int i = 0; i <  objects.Length; i++) {
			iTween.MoveTo(objects[i],initialObjectsPos[i],3);
		}
	}
}
