﻿using UnityEngine;
using System.Collections;



/// <summary>
/// Stores tags used by the game as constants
/// </summary>
public class Tags : MonoBehaviour {
	public const string Player = "Player";
	public const string PlayerHit = "PlayerHit";
	public const string Monster = "Monster";
	public const string BossMonster = "BossMonster"; //nao acerta o jogador mas eh acertado por ele
	public const string Weapon = "Weapon";
	public const string FurnitureVisual = "FurnitureVisual";
	public const string Wall = "Wall";
	public const string Lvl2Obstacle = "Lvl2Obstacle";
	public const string Lvl3Obstacle = "Lvl3Obstacle";
	public const string Unmovable = "Unmovable";
	public const string Floor = "Floor";
	public const string MonsterProjectile = "MonsterProjectile";
	public const string StunMonster = "StunMonster";
	public const string Explosion = "Explosion";
	public const string HarmEvent = "HarmEvent";
	public const string SelfControlledEvent = "SelfControlledEvent";
	public const string IgnoreDepth = "IgnoreDepth";
}
