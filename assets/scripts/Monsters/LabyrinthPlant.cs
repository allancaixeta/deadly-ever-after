using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LabyrinthPlant : MonsterGeneric {

    public float damageDelay = 0.5f;
    float currentDamageDelay;
    bool playerOnTop;
    float direction;	
	public bool alsoHitMonster = false;
	List<string> monsterHitListID;
	List<float> monsterHitListDuration;
	bool sendMessageToParentWhenHittingPlayer = false;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.projectile;
		monsterHitListID = new List<string> ();
		monsterHitListDuration = new List<float> ();
		gameObject.tag = Tags.SelfControlledEvent;
    }
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;
            currentDamageDelay = damageDelay;
            break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Death:
			deathCountDown = afterLife;
			break;

		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;

		case MonsterStates.Normal:
			break;

		case MonsterStates.Knockback:
			nextState = MonsterStates.Normal;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		if(magicallySlowed)
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}		
		switch(currentState)
		{
		    case MonsterStates.Start:
			    nextState = MonsterStates.Normal;						
			    break;
		
		    case MonsterStates.Normal:
                if(playerOnTop)
                {
                    currentDamageDelay -= GameController.deltaTime;
                    if (currentDamageDelay < 0)
                    {
                        currentDamageDelay = damageDelay;
                        PlayerHitController.Instance.HitPlayer(monsterDamage, "LabyrinthPlant");
                    }
                }                
                DecreaseMonsterHitListDuration();
			    break;
		
		    case MonsterStates.Hook:
			    //controller.Move(velocity);					
			    break;
			
		    case MonsterStates.Death:
			    Death ();
			    break;
			
		    case MonsterStates.Stun:
			    Stun();
			    break;
		
		    case MonsterStates.Knockback:
			    /*knockbackCountDown -= GameController.deltaTime;
			    Move();
			    if(knockbackCountDown < 0)
			    {
				    nextState = monsterStates.normal;
			    }*/
			    break;
			
		    case MonsterStates.Desync:
			    currentDesync -= GameController.deltaTime;
			    if(currentDesync < 0)
				    nextState = MonsterStates.Normal;
			    break;			
		}
	}
	
	void Velocity()
	{		
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			
	}
	
	void Move()
	{	
		transform.Translate(velocity,Space.World);
	}
	
	public void SetDirection(float d)
	{
		direction = d;		
		visual.transform.FindChild ("base").Rotate(0,0,d-90);		
	}
	
	void OnTriggerEnter(Collider other)
	{	
		if(CheckIfCurrentOrNextState(MonsterStates.Death))
		   return;
		if(alsoHitMonster && other.transform.tag == Tags.Monster && other.transform.root != transform.root)
		{
			for (int i = 0; i < monsterHitListID.Count; i++){
				if(monsterHitListID[i] == other.transform.name)
					return;
			}				
			other.GetComponent<MonsterGeneric>().Hit ((int)monsterDamage * 3);			
			monsterHitListID.Add(other.transform.name);
			monsterHitListDuration.Add(1);			
		}
		if(sendMessageToParentWhenHittingPlayer && other.transform.tag == "PlayerHit")
			transform.root.SendMessage ("PlayerHit");		
		if(other.transform.tag == Tags.PlayerHit)
		{
            currentDamageDelay = damageDelay;
            playerOnTop = true;
        }
	}

    void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == Tags.PlayerHit)
        {
            playerOnTop = false;
        }
    }

    public void SetSendMessageToParentWhenHittingPlayer()
	{
		sendMessageToParentWhenHittingPlayer = true;
	}
    
	void DecreaseMonsterHitListDuration()
	{
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime  * timeDistortion;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
	}

	protected override void OnMonsterHit()
	{
	}    
}
