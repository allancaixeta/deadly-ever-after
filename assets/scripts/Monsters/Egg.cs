using UnityEngine;
using System.Collections;

public class Egg : MonsterGeneric {

	public float timeTillBorn = 10;
	public GameObject creatureInside;
	public float maxLifeMultiplier = 3;
	float currentTimeTillBorn;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.egg;
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
			GetOutOfObject();	
			currentTimeTillBorn = timeTillBorn;
			break;
			
		case MonsterStates.Inactive:			
			break;	
			
		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			GetOutOfObject();
			break;
			
		case MonsterStates.Death:
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			FixZ ();
			LifeRegen();
			currentTimeTillBorn -= GameController.deltaTime;
			if(currentTimeTillBorn < 0)
			{
				GameObject temp = Instantiate(creatureInside,transform.position,Quaternion.identity) as GameObject;
				temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
				temp.GetComponent<MonsterGeneric> ().nextState = MonsterStates.Start;
				gameController.addMonster (temp);	
				nextState = MonsterStates.Death;
			}
			break;
			
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}
	
	void Velocity()
	{
		velocity = player.position - transform.position;
			
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{		
		controller.Move(velocity * isPlayerPushing);
	}

	public void SetMaxLife(int life)
	{
		maxLife = (int)(maxLifeMultiplier * (float)(life + scalableMaxLife * (toughnessInNumber - 1)));
		currentLife = (int)(maxLife * skills.GetDebuff ());	
	}

	protected override void OnMonsterHit()
	{
	}
}