using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public class ChaserProjectile : MonsterGeneric {

	public float lifeTime = 7;
	bool passThrough, speedRelativeToPlayer;
	public bool stunPlayer, onlyStun;
	public float stunValue;
	public GameObject FxOnFury;
	float slowWhenHit, slowTimer;
	bool sendMessageToParentWhenHittingPlayer = false;
	GameObject parent;
	float playerSpeed, timer;

	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	public float repathRate = 0.4f;
	private float lastRepath = -9999;

	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.projectile;
		if(!onlyStun)
			gameObject.tag = Tags.MonsterProjectile;
		else
			gameObject.tag = Tags.StunMonster;
		seeker = GetComponent<Seeker>();
		PlayerController playercontroller = PlayerController.Instance;
		playerSpeed = playercontroller.GetMainSpeed();
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			timer = lifeTime;
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Death:
			deathCountDown = afterLife;
			break;

		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;

		case MonsterStates.Normal:
			break;

		case MonsterStates.Knockback:
			nextState = MonsterStates.Normal;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		if(magicallySlowed)
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}		
		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			Velocity ();
			Move ();
			timer -= GameController.deltaTime;
			if(timer < 0)
			{
				nextState = MonsterStates.Start;
				gameObject.SetActive (false);
			}
			break;
		
		case MonsterStates.Hook:
			//controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death ();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			/*knockbackCountDown -= GameController.deltaTime;
			Move();
			if(knockbackCountDown < 0)
			{
				nextState = monsterStates.normal;
			}*/
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}
	
	void Velocity()
	{		
		if (Time.time - lastRepath > repathRate && seeker.IsDone()) {
			lastRepath = Time.time+ repathRate;
			seeker.StartPath (transform.position,player.position, OnPathComplete);				
		}
		if (path == null || currentWaypoint >= path.vectorPath.Count) {
			velocity = player.position - transform.position;
		}         
		else
		{
			//Direction to the next waypoint
			velocity = (path.vectorPath[currentWaypoint]-transform.position);
			if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) 
				currentWaypoint++;
		}
			
		velocity.Normalize();
		if(speedRelativeToPlayer)
		{
			velocity *= GameController.deltaTime * (playerSpeed * speed) * (timeDistortion-magicSlowEffect);
		}
		else
			velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void OnPathComplete (Path p) {
		p.Claim (this);
		if (!p.error) {
			if (path != null) path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		} else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
		}
	}
	
	void Move()
	{	
		transform.Translate(velocity,Space.World);
	}

	void OnTriggerEnter(Collider other)
	{	
		if(CheckIfCurrentOrNextState(MonsterStates.Death))
		   return;
		if(other.transform.tag == "PlayerHit")
		{
			if(sendMessageToParentWhenHittingPlayer)
				parent.SendMessage ("PlayerHit");
			if(stunPlayer)
				player.SendMessage ("Stunned",stunValue);
			if(slowWhenHit > 0)
				player.GetComponent<PlayerController>().SlowDown(slowWhenHit,slowTimer);
			if(!passThrough)
			{
				nextState = MonsterStates.Start;
				gameObject.SetActive (false);
			}
		}
	}
	
	public void SetPassThrough()
	{
		passThrough = true;
	}

	public void SetSendMessageToParentWhenHittingPlayer(GameObject parent)
	{
		sendMessageToParentWhenHittingPlayer = true;
		this.parent = parent;
	}

	public void SetSpeedRelativeToPlayer()
	{
		speedRelativeToPlayer = true;
	}

	public void SetSlowWhenHit(float sv, float st)
	{
		slowWhenHit = sv;
		slowTimer = st;
	}

	public void ResetLifeTime()
	{
		timer = lifeTime;
	}

	public void OnFury(float upgrade)
	{
		GameObject temp = Instantiate(FxOnFury,transform.position,FxOnFury.transform.rotation) as GameObject;
		temp.transform.parent = this.transform;
		temp.transform.localPosition = FxOnFury.transform.localPosition;
		UpgradeMonster (upgrade);
		speed *= (1 + upgrade);
	}

	protected override void OnMonsterHit()
	{
	}
}
