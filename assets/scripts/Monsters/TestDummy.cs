using UnityEngine;
using System.Collections;

public class TestDummy : MonsterGeneric {
	
    public enum dummyType
    {
        spin,
        beanstalk
    }

    public dummyType myDummyType;

	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.dummy;        
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
        switch (currentState)
		{
		    case MonsterStates.Start:
                visual = transform.Find("visual").gameObject;
                listOfAllSprites = visual.gameObject.GetComponentsInChildren<SpriteRenderer>(true);
                animator = visual.GetComponent<Animator>();
                GetAllMaterials();
                nextState = MonsterStates.Desync;						
			    break;
			
		    case MonsterStates.Inactive:			
			    break;	
			
		    case MonsterStates.Desync:
			    currentDesync = Random.Range (0,desyncTimer);
			    FixZ();
			    break;
		}
		return true;
	}
    
    public override void UpdateState()
	{
		if(magicallySlowed)
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}
        switch (currentState)
        {
            case MonsterStates.Start:
                nextState = MonsterStates.Normal;
                break;

            case MonsterStates.Normal:
                LifeRegen();
                Slow();  //handle slowDuration counter
                Armor(); //handle breakable armor counter      
                switch (monsterToughness)
			    {
			    case toughness.one:
			    case toughness.two:
			    case toughness.three:
			    case toughness.four:
			    case toughness.five:
				    break;
			    }
			    break;
			
		    case MonsterStates.Hook:
			    controller.Move(velocity);					
			    break;
			
		    case MonsterStates.Death:
			    DestroyItSelf();	
			    break;
			
		    case MonsterStates.Stun:
			    Stun();
			    break;
			
		    case MonsterStates.Knockback:
			    knockbackCountDown -= GameController.deltaTime;
			    Move();
			    if(knockbackCountDown < 0)
			    {
				    nextState = MonsterStates.Normal;
			    }
			    break;
			
		    case MonsterStates.Desync:
			    currentDesync -= GameController.deltaTime;
			    if(currentDesync < 0)
				    nextState = MonsterStates.Normal;
			    break;			
		}
	}
	
	void Velocity()
	{
		velocity = player.position - transform.position;
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);			
	}
	
	void Move()
	{		
		controller.Move(velocity * isPlayerPushing);
	}

	protected override void OnMonsterHit()
	{
	}
}