using UnityEngine;
using System.Collections;
//Note this line, if it is left out, the script won't know that the class 'Path' exists and it will throw compiler errors
//This line should always be present at the top of scripts which use pathfinding
using Pathfinding;

public class Hydra : MonsterGeneric {
	
	public GameObject smallerCopy;
	public float timeToCreateACopy = 3;
	float currentTimeToNextCopy = 0;
	public int numberOfCopiesPerCicle = 2;
	public float birthTime = 1;
	public int damageGrowth = 1;
	public int lifeGrowth = 2;
	public float sizeGrowth = 0.1f;
	float currentBirthTime = 0;
	public bool isFilho = false;
	public bool isNeto = false;
	Transform cenarioLimits, goalPosition;
	bool stopped = false;
	float maxStoppedTime = 2;
	float currentStoppedTime;
	Vector3 lastPosition;
	private float distancefromGoal;
	Vector3 difference;
	float joinDelay = 2;	
	Seeker seeker;
	 //The calculated path
    public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 3;
 
    //The waypoint we are currently moving towards
    private int currentWaypoint = 0;
    
    public float repathRate = 0.5f;
    private float lastRepath = -9999;
	float radiusCollider;
	public GameObject HydraFather;
	bool InstantiateAfterDeath = false;

	bool growed = false;
	public float attackTriggerRange = 10;
	public float foresseTimer = 5;
	public float attackCooldown = 2;
	float currentAttackCooldown = 2;

	public float attackDuration = 1;
	float currentAttackDuration = 1;

	public GameObject visualProj, projectile;
	public override void SpecificMonsterAwake()
	{
		seeker = GetComponent<Seeker>();
		goalPosition = new GameObject().transform;
		monsterType = MonsterGeneric.monsterTypes.hydra;
		radiusCollider = GetComponent<CharacterController>().radius;
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
		switch(currentState)
		{
		case MonsterStates.Start:
			currentTimeToNextCopy = timeToCreateACopy * GetCooldownModifier ();
			GetOutOfObject();
			StageController stage = (StageController) FindObjectOfType(typeof(StageController));	
			cenarioLimits = stage.GetRoom().GetCenarioLImits ();
			CreateNewGoal ();	
			nextState = MonsterStates.Desync;
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Preparation:
			FixZ();
			currentBirthTime = birthTime;
			animator.SetBool("birth",true);
			animator.speed = 1/currentBirthTime;
			break;

		case MonsterStates.Normal:
			animator.speed = 1;
			break;

		case MonsterStates.Knockback:
			if(!isNeto)
				animator.SetBool("birth",false);
			break;

		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
			{
				if(lastState == MonsterStates.Preparation)
					currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
				else
					currentDesync = Random.Range (desyncTimeMin * GetCooldownModifier(),desyncTimer/2);
			}
			animator.SetTrigger("idle");
			animator.speed = 1/currentDesync;
			FixZ();
			break;

		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			break;

		case MonsterStates.Attack:
			currentAttackCooldown = attackCooldown;
			currentAttackDuration = attackDuration;
			break;
		}
		return true;
	}

	Vector3 playerFuturePos;

	public override void UpdateState()
	{
		joinDelay -= GameController.deltaTime;
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			LifeRegen ();
			/*Calcula se ele travou em algum lugar por mais de 2 segundos, para recriar o goal nesse caso*/
			difference = lastPosition - transform.position;
			if(stopped)
			{
				currentStoppedTime -= GameController.deltaTime;
				if(currentStoppedTime < 0)
				{
					CreateNewGoal ();
					stopped = false;
				}
				else
				{					
					if(Vector2.Distance (lastPosition,transform.position) > 0.2f)
					{	
						stopped = false;
						lastPosition = transform.position;
					}
				}
			}
			else
			{				
				if((difference.x < 0.1f && difference.y < 0.1f))
				{
					stopped = true;
					currentStoppedTime = maxStoppedTime;
				}
				else
					lastPosition = transform.position;
			}			
			Velocity ();
			Move ();
			distancefromGoal = Vector2.Distance (transform.position,goalPosition.position);
			if(distancefromGoal < 3)
				CreateNewGoal ();
			currentTimeToNextCopy -= GameController.deltaTime;
			currentAttackCooldown -= GameController.deltaTime;
			if (Vector2.Distance (player.position, transform.position) < attackTriggerRange && currentAttackCooldown < 0)
			{
				nextState = MonsterStates.Attack;
				Vector2 tv = player.GetComponent<PlayerController> ().GetCurrentSpeed ();
				if (tv == Vector2.zero)
					playerFuturePos = player.position;
				else
					playerFuturePos = new Vector2 (player.position.x + tv.x * foresseTimer, player.position.y + tv.y * foresseTimer);
				GameObject temp = Instantiate (visualProj, transform.position, Quaternion.identity) as GameObject;
				temp.GetComponent<GoToTarget> ().SetTarget (playerFuturePos,attackDuration);
				gameController.addUpdatableGameObject (temp);
			}
			switch(monsterToughness)
			{
			case toughness.one:				
				if(isNeto || isFilho)
					return;
				if(currentTimeToNextCopy < 0)
				{
					nextState = MonsterStates.Preparation;	
				}
				break;
				
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				if(isNeto)
					return;
				if(currentTimeToNextCopy < 0)
				{
					nextState = MonsterStates.Preparation;				
				}
				break;
			}
			break;
		
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;

		case MonsterStates.Attack:					
			currentAttackDuration -= GameController.deltaTime;
			if (currentAttackDuration < 0) {
				nextState = MonsterStates.Desync;
				//toca animacao de idle
				GameObject temp = Instantiate (projectile, playerFuturePos, Quaternion.identity) as GameObject;
				temp.SendMessage (MnstMsgs.SetToughness, monsterToughness);
				gameController.addMonster (temp);	
			}
			break;

		case MonsterStates.Death:
			deathCountDown -= GameController.deltaTime;
			if(deathCountDown < 0)
			{
				if(InstantiateAfterDeath)
				{
					if(isFilho || isNeto)
					{
						GameObject temp = Instantiate(HydraFather,transform.position,Quaternion.Euler (0,0,0)) as GameObject;
						temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
						if(monsterToughness == toughness.five)
						{
							if(growed)
								temp.SendMessage ("SuperGrow");
							else
								temp.SendMessage ("Grow");
						}
						temp.GetComponent<MonsterGeneric>().nextState = MonsterStates.Start;
						gameController.addMonster(temp);
					}
				}
				DestroyItSelf();	
			}
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
			{
				nextState = MonsterStates.Normal;	
				if(!isNeto)
					animator.SetBool("birth",false);
			}
			break;
			
		case MonsterStates.Preparation:
			currentBirthTime -= GameController.deltaTime;
			if(currentBirthTime < 0)
			{
				if(isFilho)
					CreateNeto ();
				else
					CreateFilho ();
				nextState = MonsterStates.Desync;
			}				
			break;
		}
	}
	Vector3 temp;
	void CreateNewGoal()
	{
		temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
			cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
		             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,temp - upTemp);
		RaycastHit hit;
		int count = 0;
		if(Physics.SphereCast (ray,radiusCollider,out hit, 100)){			
			while(hit.collider.tag != Tags.Floor)
			{			
				count++;
				temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
				                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
				upTemp = temp + new Vector3(0,0,-10);
				ray = new Ray(upTemp,temp - upTemp);
				Debug.DrawRay(upTemp,temp - upTemp,Color.white,5);
				Physics.SphereCast (ray,radiusCollider,out hit, 100);
				if(count == 10)
				{
					goalPosition.position = temp;		
					lastRepath = Time.time+ repathRate;
					seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);
					nextState = MonsterStates.Desync;
					return;
				}
			}
		}
		goalPosition.position = temp;		
		lastRepath = Time.time+ repathRate;
		seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);
		nextState = MonsterStates.Desync;
	}
	
	public void OnPathComplete (Path p) {
        p.Claim (this);
        if (!p.error) {
            if (path != null) path.Release (this);
            path = p;
            //Reset the waypoint counter
            currentWaypoint = 0;
        } else {
            p.Release (this);
            Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
        }
    }
	
	void CreateFilho()
	{
		for (int i = 0; i < numberOfCopiesPerCicle; i++) {		
			GameObject temp = Instantiate(smallerCopy,transform.position,Quaternion.identity) as GameObject;
			temp.transform.Translate(1.249184f,0,0,Space.Self);
			temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
			gameController.addMonster (temp);
		}
		currentTimeToNextCopy = timeToCreateACopy * GetCooldownModifier ();
	}
	
	void CreateNeto()
	{
		for (int i = 0; i < numberOfCopiesPerCicle; i++) {	
			GameObject temp = Instantiate(smallerCopy,transform.position,Quaternion.identity) as GameObject;
			temp.transform.Translate(1.249184f,0,0,Space.Self);
			temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
			gameController.addMonster (temp);
		}
		currentTimeToNextCopy = timeToCreateACopy * GetCooldownModifier ();
	}
	
	
	void Velocity()
	{  
        if (path == null) {
            //We have no path to move after yet
            return;
        }               
		if (currentWaypoint > path.vectorPath.Count) 
			return; 
        if (currentWaypoint == path.vectorPath.Count) {
            Debug.Log ("End Of Path Reached");
            currentWaypoint++;
			CreateNewGoal ();
            return;
        }       
        //Direction to the next waypoint
        velocity = (path.vectorPath[currentWaypoint]-transform.position);
			
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{		
		 if (path == null) {
            //We have no path to move after yet
            return;
        } 
		if (currentWaypoint > path.vectorPath.Count) 
			return; 
		if (Time.time - lastRepath > repathRate && seeker.IsDone()) {
			lastRepath = Time.time+ repathRate;
			seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);				
		}
		controller.Move(velocity * isPlayerPushing);
		//if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
        if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) 
			currentWaypoint++;            
	}
	
	void OnTriggerEnter(Collider other)
	{		
		switch(monsterToughness)
		{
		case toughness.one:
		case toughness.two:
			return;
		case toughness.three:
			if(isNeto && joinDelay < 0 && other.transform.tag == Tags.Monster && name != other.name)
			{
				MonsterGeneric m = other.GetComponent<MonsterGeneric>();
				if(m.GetMonsterType() == monsterTypes.hydra)
				{
					Hydra h = other.GetComponent<Hydra>();
					if(h.isNeto)
					{
						gameController.JoinTwoHydras(this.gameObject,other.gameObject);
					}
				}
			}
			break;
		case toughness.four:
		case toughness.five:
			if(joinDelay < 0 && other.transform.tag == Tags.Monster && name != other.name)
			{
				if(isFilho)
				{
					MonsterGeneric m = other.GetComponent<MonsterGeneric>();
					if(m.GetMonsterType() == monsterTypes.hydra)
					{
						Hydra h = other.GetComponent<Hydra>();
						if(h.isFilho)
						{							
							gameController.JoinTwoHydras(this.gameObject,other.gameObject);
						}
					}
				}
				if(isNeto)
				{
					MonsterGeneric m = other.GetComponent<MonsterGeneric>();
					if(m.GetMonsterType() == monsterTypes.hydra)
					{
						Hydra h = other.GetComponent<Hydra>();
						if(h.isNeto)
						{
							gameController.JoinTwoHydras(this.gameObject,other.gameObject);
						}
					}
				}
			}
			break;
		}		
		return;		
	}
	
	public bool IsFilho()
	{
		return isFilho;	
	}
	
	public bool IsNeto()
	{
		return isNeto;
	}


	public void Grow()
	{
		maxLife += lifeGrowth;
		currentLife += lifeGrowth;
		monsterDamage += damageGrowth;
		transform.localScale = new Vector3(transform.localScale.x + sizeGrowth,transform.localScale.y + sizeGrowth,transform.localScale.z);
		animator.SetTrigger("grow");
		growed = true;
	}

	public void SuperGrow()
	{
		maxLife += 3 * lifeGrowth;
		currentLife += 3 * lifeGrowth;
		monsterDamage += 3 * damageGrowth;
		transform.localScale = new Vector3(transform.localScale.x + 3 * sizeGrowth,transform.localScale.y + 3 * sizeGrowth,transform.localScale.z);
		animator.SetTrigger("grow");
		growed = true;
	}
    	
	void OnDestroy () {
		if(goalPosition)
			Destroy(goalPosition.gameObject);
	}

	public void AnimateJoinTwoHydras(bool rightSide)
	{
		if(rightSide)
		{
			Vector3 v = visual.transform.localScale;
			v.x = -v.x;
			visual.transform.localScale = v;
		}
		animator.SetTrigger("grow");
		nextState = MonsterStates.Death;
		deathCountDown = 1;

	}

	public void InstantiateJoinedHydra()
	{
		InstantiateAfterDeath = true;
		deathCountDown = 2;
	}

	protected override void OnMonsterHit()
	{
	}
}
