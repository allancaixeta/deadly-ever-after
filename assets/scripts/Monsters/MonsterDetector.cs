﻿using UnityEngine;
using System.Collections;

public class MonsterDetector : MonoBehaviour {

	public GameObject target;
	public bool projectile;
	void OnTriggerEnter(Collider other)
	{
		if (projectile) 
		{
			if(other.transform.tag == "MonsterProjectile" || other.transform.tag == "StunMonster")
				target.SendMessage ("OnTriggerEnter",this.GetComponent<Collider>());
		}
		else
		{
			if(other.transform.tag == Tags.Monster)
				target.SendMessage ("OnTriggerEnter",this.GetComponent<Collider>());
		}
	}
}
