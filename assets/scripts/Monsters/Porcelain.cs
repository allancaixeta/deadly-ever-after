using UnityEngine;
using System.Collections;
using Pathfinding;

public class Porcelain : MonsterGeneric {

	public Transform goalPosition;
	public GameObject manager;

	public float minForesseTimer, maxForesseTimer;

	float foresseTimer = 3;
	public float distanceMoveToDesync = 100;
	public float attackIncrementalSpeed = 1.5f;
	public GameObject projectile;
	public float recoveryTimeAfterPlayerGaze = 0.2f;
	public float distanceMoveToProjectile = 500;
	public float afterProjectileRestTime = 2;
	public float lvl3ProjectileUpgradeWithFury = 0.15f;
	public float lvl4AdditionalSpeed = 3;
	public float lvl5TimeToSinisterLaughter = 2;
	public float lvl5StunTime= 0.3f;
	public float lvl5Slowtime = 2;
	public float lvl5SlowValue = 0.2f;
	public AudioClip sinisterlaughSound;
	LineOfSight lineOfSight;
	PorcelainsManager porcelainsManager;
	bool playerLookingAt, justStoppedLookingAt;
	Vector3 lastPosition;
	float distanceMoved, distanceMoveTotal, afterProjectileTimer, additionalSpeed, countdownToLaughter;
	Fury fury;
	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 1;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	public float repathRate = 0.2f;
	private float lastRepath = -9999;
	float originalBoxCenterZ;
	public float attackTriggerRange = 7;
	public float preparationTimer = 0.25f;
	float currentPreparationTimer = 0;
	public float attackDuration = 1;
	float currentAttackDuration = 1.5f;

	public override void SpecificMonsterAwake()
	{
		porcelainsManager = PorcelainsManager.Instance;
		porcelainsManager.RegisterDoll (this);
		seeker = GetComponent<Seeker>();
		goalPosition.position = player.position;	
		monsterType = MonsterGeneric.monsterTypes.porcelain;
		fury = GetComponent<Fury>();		 
		lineOfSight = GetComponent<LineOfSight>();
		originalBoxCenterZ = GetComponent<BoxCollider> ().center.z;
		foresseTimer = Random.Range (minForesseTimer, maxForesseTimer);
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
		switch(currentState)
		{
		case MonsterStates.Start:
			GetOutOfObject();
			nextState = MonsterStates.Desync;						
			projectile = Instantiate (projectile,transform.position,projectile.transform.rotation) as GameObject;
			projectile.SendMessage (MnstMsgs.SetToughness,monsterToughness);
			gameController.addMonster (projectile);
			projectile.SetActive (false);
			switch(monsterToughness)
			{
			default:
			case toughness.one:
			case toughness.two:
			case toughness.three:
				break;
			case toughness.four:
			case toughness.five:
				porcelainsManager.SetLvl4 ();
				break;
			}
			countdownToLaughter = lvl5TimeToSinisterLaughter * GetCooldownModifier();
			BoxCollider b = GetComponent<BoxCollider> ();
			b.center = new Vector3(b.center.x,b.center.y,originalBoxCenterZ);
			distanceMoveToProjectile *= 1 + (Random.Range (minForesseTimer,maxForesseTimer) * GetCooldownModifier());
			distanceMoveToDesync *= 1 + (Random.Range (minForesseTimer,maxForesseTimer) * GetCooldownModifier());
			break;
			
		case MonsterStates.Inactive:			
			break;	

		case MonsterStates.Normal:
			distanceMoved = 0;
			lastPosition = transform.position;
			break;
			
		case MonsterStates.Desync:
			gameObject.layer = MonsterGeneric.nonHarmfulLayer;
            Passive();
            if (lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else			
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier() + (projectile.activeSelf ? afterProjectileTimer : 0);
			if(justStoppedLookingAt)
				currentDesync = recoveryTimeAfterPlayerGaze;
			justStoppedLookingAt = false;
			FixZ();
			GetOutOfObject();
			break;

		case MonsterStates.Preparation:
			currentPreparationTimer = preparationTimer;
			//toca animacao preparacao
			CalculatePlayerPosition ();
			animator.SetBool ("prepare",true);
			break;

		case MonsterStates.Attack:
			gameObject.layer = MonsterGeneric.giantLayer;
            Agressive();
			currentAttackDuration = attackDuration;
			//toca animacao de ataque	
			animator.SetBool ("prepare", false);
			animator.SetBool ("attack",true);
			break;

		case MonsterStates.Death:
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			LifeRegen ();
			CalculatePlayerPosition ();
			Velocity ();
			Move ();
			if (Vector2.Distance (player.position, transform.position) < attackTriggerRange && !playerLookingAt && lineOfSight.hasLineOfSight)
			{
				nextState = MonsterStates.Preparation;
			}
			switch(monsterToughness)
			{
			case toughness.one:
				if(Vector2.Distance (lastPosition,transform.position) > 0.1f)
				{
					distanceMoved += Vector2.Distance (lastPosition,transform.position);
					lastPosition = transform.position;					
				}
				if(distanceMoved > distanceMoveToDesync)
					nextState = MonsterStates.Desync;
				break;
			case toughness.two:
				Lvl2 ();
				break;
			case toughness.three:
			case toughness.four:
				Lvl2 ();
				if(!playerLookingAt)
					fury.CalculateFury();
				else
					fury.StopAcumulatingFury();
				break;
			case toughness.five:
				Lvl2 ();
				if(!playerLookingAt)
				{
					fury.CalculateFury();
					countdownToLaughter = lvl5TimeToSinisterLaughter * GetCooldownModifier();
				}
				else
				{
					fury.StopAcumulatingFury();
					countdownToLaughter  -= GameController.deltaTime;
					if(countdownToLaughter < 0)
					{
						animator.SetTrigger ("sinisterlaugh");
						gameController.MonsterAudioSource.PlayOneShot (sinisterlaughSound);
						countdownToLaughter = lvl5TimeToSinisterLaughter * GetCooldownModifier();
						player.SendMessage ("Stunned",lvl5StunTime);
						player.GetComponent<PlayerController>().SlowDown(lvl5SlowValue,lvl5Slowtime);
						GameObject.Find ("CameraControl").GetComponent<CameraFollow>().ShakeCamera (0.3f,1);
						nextState = MonsterStates.Desync;
					}
				}
				break;
			}
			break;
			
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			Knock();
			break;

		case MonsterStates.Preparation:					
			currentPreparationTimer -= GameController.deltaTime;
			if (currentPreparationTimer < 0) {
				nextState = MonsterStates.Attack;
			}
			break;

		case MonsterStates.Attack:			
			velocity = goalPosition.position - transform.position;
			velocity.Normalize();
			velocity *= GameController.deltaTime * attackIncrementalSpeed * (speed + (fury.GetOnFury() ? fury.furySpeed * fury.maxFuryPoints : 0)) * (timeDistortion-magicSlowEffect);
			controller.Move(velocity * isPlayerPushing);
			currentAttackDuration -= GameController.deltaTime;
			if (currentAttackDuration < 0) {
				nextState = MonsterStates.Desync;
				animator.SetBool ("attack",false);
				//toca animacao de idle
			}
			break;


		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}

	void CalculatePlayerPosition()
	{
		Vector2 tv = player.GetComponent<PlayerController>().GetCurrentSpeed();
		if (tv == Vector2.zero)
			goalPosition.position = player.position;
		else
			goalPosition.position = new Vector2(player.position.x + tv.x * foresseTimer,player.position.y + tv.y * foresseTimer);		
	}
	
	void Velocity()
	{		
		if (Time.time - lastRepath > repathRate && seeker.IsDone()) {
			lastRepath = Time.time+ repathRate;
			seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);				
		}
		if (path == null || currentWaypoint >= path.vectorPath.Count) {
			velocity = goalPosition.position - transform.position;
		}         
		else
		{
			//Direction to the next waypoint
			velocity = (path.vectorPath[currentWaypoint]-transform.position);
			if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) 
				currentWaypoint++;
		}			
		velocity.Normalize();
		if(playerLookingAt)
			velocity *= 0;
		else
			velocity *= GameController.deltaTime * (speed + additionalSpeed + (fury.GetOnFury() ? fury.furySpeed * fury.maxFuryPoints : 0)) * (timeDistortion-magicSlowEffect);			
	}

	void OnPathComplete (Path p) {
		p.Claim (this);
		if (!p.error) {
			if (path != null) path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		} else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
		}
	}

	void Lvl2()
	{
		if(Vector2.Distance (lastPosition,transform.position) > 0.1f)
		{
			distanceMoved += Vector2.Distance (lastPosition,transform.position);
			distanceMoveTotal += Vector2.Distance (lastPosition,transform.position);
			lastPosition = transform.position;					
		}
		if(distanceMoved > distanceMoveToDesync)
			nextState = MonsterStates.Desync;
		if(distanceMoveTotal > distanceMoveToProjectile && !projectile.activeSelf)
		{
			distanceMoveTotal = 0;
			projectile.transform.position = transform.position;
			projectile.SetActive (true);
			projectile.SendMessage ("ResetLifeTime");
			if(fury.GetOnFury ())
			{
				projectile.SendMessage ("OnFury",lvl3ProjectileUpgradeWithFury);
				fury.ResetFury();
			}
			afterProjectileTimer = afterProjectileRestTime * GetCooldownModifier();
			nextState = MonsterStates.Desync;
		}
	}

	void Move()
	{		
		if (Vector2.Distance (player.position, transform.position) < stopMovingIfCloserThenThisDistance)
			return;
		controller.Move(velocity * isPlayerPushing);
	}

	public void PlayerLookingAt()
	{
		playerLookingAt = true;
		additionalSpeed = 0;
		animator.SetBool ("targeted",true);
		if (CheckIfCurrentOrNextState (MonsterStates.Preparation)) 
		{
			animator.SetBool ("prepare", false);
			nextState = MonsterStates.Desync;
			//TODO: som de ataque cancelado
		}
	}

	public void PlayerNotLookingAtAnymore()
	{
		playerLookingAt = false;
		animator.SetBool ("targeted",false);
		if(!CheckIfCurrentOrNextState (MonsterStates.Attack)) 
		{
			nextState = MonsterStates.Desync;
			justStoppedLookingAt = true;
		}
	}

	public void Lvl4PlayerNotLookingAtThisDool()
	{
		switch(monsterToughness)
		{
		default:
		case toughness.one:
		case toughness.two:
		case toughness.three:
			break;
		case toughness.four:
		case toughness.five:
			additionalSpeed = lvl4AdditionalSpeed;
			break;
		}
	}

	public void Lvl4PlayerNotLookingAtAnyone()
	{
		additionalSpeed = 0;
	}

	protected override void OnMonsterHit()
	{
	}

	void OnDestroy()
	{
		porcelainsManager.UnregisterDoll (this);
	}
}