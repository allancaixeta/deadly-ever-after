using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof (LineOfSight))]
public class SpinTop : MonsterGeneric {

	public LerpEquationTypes lerpAccel, lerpDeaccel;
	public float gainSpeedTime = 1;
	public float speedInGain = 10;
	public float looseSpeedTime = 3;
	public float speedInLoose = 3;
	public int levelThreeArmor = 5;
	public float levelFourChargeIncrease = 3;
	int numberOfCycles;
	public int levelFiveDamage = 3;
	public int levelFiveArmorIncrement = 1;
	public GameObject bite;
	public float swallowDuration = 0.3f;	
	[Range (0,2)]
	public float decreaseSpeeWithNoLineOfSight = 0.5f;
	float loopControl = 0;
	bool speedUp = true;
	bool playerHittedThisLoopControl = false;
	float defaultDamage;
	int originalLifeRegen, originalArmor;
	LineOfSight lineOfSight;
	public bool restartCicleAfterBeingHit = false;
	Vector3 straightToPlayerVelocity;
	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 1f;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;

	ParticleSystem.MainModule tornado, sparks;
	// Use this for initialization
	public override void SpecificMonsterAwake()
	{		
		seeker = GetComponent<Seeker>();
		loopControl = gainSpeedTime;
		defaultDamage = monsterDamage;
		lineOfSight = GetComponent<LineOfSight>();
		if (!lineOfSight) {
			gameObject.AddComponent <LineOfSight>();
			lineOfSight = GetComponent<LineOfSight>();
		}
		monsterType = MonsterGeneric.monsterTypes.spintop;
		speedInGain = speedInGain * (1 - skills.GetAccel (false));
		speedInLoose = speedInLoose * (1 - skills.GetAccel (false));
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			switch(monsterToughness)
			{
			case toughness.one:		
			case toughness.two:
			case toughness.three:
			case toughness.five:
			case toughness.four:
				speed = speedInLoose;
				break;
			}	
			tornado =  visual.transform.Find("base/Pivot/CFX_Tornado_Straight").GetComponent<ParticleSystem>().main;
			sparks =  visual.transform.Find("base/Pivot/CFX2_Sparks_Source").GetComponent<ParticleSystem>().main;
			nextState = MonsterStates.Desync;
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
				numberOfCycles = 0;
				break;
			
			case toughness.four:
			case toughness.five:
				numberOfCycles = -1;
				break;
			}
			originalArmor = armor;
			break;

		case MonsterStates.Desync:
			animator.speed = 1;
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			animator.SetTrigger ("idle");
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
				numberOfCycles = 0;
				break;
				
			case toughness.four:
			case toughness.five:
				if(numberOfCycles < 5)
				{
					numberOfCycles++;
					if(numberOfCycles > 0)
					{
						tornado.startSizeMultiplier = tornado.startSizeMultiplier * 1.2f ;
						tornado.startSpeedMultiplier = tornado.startSpeedMultiplier * 1.2f;
						sparks.startSizeMultiplier = sparks.startSizeMultiplier * 1.2f ;
						sparks.startSpeedMultiplier = sparks.startSpeedMultiplier * 1.2f;
						//sparks.gravityModifierMultiplier = sparks.gravityModifierMultiplier * 1.2f;
					}
				}				
				break;
			}
			FixZ();
			break;	
			
		case MonsterStates.Inactive:			
			break;	

		case MonsterStates.Normal:
			animator.speed = 1;
            animator.SetTrigger("accel");
            FacingDirection();
            if (monsterToughness == toughness.five)
				monsterDamage = levelFiveDamage;
			break;	

		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			break;
		}
		return true;
	}
    Vector3 ficaAqui = new Vector3(0,1.8f,0);
    bool flipa;
    public void LateUpdate()
    {     
        visual.transform.localPosition = ficaAqui;
        if(flipa)
            visual.transform.localScale = new Vector3(-1, visual.transform.localScale.y, visual.transform.localScale.z);
    }
	// Update is called once per frame
	public override void UpdateState()
	{        
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			LifeRegen();
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				if(speedUp)
				{
					GainSpeed ();
				}
				else
				{
					LooseSpeed();					
				}	
				Velocity ();
				MoveToPlayer ();
				break;
			}
			break;
			
		case MonsterStates.Hook:								
			break;
			
		case MonsterStates.Death:
			Death ();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
			{
				loopControl = gainSpeedTime;
				speedUp = true;
				gameObject.layer = MonsterGeneric.normalLayer;
                Agressive();
                nextState = MonsterStates.Normal;
				seeker.StartPath (transform.position,player.position, OnPathComplete);
			}
			break;
			
		}
	}	

	void GainSpeed()
	{
		loopControl -= GameController.deltaTime;									
		if(loopControl < 0)
		{
			loopControl = looseSpeedTime;
			speedUp = false;
			gameObject.layer = MonsterGeneric.nonHarmfulLayer;
            Passive();
			monsterDamage = defaultDamage;
            animator.SetTrigger("slowstart");
            switch (monsterToughness)
			{
			case toughness.one:
			case toughness.two:
				break;
			case toughness.three:
			case toughness.four:
			case toughness.five:
				armor += levelThreeArmor;
				UpdateArmorInfinite();				
				break;
			}
			playerHittedThisLoopControl = false;
			animator.speed = 1/looseSpeedTime;
		}
		speed = lerpAccel.Lerp(speedInLoose, speedInGain + (numberOfCycles * levelFourChargeIncrease),1- loopControl/gainSpeedTime);
		if(speed > MonsterGeneric.speedCap * 2)
			speed = MonsterGeneric.speedCap * 2;
	}

	void LooseSpeed()
	{
		loopControl -= GameController.deltaTime;									
		if(loopControl < 0)
		{
			loopControl = gainSpeedTime;
			speedUp = true;
			nextState = MonsterStates.Desync;
			switch(monsterToughness)
			{
			case toughness.three:
			case toughness.four:
			case toughness.five:
				armor -= levelThreeArmor;
				UpdateArmorInfinite();
				break;
			}
		}
		speed = lerpDeaccel.Lerp(speedInLoose, speedInGain, loopControl/looseSpeedTime);
	}

	void Velocity()
	{
		if (path == null || currentWaypoint >= path.vectorPath.Count) {
			//velocity = player.position - transform.position;
			velocity = straightToPlayerVelocity;
		}         
		else
		{
			//Direction to the next waypoint
			velocity = (path.vectorPath[currentWaypoint]-transform.position);
			if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) 
				currentWaypoint++;
			straightToPlayerVelocity = velocity;
		}
			
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (lineOfSight.hasLineOfSight?1:decreaseSpeeWithNoLineOfSight)  * (timeDistortion-magicSlowEffect);
		if(lineOfSight.hasLineOfSight)
		{
            tornado.simulationSpeed = 1;
		}
		else
		{
			tornado.simulationSpeed = decreaseSpeeWithNoLineOfSight;			
		}
	}

	void MoveToPlayer()
	{
		if (Vector2.Distance (player.position, transform.position) < stopMovingIfCloserThenThisDistance)
			return;
		Move ();
	}

	void OnPathComplete (Path p) {
		p.Claim (this);
		if (!p.error) {
			if (path != null) path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		} else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
		}
	}

	void Move()	{	
		controller.Move(velocity * isPlayerPushing);
	}
		
	void OnTriggerEnter(Collider other)
	{
		if(other.transform.tag == "PlayerHit" && speedUp) 
		{
			if(!playerHittedThisLoopControl)
			{
				playerHittedThisLoopControl = true;
				switch(monsterToughness)
				{
				case toughness.five:					
					SwallowPlayer();
					break;
				case toughness.three:
				case toughness.four:
					AbsorbLife();
					break;
				}
				loopControl = 0;
			}
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);
	}
	
	void SwallowPlayer()
	{
		AbsorbLife ();
		GameObject temp = Instantiate(gameController.shieldAbsorb,GameObject.Find ("lifeAbsorbPosition").transform.position,Quaternion.Euler (0,0,0)) as GameObject;
		temp.GetComponent<GoToTarget> ().SetTarget (this.transform,2,this.transform,"FinishSwallowing");
		gameController.addUpdatableGameObject (temp);
		player.SendMessage ("Stunned",swallowDuration);
		GameObject temp2 = Instantiate (bite, player.position, Quaternion.Euler(0,0,90)) as GameObject;
		temp2.transform.parent = player;
		GameObject.Find ("CameraControl").GetComponent<CameraFollow>().ShakeIntenseCamera (0.3f,1);
	}

	public void FinishSwallowing()
	{
		armor += levelFiveArmorIncrement;
		originalArmor += levelFiveArmorIncrement;
		UpdateArmorInfinite ();
	}

	protected override void OnMonsterHit()
	{
		if(restartCicleAfterBeingHit && !CheckIfCurrentOrNextState (MonsterStates.VOID))
		{
			seeker.StartPath (transform.position,player.position, OnPathComplete);
			loopControl = gainSpeedTime;
			speedUp = true;
			switch(monsterToughness)
			{
			case toughness.three:
			case toughness.four:
			case toughness.five:
				armor = originalArmor;
				UpdateArmorInfinite();
				break;
			}
			if(CheckIfCurrentOrNextState(MonsterStates.Knockback))
			{
				saveState = MonsterStates.Normal;
			}
			else
			{
				if(CheckIfCurrentOrNextState(MonsterStates.Stun))
					saveStateBeforeStun = MonsterStates.Normal;
				else
					nextState = MonsterStates.Normal;
			}	

		}
	}

    void FacingDirection()
    {
        if (player.position.y - transform.position.y > 0)
        {
           // ///if (visual.transform.localScale.x == 1)
             //   visual.transform.localScale = new Vector3(-1, visual.transform.localScale.y, visual.transform.localScale.z);
            flipa = true;
        }
        else
        {
           // if (visual.transform.localScale.x == -1)
            //    visual.transform.localScale = new Vector3(1, visual.transform.localScale.y, visual.transform.localScale.z);
            flipa = false;
        }
       
    }
}
