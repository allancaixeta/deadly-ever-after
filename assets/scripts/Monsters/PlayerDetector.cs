﻿using UnityEngine;
using System.Collections;

public class PlayerDetector : MonoBehaviour {

	public GameObject target;
	void OnTriggerEnter(Collider other)
	{
		if(other.transform.tag == "PlayerHit")
			target.SendMessage ("OnTriggerEnter",this.GetComponent<Collider>());
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);
	}
}
