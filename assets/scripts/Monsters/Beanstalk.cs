using UnityEngine;
using System.Collections;

public class Beanstalk : MonsterGeneric {
	
	public GameObject projectile;
	float currentCooldown;
    public float shootAnimAntecipationTimer = 0.5f;
	public float shootCooldown = 3;
	public float initialAngle = 0;
	public float angleSpeed = 45;
	public float lvl4angleSpeed = 11;
	public float shootingSpreeDuration = 3;
	public float shootingSpreeRecharge = 0.4f;
	public float lvl4shootingSpreeRecharge = 0.33f;
	public float lvl5SlowValue = 0.2f;
	public float lvl5Slowtime = 2;
	public bool randomAngles,randomInitialAngles;
	public float playerDistanceIncreaseCooldownRate;
	float shootingSpreeCurrentRecharge, shootingSpreeCurrentDuration, currentAngleSpeed;
	int attackCounter = 0;
	public override void SpecificMonsterAwake()
	{		
		monsterType = MonsterGeneric.monsterTypes.beanstalk;
		currentCooldown = shootCooldown * GetCooldownModifier ();
		shootingSpreeCurrentRecharge = shootingSpreeRecharge;
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			GetOutOfObject();
			nextState = MonsterStates.Desync;						
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Normal:
			currentCooldown = (shootCooldown * GetCooldownModifier ()) + Vector2.Distance (transform.position,player.position) * playerDistanceIncreaseCooldownRate;//aumenta o cooldown com a distancia
			shootingSpreeCurrentRecharge = shootingSpreeRecharge;
			shootingSpreeCurrentDuration = shootingSpreeDuration;
			currentAngleSpeed = 0;
			if(randomAngles) //Variação de ângulo a cada tiro é fixa (90 graus normalmente) ou aleatória?
				angleSpeed = Random.Range (0,180);
			if(randomInitialAngles) //O primeiro tiro é sempre  no ângulo zero (ou seja, na direção para baixo do monstro) ou é aleatório?
				initialAngle = Random.Range (0,360);
			break;
			
		case MonsterStates.Desync:
			GetOutOfObject();
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			break;

		case MonsterStates.Knockback:

			break;

		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			break;
		}
		return true;
	}

	public override void UpdateState()
	{	
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			LifeRegen ();
			switch(monsterToughness)
			{
			case toughness.one:
				currentCooldown -= GameController.deltaTime;
				if(currentCooldown < 0)
				{
					animator.SetBool("attack",false);
					for (int i = 0; i < 4; i++) {
						Shoot (((attackCounter % 2) == 0 ? initialAngle : initialAngle + 45) + (angleSpeed * 2 *i));
					}
					nextState = MonsterStates.Desync;
					attackCounter++;
				}
				else if(currentCooldown < shootAnimAntecipationTimer)
						animator.SetBool("attack", true);
				break;
			case toughness.two:
				currentCooldown -= GameController.deltaTime;
				if(currentCooldown < 0)
				{
					animator.SetBool("attack", false);
					for (int i = 0; i < 8; i++) {
						Shoot (initialAngle + (angleSpeed * i));
					}
					nextState = MonsterStates.Desync;
				}
				else if(currentCooldown < shootAnimAntecipationTimer)
					animator.SetBool("attack", true);
				break;
			case toughness.three:
			case toughness.four:
			case toughness.five:
				if(shootingSpreeCurrentDuration < 0)
				{
					animator.SetBool("attack", false);
					currentCooldown -= GameController.deltaTime;
					if(currentCooldown < 0)
					{
						nextState = MonsterStates.Desync;
					}
				}
				else
				{
					shootingSpreeCurrentDuration -= GameController.deltaTime;
					shootingSpreeCurrentRecharge -= GameController.deltaTime;
					if(shootingSpreeCurrentRecharge < 0)
					{				
						shootingSpreeCurrentRecharge = (monsterToughness == MonsterGeneric.toughness.three ? shootingSpreeRecharge : lvl4shootingSpreeRecharge);
						for (int i = 0; i < 4; i++) {
							Shoot (initialAngle + currentAngleSpeed + (angleSpeed * 2 * i));
						}
						currentAngleSpeed += (monsterToughness == MonsterGeneric.toughness.three ? angleSpeed : lvl4angleSpeed);
					}	
                    else
                    {
                        if (shootingSpreeCurrentRecharge < shootAnimAntecipationTimer)
                        {
                            animator.SetBool("attack", true);

                        }
                    }
				}
				break;
			}
			break;
		
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			//inicia a animacao de tiros multiplos antes de comecar a atirar
			if(currentDesync < 0)
			{
				nextState = MonsterStates.Normal;
			}
			break;
			
		}
	}


	void Shoot(float angle)
	{
		GameObject temp = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;
		if(monsterToughness == MonsterGeneric.toughness.five)
			temp.GetComponent<DefaultProjectile>().SetSlowWhenHit (lvl5SlowValue, lvl5Slowtime);		
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
		temp.SendMessage ("SetDirection",angle);
		gameController.addMonster (temp);		
	}

	protected override void OnMonsterHit()
	{
	}
}