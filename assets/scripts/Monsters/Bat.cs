using UnityEngine;
using System.Collections;

public class Bat : MonsterGeneric {

	float flyingAngle;
	public float dontChangeDirectionForTheNextSeconds = 0.1f;
	float reloaddontChangeDirectionForTheNextSeconds;
	public float deviationFromReflectedAngle = 5;
	public GameObject blast, batEgg;
	public float blastCooldown = 5;
	public int numberOfBlasts = 4;
	float currentBlastCooldown;
	int blastsShot = 0;
	public bool randomShootAngle;
	GameObject [] arrayOfBlasts;



	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.bat;
		reloaddontChangeDirectionForTheNextSeconds = dontChangeDirectionForTheNextSeconds;
		invulnerable = true;
	}



	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			flyingAngle = Random.Range (0,360);
			visual.transform.FindChild ("base").rotation = Quaternion.Euler(0,0,flyingAngle);
			GetOutOfObject();
			//nextState = monsterStates.desync;	
			GetComponent<CharacterController>().center = new Vector3(0,0,GameController.FlightPositionY);
			arrayOfBlasts = new GameObject[numberOfBlasts*2];
            Agressive();
			break;
			
		case MonsterStates.Inactive:			
			break;	

		case MonsterStates.Normal:
			currentBlastCooldown = blastCooldown * GetCooldownModifier ();
			break;

		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
			{
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			}
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			break;

		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			if(monsterToughness != toughness.one)
			{
				float initialAngle = randomShootAngle ? Random.Range (0,360) : 0;
				for (int i = 0; i < numberOfBlasts; i++) {
					//Shoot (initialAngle + (90 *i),1,true);
					GameObject temp = Instantiate(blast,transform.position,Quaternion.identity) as GameObject;
					temp.SendMessage (MnstMsgs.SetToughness ,monsterToughness);
					temp.SendMessage ("SetDirection",initialAngle + (90 *i));
					temp.SendMessage ("SetAfterLifeBlast");
					gameController.addMonster (temp);	
				}
				for (int i = 0; i < blastsShot; i++) {
					arrayOfBlasts[i].SendMessage ("SetFatherIsDead");
				}
			}

			if(monsterToughness == toughness.five) {
				GameObject temp = Instantiate(batEgg,transform.position,Quaternion.identity) as GameObject;
				temp.SendMessage (MnstMsgs.SetToughness, monsterToughness);
				temp.SendMessage ("SetMaxLife",maxLife);
				gameController.addMonster (temp);	
			}
			break;
		}
		return true;
	}



	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			GameObject temp = Instantiate(blast,new Vector3(-10,-10,1.5f),Quaternion.identity) as GameObject;
			temp.SendMessage (MnstMsgs.SetToughness, monsterToughness);
			temp.SendMessage (MnstMsgs.SetDirection,(90 * blastsShot));
			//temp.SendMessage ("SetBatMaxLife",maxLife);
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
				break;
			case toughness.three:
			case toughness.four:
			case toughness.five:
				//temp.SendMessage ("SetSendMessageToParentWhenTouchAnotherBlast");
				break;
			}
			gameController.addMonster (temp);	
			arrayOfBlasts[blastsShot] = temp.gameObject;
			blastsShot++;
			if(blastsShot >= numberOfBlasts)
			{
				invulnerable = false;
				nextState = MonsterStates.Desync;
			}
			break;
			
		case MonsterStates.Normal:
			LifeRegen();
			dontChangeDirectionForTheNextSeconds -= GameController.deltaTime;
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				Velocity();
				Move ();
				currentBlastCooldown -= GameController.deltaTime;
				if(currentBlastCooldown < 0 && !CheckCollisionWithFurniture())
				{
					float initialAngle = randomShootAngle ? Random.Range (0,360) : 0;
					Shoot (initialAngle,monsterToughness == toughness.one ? 0.3f : 1, false);
					nextState = MonsterStates.Desync;
				}	
				else if(currentBlastCooldown < 0.3f)
						animator.SetTrigger("shoot");
				break;
			}
			break;
			
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			animator.SetTrigger("idle");
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}

	void Shoot(float initialAngle, float lifeTime, bool onHit)
	{
		int begin, end;
		if(onHit)
		{
			begin = numberOfBlasts;
			end = numberOfBlasts*2;
		}
		else
		{
			begin = 0;
			end = numberOfBlasts;
		}
		for (int i = begin; i < end; i++) {
			arrayOfBlasts[i].transform.position = transform.position;
			arrayOfBlasts[i].SendMessage ("SetNewLife",lifeTime);
			if(randomShootAngle)
				arrayOfBlasts[i].SendMessage("SetDirection",initialAngle + (90 *i));
		}
	}

	void Velocity()
	{		
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * flyingAngle),Mathf.Sin(Mathf.Deg2Rad * flyingAngle),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{	
		controller.Move(velocity * isPlayerPushing);
	}

	void OnControllerColliderHit(ControllerColliderHit hit) {
		switch(monsterToughness)
		{
		case toughness.one:
		case toughness.two:
		case toughness.three:
		case toughness.four:
		case toughness.five:
			if(hit.collider.tag == Tags.Lvl3Obstacle || hit.collider.tag == Tags.Wall || hit.collider.tag == Tags.Monster)
			{
				ChangeFlightDirection(hit);
			}
			break;
		}
	}

	Collider lastToHit;
	void ChangeFlightDirection(ControllerColliderHit hit)
	{
		if(dontChangeDirectionForTheNextSeconds > 0 || lastToHit == hit.collider)
			return;
		dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
		lastToHit = hit.collider;
		if (Mathf.Abs (hit.normal.x) > Mathf.Abs (hit.normal.y)) 
		{
			if(hit.normal.x > 0)
			{//right
				flyingAngle = 180 - flyingAngle + Random.Range (-deviationFromReflectedAngle,deviationFromReflectedAngle);
			}
			else
			{//left
				flyingAngle = 180 - flyingAngle + Random.Range (-deviationFromReflectedAngle,deviationFromReflectedAngle);
			}
		}
		else
		{
			if(hit.normal.y > 0)
			{//up
				flyingAngle = 360 - flyingAngle + Random.Range (-deviationFromReflectedAngle,deviationFromReflectedAngle);
			}
			else
			{//down
				flyingAngle = 360 - flyingAngle + Random.Range (-deviationFromReflectedAngle,deviationFromReflectedAngle);
			}
		}
		visual.transform.FindChild ("base").rotation = Quaternion.Euler(0,0,flyingAngle);
	}

	protected override void OnMonsterHit()
	{
		switch(monsterToughness)
		{
		case toughness.one:
		case toughness.two:
		case toughness.three:
			break;
		case toughness.four:
		case toughness.five:
			if(CheckIfCurrentOrNextState (MonsterStates.Death))
				return;
			animator.SetTrigger("shoot");
			float initialAngle = randomShootAngle ? Random.Range (0,360) : 0;
			float lifeTime = 1 - (currentBlastCooldown/blastCooldown * GetCooldownModifier ());
			if(lifeTime < 0.3f)
				lifeTime = 0.3f;
			if(blastsShot != numberOfBlasts * 2)
			{
				for (int i = numberOfBlasts; i < numberOfBlasts * 2; i++) {
					GameObject temp = Instantiate(blast,transform.position,Quaternion.identity) as GameObject;
					temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
					temp.SendMessage ("SetLifeTime",lifeTime);
					temp.SendMessage ("SetDirection",initialAngle + (90 * (i - numberOfBlasts)));
					//temp.SendMessage ("SetBatMaxLife",maxLife);
					switch(monsterToughness)
					{
					case toughness.one:
					case toughness.two:
						break;
					case toughness.three:
					case toughness.four:
					case toughness.five:
						//temp.SendMessage ("SetSendMessageToParentWhenTouchAnotherBlast");
						break;
					}
					gameController.addMonster (temp);	
					arrayOfBlasts[i] = temp.gameObject;
					blastsShot++;
				}
			}
			else
			{
				Shoot (initialAngle,lifeTime,true);
			}
			nextState = MonsterStates.Desync;
			break;
		}
	}

	bool CheckCollisionWithFurniture()
	{
		int layerMask = 1024;
		Vector3 upTemp = transform.position + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,transform.position - upTemp);
		if(Physics.SphereCast(ray,controller.radius * 1.25f,1000,layerMask))
			return true;
		else
			return false;
	}
}
