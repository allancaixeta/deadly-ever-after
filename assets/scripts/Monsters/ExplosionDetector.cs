using UnityEngine;
using System.Collections;

public class ExplosionDetector : MonoBehaviour {
	
	BombermanExplosion parent;
	public bool isUp;
	public float tillDivisionRateX = 4;
	public float tillDivisionRateY = 4;

	void Start()
	{
		parent = transform.parent.transform.parent.GetComponent<BombermanExplosion> ();
	}

	void Update()
	{
		Vector2 FloorTileMatrix = gameObject.GetComponent<Renderer>().material.mainTextureScale;	
		FloorTileMatrix.x = transform.lossyScale.x / tillDivisionRateX;
		gameObject.GetComponent<Renderer>().material.mainTextureScale = FloorTileMatrix;
	}
	
	void OnTriggerEnter(Collider other)
	{	
		if(other.transform.tag == Tags.Monster && name != other.name)
		{
			MonsterGeneric m = other.GetComponent<MonsterGeneric>();
			if(m.GetMonsterType() == MonsterGeneric.monsterTypes.hydra)
			{				
				Hydra h = other.GetComponent<Hydra>();
				if(!h.IsFilho() && !h.IsNeto())
				{
					parent.FoundAHydra(h);
				}
			}
		}	
		if(other.transform.tag == Tags.Lvl2Obstacle || other.transform.tag == Tags.Lvl3Obstacle || other.transform.tag == Tags.Wall)
		{
			if(isUp)
				parent.HitSomething(isUp ? true : false,other.transform.position.y > transform.position.y ? true : false);
			else
				parent.HitSomething(isUp ? true : false,other.transform.position.x > transform.position.x ? true : false);
		}
	}


}
