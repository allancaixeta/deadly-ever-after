using UnityEngine;
using System.Collections;

public class Swan : MonsterGeneric {

	public GameObject swanduck;
	public int ducksSpawnedAfterDeath = 3;
	public float dontChangeDirectionForTheNextSeconds = 0.2f;
	float reloaddontChangeDirectionForTheNextSeconds;
	public float knockbackDistanceOfDucklings = 3;
	public float chanceToDirectToPlayer = 0.5f;
	public int lvl2ducksSpawnedInTouching = 3;
	public float lvl2SpawningCooldown = 5;
	public int lvl4Armor = 50;
	public float lvl4ArmorCooldown = 10;
	public float lvl4ArmorCooldownDecreasedForEachDuck = 0.5f;
	public GameObject transformToParentFX;
	float direction;
	Transform lastTouchedObject;
	float resetLastTouchedObject, currentSpawningCooldown;
	bool ImADuck, IwasJustBorn;
	Fury fury;
	RoomController room;
    float lastDirectionReflected;
    public bool reflectAngles,TMNT;
    public GameObject[] turtleTMNTPrefabs = new GameObject[4];
    public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.swan;
		lastTouchedObject = transform;
		fury = GetComponent<Fury>();
		reloaddontChangeDirectionForTheNextSeconds = dontChangeDirectionForTheNextSeconds;
		room = (RoomController) FindObjectOfType(typeof(RoomController));
        if (monsterName.Contains("Son"))
        {
            ImADuck = true;
            room.IncreaseADuck();
        }
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
		switch(currentState)
		{
		case MonsterStates.Start:
			GetOutOfObject();
			nextState = MonsterStates.Desync;	
			switch(monsterToughness)
			{
			default:
			case toughness.one:
			case toughness.two:
			case toughness.three:
				break;
			case toughness.four:
			case toughness.five:
				if(!ImADuck)
				{
					armor = lvl4Armor;
					armorCooldown = lvl4ArmorCooldown;
					UpdateArmorBreakable ();
				}
				break;
			}
			currentSpawningCooldown = lvl2SpawningCooldown;
            lastDirectionReflected= Random.Range(0, 360);
            break;
			
		case MonsterStates.Inactive:			
			break;	
			
		case MonsterStates.Desync:
			gameObject.layer = MonsterGeneric.nonHarmfulLayer;
            Passive();
            if (lastState == MonsterStates.Start)
			{
				if(ImADuck)
				{
					currentDesync = 1;
					IwasJustBorn = true;
				}
				else
					currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			}
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			GetOutOfObject();
			break;

		case MonsterStates.Normal:
			gameObject.layer = MonsterGeneric.normalLayer;
            Agressive();
            invulnerable = false;            
            direction = Random.Range (0,360);
			if(Random.value < chanceToDirectToPlayer)
			{
				direction = Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
			}
			if(IwasJustBorn)
			{
				IwasJustBorn = false;
				direction = Mathf.Abs (Mathf.Atan2 (velocity.y, velocity.x) * Mathf.Rad2Deg);
			}
            else
            {
                if (reflectAngles)
                    direction = lastDirectionReflected;
            }            
			break;

		case MonsterStates.Preparation:
			gameObject.layer = MonsterGeneric.nonHarmfulLayer;
            Passive();
            break;

		case MonsterStates.Death:
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			if(!ImADuck)
			{
				for (int i = 0; i < ducksSpawnedAfterDeath; i++) {
					InstantiateADuck (i);
				}
			}
			else
				room.DecreaseADuck ();
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			LifeRegen();
			dontChangeDirectionForTheNextSeconds -= GameController.deltaTime;
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
				break;
			case toughness.three:
			case toughness.four:
			case toughness.five:
				if(ImADuck)
				{
					if(!fury.GetOnFury ())
						fury.CalculateFury();
				}
				else
				{
					if(monsterToughness == toughness.five || monsterToughness == toughness.four)
					{
						armorCooldown = lvl4ArmorCooldown - room.GetNumberOfDucks() * lvl4ArmorCooldownDecreasedForEachDuck;
						if(armorCooldown < 0.5f)
							armorCooldown = 0.5f;
					}
				}
				break;
			}
			Velocity ();
			Move ();
			resetLastTouchedObject -= GameController.deltaTime;
			if(resetLastTouchedObject < 1)
				lastTouchedObject = transform;
			currentSpawningCooldown -= GameController.deltaTime;
			break;
			
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;	

		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
		
		case MonsterStates.Preparation:
			currentSpawningCooldown -= GameController.deltaTime;
			if(currentSpawningCooldown < 0)
			{
				animator.SetBool ("birth",false);
				for (int i = 0; i < lvl2ducksSpawnedInTouching; i++) {
					InstantiateADuck (i);
				}
				currentSpawningCooldown = lvl2SpawningCooldown;
				nextState = MonsterStates.Desync;
			}
			break;
		}
	}
	
	void Velocity()
	{
		if(ImADuck && fury.GetOnFury ())
			velocity = player.position - transform.position;
		else
			velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
			
		velocity.Normalize();
		velocity *= GameController.deltaTime * (speed + (ImADuck && fury.GetOnFury() ? fury.furySpeed * fury.maxFuryPoints : 0)) * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{		
		if(ImADuck && fury.GetOnFury() && Vector2.Distance (player.position, transform.position) < stopMovingIfCloserThenThisDistance)
			return;
		controller.Move(velocity * isPlayerPushing);
	}

	void OnControllerColliderHit(ControllerColliderHit hit) {
		if(dontChangeDirectionForTheNextSeconds > 0)
		   return;
		if(hit.collider.tag == Tags.Lvl2Obstacle || hit.collider.tag == Tags.Lvl3Obstacle || hit.collider.tag == Tags.Wall)
		{
			if(CheckIfCurrentOrNextState (MonsterStates.Desync) || CheckIfCurrentOrNextState (MonsterStates.Death) || lastTouchedObject == hit.collider.transform)
				return;
			nextState = MonsterStates.Desync;
			lastTouchedObject = hit.collider.transform;
			resetLastTouchedObject = 1;
			dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
            if(reflectAngles)
            {
                if (Mathf.Abs(hit.normal.x) > Mathf.Abs(hit.normal.y))
                {
                    if (hit.normal.x > 0)
                    {//right
                        direction = 180 - direction;
                    }
                    else
                    {//left
                        direction = 180 - direction;
                    }
                }
                else
                {
                    if (hit.normal.y > 0)
                    {//up
                        direction = 360 - direction;
                    }
                    else
                    {//down
                        direction = 360 - direction;
                    }
                }
                lastDirectionReflected = direction;
            }
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(dontChangeDirectionForTheNextSeconds > 0)
			return;
		if(other.GetComponent<Collider>().tag == Tags.Monster && other.gameObject.layer != 15)
		{
			if(CheckIfCurrentOrNextState (MonsterStates.Desync) || CheckIfCurrentOrNextState (MonsterStates.Death) || lastTouchedObject == other.GetComponent<Collider>().transform)
				return;
			nextState = MonsterStates.Desync;
			lastTouchedObject = other.GetComponent<Collider>().transform;
			resetLastTouchedObject = 1;
			dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
			if(other.GetComponent<Collider>().tag == Tags.Monster && currentSpawningCooldown < 0 && monsterToughness != toughness.one && !ImADuck)
			{
				animator.SetBool ("birth",true);
				currentSpawningCooldown = 1;
				nextState = MonsterStates.Preparation;
			}
		}
		if(monsterToughness == toughness.five && ImADuck && other.GetComponent<Collider>().tag == "PlayerHit" && fury.GetOnFury ())
		{
			GameObject tempFX = Instantiate(transformToParentFX,transform.position,Quaternion.identity) as GameObject;
			ImADuck = false;
			GameObject temp = Instantiate(swanduck,transform.position,Quaternion.identity) as GameObject;
			tempFX.transform.parent = temp.transform;
			temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
			gameController.addMonster (temp);
			room.DecreaseADuck ();
			DestroyItSelf();
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);
	}

	void InstantiateADuck(int i)
	{
        GameObject temp;
        if (TMNT)
            temp = temp = Instantiate(turtleTMNTPrefabs[i], transform.position, Quaternion.identity) as GameObject;
        else
		    temp = Instantiate(swanduck,transform.position,Quaternion.identity) as GameObject;
		//temp.transform.Translate(Random.Range (-4,4),Random.Range (-4,4),0);
		float angle = Random.Range (0,360);
		temp.GetComponent<MonsterGeneric>().Knockback(knockbackDistanceOfDucklings,new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle),Mathf.Sin(Mathf.Deg2Rad * angle),0).normalized);
		Transform cenarioLimits = room.GetCenarioLImits ();
		if(temp.transform.position.x < cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 || temp.transform.position.x > cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 ||
		   temp.transform.position.y < cenarioLimits.position.y - cenarioLimits.lossyScale.y/2 || temp.transform.position.y > cenarioLimits.position.y + cenarioLimits.lossyScale.y/2)
			temp.transform.position = transform.position;
		temp.SendMessage ("SetInvulnerability",true);
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
		gameController.addMonster (temp);
		
	}
	
	protected override void OnMonsterHit()
	{
	}
}
