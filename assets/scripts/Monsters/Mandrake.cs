using UnityEngine;
using System.Collections;

public class Mandrake : MonsterGeneric {

	public GameObject projectile, roots;
	public float prepationTime, jumpCooldown, flyTime, landTime;
	public float shootCooldown = 5;	
	public float lvl3JumpExecutionTime = 1;
	public float playerJumpCooldown = 2f;
	public GameObject spine;
	public float lvl5NumberOfShots, lvl5AccelCooldown;
	public bool jumpToPlayerDirection;
	bool flying, lvl3ShootAtPlayer;
	float currentPrepationTime, currentShootCooldown, currentJumpCooldown, currentPlayerJumpCooldown, jumpControl, jumpDirection, accelCooldown;
	Vector3 jumpVelocity, jumpToPlayerPosition;
	KnockbackType defaultKnockback;
	public bool shootOtherMandrakes;
	// Use this for initialization
	public override void SpecificMonsterAwake()
	{		
		monsterType = MonsterGeneric.monsterTypes.mandrake;
		//sphere = GetComponent<SphereCollider> ();
		//originalsphereColliderY = sphere.center.y;
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			defaultKnockback = knockbackType;
			nextState = MonsterStates.Desync;
			currentShootCooldown = shootCooldown * GetCooldownModifier ();
			currentJumpCooldown = jumpCooldown * GetCooldownModifier ();
			switch(monsterToughness)
			{
			default:
			case toughness.one:
			case toughness.two:
			case toughness.three:
				break;
			case toughness.four:
			case toughness.five:
				spine = Instantiate (spine,transform.position,Quaternion.identity) as GameObject;
				spine.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
				spine.SendMessage ("SetdontUsePositiveEffect");
				gameController.addEvent (spine);
				break;
			}
			break;
		
		case MonsterStates.Desync:
			gameObject.layer = MonsterGeneric.nonHarmfulLayer;
            Passive();
            knockbackType = defaultKnockback;
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			GetOutOfObject();
			//sphere.center = new Vector3(sphere.center.x, originalsphereColliderY, sphere.center.z);
			break;	
			
		case MonsterStates.Inactive:			
			break;	

		case MonsterStates.Normal:
			currentJumpCooldown = jumpCooldown * GetCooldownModifier ();
			break;	

		case MonsterStates.Preparation:
			animator.SetTrigger ("prepare");
			currentPrepationTime = prepationTime;
			switch(monsterToughness)
			{
			default:
			case toughness.one:
			case toughness.two:
			case toughness.three:
				break;
			case toughness.four:
			case toughness.five:
				spine.transform.position = transform.position;
				break;
			}
			break;

		case MonsterStates.Attack:
			gameObject.layer = MonsterGeneric.normalLayer;
            Agressive();
            if (monsterToughness != toughness.one)
				knockbackType = KnockbackType.hulk;
			if(lvl3ShootAtPlayer)
			{
				jumpToPlayerPosition = player.transform.position;
				jumpToPlayerPosition.z = transform.position.z;
				jumpControl = lvl3JumpExecutionTime;
				gameObject.layer = MonsterGeneric.immaterialLayer;
                Agressive();
		    }
			else
			{
				if(!jumpToPlayerDirection)
					jumpDirection = Random.Range (0,360);
				else
					jumpDirection = Mathf.Atan2 (player.position.y - transform.position.y, player.position.x - transform.position.x) * Mathf.Rad2Deg;
				jumpControl = flyTime;
				jumpVelocity =  new Vector3(Mathf.Cos(Mathf.Deg2Rad * jumpDirection),Mathf.Sin(Mathf.Deg2Rad * jumpDirection),0);
				controller.center = new Vector3(0,0,GameController.FlightPositionY);
			}
			flying = true;
			animator.SetBool ("jump", true);
			break;

		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter	
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			LifeRegen();
			currentShootCooldown -= GameController.deltaTime;
			if(currentShootCooldown < 0)
			{
				if(lvl3ShootAtPlayer)
				{
					lvl3ShootAtPlayer = false;
					float angle =  Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
					Shoot (angle);
					CreateRoots();
				}
				else
					Shoot (Random.Range (0,360));
				currentShootCooldown = shootCooldown * GetCooldownModifier ();
			}
			currentJumpCooldown -= GameController.deltaTime;
			if(currentJumpCooldown < 0)
			{
				nextState = MonsterStates.Preparation;
				if(monsterToughness != toughness.one)
					Shoot (Random.Range (0,360));
			}
			accelCooldown -= GameController.deltaTime;
			currentPlayerJumpCooldown -= GameController.deltaTime;
			break;

		case MonsterStates.Attack:
			if(lvl3ShootAtPlayer)
			{
				if(flying)
				{
					jumpControl -= GameController.deltaTime;	
					if(jumpControl < 0)
					{
						jumpControl = landTime;
						flying = false;
						//controller.center = new Vector3(0,0,0);
						animator.SetBool ("jump", false);
						gameObject.layer = MonsterGeneric.normalLayer;
                        Agressive();
                        transform.position = jumpToPlayerPosition;
						currentPlayerJumpCooldown = playerJumpCooldown;
					}
				}
				else
				{
					jumpControl -= GameController.deltaTime;									
					if(jumpControl < 0)
					{
						nextState = MonsterStates.Desync;	
						//sphere.center = new Vector3(sphere.center.x, originalsphereColliderY, sphere.center.z);
					}
				}
			}
			else
			{
				if(flying)
				{
					jumpControl -= GameController.deltaTime;	
					if(!CheckCollisionWithFurniture() && jumpControl < 0)
					{
						jumpControl = landTime;
						flying = false;
						controller.center = new Vector3(0,0,0);
						animator.SetBool ("jump", false);
					}
					lastTimeTouchedAWall--;
					Velocity ();
					Move ();
				}
				else
				{
					jumpControl -= GameController.deltaTime;									
					if(jumpControl < 0)
					{
						nextState = MonsterStates.Desync;	
					}
					Velocity ();
					Move ();
				}
			}
			break;

		case MonsterStates.Preparation:
			currentPrepationTime -= GameController.deltaTime;
			if(currentPrepationTime < 0)
				nextState = MonsterStates.Attack;
			break;

		case MonsterStates.Hook:								
			break;
			
		case MonsterStates.Death:
			Death ();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}

	void Shoot(float angle)
	{
		GameObject temp = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
		gameController.addMonster (temp);	
		if (shootOtherMandrakes)
			return;
		temp.SendMessage ("SetDirection",angle);
		temp.SendMessage ("SetParent",this.gameObject);
	}

	void CreateRoots()
	{
		GameObject temp = Instantiate (roots,transform.position,Quaternion.identity) as GameObject;
		temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		temp.SendMessage ("SetdontUsePositiveEffect");
		gameController.addEvent (temp);
	}

	void Velocity()
	{
		velocity = jumpVelocity;
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			
	}

	void Move()	{		

		controller.Move(velocity * isPlayerPushing);
	}
		
	void OnTriggerEnter(Collider other)
	{

	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);
	}

	protected override void OnMonsterHit()
	{
	}

	bool CheckCollisionWithFurniture()
	{
		int layerMask = 1024;
		Vector3 upTemp = transform.position + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,transform.position - upTemp);
		if(Physics.SphereCast(ray,controller.radius,1000,layerMask))
			return true;
		else
			return false;
	}

	int lastTimeTouchedAWall = 0;
	void OnControllerColliderHit(ControllerColliderHit hit) 
	{
		if(CheckIfCurrentOrNextState(MonsterStates.Attack) && lastTimeTouchedAWall < 0)
		{
			switch(hit.transform.tag)
			{
			case Tags.Wall:
			case Tags.Lvl3Obstacle:
				if(Mathf.Abs (hit.moveDirection.x) > Mathf.Abs (hit.moveDirection.y))
				{
					jumpVelocity.x = -jumpVelocity.x;
					lastTimeTouchedAWall = 10;
				}
				else
				{
					jumpVelocity.y = -jumpVelocity.y;
					lastTimeTouchedAWall = 10;
				}
				break;
			}
		}
	}

	public void ShootPlayer()
	{
		switch(monsterToughness)
		{
		default:
		case toughness.one:
		case toughness.two:
			break;
		case toughness.three:
		case toughness.four:
		case toughness.five:
			if(currentPlayerJumpCooldown <= 0 && !(CheckIfCurrentOrNextState (MonsterStates.Attack)))
			{
				if(CheckIfCurrentOrNextState (MonsterStates.Preparation))
					animator.SetTrigger ("prepare");
				nextState = MonsterStates.Attack;
				currentShootCooldown = -1;
				lvl3ShootAtPlayer = true;
			}
			break;
		}
	}

	public void AccelMetabolism()
	{
		if(monsterToughness == toughness.five && accelCooldown < 0)
		{
			for (int i = 0; i < lvl5NumberOfShots; i++) {
				Shoot (Random.Range(0,360));
			}
			accelCooldown = lvl5AccelCooldown;
		}
	}
}