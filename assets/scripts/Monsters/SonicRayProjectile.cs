using UnityEngine;
using System.Collections;

public class SonicRayProjectile : MonsterGeneric {
	
	float direction;
	public float lifeTime = 7;
	float currentLifeTime;
	public int bounceLimit = 3;
	public int lvl4bouncelimit = 6;
	public float sizeIncrease = 0.25f;
	public LerpEquationTypes lerpSizeIncrease;
	public float dontChangeDirectionForTheNextSeconds = 0.2f;
	float reloaddontChangeDirectionForTheNextSeconds;
	GameObject parent;
	Vector3 initialSize, finalSize;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.projectile;
		reloaddontChangeDirectionForTheNextSeconds = dontChangeDirectionForTheNextSeconds;
		gameObject.tag = "MonsterProjectile";
		currentLifeTime = lifeTime;
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;	
			switch(monsterToughness)
			{
			case toughness.three:
			case toughness.four:
			case toughness.five:
				bounceLimit = lvl4bouncelimit;
				break;
			}
			initialSize = transform.localScale;
			finalSize = initialSize * (1 + sizeIncrease);
			break;
			
		case MonsterStates.Inactive:			
			break;	

		case MonsterStates.Death:
			deathCountDown = afterLife;
			break;

		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;
			
		case MonsterStates.Normal:
			break;
			
		case MonsterStates.Knockback:
			nextState = MonsterStates.Normal;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter	
		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			Velocity ();
			Move ();
			dontChangeDirectionForTheNextSeconds -= GameController.deltaTime;
			Vector3 temp = lerpSizeIncrease.Lerp (finalSize,initialSize,currentLifeTime / lifeTime);
			temp.z = 1;
			transform.localScale = temp;
			currentLifeTime -= GameController.deltaTime;
			if(currentLifeTime < 0)
				nextState = MonsterStates.Death;
			break;
			
		case MonsterStates.Hook:
			//controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			/*knockbackCountDown -= GameController.deltaTime;
			Move();
			if(knockbackCountDown < 0)
			{
				nextState = monsterStates.normal;
			}*/
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}

	public void SetDirection(float d)
	{
		direction = d;		
		visual.transform.FindChild ("base").Rotate(0,0,d-90);		
	}

	public void SetParent(GameObject father)
	{
		parent = father;		
	}

	void Velocity()
	{		
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			
	}
	
	void Move()
	{	
		controller.Move(velocity);
	}

	Collider lastToHit;
	void OnControllerColliderHit(ControllerColliderHit hit)
	{	
		if(dontChangeDirectionForTheNextSeconds > 0 || lastToHit == hit.collider || hit.transform.tag == "PlayerHit")
			return;
		dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
		animator.SetTrigger ("reflect");
		bounceLimit--;
		if(bounceLimit < 0)
			nextState = MonsterStates.Death;
		lastToHit = hit.collider;
		if (Mathf.Abs (hit.normal.x) > Mathf.Abs (hit.normal.y)) 
		{
			if(hit.normal.x > 0)
			{//right
				direction = 180 - direction;
			}
			else
			{//left
				direction = 180 - direction;
			}
		}
		else
		{
			if(hit.normal.y > 0)
			{//up
				direction = 360 - direction;
			}
			else
			{//down
				direction = 360 - direction;
			}
		}
		visual.transform.FindChild ("base").rotation = Quaternion.Euler(0,0,direction);
	}	

	void OnTriggerEnter(Collider other)
	{
		if(CheckIfCurrentOrNextState(MonsterStates.Death))
		   return;
		if(other.transform.tag == "PlayerHit")
		{
			if(!parent)
			{
				nextState = MonsterStates.Death;
				return;
			}
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
				break;
			case toughness.three:
			case toughness.four:
			case toughness.five:
				parent.SendMessage ("ShootPlayer",SendMessageOptions.DontRequireReceiver);
				break;
			}
			nextState = MonsterStates.Death;
		}
		if(monsterToughness == toughness.five && other.GetComponent<Mandrake>() && dontChangeDirectionForTheNextSeconds < -0.5f)
		{
			parent.SendMessage ("AccelMetabolism",SendMessageOptions.DontRequireReceiver);
			nextState = MonsterStates.Death;
		}
	}

	protected override void OnMonsterHit()
	{
	}
}