﻿public enum MonsterStates{
	VOID=0,
	Start=1,
	Inactive=2,
	Normal=3,
	Movimentation=4,
	Attack=5,
	Knockback=6,
	Death=7,
	Hook=8,
	Stun=9,
	Desync=10,//usado diferente de monstro para monstro, serve para diferenciar um monstro de outros de sua especie
	Preparation=11,
	Retreat=12
}
