﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Prefab("GhostManager")]
public class GhostManager : Singleton<GhostManager> {

	public List<Ghost> tList;
	protected GameController gameController;
	public int maxAttackingUnitsLvl1 = 2;
	public int maxAttackingUnitsLvl2 = 2;
	public int maxAttackingUnitsLvl3 = 2;
	public int maxAttackingUnitsLvl4 = 2;
	public int maxAttackingUnitsLvl5 = 2;

	public int cowardTriggerNumberOfUnitsLvl1 = 2;
	public int cowardTriggerNumberOfUnitsLvl2 = 2;
	public int cowardTriggerNumberOfUnitsLvl3 = 2;
	public int cowardTriggerNumberOfUnitsLvl4 = 2;
	public int cowardTriggerNumberOfUnitsLvl5 = 2;

	int maxAttackingUnits = 1;
	bool turnOnCowardBehavior;

	public int AttackingUnits { get; protected set; }

	public float lvl3DistanceRequiredForArmor = 8;
	public int lvl3GhostsRequiredForArmor= 4;

	public float lvl5DistanceRequiredForBigGhost = 5;
	public int lvl5GhostsRequiredForBigGhost= 10;

	public GameObject bigghost;
	public LerpEquationTypes lerp;
	public float bigGhostFormationTime, bigGhostCooldown;
	bool makingBigGhost, bigGhostIsAlive;
	int bigGhostBase;
	float currentBigGhostFormationTimer, currentBigGhostCooldown;
	Ghost []ghostsInsideBigGhost;
	protected void Awake() {
		gameController = GameController.Instance;
		switch(StageController.Instance.GetToughness ())
		{
		case 1:
			maxAttackingUnits = maxAttackingUnitsLvl1;
			break;
		case 2:
			maxAttackingUnits = maxAttackingUnitsLvl2;
			break;
		case 3:
			maxAttackingUnits = maxAttackingUnitsLvl3;
			break;
		case 4:
			maxAttackingUnits = maxAttackingUnitsLvl4;
			break;
		case 5:
			maxAttackingUnits = maxAttackingUnitsLvl5;
			break;
		}
		tList = new List<Ghost>();
		ghostsInsideBigGhost = new Ghost[lvl5GhostsRequiredForBigGhost];
        currentBigGhostCooldown = bigGhostCooldown/3;
    }

	void Update () 
	{
		if (makingBigGhost || bigGhostIsAlive) 
		{
			if(makingBigGhost)
			{
				currentBigGhostFormationTimer -= GameController.deltaTime;
				for (int i = 1; i < ghostsInsideBigGhost.Length; i++) 
					ghostsInsideBigGhost[i].transform.position = lerp.Lerp (ghostsInsideBigGhost[0].transform.position,ghostsInsideBigGhost[i].transform.position,currentBigGhostFormationTimer/bigGhostFormationTime);
				if(currentBigGhostFormationTimer < 0)
				{
					makingBigGhost = false;
					GameObject temp = Instantiate(bigghost,ghostsInsideBigGhost[0].transform.position,Quaternion.identity) as GameObject;
					temp.SendMessage (MnstMsgs.SetToughness,MonsterGeneric.toughness.five);
					temp.SendMessage("ThisIsBigGhost");
					gameController.addMonster (temp);
					for (int k = 0; k < ghostsInsideBigGhost.Length; k++) {
						ghostsInsideBigGhost[k].gameObject.SetActive (false);
					}
					currentBigGhostCooldown = bigGhostCooldown;
				}
			}
		} 
		else 
		{
			currentBigGhostCooldown -= GameController.deltaTime;
			for (int i = 0; i < tList.Count; i++) {
				int count = -1;
				switch (tList[i].monsterToughness) {
				case MonsterGeneric.toughness.one:
				case MonsterGeneric.toughness.two:
					break;
				case MonsterGeneric.toughness.three:
				case MonsterGeneric.toughness.four:
					ResolveLvl3Armor(i);
					break;
				case MonsterGeneric.toughness.five:
					if(currentBigGhostCooldown < 0)
					{
						count = -1;
						for (int j = 0; j < tList.Count; j++) {
							if (Vector2.Distance (tList [i].transform.position, tList [j].transform.position) < lvl5DistanceRequiredForBigGhost)
								count++;
						}
						if (count > lvl5GhostsRequiredForBigGhost) {
							bigGhostBase = i;
							makingBigGhost = true;
							bigGhostIsAlive = true;
							currentBigGhostFormationTimer = bigGhostFormationTime;
							ghostsInsideBigGhost[0] = tList [bigGhostBase];
							ghostsInsideBigGhost[0].BecomePartOfBigGhost();
							count = 1;
							for (int j = 0; j < tList.Count; j++) {
								if (bigGhostBase != j && Vector2.Distance (ghostsInsideBigGhost[0].transform.position, tList [j].transform.position) < lvl5DistanceRequiredForBigGhost && count < lvl5GhostsRequiredForBigGhost) {
									tList [j].BecomePartOfBigGhost();
									ghostsInsideBigGhost[count] = tList [j];
									count++;
								}
							}
							for (int k = 0; k < ghostsInsideBigGhost.Length; k++) {
								tList.Remove (ghostsInsideBigGhost[k]);
							}
							tList.Remove(ghostsInsideBigGhost[0]);
							return;
						}
					}
                    ResolveLvl3Armor(i);
					break;
				}
			}
		}
	}

    public void ResolveLvl3Armor(int i)
    {
        int count = -1;
        for (int j = 0; j < tList.Count; j++)
        {
            if (Vector2.Distance(tList[i].transform.position, tList[j].transform.position) < lvl3DistanceRequiredForArmor)
                count++;
        }
        if (count > lvl3GhostsRequiredForArmor)
            tList[i].ChangeArmorLvl3(true);
        else
            tList[i].ChangeArmorLvl3(false);
    }

	public bool TryGetAttackPermission() {
		if(AttackingUnits < maxAttackingUnits) {
			++AttackingUnits;
			return(true);
		} else {
			return(false);
		}
	}



	public void LiftAttackPermission() {
		--AttackingUnits;
	}


	public void EvaluateCowardNumber()
	{
		if (turnOnCowardBehavior)
			return;
		int cowardNumber;
		switch(StageController.Instance.GetToughness ())
		{
		default:
		case 1:
			cowardNumber = cowardTriggerNumberOfUnitsLvl1;			
			break;
		case 2:
			cowardNumber = cowardTriggerNumberOfUnitsLvl2;	
			break;
		case 3:
			cowardNumber = cowardTriggerNumberOfUnitsLvl3;	
			break;
		case 4:
			cowardNumber = cowardTriggerNumberOfUnitsLvl4;	
			break;
		case 5:
			cowardNumber = cowardTriggerNumberOfUnitsLvl5;	
			break;
		}
		if(tList.Count <= cowardNumber)
		{
			turnOnCowardBehavior = true;
			for (int i = 0; i < tList.Count; i++) {
				tList[i].ActivateCowardBehavior();
			}
		}
	}

	public void DeathOfBigGhost(Vector3 position)
	{
		for (int k = 0; k < ghostsInsideBigGhost.Length; k++) {
			tList.Add(ghostsInsideBigGhost[k]);
			ghostsInsideBigGhost[k].gameObject.SetActive (true);
			ghostsInsideBigGhost[k].GetOutBigGhost ();
			ghostsInsideBigGhost[k].transform.position = position;
		}
		bigGhostIsAlive = false;
	}
}