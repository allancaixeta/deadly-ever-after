﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrowingProjectile : MonsterGeneric {

	float direction;
	public float lifeTime = 7;
	public float growthTime= 0.5f;
	public LerpEquationTypes lerp;
	float currentGrowthTime;
	bool passThrough = false;
	public bool alsoHitMonster = false;
	public bool stunPlayer, onlyStun;
	public float stunValue;
	public Sprite awesomeLooks;
	public GameObject FxOnFury;
	List<string> monsterHitListID;
	List<float> monsterHitListDuration;
	float slowWhenHit, slowTimer;
	bool sendMessageToParentWhenHittingPlayer = false;

	Vector3 originalSize;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.projectile;
		monsterHitListID = new List<string> ();
		monsterHitListDuration = new List<float> ();
		if(!onlyStun)
			gameObject.tag = Tags.MonsterProjectile;
		else
			gameObject.tag = Tags.StunMonster;
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
			currentGrowthTime = growthTime;
			originalSize = transform.localScale;
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Death:
			deathCountDown = afterLife;
			break;

		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;

		case MonsterStates.Normal:
			/*if(passThrough)
			{
				Color c = visual.renderer.material.color;
				c.a =  0.5f;
				visual.renderer.material.color = c;
			}*/
			if(passThrough && awesomeLooks)
			{
				visual.GetComponent<SpriteRenderer>().sprite = awesomeLooks;
			}
			if(slowWhenHit > 0 && awesomeLooks)
			{
				visual.GetComponent<SpriteRenderer>().sprite = awesomeLooks;
			}
			break;

		case MonsterStates.Knockback:
			nextState = MonsterStates.Normal;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		if(magicallySlowed)
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}		
		switch(currentState)
		{
		case MonsterStates.Start:
			currentGrowthTime -= GameController.deltaTime;
			if(currentGrowthTime < 0)
				nextState = MonsterStates.Normal;						
			transform.localScale = lerp.Lerp (Vector3.zero,originalSize,1-currentGrowthTime/growthTime);
			break;
		
		case MonsterStates.Normal:
			Velocity ();
			Move ();
			lifeTime -= GameController.deltaTime;
			if(lifeTime < 0)
				nextState = MonsterStates.Death;
			DecreaseMonsterHitListDuration();
			break;
		
		case MonsterStates.Hook:
			//controller.Move(velocity);					
			break;

		case MonsterStates.Death:
			Death ();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			/*knockbackCountDown -= GameController.deltaTime;
			Move();
			if(knockbackCountDown < 0)
			{
				nextState = monsterStates.normal;
			}*/
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}
	
	void Velocity()
	{
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			
	}
	
	void Move()
	{	
		transform.Translate(velocity,Space.World);
	}
	
	public void SetDirection(float d)
	{
		direction = d;		
		visual.transform.Rotate(0,0,d);		
	}
	
	void OnTriggerEnter(Collider other)
	{	
		if(CheckIfCurrentOrNextState(MonsterStates.Death))
		   return;
		if(alsoHitMonster && other.transform.tag == Tags.Monster && other.transform.root != transform.root)
		{
			for (int i = 0; i < monsterHitListID.Count; i++){
				if(monsterHitListID[i] == other.transform.name)
					return;
			}				
			other.GetComponent<MonsterGeneric>().Hit ((int)monsterDamage * 3);
			if(slowWhenHit > 0)
				other.GetComponent<MonsterGeneric>().SlowDown (slowWhenHit,slowTimer);
			if(!passThrough)
			{
				nextState = MonsterStates.Death;
				return;
			}
			else
			{
				monsterHitListID.Add(other.transform.name);
				monsterHitListDuration.Add(1);
			}
		}
		if(sendMessageToParentWhenHittingPlayer && other.transform.tag == "PlayerHit")
			transform.root.SendMessage ("PlayerHit");		
		if(passThrough)
			return;
		if((other.transform.tag == Tags.Lvl3Obstacle  || other.transform.tag == Tags.Wall || other.transform.tag == Tags.PlayerHit))
		{
			nextState = MonsterStates.Death;
			if(other.transform.tag == "PlayerHit")
			{
				if(stunPlayer)
					player.SendMessage ("Stunned",stunValue);
				if(slowWhenHit > 0)
					player.GetComponent<PlayerController>().SlowDown(slowWhenHit,slowTimer);
			}
		}
	}
	
	public void SetPassThrough()
	{
		passThrough = true;
	}

	public void SetSendMessageToParentWhenHittingPlayer()
	{
		sendMessageToParentWhenHittingPlayer = true;
	}


	public void SetSlowWhenHit(float sv, float st)
	{
		slowWhenHit = sv;
		slowTimer = st;
	}

	void DecreaseMonsterHitListDuration()
	{
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime  * timeDistortion;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
	}

	protected override void OnMonsterHit()
	{
	}

	public void OnFury(float upgrade)
	{
		GameObject temp = Instantiate(FxOnFury,transform.position,FxOnFury.transform.rotation) as GameObject;
		temp.transform.parent = this.transform;
		temp.transform.localPosition = FxOnFury.transform.localPosition;
		UpgradeMonster (upgrade);
		speed *= (1 + upgrade);
	}
}
