using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlastUnity : MonsterGeneric {

	float direction;
	public float lifeTime = 7;
	bool passThrough = false;
	//bool generateSomethingWhenTouch;
	public bool alsoHitMonster = false;
	public bool stunPlayer, onlyStun;
	public float stunValue;
	List<string> monsterHitListID;
	List<float> monsterHitListDuration;
	float slowWhenHit, slowTimer;
	bool sendMessageToParentWhenHittingPlayer = false;
	GameObject father;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.projectile;
		monsterHitListID = new List<string> ();
		monsterHitListDuration = new List<float> ();
		gameObject.tag = Tags.Explosion;
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.VOID:
			gameObject.layer = originalLayer;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Death:
			nextState = MonsterStates.VOID;
			break;

		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;

		case MonsterStates.Normal:
			break;

		case MonsterStates.Knockback:
			nextState = MonsterStates.Normal;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		Slow ();	
		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			Velocity ();
			Move ();
			lifeTime -= GameController.deltaTime;
			if(lifeTime < 0)
				DestroyItSelf();
			DecreaseMonsterHitListDuration();
			break;
		
		case MonsterStates.Hook:
			//controller.Move(velocity);					
			break;
						
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			/*knockbackCountDown -= GameController.deltaTime;
			Move();
			if(knockbackCountDown < 0)
			{
				nextState = monsterStates.normal;
			}*/
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}
	
	void Velocity()
	{
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			
	}
	
	void Move()
	{	
		transform.Translate(velocity,Space.World);
	}
	
	public void SetDirection(float d)
	{
		direction = d;		
		visual.transform.Rotate(0,0,d);		
	}
	
	void OnTriggerEnter(Collider other)
	{	
		if(CheckIfCurrentOrNextState(MonsterStates.Death))
		   return;
		if(alsoHitMonster && other.transform.tag == Tags.Monster && other.transform.root != transform.root)
		{
			for (int i = 0; i < monsterHitListID.Count; i++){
				if(monsterHitListID[i] == other.transform.name)
					return;
			}				
			other.GetComponent<MonsterGeneric>().Hit ((int)monsterDamage * 3);
			if(slowWhenHit > 0)
				other.GetComponent<MonsterGeneric>().SlowDown (slowWhenHit,slowTimer);
			if(!passThrough)
			{
				nextState = MonsterStates.Death;
				return;
			}
			else
			{
				monsterHitListID.Add(other.transform.name);
				monsterHitListDuration.Add(1);
			}
		}
		if(sendMessageToParentWhenHittingPlayer && other.transform.tag == Tags.PlayerHit)
			transform.root.SendMessage ("PlayerHit");	
		if(passThrough)
			return;
		if((other.transform.tag == Tags.Lvl2Obstacle  || other.transform.tag == Tags.Lvl3Obstacle  || other.transform.tag == Tags.Wall || other.transform.tag == Tags.PlayerHit))
		{
			nextState = MonsterStates.Death;
			if(other.transform.tag == Tags.PlayerHit)
			{
				if(stunPlayer)
					player.SendMessage ("Stunned",stunValue);
				if(slowWhenHit > 0)
					player.GetComponent<PlayerController>().SlowDown(slowWhenHit,slowTimer);
			}
		}
	}

	public void SetGenerateSomethingWhenTouch(GameObject father)
	{
		//generateSomethingWhenTouch = true;
		this.father = father;
	}

	public void SetPassThrough()
	{
		passThrough = true;
	}

	public void SetSendMessageToParentWhenHittingPlayer()
	{
		sendMessageToParentWhenHittingPlayer = true;
	}

	public void SetSlowWhenHit(float sv, float st)
	{
		slowWhenHit = sv;
		slowTimer = st;
	}

	void DecreaseMonsterHitListDuration()
	{
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime  * timeDistortion;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
	}

	public GameObject GetFather()
	{
		return father;
	}

	protected override void OnMonsterHit()
	{
	}
}
