﻿using UnityEngine;
using System.Collections;

public class PossessiveSoul : MonoBehaviour {

	public float timeToReachTarget = 2;
	float currentTimeToReachTarget;
	public GameObject FXOnDeath, FXOnPossession;
	GameController gameController;
	float scaler;
	Transform target;
	string originalGrimReaperName;
	bool jobDone;
	Vector3 bornPosition, defaultScale, invertedScale;
	// Use this for initialization
	void Awake () {
		gameController = GameController.Instance;
		Instantiate (FXOnDeath,transform.position,Quaternion.identity);
		target = this.transform;
		bornPosition = this.transform.position;
		defaultScale = transform.localScale;
		invertedScale = defaultScale;
		invertedScale.y = -invertedScale.y;
	}

	public void UpdateMeGameController()
	{
		if(!jobDone)
		{
			if(!target)
			{
				gameController.removeUpdatableGameObjectWithouthDestroy (this.gameObject);
				Destroy (this.gameObject);
				return;
			}
			currentTimeToReachTarget -= GameController.deltaTime;
			transform.position = Vector3.Lerp (target.GetComponent<MonsterGeneric>().GetMonsterCenter (),bornPosition,currentTimeToReachTarget/timeToReachTarget);
			if(target.transform.position.x > transform.position.x)
				transform.localScale = defaultScale;
			else
				transform.localScale = invertedScale;
			if(currentTimeToReachTarget < 0)
			{
				target.GetComponent<MonsterGeneric>().UpgradeMonster (scaler);
				GetComponent<Animator>().SetTrigger ("possess");
				jobDone = true;
				transform.parent = target;
				transform.position = target.GetComponent<MonsterGeneric>().GetMonsterCenter () + (Vector3.up * 3 * target.transform.localScale.y);// Vector3(0,4,0);
				Instantiate (FXOnPossession,transform.position,Quaternion.identity);
				gameController.removeUpdatableGameObjectWithouthDestroy (this.gameObject);
				this.enabled = false;
			}
		}
	}

	public void FindTarget()
	{
		target = gameController.FindClosestMonster (transform.position,originalGrimReaperName);
		if(!target)
			Destroy (gameObject);
		else
		{
			print (target.name);
			print (target.GetComponent<MonsterGeneric> ().GetMonsterType ());
			gameController.addUpdatableGameObject (this.gameObject);
		}
	}

	public void SetScaler(float scaler, string originalGrimReaperName)
	{
		this.scaler = scaler;
		this.originalGrimReaperName = originalGrimReaperName;
		currentTimeToReachTarget = timeToReachTarget;
		FindTarget ();
	}
}
