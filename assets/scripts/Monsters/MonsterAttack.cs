using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterAttack : MonsterGeneric {

	float direction;
	public float lifeTime = 1;
	float maxLifeTime;
	public float sizeIncrement = 2;
    Vector3 size;
	bool passThrough = true;
	public bool stunPlayer, onlyStun;
	public float stunValue;
	float slowWhenHit, slowTimer;
	bool sendMessageToParentWhenHittingPlayer = false;
	public LerpEquationTypes lerp = LerpEquationTypes.Quadratic;
	Fury fury;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.projectile;
		if(!onlyStun)
			gameObject.tag = Tags.MonsterProjectile;
		else
			gameObject.tag = Tags.StunMonster;
		maxLifeTime = lifeTime;
		fury = GetComponent<Fury> ();
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
            size = transform.localScale;
            nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Death:
			deathCountDown = afterLife;
			break;

		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;

		case MonsterStates.Normal:
			break;

		case MonsterStates.Knockback:
			nextState = MonsterStates.Normal;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		if(magicallySlowed)
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}		
		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			Velocity ();
			Move ();
			Vector3 scale = lerp.Lerp (size, Vector3.one * sizeIncrement, 1 - lifeTime / maxLifeTime);
			scale.z = 1;
			transform.localScale = scale;
			lifeTime -= GameController.deltaTime;
			if(lifeTime < 0)
				nextState = MonsterStates.Death;
			break;
		
		case MonsterStates.Hook:
			//controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death ();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			/*knockbackCountDown -= GameController.deltaTime;
			Move();
			if(knockbackCountDown < 0)
			{
				nextState = monsterStates.normal;
			}*/
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}
	
	void Velocity()
	{	
		velocity = new Vector3(Mathf.Cos(direction * Mathf.Deg2Rad),Mathf.Sin(direction * Mathf.Deg2Rad),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{	
		transform.Translate(velocity,Space.World);
	}
	
	public void SetDirection(float d)
	{		
		direction = d;		
		visual.transform.FindChild ("base").Rotate(0,0,d-90);		
	}
	
	void OnTriggerEnter(Collider other)
	{	
		if(CheckIfCurrentOrNextState(MonsterStates.Death))
		   return;
		if(sendMessageToParentWhenHittingPlayer && other.transform.tag == "PlayerHit")
			transform.root.SendMessage ("PlayerHit");		
		if(passThrough)
			return;
		if(other.transform.tag == Tags.PlayerHit)
		{
			nextState = MonsterStates.Death;
			if(stunPlayer)
				player.SendMessage ("Stunned",stunValue);
			if(slowWhenHit > 0)
				player.GetComponent<PlayerController>().SlowDown(slowWhenHit,slowTimer);		
		}
	}
	
	public void SetPassThrough()
	{
		passThrough = true;
	}

	public void SetSendMessageToParentWhenHittingPlayer()
	{
		sendMessageToParentWhenHittingPlayer = true;
	}

	public void SetSlowWhenHit(float sv, float st)
	{
		slowWhenHit = sv;
		slowTimer = st;
	}

	protected override void OnMonsterHit()
	{
	}

	public void OnFury()
	{
		GameObject temp = Instantiate(fury.FxOnFury,transform.position,fury.FxOnFury.transform.rotation) as GameObject;
		temp.transform.parent = this.transform;
		temp.transform.localPosition = fury.FxOnFury.transform.localPosition;
		transform.localScale = new Vector3(transform.localScale.x * (monsterDamage + fury.furyDamageAddition / monsterDamage),
			transform.localScale.y * (monsterDamage + fury.furyDamageAddition / monsterDamage),
			transform.localScale.z);
		monsterDamage = monsterDamage + fury.furyDamageAddition;
		speed *= (1 + fury.furySpeed);
	}
}