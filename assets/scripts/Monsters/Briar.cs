using UnityEngine;
using System.Collections;

public class Briar : MonsterGeneric {

	public float shootCooldown = 3;
	public GameObject projectile,laser;
	public float timeForPlayerToMoveBeforeTheLightning = 1.5f;
	float currentCooldown;
	Vector3 lastPlayerPosition;
	float lastTimePlayerMoved;
	LineOfSight lineOfSight;
	float angleOfLastShot;
	public GameObject target, thunder;
	Transform rose;
	SpriteRenderer targetinner, targetouter;
	public override void SpecificMonsterAwake()
	{		
		monsterType = MonsterGeneric.monsterTypes.briar;
		currentCooldown = shootCooldown * GetCooldownModifier ();	
		lineOfSight = GetComponent<LineOfSight>();
		if (!lineOfSight) {
			gameObject.AddComponent <LineOfSight>();
			lineOfSight = GetComponent<LineOfSight>();
		}
		target = Instantiate (target, player.position, Quaternion.identity) as GameObject;
		targetinner = target.transform.Find ("thundertargetinner").GetComponent<SpriteRenderer>();
		blackWithAlpha = targetinner.color;
		whiteWithAlpha = Color.white;
		whiteWithAlpha.a = targetinner.color.a;
		targetouter = target.transform.Find ("thundertargetouter").GetComponent<SpriteRenderer>();
		target.SetActive (false);
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			lastPlayerPosition = player.position;
			GetOutOfObject();
			rose = visual.transform.Find ("base/rose");
			rose.transform.localScale = new Vector3(0,0,1);
			nextState = MonsterStates.Desync;		
			break;
			
		case MonsterStates.Inactive:			
			break;	
			
		case MonsterStates.Normal:
			currentCooldown = shootCooldown * GetCooldownModifier ();			
			targetinner.color = blackWithAlpha;
			targetouter.color = blackWithAlpha;
			break;
			
		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			break;
			
		case MonsterStates.Knockback:
			break;

		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			Destroy (target);
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter	
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			LifeRegen ();
			switch(monsterToughness)
			{
			case toughness.one:
				currentCooldown -= GameController.deltaTime;
				if(currentCooldown < 0)
				{
					angleOfLastShot = ShootAtPlayer();
					//print (angleOfLastShot);
					InstantiateShoot(angleOfLastShot);
					nextState = MonsterStates.Desync;
				}
				else if(currentCooldown < 0.2f)
					animator.SetTrigger("shoot");
				break;
			case toughness.two:
				currentCooldown -= GameController.deltaTime;
				if(currentCooldown < 0)
				{
					angleOfLastShot = ShootAtFuture();
					InstantiateShoot(angleOfLastShot);
					nextState = MonsterStates.Desync;
				}
				else if(currentCooldown < 0.2f)
					animator.SetTrigger("shoot");
				break;
			case toughness.three:
				currentCooldown -= GameController.deltaTime;
				if(currentCooldown < 0)
				{
					angleOfLastShot = ShootAtPlayer();
					InstantiateShoot(angleOfLastShot);
					if(lastTimePlayerMoved < 1)
					{
						angleOfLastShot = ShootAtFuture();
						InstantiateShoot(angleOfLastShot);
					}
					nextState = MonsterStates.Desync;
				}
				else if(currentCooldown < 0.2f)
					animator.SetTrigger("shoot");
				break;			
			case toughness.four:
				currentCooldown -= GameController.deltaTime;
				if(currentCooldown < 0)
				{
					angleOfLastShot = ShootAtPlayer();
					InstantiateShoot(angleOfLastShot);
					if(lastTimePlayerMoved < 1)
					{
						angleOfLastShot = ShootAtMiddle();
						InstantiateShoot(angleOfLastShot);
						angleOfLastShot = ShootAtFuture();
						InstantiateShoot(angleOfLastShot);
					}
					nextState = MonsterStates.Desync;
				}
				else if(currentCooldown < 0.2f)
					animator.SetTrigger("shoot");
				break;
			case toughness.five:
				currentCooldown -= GameController.deltaTime;
				if(currentCooldown < 0)
				{
					if(lastTimePlayerMoved > timeForPlayerToMoveBeforeTheLightning)
						ShootLaser();
					else
					{
						angleOfLastShot = ShootAtPlayer();
						InstantiateShoot(angleOfLastShot);
						angleOfLastShot = ShootAtMiddle();
						InstantiateShoot(angleOfLastShot);
						angleOfLastShot = ShootAtFuture();
						InstantiateShoot(angleOfLastShot);
					}
					nextState = MonsterStates.Desync;
				}
				else if(currentCooldown < 0.2f)
					animator.SetTrigger("shoot");
				break;
			}
			rose.transform.localScale = new Vector3(1-currentCooldown/shootCooldown * GetCooldownModifier (),1-currentCooldown/shootCooldown * GetCooldownModifier (),1);
			CheckPlayerMoved();
			break;
			
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			//inicia a animacao de tiros multiplos antes de comecar a atirar
			if(currentDesync < 0)
			{
				nextState = MonsterStates.Normal;
			}
			break;
			
		}
	}
	
	float ShootAtPlayer()
	{
		float angle = Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		return angle;
	}

	float ShootAtFuture()
	{
		float tx = player.position.x - transform.position.x;
		float ty = player.position.y - transform.position.y;
		Vector2 tv = player.GetComponent<PlayerController>().GetCurrentSpeed();
		float v = projectile.GetComponent<MonsterGeneric>().speed;
		// Get quadratic equation components
		float a = tv.x*tv.x + tv.y*tv.y - v*v;
		float b = 2 * (tv.x * tx + tv.y * ty);
		float c = tx*tx + ty*ty; 				
		// Solve quadratic
		Vector2 ts = quad(a, b, c); // See quad(), below
		
		// Find smallest positive solution
		Vector2 sol = Vector2.zero;
		
		float t0 = ts.x;
		float t1 = ts.y;
		float t = Mathf.Min(t0, t1);
		if (t < 0) 
			t = Mathf.Max(t0, t1); 
		if(t <= 0)
			t = 1;
		sol = new Vector2(player.position.x + tv.x*t,player.position.y + tv.y*t);		
		float angle = Mathf.Atan2 (sol.y - transform.position.y, sol.x - transform.position.x) * Mathf.Rad2Deg;
		if(float.IsNaN(angle))
		{
			angle = Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
			if(float.IsNaN (angle))
				angle = 0;
		}
		return angle;
	}

	/**
	 * Return solutions for quadratic
	 */
	Vector2 quad(float a, float b, float c) {
		Vector2 sol = Vector2.zero;
		if (Mathf.Abs(a) < 0) {
			if (Mathf.Abs(b) < 0) {
				sol = Mathf.Abs(c) < 0 ? new Vector2(0,0) : Vector2.zero;
			} else {
				sol = new Vector2(-c/b, -c/b);
			}
		} else {
			float disc = b*b - 4*a*c;
			if (disc >= 0) {
				disc = Mathf.Sqrt(disc);
				a = 2*a;
				sol = new Vector2((-b-disc)/a, (-b+disc)/a);
			}
		}
		return sol;
	}

	float ShootAtMiddle()
	{
		float angle = ShootAtFuture();
		float angle2 = ShootAtPlayer();
			
		float angleFinal = (angle + angle2) /2;
		if (angle > 130 && angle2 < -130 || angle < -130 && angle2 > 130)
		{
			float aux = (360 - Mathf.Abs(angle) - Mathf.Abs(angle2)) /2;
			if(Mathf.Abs(angle) > Mathf.Abs(angle2))
				angleFinal = angle2 + (angle2 > 0 ? aux : -aux);
			else
				angleFinal = angle + (angle > 0 ? aux : -aux);
		}
		return angleFinal;
	}

	void InstantiateShoot(float angle)
	{
		GameObject temp = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);		
		temp.SendMessage ("SetDirection",angle);
		gameController.addMonster (temp);		
	}

	void ShootLaser()
	{
		GameObject temp = Instantiate(laser,player.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
		//temp.SendMessage ("SetDirection",angle);
		temp.SendMessage ("SetPassThrough");
		gameController.addMonster (temp);	
		target.SetActive (false);
		Instantiate (thunder, transform.position, thunder.transform.rotation);
		temp = Instantiate (thunder, new Vector3(player.position.x-15,player.position.y,player.position.z) , thunder.transform.rotation) as GameObject;
		temp.transform.localRotation = Quaternion.Euler (0,-temp.transform.localRotation.eulerAngles.y,0);
	}
	Color blackWithAlpha, whiteWithAlpha;
	void CheckPlayerMoved()
	{
		if(Vector2.Distance (player.position,lastPlayerPosition)> 0.1f)
		{
			lastTimePlayerMoved = 0;
			target.SetActive (false);
		}
		if(monsterToughness == toughness.five)
			target.SetActive (true);
		lastTimePlayerMoved += GameController.deltaTime;
		float percentageToShootLaser = 0;
		if(timeForPlayerToMoveBeforeTheLightning - lastTimePlayerMoved > currentCooldown || currentCooldown > timeForPlayerToMoveBeforeTheLightning)
			target.SetActive (false);
		else
		{
			percentageToShootLaser = currentCooldown/timeForPlayerToMoveBeforeTheLightning;
			target.transform.position = Vector3.Lerp (player.position,this.transform.position,percentageToShootLaser);
			if(percentageToShootLaser < 0.5f)
				targetinner.color = Color.Lerp (whiteWithAlpha,blackWithAlpha,percentageToShootLaser * 2);
			else
				targetouter.color = Color.Lerp (whiteWithAlpha,blackWithAlpha,(percentageToShootLaser - 0.5f) * 2);
		}
		lastPlayerPosition = player.position;
	}

	protected override void OnMonsterHit()
	{
		animator.SetTrigger("onhit");
		currentCooldown = shootCooldown * GetCooldownModifier ();
	}
}
