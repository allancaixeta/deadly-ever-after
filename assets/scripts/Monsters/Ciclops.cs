using UnityEngine;
using System.Collections;
using Pathfinding;

[RequireComponent (typeof (LineOfSight))]
public class Ciclops : MonsterGeneric {

	public float chargeDamage = 5;
	float originalDamage;
	int originalLifeRegen;
	public float chargeSpeed = 30;
	public float lvl2SpeedIncrease = 2;
	public float startChargeAnimationDuration = 1;
	public float lvl3StunTime= 1;
	public int lvl4LifeRegen = 1;
	public float lvl5ProjectileDuration = 10;
	public float lvl5Cooldown = 3;
	float currentProjectileDuration, currentProjectileCooldown;
	public GameObject chaserProjectile;
	public AudioClip roarSound;
	Transform cenarioLimits;
	GameObject exclamation;
	Vector3 goalPosition;
	Fury fury;
	LineOfSight lineOfSight;
	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;	
	public float repathRate = 0.5f;
	private float lastRepath = -9999;
	float radiusCollider, distancefromGoal, oneSecond, additionalChargeSpeed, additionalPatrolSpeed;
	bool playerHittedThisLoopControl = false;
	int getAFuryPoint;
	bool acquiredLineOfSightWhileInFury;

	GameObject redLineOfSight;
	bool stopped = false;
	float maxStoppedTime = 2;
	float currentStoppedTime;
	Vector3 difference;
	Vector3 lastPosition;
	public override void SpecificMonsterAwake()
	{
		seeker = GetComponent<Seeker>();
		lineOfSight = GetComponent<LineOfSight>();
		goalPosition = new Vector3 (50,50,0);
		radiusCollider = GetComponent<CharacterController>().radius;
		if (!lineOfSight) {
			gameObject.AddComponent <LineOfSight>();
			lineOfSight = GetComponent<LineOfSight>();
		}
		monsterType = MonsterGeneric.monsterTypes.ciclops;
		fury = GetComponent<Fury>();
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			GetOutOfObject();
			nextState = MonsterStates.Desync;
			StageController stage = (StageController) FindObjectOfType(typeof(StageController));	
			cenarioLimits = stage.GetRoom().GetCenarioLImits ();
			originalDamage = monsterDamage;
			originalLifeRegen = lifeRegen;
			exclamation = visual.transform.Find ("exclamation").gameObject;
			exclamation.SetActive (false);
			chaserProjectile = Instantiate (chaserProjectile,transform.position,chaserProjectile.transform.rotation) as GameObject;
			chaserProjectile.SendMessage (MnstMsgs.SetToughness,monsterToughness);
			chaserProjectile.SendMessage ("SetSpeedRelativeToPlayer");
			chaserProjectile.SendMessage("SetSendMessageToParentWhenHittingPlayer",this.gameObject);
			gameController.addMonster (chaserProjectile);
			chaserProjectile.SetActive (false);
			currentProjectileDuration = lvl5ProjectileDuration;
			currentProjectileCooldown = lvl5Cooldown;
			redLineOfSight = transform.FindChild("lineofsight").gameObject;
			CreateNewGoal ();
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Preparation:
			attackCountDown = startChargeAnimationDuration;
			//animator.SetBool("precharge", true);
			//animator.SetBool("idle", false);
			seeker.StartPath (transform.position,player.position, OnPathComplete);				
			exclamation.SetActive (true);
			break;

		case MonsterStates.Normal:
			redLineOfSight.SetActive (false);
			exclamation.SetActive (false);				
			playerHittedThisLoopControl = false;
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
				break;
			case toughness.four:
				lifeRegen = originalLifeRegen + lvl4LifeRegen;
				break;
			case toughness.five:
				lifeRegen = originalLifeRegen + lvl4LifeRegen;			
				break;
			}
			additionalChargeSpeed = 0;
			break;

		case MonsterStates.Attack:		
			redLineOfSight.SetActive (true);
			lifeRegen = originalLifeRegen;
			monsterDamage = originalDamage + chargeDamage + (fury.GetOnFury () ? (fury.furyDamageAddition * (float)fury.maxFuryPoints) : 0);
			additionalPatrolSpeed = 0;
			getAFuryPoint = 0;
			acquiredLineOfSightWhileInFury = false;
			//chaserProjectile.SetActive (false);
			break;

		case MonsterStates.Desync:
			redLineOfSight.SetActive (false);
			exclamation.SetActive (false);
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			if(playerHittedThisLoopControl)
			{
				currentDesync = 3;
				fury.ResetFury();
				fury.StopAcumulatingFury ();
			}
			FixZ();
			GetOutOfObject();
			monsterDamage = originalDamage;
			break;

		case MonsterStates.Death:
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			chaserProjectile.GetComponent<MonsterGeneric>().nextState = MonsterStates.Death;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
            Agressive();
            nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal://patrol mode
			FixZ ();
			LifeRegen();
			/*Calcula se ele travou em algum lugar por mais de 2 segundos, para recriar o goal nesse caso*/
			difference = lastPosition - transform.position;
			if(stopped)
			{
				currentStoppedTime -= GameController.deltaTime;
				if(currentStoppedTime < 0)
				{
					CreateNewGoal ();
					stopped = false;
				}
				else
				{					
					if(Vector2.Distance (lastPosition,transform.position) > 0.2f)
					{	
						stopped = false;
						lastPosition = transform.position;
					}
				}
			}
			else
			{				
				if((difference.x < 0.1f && difference.y < 0.1f))
				{
					stopped = true;
					currentStoppedTime = maxStoppedTime;
				}
				else
					lastPosition = transform.position;
			}			

			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				Velocity ();
				Move ();
				distancefromGoal = Vector2.Distance (transform.position,goalPosition);
				if(distancefromGoal < 1)
				{
					nextState = MonsterStates.Desync;
					CreateNewGoal ();
				}
				break;
			}
			if(lineOfSight.hasLineOfSight)
				nextState = MonsterStates.Preparation;
			if(getAFuryPoint > 0)
			{
				fury.CalculateFury ();
				oneSecond -= GameController.deltaTime;
				if(oneSecond < 0)
				{
					getAFuryPoint--;
					oneSecond = 1.05f;
				}
				if(fury.GetOnFury())
				{
					nextState = MonsterStates.Attack;
					goalPosition = player.transform.position;
					seeker.StartPath (transform.position,goalPosition, OnPathComplete);				
				}
			}
			else
				fury.StopAcumulatingFury ();
			if(monsterToughness == toughness.five)
			{
				if(currentProjectileCooldown < 0)
				{
					if(currentProjectileDuration == lvl5ProjectileDuration)
					{
						chaserProjectile.transform.position = transform.position;
						chaserProjectile.SetActive (true);
					}
					currentProjectileDuration -= GameController.deltaTime;
					if(currentProjectileDuration < 0)
					{
						currentProjectileCooldown = lvl5Cooldown;
						chaserProjectile.SetActive (false);
						currentProjectileDuration = lvl5ProjectileDuration;
					}
				}
				else
					currentProjectileCooldown -= GameController.deltaTime;
			}
			break;
		
		case MonsterStates.Attack://charge mode            
			LifeRegen();
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				Charge();
				Move ();
				if(!fury.GetOnFury () || acquiredLineOfSightWhileInFury)
				{
					redLineOfSight.transform.localRotation = Quaternion.Euler (0, 0, Mathf.Atan2 (player.position.y - redLineOfSight.transform.position.y, 
																				player.position.x - redLineOfSight.transform.position.x) * Mathf.Rad2Deg - 90);
					redLineOfSight.transform.localScale = new Vector3(Vector2.Distance(redLineOfSight.transform.position,player.position)/transform.lossyScale.x,redLineOfSight.transform.localScale.y,redLineOfSight.transform.localScale.z);
					if(!lineOfSight.hasLineOfSight)
					{
						nextState = MonsterStates.Desync;	
						goalPosition = player.position;
						if(monsterToughness == toughness.five)
						{
							currentProjectileCooldown = lvl5Cooldown;
						}
						if(fury.GetOnFury ())
						{
							switch(monsterToughness)
							{
							case toughness.one:
							case toughness.two:
								break;
							case toughness.three:
							case toughness.four:							
							case toughness.five:
								Roar ();
								break;
							}
							fury.ResetFury();
							fury.StopAcumulatingFury ();
						}
					}
				}
				else
				{
					redLineOfSight.transform.localRotation = Quaternion.Euler (0,0,Mathf.Atan2 (goalPosition.y - redLineOfSight.transform.position.y, 
																				goalPosition.x - redLineOfSight.transform.position.x) * Mathf.Rad2Deg - 90);
					redLineOfSight.transform.localScale = new Vector3(Vector2.Distance(redLineOfSight.transform.position,goalPosition),redLineOfSight.transform.localScale.y,redLineOfSight.transform.localScale.z);
					if(lineOfSight.hasLineOfSight)
					{
						acquiredLineOfSightWhileInFury = true;
						goalPosition = player.transform.position;
						seeker.StartPath (transform.position,goalPosition, OnPathComplete);
					}
					else
					{
						distancefromGoal = Vector2.Distance (transform.position,goalPosition);
						if(distancefromGoal < 1)
						{
							acquiredLineOfSightWhileInFury = true;
						}
					}
				}
				break;
			}
			break;

		case MonsterStates.Preparation:
			attackCountDown -= GameController.deltaTime;
			if(attackCountDown < 0)
			{
				nextState = MonsterStates.Attack;
				//animator.SetBool("precharge", false);
				playerHittedThisLoopControl = false;
			}
			break;

		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
		}
	}

    void Velocity()
    {
        if (Time.time - lastRepath > repathRate && seeker.IsDone())
        {
            lastRepath = Time.time + repathRate;
            seeker.StartPath(transform.position, goalPosition, OnPathComplete);
        }
        if (path == null || currentWaypoint > path.vectorPath.Count)
            return;
        if (currentWaypoint == path.vectorPath.Count)
        {
            Debug.Log("End Of Path Reached");
            currentWaypoint++;
            if (lineOfSight.hasLineOfSight)
                nextState = MonsterStates.Preparation;
            else
                nextState = MonsterStates.Desync;
            return;
        }
        //Direction to the next waypoint
        velocity = (path.vectorPath[currentWaypoint] - transform.position);

        velocity.Normalize();
        velocity *= GameController.deltaTime * (speed + additionalPatrolSpeed) * (timeDistortion - magicSlowEffect);
        if (Vector2.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
            currentWaypoint++;
    }

    void Charge()
    {
        if (Time.time - lastRepath > repathRate && seeker.IsDone())
        {
            lastRepath = Time.time + repathRate;
            if (!fury.GetOnFury() || acquiredLineOfSightWhileInFury)
                seeker.StartPath(transform.position, player.position, OnPathComplete);
            else
                seeker.StartPath(transform.position, goalPosition, OnPathComplete);
        }
        if(lineOfSight.hasLineOfSight)
        {
            velocity = player.position - transform.position;
        }
        else
        {
            if (path == null || currentWaypoint >= path.vectorPath.Count)
            {
                velocity = player.position - transform.position;
            }
            else
            {
                //Direction to the next waypoint
                velocity = (path.vectorPath[currentWaypoint] - transform.position);
                if (Vector2.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
                    currentWaypoint++;
            }
        }       
        velocity.Normalize();
        velocity *= GameController.deltaTime * (chargeSpeed + additionalChargeSpeed) * (timeDistortion - magicSlowEffect) * (fury.GetOnFury() ? 1 + fury.furySpeed * fury.maxFuryPoints : 1);
    }

    void Move()
	{	
		if (Vector2.Distance (player.position, transform.position) < stopMovingIfCloserThenThisDistance)
			return;
		controller.Move(velocity * isPlayerPushing);
	}

	protected override void OnMonsterHit()
	{

		if(!(CheckIfCurrentOrNextState(MonsterStates.Preparation) || CheckIfCurrentOrNextState (MonsterStates.Attack)))
		{
			if(currentState == MonsterStates.Knockback && saveState == MonsterStates.Attack)
			{
				if(monsterToughness != toughness.one)
				{
					additionalChargeSpeed += lvl2SpeedIncrease;
				}
			}
			else
				{
				goalPosition = player.transform.position;
				seeker.StartPath (transform.position,goalPosition, OnPathComplete);
				if(monsterToughness != toughness.one)
				{
					oneSecond = 1.05f;
					getAFuryPoint++;
					additionalPatrolSpeed += lvl2SpeedIncrease;
				}
			}
		}
		else
		{
			if(monsterToughness != toughness.one)
			{
				additionalChargeSpeed += lvl2SpeedIncrease;
			}
		}
	
	}

	Vector3 temp;
	void CreateNewGoal()
	{
		temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
		                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
		                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
		             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,temp - upTemp);
		RaycastHit hit;
		int count = 0;
		if(Physics.SphereCast (ray,radiusCollider,out hit, 100)){			
			while(hit.collider.tag != Tags.Floor)
			{			
				count++;
				temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
				                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
				upTemp = temp + new Vector3(0,0,-10);
				ray = new Ray(upTemp,temp - upTemp);
				Debug.DrawRay(upTemp,temp - upTemp,Color.white,5);
				Physics.SphereCast (ray,radiusCollider,out hit, 100);
				if(count == 10)
				{
					goalPosition = temp;		
					lastRepath = Time.time+ repathRate;
					seeker.StartPath (transform.position,goalPosition, OnPathComplete);
					return;
				}
			}
		}
		goalPosition = temp;		
		lastRepath = Time.time+ repathRate;
		seeker.StartPath (transform.position,goalPosition, OnPathComplete);
	}

	public void OnPathComplete (Path p) {
		p.Claim (this);
		if (!p.error) {
			if (path != null) path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
        } else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
		}
	}

	void OnTriggerEnter(Collider other)
	{	
		switch(monsterToughness)
		{
		case toughness.one:	
		case toughness.two:
		case toughness.three:
		case toughness.four:
		case toughness.five:
			if(other.transform.tag == "PlayerHit")
			{
				playerHittedThisLoopControl = true;
				nextState = MonsterStates.Desync;
			}
			break;
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);		
	}

	void Roar()
	{
		gameController.MonsterAudioSource.PlayOneShot (roarSound);
		player.SendMessage ("Stunned",lvl3StunTime);
		GameObject.Find ("CameraControl").GetComponent<CameraFollow>().ShakeCamera (0.3f,1);
		playerHittedThisLoopControl = true;
	}

	public void PlayerHit()
	{
		goalPosition = player.transform.position;
		seeker.StartPath (transform.position,goalPosition, OnPathComplete);
		currentProjectileDuration = 0;
	}
}