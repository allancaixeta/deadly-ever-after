using UnityEngine;
using System.Collections;

public class Connected : MonsterGeneric {

	public GameObject brotherPrefab;
	bool bigBrother;
    SpriteRenderer hand1, hand2;
	float direction;
	public float timeToMoveinDefinedDirection = 1;
	float currentTimeToMoveinDefinedDirection;
	public bool randomTimeToMoveinDefinedDirection = false;
	public float randomModifier = 0.5f;
	bool brotherIsDead;
	public float timeToMournDeadBrother = 5;
	float currentTimeToMournDeadBrother;
	public float distanceToOriginalWhenBorn = 5;

	public float linkDuration = 1;
	public float deathLinkDuration = 3;
	float currentLinkDuration;
	public float linkCooldown = 3;
	float currentLinkCooldown;
	public float maxDistance = 20;
	GameObject link;
	float tempDistance;
	public int lightningSize = 1; //grossura do raio
	int lightningRealSize; //grossura real do raio
	public float lightningSpeed = 1f; // velocidade do ruido do raio
	public float amplitude = 1;
	Perlin noise;
	float oneOverLightningSize;
	int lightningLength;
	ParticleSystem.Particle[] particles;

	public GameObject changePlacesFX;
	GameObject []changeplaces;
	public float cooldownToChancePlaces;
	float currentCooldownToChancePlaces;
	Vector3 destination, origin;
	public float smoothChangePlacesMovement= 0.5f;
	public float changeplacesSize = 1;
	MonsterStates stateBeforeChangePlaces;
	public float lvl3deathLinkDuration = 1;
    bool lvl3LastChangePlaces = false;
	public int lvl2Armor = 5;
	int originalArmor;
	GameObject lvl4Laser;
    bool crossedPositions;
    public GameObject lvl4LaserPrefab, lvl5MinePrefab;
	bool laserIsOn, linkIsLaser;
	float lvl5LasertempAngle;
	Connected brother;
	KnockbackType savedKnockbackType;
	float originalScale;

	float angleBetweenBrothers, angleOfPlayer;
	public float angleDifferenceForAttack = 1;
    LerpEquationTypes lerpHandsColor = LerpEquationTypes.Quadratic;
	public override void SpecificMonsterAwake()
	{
		originalScale = transform.localScale.x;
		monsterType = MonsterGeneric.monsterTypes.connected;
		link = transform.FindChild("link").gameObject;
		link.SetActive (false);
		currentCooldownToChancePlaces = cooldownToChancePlaces * GetCooldownModifier ();
		invulnerable = true;        
    }
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState) // o unico monstro que pode ressuscitar
			return false;
		previousState = currentState;
		currentState = nextState;
		switch(currentState)
		{
		    case MonsterStates.Start:
			    bigBrother = true;
			    GameObject temp = Instantiate (this.gameObject,transform.position + new Vector3(Random.Range(-distanceToOriginalWhenBorn,distanceToOriginalWhenBorn),Random.Range(-distanceToOriginalWhenBorn,distanceToOriginalWhenBorn),0) ,Quaternion.identity) as GameObject;
			    brother = temp.GetComponent<Connected>();
			    brother.transform.localScale = new Vector3(originalScale,originalScale,1);
			    Destroy(brother.transform.FindChild("visual").gameObject);
			    brother.SendMessage (MnstMsgs.SetToughness,monsterToughness);
			    brother.SendMessage ("SetBrother",this);
			    brother.GetOutOfObject ();
			    brother.nextState = MonsterStates.Desync;
			    gameController.addMonster (temp);
                FindHands();
                brother.FindHands();
                noise = new Perlin();
			    currentLinkCooldown = linkCooldown;
			    currentLinkDuration = linkDuration;
			    tempDistance = Vector2.Distance (brother.transform.position, transform.position);
			    originalArmor = armor;
			    if(bigBrother)
			    {
				    RecreateLinkParticles();				
				    LinkRender();
			    }                
                nextState = MonsterStates.Desync;						
			    break;
			
		    case MonsterStates.Normal:	
			    break;	

		    case MonsterStates.Death:
			    if(!brother)
			    {
				    currentLife = maxLife;
				    nextState = MonsterStates.Start;
				    return true;
			    }
			    visual.transform.position = new Vector3(visual.transform.position.x,visual.transform.position.y,12);
			    if(brotherIsDead)
			    {
				    brother.BrotherDied();
				    deathCountDown = afterLife;
				    animator.SetBool ("dead",true);
				    link.SetActive (false);
				    if(lastState == MonsterStates.Attack && bigBrother)
				    {
					    DestroyChangePlacesFX();
				    }
			    }
			    else
			    {
				    brother.BrotherDied();
				    deathCountDown = afterLife;
				    animator.SetBool ("dead",true);
				    link.SetActive (false);
				    animator.SetBool ("changeplaces",false);
				    animator.SetBool ("laser",false);
				    laserIsOn = false;
				    currentCooldownToChancePlaces = cooldownToChancePlaces * GetCooldownModifier ();
				    gameObject.layer = originalLayer;
				    if(bigBrother)
				    {
					    currentLinkCooldown = 0;
					    switch(monsterToughness)
					    {
					    case toughness.one:
					    case toughness.two:
						    currentLinkDuration = deathLinkDuration;
						    break;
					    default:
						    currentLinkDuration = lvl3deathLinkDuration;
						    break;
					    }
					    if(lastState == MonsterStates.Attack)
					    {
						    DestroyChangePlacesFX();
					    }
				    }
			    }
			    break;

		    case MonsterStates.Knockback:
			    if(lastState == MonsterStates.Attack)
				    nextState = MonsterStates.Attack;
			    break;

		    case MonsterStates.Attack:
			    stateBeforeChangePlaces = lastState;
			    animator.SetBool ("changeplaces",true);
			    destination = brother.transform.position;
			    gameObject.layer = 16; //ghost layer
                Agressive();
			    if(bigBrother)
			    {
				    RecreateChangePlacesParticles();
				    if(monsterToughness == toughness.four || monsterToughness == toughness.five)
				    {					   	
					    lvl5LasertempAngle = Mathf.Atan2 (brother.transform.position.y - transform.position.y, brother.transform.position.x - transform.position.x) * Mathf.Rad2Deg;                    
                    }
			    }
			    savedKnockbackType = knockbackType;
			    knockbackType = KnockbackType.hulk;
                link.SetActive(false);
                break;

		    case MonsterStates.Desync:
			    invulnerable = false;
			    animator.speed = 1;
			    animator.SetTrigger ("idle");
			    GetOutOfObject();
                if (lastState == MonsterStates.Start)
                {
                    currentDesync = Random.Range(1, GameController.InitialMaxDesyncTime);
                }
                else
                    currentDesync = Random.Range(desyncTimeMin, desyncTimer) * GetCooldownModifier();
			    FixZ();
			    break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter                        
        switch (currentState)
		{
		    case MonsterStates.VOID:
			    nextState = MonsterStates.Start;
			    break;

		    case MonsterStates.Start:
			    nextState = MonsterStates.Normal;						
			    break;
	
		    case MonsterStates.Normal:
                Color tmp = hand1.color;
                tmp.a = lerpHandsColor.Lerp(0, 1, 1 - currentCooldownToChancePlaces / (cooldownToChancePlaces * GetCooldownModifier()));
                hand1.color = tmp;
                hand2.color = tmp;
                LifeRegen();
                CheckPlayerBetween();
                switch (monsterToughness)
			    {
			        case toughness.one:
                        if (brotherIsDead)
                        {
                            MournBrother();
                        }
                        else
                        {
                            Lvl1();
                            currentCooldownToChancePlaces -= GameController.deltaTime;
                            if (bigBrother && currentCooldownToChancePlaces < 0 && playerBetween)
                            {
                                nextState = MonsterStates.Attack;
                                brother.nextState = MonsterStates.Attack;
                            }
                        }
				        break;
			        case toughness.two:
                    case toughness.three:
                    case toughness.four:
                    case toughness.five:
                        if (brotherIsDead)
				        {
					        MournBrother();
				        }
				        Lvl1();
                        currentCooldownToChancePlaces -= GameController.deltaTime;
                        if (bigBrother && currentCooldownToChancePlaces < 0 && playerBetween)
                        {
                            nextState = MonsterStates.Attack;
                            brother.nextState = MonsterStates.Attack;
                        }
                        break;
			    }
			    break;

		    case MonsterStates.Preparation:
			    break;

		    case MonsterStates.Attack:
			    ChangePlaces ();
			    break;

		    case MonsterStates.Hook:
			    controller.Move(velocity);					
			    break;
			
		    case MonsterStates.Death:
			    if(brotherIsDead)
			    {
                    switch (monsterToughness)
                    {
                        default:
                        case toughness.two:
                            Death();
                            break;
                        case toughness.three:
                        case toughness.four:
                        case toughness.five:
                            if (lvl3LastChangePlaces)
                            {
                                Death();                               
                            }
                            else
                            {
                                currentCooldownToChancePlaces -= GameController.deltaTime;
                                if (currentCooldownToChancePlaces < 0)
                                {
                                    nextState = MonsterStates.Attack;
                                    knockbackType = KnockbackType.hulk;
                                    brother.nextState = MonsterStates.Attack;
                                    lvl3LastChangePlaces = true;
                                }
                            }                           
                            break;
                    }
                    
				    //brother.DestroyItSelf();
			    }
			    else
			    {				    
				    if(bigBrother)
				    {
					    CheckForLink ();
                        CheckPlayerBetween();
                        switch (monsterToughness)
					    {
					        default:
                                break;
                            case toughness.two:											        
					        case toughness.three:
					        case toughness.four:
					        case toughness.five:
                                currentCooldownToChancePlaces -= GameController.deltaTime;
                                if (currentCooldownToChancePlaces < 0 && playerBetween)
                                {
                                    nextState = MonsterStates.Attack;
                                    brother.nextState = MonsterStates.Attack;
                                }
						    break;
					    }
				    }
			    }
			    break;
			
		    case MonsterStates.Stun:
			    Stun();
			    break;
			
		    case MonsterStates.Knockback:
			    Knock();
			    break;
			
		    case MonsterStates.Desync:
			    currentDesync -= GameController.deltaTime;
			    if(currentDesync < 0)
				    nextState = MonsterStates.Normal;
                if (bigBrother)
                {
                    CheckPlayerBetween();
                    if (currentCooldownToChancePlaces < 0 && playerBetween)
                    {
                        nextState = MonsterStates.Attack;
                        brother.nextState = MonsterStates.Attack;
                    }
                }
                
                break;			
		}
	}
    bool playerBetween;
	void CheckPlayerBetween()
	{								
		angleBetweenBrothers = Mathf.Atan2 (brother.transform.position.y - transform.position.y, brother.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		angleOfPlayer = Mathf.Atan2 (player.position.y - transform.position.y, player.position.x - transform.position.x) * Mathf.Rad2Deg;
        if (Vector2.Distance(transform.position, player.position) < Vector2.Distance(brother.transform.position, transform.position) && Mathf.Abs(angleBetweenBrothers - angleOfPlayer) < angleDifferenceForAttack)            
            playerBetween = true;            
        else
            playerBetween = false;
	}

	void MournBrother()
	{
		currentTimeToMournDeadBrother -= GameController.deltaTime;
		if(currentTimeToMournDeadBrother < 0)
		{
			RessurectBrother();
		}
		if(monsterToughness == toughness.one && bigBrother)
			CheckForLink ();
	}

	void Lvl1()
	{
		currentTimeToMoveinDefinedDirection -= GameController.deltaTime;
		if(currentTimeToMoveinDefinedDirection < 0)
		{
			ChangeDirection ();
			if((!bigBrother || currentLinkCooldown > 0))
			{
				nextState = MonsterStates.Desync;	
				return;
			}
		}
		Velocity ();
		Move ();
		if(bigBrother && (currentState == MonsterStates.Normal || brotherIsDead))
		{
			CheckForLink();
		}
	}

	void ChangePlaces()
	{
		VelocityChangePlaces();
		Move ();
		if(bigBrother && (monsterToughness == toughness.five || monsterToughness == toughness.four))
		{
			if(!laserIsOn)
			{
                if (Vector2.Distance(transform.position, brother.transform.position) < 0.5f)
                {
                    laserIsOn = true;
                    animator.SetBool("laser", true);
                    brother.SetLaserAnimation();
                    lvl4Laser = Instantiate(lvl4LaserPrefab, new Vector3(this.transform.position.x - (this.transform.position.x - brother.transform.position.x) / 2,
                                                                        this.transform.position.y - (this.transform.position.y - brother.transform.position.y) / 2,
                                                                        1.5f), Quaternion.Euler(0, 0, lvl5LasertempAngle)) as GameObject;
                    lvl4Laser.SendMessage("SetPassThrough");
                    lvl4Laser.transform.rotation = Quaternion.Euler(0, 0, lvl5LasertempAngle);
                    linkIsLaser = true;
                    link.SetActive(true);
                    RecreateLinkParticles();
                    LinkRender();
                }
			}
			if(laserIsOn)
			{
				LinkRender();
				lvl4Laser.transform.localScale = new Vector3(Vector2.Distance(transform.position, brother.transform.position),lvl4Laser.transform.localScale.y,
				                                             lvl4Laser.transform.localScale.z);
			}
		}
		if(Vector2.Distance (transform.position,destination ) < stopMovingIfCloserThenThisDistance)
		{
			transform.position = destination;
			StopChangincPlaces ();
			knockbackType = savedKnockbackType;
			switch(monsterToughness)
			{
			    case toughness.one:			    		
				    nextState= MonsterStates.Desync;
				    break;
                case toughness.two:
                case toughness.three:
			    case toughness.four:
			    case toughness.five:
				    if(stateBeforeChangePlaces == MonsterStates.Death)
				    {
					    currentState = MonsterStates.Death;
					    nextState = MonsterStates.Death;
                        if(lvl3LastChangePlaces)
                            brother.SetLastChangePlaces();
                    }
				    else
					    nextState= MonsterStates.Desync;
				    break;
			}
			gameObject.layer = MonsterGeneric.nonHarmfulLayer;
            Passive();
            return;
		}
	}		
			
	void CheckForLink()
	{
		currentLinkCooldown -= GameController.deltaTime;
		if(currentLinkCooldown < 0)
		{
			link.SetActive (true);
			LinkRender();
			currentLinkDuration -= GameController.deltaTime;
			if(currentLinkDuration < 0)
			{
				currentLinkCooldown = linkCooldown;
				currentLinkDuration = linkDuration;
				link.SetActive (false);
			}
		}
	}

	void Velocity()
	{		
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}

	void VelocityChangePlaces()
	{
		velocity = destination - transform.position;		
		velocity.Normalize();
		velocity *= smoothChangePlacesMovement * GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{		
		controller.Move(velocity * isPlayerPushing);
	}

	void RecreateLinkParticles()
	{
		tempDistance = Vector2.Distance (brother.transform.position, transform.position);
		lightningRealSize = (int)tempDistance * 4 * lightningSize;
		oneOverLightningSize = 1f / (float)lightningRealSize;		
		particles = new ParticleSystem.Particle[lightningRealSize];
		link.GetComponent<ParticleSystem>().enableEmission = false;	
		link.GetComponent<ParticleSystem>().Emit(lightningRealSize);
		lightningLength = link.GetComponent<ParticleSystem>().GetParticles(particles);
	}

	void RecreateChangePlacesParticles()
	{
		origin = transform.position;
		tempDistance = Vector2.Distance (destination, transform.position);
		int size = (int)(tempDistance * changeplacesSize);
		changeplaces = new GameObject[size];
		float oneOverSize = 1f / (float)size;	
		for (int i=0; i < size; i++)
		{
			Vector3 position;
			position = Vector3.Lerp(origin, destination, oneOverSize * (float)i);								
			changeplaces[i] = Instantiate (changePlacesFX,position,Quaternion.identity) as GameObject;
		}
	}

	void LinkRender()
	{
		if(Mathf.Abs(tempDistance-Vector2.Distance (brother.transform.position, transform.position)) > 1)
		{
			RecreateLinkParticles();
		}
		float timex = Time.time * lightningSpeed * 0.1365143f;
		float timey = Time.time * lightningSpeed * 1.21688f;
		float timez = Time.time * lightningSpeed * 2.5564f;
		for (int i=0; i < lightningLength; i++)
		{
			Vector3 position;
			position = Vector3.Lerp(this.transform.position, brother.transform.position, oneOverLightningSize * (float)i);			
			Vector3 offset = new Vector3(noise.Noise(timex + position.x, timex + position.y, timex + position.z),
			                             noise.Noise(timey + position.x, timey + position.y, timey + position.z),
			                             noise.Noise(timez + position.x, timez + position.y, timez + position.z));
			position += (offset * ((float)i * oneOverLightningSize)) * (Random.value + amplitude);						
			particles[i].position = position;
			if(linkIsLaser)
				particles[i].color = Color.red;
			else
				particles[i].color = Color.white;
			particles[i].remainingLifetime = 1;
		}
		link.GetComponent<ParticleSystem>().SetParticles(particles,lightningLength);		
	}


	public void SetBrother(Connected b)
	{
		brother = b;
	}

	public void BrotherDied()
	{
		brotherIsDead = true;
		if(CheckIfCurrentOrNextState (MonsterStates.Death))
			return;
		link.SetActive (false);
		currentLinkCooldown = 0;
		if(CheckIfCurrentOrNextState(MonsterStates.Attack))
		{
			StopChangincPlaces();
			if(stateBeforeChangePlaces == MonsterStates.Death)
			{
				currentState = MonsterStates.Death;
				nextState = MonsterStates.Death;
				deathCountDown = afterLife;         
				return;
			}
		}
		switch(monsterToughness)
		{
		case toughness.one:
		case toughness.two:
			currentLinkDuration = deathLinkDuration;
			break;
		default:
			currentLinkDuration = lvl3deathLinkDuration;
			break;
		}
		currentTimeToMournDeadBrother = timeToMournDeadBrother;
		switch(monsterToughness)
		{
		case toughness.one:
			animator.SetBool ("paralysed",true);
			nextState= MonsterStates.Normal;
			return;
		case toughness.two:
		case toughness.three:
		case toughness.four:
		case toughness.five:
			armor += originalArmor + lvl2Armor;
			UpdateArmorInfinite ();
			nextState= MonsterStates.Desync;
			break;
		}
	}

	public void RessurectBrother()
	{
		brother.Ressurect ();
		brotherIsDead = false;
		animator.SetBool ("paralysed",false);
		switch(monsterToughness)
		{
		default:
			return;
		case toughness.two:
		case toughness.three:
		case toughness.four:
		case toughness.five:
			armor = originalArmor;
			UpdateArmorInfinite ();
			break;
		}
	}

	public void Ressurect()
	{
		gameObject.layer = originalLayer;
		animator.SetBool ("dead",false);
		visual.transform.position = new Vector3(visual.transform.position.x,visual.transform.position.y,0);
		nextState = MonsterStates.Desync;
		int aux = lifeRegen;
		lifeRegen = maxLife;
		LifeRegen ();
		lifeRegen = aux;
		currentLife = maxLife;
		if(monsterToughness == toughness.five)
		{
			GameObject mine = Instantiate (lvl5MinePrefab,transform.position,Quaternion.identity) as GameObject;
			mine.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
			mine.SendMessage ("SetdontUsePositiveEffect");
			gameController.addEvent (mine);
		}
	}

	void DestroyChangePlacesFX()
	{
		if((monsterToughness == toughness.four || monsterToughness == toughness.five ) && linkIsLaser)
		{
            laserIsOn = false;
            lvl4Laser.GetComponent<MonsterGeneric>().DestroyItSelf();
			linkIsLaser = false;
			link.SetActive (false);
		}
		for (int i = 0; i < changeplaces.Length; i++) {
			Destroy(changeplaces[i]);
		}
	}

	public GameObject[] GetChangePlacesFX()
	{
		return changeplaces;
	}

	public bool BrothersNotLinkingNow()
	{
		return bigBrother ? (currentLinkDuration == linkDuration) :	(brother.GetcurrentLinkOff());
	}

	public bool BrothersNotOnDesyncNow()
	{
		return !brother.CheckIfCurrentOrNextState (MonsterStates.Desync);
	}

	public float GetcurrentCooldownToChancePlaces()
	{
		return currentCooldownToChancePlaces;
	}

	public void SetcurrentCooldownToChancePlaces(float cooldowntochanceplaces)
	{
		currentCooldownToChancePlaces = cooldowntochanceplaces * GetCooldownModifier();
    }

    public void SetLastChangePlaces()
    {
        lvl3LastChangePlaces = true;
    }

    public bool GetcurrentLinkOff()
	{
		return currentLinkDuration == linkDuration;
	}

	void StopChangincPlaces()
	{
		animator.SetBool ("changeplaces",false);
		animator.SetBool ("laser",false);
		laserIsOn = false;
		if(bigBrother)
			DestroyChangePlacesFX();
		currentCooldownToChancePlaces = cooldownToChancePlaces * GetCooldownModifier ();
		gameObject.layer = originalLayer;
	}

	void SetLaserAnimation()
	{
		animator.SetBool ("laser", true);
	}

	void ChangeDirection()
	{
		if(randomTimeToMoveinDefinedDirection)
			currentTimeToMoveinDefinedDirection = Random.Range (timeToMoveinDefinedDirection - timeToMoveinDefinedDirection / randomModifier,
			                                                    timeToMoveinDefinedDirection + timeToMoveinDefinedDirection * randomModifier);
		else
			currentTimeToMoveinDefinedDirection = timeToMoveinDefinedDirection;
		if(Vector2.Distance(transform.position,brother.transform.position) > maxDistance)
		{
			float angle = Mathf.Atan2 (brother.transform.position.y - transform.position.y, brother.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
			direction = Random.Range (angle-10,angle+10);
		}
		else
			direction = Random.Range (0,360);
	}

	protected override void OnMonsterHit()
	{
		switch(monsterToughness)
		{
		case toughness.one:
		case toughness.two:
			break;
		case toughness.three:
		case toughness.four:
		case toughness.five:
			if(brotherIsDead && !CheckIfCurrentOrNextState (MonsterStates.Death) && !CheckIfCurrentOrNextState(MonsterStates.Attack))
			{				
                nextState = MonsterStates.Attack;
                brother.nextState = MonsterStates.Attack;
            }
			break;
		}
	}

	public bool IsBrotherDead()
	{
		return brotherIsDead;
	}

    public void FindHands()
    {
        hand1 = visual.transform.Find("base/hand1").GetComponent<SpriteRenderer>();
        hand2 = visual.transform.Find("base/hand2").GetComponent<SpriteRenderer>();
    }
}