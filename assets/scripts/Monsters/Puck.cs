using UnityEngine;
using System.Collections;

public class Puck : MonsterGeneric {
	
	public float speedVertical = 10;
	public float speedHorizontal = 1;
	public float horizontalRange = 0.1f;
	public float puckSpeed = 0.01f;
	public float growthSize = 4;
	public float growthRate = 0.01f;
	float direction = 1;
	Transform cenarioLimits;
	float angle;
	int goingright = 1;
	bool changeDirection = false;
	public Vector3 spwPoint = new Vector3(16,51,0);
	SphereCollider body;
	Transform spwnArea,goalPosition;
	bool targetingPlayer;
	public float lvl4SpeedAmplifier = 3;
	Fury fury;
	float defaultSpeed;
	bool growing = false;
	Vector3 originalScale;
	float currentGrowth;
	enum movingstates
	{
		up,
		down
	}
	movingstates move = movingstates.up;
	public override void SpecificMonsterAwake()
	{
		monsterType = monsterTypes.puck;
		invulnerable = true;
		goalPosition = new GameObject().transform;
		monsterType = MonsterGeneric.monsterTypes.puck;
		body = GetComponent<SphereCollider>();
		spwnArea = transform.FindChild ("spwnArea");
		body.center = new Vector3(Random.Range(-spwnArea.lossyScale.x/2,spwnArea.lossyScale.x/2),
		                          Random.Range(-spwnArea.lossyScale.y/2,spwnArea.lossyScale.y/2),
		                          body.center.z);
		ChangeGoal ();
		fury = GetComponent<Fury>();
		defaultSpeed = speed;
		/*For monster projectile
		GetComponent<CharacterController>().center = new Vector3(0,5,0);
		GetComponent<SphereCollider>().center = new Vector3(0,3,0);
		*/
	}

	void ChangeGoal()
	{
		goalPosition.position = new Vector3(Random.Range(-spwnArea.lossyScale.x/2,spwnArea.lossyScale.x/2),
		                                    Random.Range(-spwnArea.lossyScale.y/2,spwnArea.lossyScale.y/2),
		                                    body.center.z);
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
			cenarioLimits = GameObject.Find("CenarioLimits").transform;
			visual.SendMessage(MnstMsgs.SetToughness,monsterToughness);
			visual.SendMessage ("SetPuckPai",this.gameObject);
			switch(monsterToughness)
			{
			case toughness.four:
			case toughness.five: 
				visual.SendMessage ("SetDamage",monsterDamage);
				break;
			}
			originalScale = visual.transform.localScale;
			nextState = MonsterStates.Desync;	
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		if(magicallySlowed)
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}		
		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			switch(monsterToughness)
			{
			case toughness.one:
				CloudMovement();
				Velocity ();
				Move();		
				break;
			case toughness.two:
			case toughness.three:
				CloudMovement();
				Velocity ();
				Move();
				MoveBody();
				if(Vector2.Distance (goalPosition.position,body.center) < 1)
				{
					ChangeGoal ();
				}
				break;			
			case toughness.four:
				CloudMovement();
				Velocity ();
				Move();	
				break;
			case toughness.five:
				CloudMovement();
				Velocity ();
				Move();
				if(fury.GetOnFury())
				{
					speed = defaultSpeed + fury.furySpeed * fury.maxFuryPoints * defaultSpeed;
					if(currentGrowth == 0)
						growing = true;
					if(growing)
					{
						currentGrowth += growthRate;
						if(currentGrowth > 1)
							growing = false;
						visual.transform.localScale = Vector3.Lerp(originalScale,originalScale*growthSize,currentGrowth);
					}
				}
				else
				{
				 	fury.CalculateFury();
					speed = defaultSpeed;
					if(growing)
					{
						currentGrowth -= growthRate*5;
						if(currentGrowth <= 0)
						{
							growing = false;
							currentGrowth = 0;
						}
						visual.transform.localScale = Vector3.Lerp(originalScale,originalScale*growthSize,currentGrowth);
					}
					else
						if(visual.transform.localScale != originalScale)
						{
							growing = true;
						}
				}
				break;
			}
			break;
		
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			DestroyItSelf();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			knockbackCountDown -= GameController.deltaTime;
			Move();
			if(knockbackCountDown < 0)
			{
				nextState = MonsterStates.Normal;
			}
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}

	bool returningToSpawnPos = false;
	void Velocity()
	{
		if(returningToSpawnPos)
		{
			velocity = cenarioLimits.position - transform.position;
			
			velocity.Normalize();
			velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			if(Vector2.Distance (cenarioLimits.position,transform.position) < 50)
				returningToSpawnPos = false;
		}
		else
		{
			velocity = new Vector3(speedHorizontal*direction*goingright,Mathf.Sin(Mathf.Deg2Rad * angle * goingright)*speedVertical*direction,0);
				
			velocity.Normalize();
			velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			if(Vector2.Distance (cenarioLimits.position,transform.position) > 100)
				returningToSpawnPos = true;
		}
	}
	
	void Move()
	{		
		controller.Move(velocity * isPlayerPushing);
	}

	void CloudMovement()
	{
		switch(move)
		{
		case movingstates.up:
			angle += horizontalRange;
			if(angle > 360)
			{
				move = movingstates.down;
				angle = 0;
				direction = direction == 1 ? -1 : 1;
				speedHorizontal /= 2;
				if(changeDirection)
				{
					goingright = goingright == 1 ? -1 : 1;
					changeDirection = false;
				}
			}
			break;
		case movingstates.down:
			angle += horizontalRange;
			if(angle > 360)
			{
				move = movingstates.up;
				speedHorizontal *= 2;
				angle = 0;
				direction = direction == 1 ? -1 : 1;
				if(changeDirection)
				{
					goingright = goingright == 1 ? -1 : 1;
					changeDirection = false;
				}
			}
			break;
		}
		if(transform.position.x < cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 && goingright == -1)
			changeDirection = true;
		if(transform.position.x > cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 && goingright == 1)
			changeDirection = true;	
	}

	void MoveBody()
	{
		body.center = Vector3.Lerp(body.center,goalPosition.position,puckSpeed);
	}

	void OnDestroy () {
		if(goalPosition)
			Destroy(goalPosition.gameObject);
	}

	public void PlayerTargeted()
	{
		fury.SetOnFury (false);
	}

	protected override void OnMonsterHit()
	{
	}
}
