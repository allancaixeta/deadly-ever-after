using UnityEngine;
using System.Collections;

public class Esqueleto : MonsterGeneric {

	public override void SpecificMonsterAwake()
	{
		//Criar um novo valor para um novo monstro, e setar aqui.
		monsterType = MonsterGeneric.monsterTypes.spintop;
	}
	
	public override bool StartState()
	{
		//verifica se eh um estado novo, e tem uma verificacao para nao trocar do estado death para outro (sem ressuscitar!)
		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
				
		if(currentLife < 0)
			nextState = MonsterStates.Death;
		//efetiva a troca de estado
		currentState = nextState;
		
		switch(currentState)
		{
		//estado inicial, somente roda uma vez por monstro
		case MonsterStates.Start:
			GetOutOfObject();
			nextState = MonsterStates.Desync;						
			break;
			
		//usavel para caso queira "desativar" o monstro por um tempo por algum motivo	
		case MonsterStates.Inactive:			
			break;	
		
		//esse estado eh usado para dessincronizar os monstros, para q eles nao façam as acoes ao 
		//mesmo tempo q seus companheiros (por ex. para q monstros nao atirem todos ao mesmo tempo
		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			GetOutOfObject();
			break;
	
		//vai para esse estado quando o monstro morre, e ativa a animacao de morte
		case MonsterStates.Death:
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		//estado nulo inicial, sempre leva para o estado start
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;
			
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;

		//esse eh o estado em q mais acontece o acrescimo de codigo
		case MonsterStates.Normal:
			//if(Random.value <= desyncChance)
			//	nextState = monsterStates.desync;
			LifeRegen();//funcao q verifica se o monstro regenera vida, e ja regenera a vida. 

			//Esse switch divide a logica do monstro em niveis.
			//NOVO CODIGO GERALMENTE EH ADICIONADO SOMENTE AQUI
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				break;
			}
			break;
		
		//estado usado para quando o monstro eh fiscado por uma mecanica q puxa ele
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		//estado de morte
		case MonsterStates.Death:
			Death();	
			break;
				
		//estado quando ele esta stunado		
		case MonsterStates.Stun:
			Stun();
			break;
		
		//estado para quando o monstro eh acertado por uma arma q empurra ele
		case MonsterStates.Knockback:
			Knock();
			break;
			
		//estado de desincronizacao
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}
	
	//funcao usada para calcular a direcao e velocidade que o monstro vai andar. Nesse caso ela soh faz o monstro andar para cima do jogador.
	void Velocity()
	{
		velocity = player.position - transform.position;
			
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	//Funcao chamada quando se quer efetivamente mover o monstro (apos calcular o vetor de velocidade)
	void Move()
	{		
		controller.Move(velocity * isPlayerPushing);
	}

	//Funcao chamada quando o monstro eh acertado por uma arma. Essa funcao somente eh usada por monstros q reagem de alguma forma quando sao acertados.
	protected override void OnMonsterHit()
	{
	}
}
