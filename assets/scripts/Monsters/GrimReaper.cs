using UnityEngine;
using System.Collections;

public class GrimReaper : MonsterGeneric {
	public GameObject grimreaperChain;
	public Transform scytheOrigin;
	public float timeToMoveInDirection = 3;
	public float distanceFromPlayerWalkModifier = 0.1f;
	public float delayBeforeDeathRage = 2;
	public float deathRage = 5;
	public float deathRageHitCooldown = 1;
	public float possessionUpgrade = 0.25f;
	public float personalDeathClockTimer = 10;
	float currentDeathRage;
	float currentTimeToMoveInDirection;
	Vector3 direction;
	directions auxDirection;
	KnockbackType originalKnockback;
	public GameObject scythe;
	DefaultProjectile currentScythe;
	public float scytheDuration = 1;
	float currentScytheDuration;
	public float minDesyncTime = 3;
	float initialAngle, finalAngle;
	public GameObject soul, personalDeathClock, darkness, lightGR;
	GameObject currentSoul, currentDeathClock, lightOnPlayer;
	bool activeDeathClock;
    public LerpEquationTypes scytheLerp;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.grimreaper;
		originalKnockback = knockbackType;
		darkness = Instantiate (darkness,player.position,Quaternion.identity) as GameObject;
		darkness.transform.parent = player;
		darkness.transform.localPosition = new Vector3 (0,0,50);
		darkness.SetActive (false);
		lightOnPlayer = Instantiate (lightGR,player.position,lightGR.transform.rotation) as GameObject;
		lightOnPlayer.transform.parent = player;
		lightOnPlayer.transform.localPosition = new Vector3 (0,3,40);
		lightOnPlayer.SetActive (false);
		lightGR= Instantiate (lightGR,player.position,lightGR.transform.rotation) as GameObject;
		lightGR.transform.parent = transform;
		lightGR.transform.localPosition = new Vector3 (0,3,40);
		lightGR.SetActive (false);
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			GetOutOfObject();
			nextState = MonsterStates.Desync;	
			currentScythe = (Instantiate(scythe,scytheOrigin.position,Quaternion.identity) as GameObject).GetComponent<DefaultProjectile>();
			currentScythe.SendMessage(MnstMsgs.SetToughness,monsterToughness);
			currentScythe.SendMessage("SetPassThrough");
			currentScythe.SendMessage("SetSendMessageToParentWhenHittingPlayer");
            currentScythe.SendMessage("SetInvulnerability",true);
            currentScythe.SendMessage("SetInvulnerability",true);
            currentScythe.SendMessage("SetMonsterType", monsterTypes.grimreaperScythe);
            currentScythe.transform.parent = transform;
            currentScythe.transform.localScale = Vector3.one;
            currentScythe.gameObject.SetActive (false);
			break;
			
		case MonsterStates.Inactive:			
			break;	

		case MonsterStates.Normal:	
			int tempDirection;
			switch(monsterToughness)
			{
			default:
			case toughness.one:
			case toughness.two:			
				tempDirection = GetPlayer8Direction ();
				break;
			case toughness.three:
			case toughness.four:			
			case toughness.five:	
				tempDirection = GetPlayer8Direction ();
				InstantiateChain();
				break;
			}
			SetDirection (tempDirection);
			currentTimeToMoveInDirection = timeToMoveInDirection + Vector2.Distance (transform.position,player.position) * distanceFromPlayerWalkModifier;
			currentScytheDuration = scytheDuration;
			break;	

		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			GetOutOfObject();
			break;

		case MonsterStates.Death:
			OnMonsterHit ();
			Destroy (darkness);
			Destroy (GetComponent<Light>());
			Destroy (lightOnPlayer);
			deathCountDown = afterLife;
			currentDeathRage = deathRage;
			attackCountDown = deathRageHitCooldown * GetCooldownModifier ();
			currentScytheDuration = scytheDuration;
			playerHitThisCycle = false;
			switch(monsterToughness)
			{
			default:
			case toughness.one:			
				animator.SetTrigger ("die");
				break;
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				gameObject.layer = 16;                        
				CapsuleCollider s = GetComponent<CapsuleCollider>();
				s.enabled = false;
				animator.SetTrigger ("ghost");
				break;
			}
			break;
		}
		return true;
	}

	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			LifeRegen();
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				currentTimeToMoveInDirection -= GameController.deltaTime;
				if(currentTimeToMoveInDirection < 0)
				{
					if(currentScytheDuration == scytheDuration)
					{
						currentScythe.transform.position = scytheOrigin.position;
						currentScythe.gameObject.SetActive (true);
						int tempDirection = GetPlayer8Direction ();
						SetDirection (tempDirection);
						StartScytheAngle();
						knockbackType = KnockbackType.hulk;
					}
					currentScytheDuration -= GameController.deltaTime * (timeDistortion-magicSlowEffect);
					currentScythe.transform.localRotation = Quaternion.Euler (0,0, scytheLerp.Lerp(finalAngle,initialAngle,currentScytheDuration/scytheDuration));
					//currentScythe.transform.Rotate(Vector3.forward);
					if(currentScytheDuration < 0)
					{
						currentScythe.gameObject.SetActive (false);
						nextState = MonsterStates.Desync;
						knockbackType = originalKnockback;
					}
				}
				else
				{
					Velocity ();
					Move ();
				}
				break;
			}
			break;
			
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
					
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;

		case MonsterStates.Death:
			switch(monsterToughness)
			{
			default:
			case toughness.one:			
				Death ();
				break;
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				delayBeforeDeathRage -= GameController.deltaTime;
				if(delayBeforeDeathRage < 0)
				{
					currentDeathRage -= GameController.deltaTime;
					if(currentDeathRage < 0)
					{
						if((monsterToughness == toughness.four || monsterToughness == toughness.five) && !playerHitThisCycle && !currentSoul && GameController.Instance.CountNumberOfMonstersInTheRoom() > 1)
						{
							currentSoul = Instantiate(soul,transform.position,soul.transform.rotation) as GameObject;
							currentSoul.GetComponent<PossessiveSoul>().SetScaler (possessionUpgrade,this.name);
						}
						animator.SetTrigger ("die");
						Death ();	
					}
					else
					{
						attackCountDown -= GameController.deltaTime;
						if(attackCountDown < 0)
						{
							if(currentScytheDuration == scytheDuration)
							{
								currentScythe.transform.position = scytheOrigin.position;
								currentScythe.gameObject.SetActive (true);
								int tempDirection = GetPlayer8Direction ();
								SetDirection (tempDirection);
								StartScytheAngle();
							}
							currentScytheDuration -= GameController.deltaTime * (timeDistortion-magicSlowEffect);
							currentScythe.transform.localRotation = Quaternion.Euler (0,0, scytheLerp.Lerp(finalAngle,initialAngle,currentScytheDuration/scytheDuration));
							if(currentScytheDuration < 0)
							{
								currentScythe.gameObject.SetActive (false);
								attackCountDown = deathRageHitCooldown * GetCooldownModifier ();
								currentScytheDuration = scytheDuration;
								int tempDirection;
								tempDirection = GetPlayer8Direction ();
								SetDirection (tempDirection);
							}
						}
						else
						{
							Velocity ();
							Move ();
						}
						
					}
				}
				else
				{
					if(delayBeforeDeathRage < 0.2f)
					{
						int tempDirection;
						tempDirection = GetPlayer8Direction ();
						SetDirection (tempDirection);
					}
				}
				break;
			}
			break;
		}
	}

	void StartScytheAngle()
	{
		//currentScythe.transform.Translate(direction * 2,Space.World);
		switch(auxDirection)
		{
		default:
		    case directions.R:
                initialAngle = 270;
                finalAngle = 90;
                animator.SetTrigger ("R");
			    break;
		    case directions.L:
                initialAngle = -90;
                finalAngle = 90;
                animator.SetTrigger ("L");
			    break;
		    case directions.U:
                initialAngle = 220;
                finalAngle = 340;
                animator.SetTrigger ("U");
			    break;
		    case directions.D:
                initialAngle = 180;
                finalAngle = 0;
                animator.SetTrigger ("D");
			    break;
		    case directions.UR:
                initialAngle = -45;
                finalAngle = -225;
                animator.SetTrigger ("U");
			    animator.SetTrigger ("R");
			    break;
		    case directions.UL:
                initialAngle = -135;
                finalAngle = 45;
                animator.SetTrigger ("U");
			    animator.SetTrigger ("L");
			    break;
		    case directions.DR:
                initialAngle = 225;
                finalAngle = 45;
                animator.SetTrigger ("D");
			    animator.SetTrigger ("R");
			    break;
		    case directions.DL:
                initialAngle = -45;
                finalAngle = 135;
                animator.SetTrigger ("D");
			    animator.SetTrigger ("L");
			    break;
		}	
		currentScythe.transform.localRotation = Quaternion.Euler(0,0,initialAngle);
	}

	int GetPlayer8Direction()
	{
		float tempAngleCalc = Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		int tempDirection = -1;
		if(tempAngleCalc > -22.5f && tempAngleCalc < 22.5f)
			tempDirection = 3;
		if(tempAngleCalc > 22.5f && tempAngleCalc < 67.5f)
			tempDirection = 4;
		if(tempAngleCalc > 67.5f && tempAngleCalc < 112.5f)
			tempDirection = 0;
		if(tempAngleCalc > 112.5f && tempAngleCalc < 157.5f)
			tempDirection = 6;
		if(tempAngleCalc > -157.5f && tempAngleCalc < -112.5f)
			tempDirection = 7;
		if(tempAngleCalc < -67.5f && tempAngleCalc > -112.5f)
			tempDirection = 1;
		if(tempAngleCalc < -22.5f && tempAngleCalc > -67.5f)
			tempDirection = 5;		
		if(tempDirection == -1)
			tempDirection = 2;
		return tempDirection;
	}
	
	void SetDirection(int tempDirection)
	{
		switch(tempDirection)
		{
		case 0:
			direction = Vector3.right;
			auxDirection = directions.R;
			break;
		case 1:
			direction = Vector3.left;
			auxDirection = directions.L;
			break;
		case 2:
			direction = Vector3.up;
			auxDirection = directions.U;
			break;
		case 3:
			direction = Vector3.down;
			auxDirection = directions.D;
			break;
		case 4:
			direction = Vector3.down + Vector3.right;
			auxDirection = directions.DR;
			break;
		case 5:
			direction = Vector3.down + Vector3.left;
			auxDirection = directions.DL;
			break;
		case 6:
			direction = Vector3.up + Vector3.right;
			auxDirection = directions.UR;
			break;
		case 7:
			direction = Vector3.up + Vector3.left;
			auxDirection = directions.UL;
			break;
		}
		direction.Normalize ();
	}

	void InstantiateChain()
	{
		float angle = Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		GameObject temp = Instantiate(grimreaperChain,transform.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);		
		temp.SendMessage ("SetDirection",angle);
		gameController.addMonster (temp);		
	}

	void Velocity()
	{
		velocity.x = -direction.y;
		velocity.y = direction.x;
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{		
		controller.Move(velocity * isPlayerPushing);
	}
	bool playerHitThisCycle;
	public void PlayerHit()
	{
		playerHitThisCycle = true;
		currentDeathRage = currentScytheDuration;
		if(monsterToughness == toughness.five && CheckIfCurrentOrNextState (MonsterStates.Normal))
		{
			GameObject CheckifPlayerHasPersonalDeathClock = GameObject.Find("PersonalDeathClock(Clone)");
			if(!CheckifPlayerHasPersonalDeathClock && !activeDeathClock)
			{
				currentDeathClock = Instantiate (personalDeathClock,player.position,Quaternion.identity) as GameObject;
				currentDeathClock.transform.parent = player;
				currentDeathClock.SendMessage ("SetasPersonalClock",true);
				currentDeathClock.SendMessage ("ClockTimer",personalDeathClockTimer);
				currentDeathClock.SendMessage ("SetdontUsePositiveEffect");
				currentDeathClock.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
				gameController.addEvent (currentDeathClock);
				currentDeathClock.transform.localPosition = Vector3.zero;
				activeDeathClock = true;
				darkness.SetActive (true);
				Color c = Color.black;
				c.a = 0;
				darkness.GetComponent<Renderer>().material.color = c;
				c.a = 0.1f;
				iTween.ColorTo(darkness,c,1);
				lightOnPlayer.SetActive (true);
				lightGR.SetActive (true);
			}
		}
	}
	protected override void OnMonsterHit()
	{
		Transform CheckifPlayerHasPersonalDeathClock = player.Find ("DeathClock(Clone)");
		if(activeDeathClock || CheckifPlayerHasPersonalDeathClock)
		{
			gameController.removeEvent (currentDeathClock);
			activeDeathClock = false;
			darkness.SetActive (false);
			lightOnPlayer.SetActive (false);
			lightGR.SetActive (false);
		}
	}
}
