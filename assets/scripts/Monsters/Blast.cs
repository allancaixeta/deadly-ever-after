using UnityEngine;
using System.Collections;

public class Blast : MonsterGeneric {
	
	float direction;
	public GameObject oneUnityBlast, egg;
	public float lifeTime = 7;
	public int ammountOfMiniBlasts = 5;
	public float loadRate = 0.2f;
	float currentLoadRate;// batLife;
	//bool sendMessageToParentWhenTouchAnotherBlast = false;
	public float dontChangeDirectionForTheNextSeconds = 0.15f;
	float reloaddontChangeDirectionForTheNextSeconds, lifeTimeTotal;
	Transform [] arrayOfBlasts;
	int nextBlastToMove, blastsInstantiateds;
	bool batFatherIsDead;

	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.projectile;
		reloaddontChangeDirectionForTheNextSeconds = dontChangeDirectionForTheNextSeconds;
		lifeTimeTotal = lifeTime;
		gameObject.tag = Tags.MonsterProjectile;
	}
	
	public override bool StartState()
	{		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
		switch(currentState)
		{
		case MonsterStates.VOID:
			transform.position = new Vector3(-10,-10,0);
//			if(blastsInstantiateds != ammountOfMiniBlasts)
//			{
//				Debug.LogError (blastsInstantiateds);
//			}
			lastToEnd = nextBlastToMove;
			currentLoadRate = loadRate;
			arrayOfBlasts[nextBlastToMove].position = transform.position;
			nextBlastToMove++;
			if(nextBlastToMove >= arrayOfBlasts.Length)
				nextBlastToMove = 0;
			break;

		case MonsterStates.Start:
			arrayOfBlasts = new Transform[ammountOfMiniBlasts];
			nextBlastToMove = 0;
			currentLoadRate = loadRate;
			break;
			
		case MonsterStates.Inactive:			
			break;	
			
		case MonsterStates.Death:
			deathCountDown = (ammountOfMiniBlasts - 1) * loadRate;
			if(blastsInstantiateds == ammountOfMiniBlasts)
			{
				lastToDie = nextBlastToMove;
			}
			else
			{
				nextBlastToMove = 0;
				lastToDie = blastsInstantiateds;
			}
			break;
			
		case MonsterStates.Desync:
			currentDesync = Random.Range (0,desyncTimer);
			FixZ();
			break;
			
		case MonsterStates.Normal:
			currentLoadRate = loadRate;
			break;
			
		case MonsterStates.Knockback:
			nextState = MonsterStates.Normal;
			break;
		}
		return true;
	}

	int lastToEnd, lastToDie;



	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter	
		switch(currentState)
		{
		case MonsterStates.VOID:
			currentLoadRate -= GameController.deltaTime;
			if(currentLoadRate < 0 && lastToEnd != nextBlastToMove)
			{
				currentLoadRate = loadRate;
				arrayOfBlasts[nextBlastToMove].position = transform.position;
				nextBlastToMove++;
				if(nextBlastToMove >= arrayOfBlasts.Length)
					nextBlastToMove = 0;
			}
			break;

		case MonsterStates.Start: //instancia 1 por 1 dos blasts unitys, e depois passar para o estado normal onde eles somente trocam de posicao
			dontChangeDirectionForTheNextSeconds -= GameController.deltaTime;
			Velocity ();
			Move ();
			lifeTime -= GameController.deltaTime;
			currentLoadRate -= GameController.deltaTime;
			if(currentLoadRate < 0)
			{
				currentLoadRate = loadRate;
				GameObject temp = Instantiate (oneUnityBlast, transform.position, oneUnityBlast.transform.rotation) as GameObject;
				temp.SendMessage (MnstMsgs.SetToughness, monsterToughness);
				temp.SendMessage ("SetPassThrough");
				switch(monsterToughness)
				{
				case toughness.one:
				case toughness.two:
					break;
				case toughness.three:
				case toughness.four:
				case toughness.five:
					//temp.SendMessage ("SetGenerateSomethingWhenTouch",this.gameObject);
					break;
				}
				gameController.addMonster (temp);
				arrayOfBlasts[blastsInstantiateds] = temp.transform;
				blastsInstantiateds++;
				if(blastsInstantiateds >= ammountOfMiniBlasts)
				{
					nextBlastToMove = 0;
					nextState = MonsterStates.Normal;						
				}
			}


			break;
			
		case MonsterStates.Normal:
			dontChangeDirectionForTheNextSeconds -= GameController.deltaTime;
			Velocity ();
			Move ();
			lifeTime -= GameController.deltaTime;
			if(lifeTime < 0)
			{
				if(batFatherIsDead)
					nextState = MonsterStates.Death;
				else
					nextState = MonsterStates.VOID;
			}
			currentLoadRate -= GameController.deltaTime;
			if(currentLoadRate < 0)
			{
				currentLoadRate = loadRate;
				arrayOfBlasts[nextBlastToMove].position = transform.position;
				arrayOfBlasts[nextBlastToMove].Find ("visual/base").GetComponent<ParticleSystem>().Stop(true);
				arrayOfBlasts[nextBlastToMove].Find ("visual/base").GetComponent<ParticleSystem>().Play(true);
				arrayOfBlasts[nextBlastToMove].GetComponent<MonsterGeneric>().nextState = MonsterStates.Normal;
				nextBlastToMove++;
				if(nextBlastToMove >= arrayOfBlasts.Length)
					nextBlastToMove = 0;
			}
			break;
			
		case MonsterStates.Hook:
			//controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			currentLoadRate -= GameController.deltaTime;
			if(currentLoadRate < 0)
			{
				currentLoadRate = loadRate;
				arrayOfBlasts[nextBlastToMove].SendMessage (MnstMsgs.DestroyItself,SendMessageOptions.DontRequireReceiver);
				//Destroy (arrayOfBlasts[nextBlastToMove]);
				nextBlastToMove++;
				if(nextBlastToMove >= arrayOfBlasts.Length)
					nextBlastToMove = 0;
				if(lastToDie == nextBlastToMove)
					DestroyItSelf();
			}
			/*deathCountDown -= GameController.deltaTime;
			if(deathCountDown < 0)
			{
				DestroyItSelf();
			}*/
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			/*knockbackCountDown -= GameController.deltaTime;
			Move();
			if(knockbackCountDown < 0)
			{
				nextState = monsterStates.normal;
			}*/
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}



	void Velocity()
	{
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
			
	}



	void Move()
	{	
		transform.Translate(velocity,Space.World);
	}



	float initialDirection;
	public void SetDirection(float d)
	{
		direction = d;	
		initialDirection = d;
		visual.transform.rotation = Quaternion.Euler (0,0,d);		
	}



	public void SetSendMessageToParentWhenTouchAnotherBlast()
	{
		//sendMessageToParentWhenTouchAnotherBlast = true;
	}



	public void SetNewLife(float l)
	{
		lifeTime = lifeTimeTotal * l;
		if (blastsInstantiateds < ammountOfMiniBlasts) 
		{
			for (int i = blastsInstantiateds; i < ammountOfMiniBlasts; i++) {
				GameObject temp = Instantiate (oneUnityBlast, transform.position, oneUnityBlast.transform.rotation) as GameObject;
				temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
				temp.SendMessage ("SetPassThrough");
				switch(monsterToughness)
				{
				case toughness.one:
				case toughness.two:
					break;
				case toughness.three:
				case toughness.four:
				case toughness.five:
					//temp.SendMessage ("SetGenerateSomethingWhenTouch",this.gameObject);
					break;
				}
				gameController.addMonster (temp);
				arrayOfBlasts[blastsInstantiateds] = temp.transform;
				blastsInstantiateds++;
			}		
		}
		nextState = MonsterStates.Normal;
		currentState = nextState;
		currentLoadRate = 0;
		nextBlastToMove = 0;
		dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
		SetDirection (initialDirection);
	}

	public void SetLifeTime(float l)
	{
		lifeTime = lifeTimeTotal * l;
	}

	public void SetBatMaxLife(float l)
	{
		//batLife = l;
	}

	public void SetAfterLifeBlast()
	{
		dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
		batFatherIsDead = true;
	}

	public void TwoBlastsTouching(BlastUnity otherBlast)
	{
		/*if(!generatedEgg && otherBlast.GetFather ().name != gameObject.name)
		{
			print ("EggGenerated");
			otherBlast.GetFather ().SendMessage ("EggGenerated");
			generatedEgg= true;
			GameObject temp = Instantiate (egg, otherBlast.transform.position, Quaternion.identity) as GameObject;
			temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
			temp.SendMessage ("SetMaxLife",batLife);
			gameController.addMonster (temp);
		}*/
	}
	string lastTouchedObject = "";
	Collider lastToHit;
	void OnTriggerEnter(Collider other)
	{	
		if(dontChangeDirectionForTheNextSeconds > 0 || CheckIfCurrentOrNextState(MonsterStates.Death) || lastToHit == other)
			return;
		dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
		lastToHit = other;
		if(other.transform.tag == Tags.Lvl2Obstacle  || other.transform.tag == Tags.Lvl3Obstacle || other.transform.tag == Tags.Wall)
		{
			switch(monsterToughness)
			{
			default:
			case toughness.one:
			case toughness.two:
				if(batFatherIsDead)
					nextState = MonsterStates.Death;
				else
					nextState = MonsterStates.VOID;
				break;
			case toughness.three:
			case toughness.four:
			case toughness.five:
				if(lastTouchedObject == other.transform.name)
					return;
				lastTouchedObject = other.transform.name;
				visual.transform.Rotate(0,0,(Random.value < 0.5 ? 90 : - 90));	
				direction = visual.transform.rotation.eulerAngles.z;		
				break;
			}			
		}
	}

	public void SetFatherIsDead()
	{
		batFatherIsDead = true;
		if(currentState == MonsterStates.VOID || currentState != MonsterStates.Normal)
			nextState = MonsterStates.Death;
	}

	protected override void OnMonsterHit()
	{
	}
}