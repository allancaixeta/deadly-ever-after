using UnityEngine;
using System.Collections;
using Pathfinding;

#pragma warning disable 0414 // private field assigned but not used.

public class Pumpkin : MonsterGeneric {

	public GameObject projectile;
	public Transform goalPosition;
	public float radiusSize = 25;
	public float shootCooldown = 3;
	public float shotAmmount = 3;
	public float intervalAngle = 120;
	public bool randomInitialAngles, randomIntervalAngle;
	public float holdTime = 0.5f;
	public float chargeSpeed = 30;
	public float chargeDuration = 1;
	public float chargeCooldown = 1;
	public GameObject lvl3Clone, cloneMagicAura;
	public bool cloneAlsoShoots, cloneSizeIsDifferentThenOriginal;
	public int lvl4LifeRegen = 1;
	public float lvl4Upgrade = 0.25f;
	public GameObject absorbClone;
	public float lvl5FuryProjectileUpgrade = 0.25f;

	float initialAngle = 0;
	Vector3 lastVelocity,lastPosition;
	CapsuleCollider capsuleCollider;
	float angle, currentCooldown, currentChargeDuration, currentChargeCooldown, joinDelay, oneSecond;
	float maxStoppedTime = 0.7f;
	float currentStoppedTime;
	float x_temp, y_temp;
	private float distancefromGoal;
	bool dashing, lastDashDirectionWasX, ImTheClone;
	CursorController currentCursor;
	Vector2 dashingDirection;
	int getAFuryPoint;
	Fury fury;
	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 1;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	public float repathRate = 0.5f;
	private float lastRepath = -9999;

	public float attackTriggerRange = 3;

	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.pumpkin;
		seeker = GetComponent<Seeker>();
		goalPosition.position = player.position;
		currentCursor = CursorController.Instance;
		capsuleCollider = GetComponent<CapsuleCollider> ();
		chargeSpeed = chargeSpeed * (1 - skills.GetAccel (false));
		fury = GetComponent<Fury>();
		invulnerable = true;
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState)
			return false;
		previousState = currentState;
		currentState = nextState;
		switch(currentState)
		{
		case MonsterStates.Start:
			GetOutOfObject();
			nextState = MonsterStates.Desync;	
			angle = Random.Range (0,360);
			getAFuryPoint = 0;
            currentChargeCooldown = chargeCooldown * GetCooldownModifier();
            currentChargeDuration = chargeDuration;
            if (!ImTheClone)
		    {
				lvl3Clone = Instantiate(lvl3Clone,transform.position,Quaternion.identity) as GameObject;		
				lvl3Clone.SendMessage (MnstMsgs.SetToughness,monsterToughness);
				lvl3Clone.SendMessage ("SetImTheClone",this);
				if(!cloneSizeIsDifferentThenOriginal)
					lvl3Clone.transform.localScale = transform.localScale;
				gameController.addMonster (lvl3Clone);
				switch(monsterToughness)
				{
				case toughness.one:
				case toughness.two:
				case toughness.three:
					break;
				case toughness.four:
				case toughness.five:
					lifeRegen += lvl4LifeRegen;
					break;
				}
			}
			invulnerable = false;
			break;
			
		case MonsterStates.Inactive:
			attackCountDown = 0.1f;
			currentLife = maxLife;
			Disable ();
			break;	

		case MonsterStates.Normal:
			lastPosition = transform.position;
			currentCooldown = shootCooldown * GetCooldownModifier ();
			//currentChargeCooldown = chargeCooldown  * GetCooldownModifier ();
			//currentChargeDuration = chargeDuration;
			break;

		case MonsterStates.Preparation:
			if(randomInitialAngles) //O primeiro tiro é sempre  no ângulo zero (ou seja, na direção para baixo do monstro) ou é aleatório?
				initialAngle = Random.Range (0,360);
			currentCooldown = holdTime;
			if(ImTheClone && !(cloneAlsoShoots && capsuleCollider.enabled))//fail safe para caso entre aqui por algum motivo errado
				nextState = MonsterStates.Normal;
			break;

		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			GetOutOfObject();
			break;
			
		case MonsterStates.Death:
			deathCountDown = afterLife;
			if(ImTheClone)
			{
				ShootAtPlayer (false);
				father.ShootAtPlayer (true);
				nextState = MonsterStates.Inactive;
			}
			else
			{
				animator.SetTrigger ("die");
				lvl3Clone.transform.position = transform.position;
				lvl3Clone.SendMessage ("DestroyItSelf");
			}
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			if(Random.value <= desyncChance)
				nextState = MonsterStates.Desync;
			LifeRegen();
			if(!dashing)
			{
				//confere se parou de andar por 1s, ou seja, nao consegue chegar no alvo
				currentStoppedTime = Vector2.Distance (lastPosition,transform.position) > 0.02f ? 0 : currentStoppedTime + GameController.deltaTime;
				lastPosition = transform.position;
				if(currentStoppedTime > maxStoppedTime)
				{
					currentStoppedTime = 0;
					angle = Random.Range (0,360);
				}
				CalculateMovePosition();
				Velocity();
				Move();
				if(!ImTheClone || cloneAlsoShoots)
				{
					currentCooldown -= GameController.deltaTime;
					if(currentCooldown < 0 || Vector2.Distance(player.position,transform.position) < attackTriggerRange)
					{
						nextState = MonsterStates.Preparation;
					}
				}
				currentChargeCooldown -= GameController.deltaTime;
				if(currentChargeCooldown < 0)
					CheckForPlayerLineofSight();
			}
			else
			{
				switch(monsterToughness)
				{
				case toughness.one:
				case toughness.two:
				case toughness.three:
				case toughness.four:
				case toughness.five:
					Charge ();
					Move ();
					currentChargeDuration -= GameController.deltaTime;
					if(currentChargeDuration < 0)
					{
						dashing = false;
						animator.SetBool ("dash",false);
                        currentChargeCooldown = chargeCooldown * GetCooldownModifier();
                        currentChargeDuration = chargeDuration;
                    }
					break;
				}
			}
			joinDelay -= GameController.deltaTime;
			if(monsterToughness == toughness.five)
			{
				if(getAFuryPoint > 0)
				{
					fury.CalculateFury ();
					oneSecond -= GameController.deltaTime;
					if(oneSecond < 0)
					{
						getAFuryPoint--;
						oneSecond = 1.05f;
					}
					if(fury.GetOnFury())
					{
						getAFuryPoint = 0;		
					}
				}
				else
					fury.StopAcumulatingFury ();
			}
			break;

		case MonsterStates.Preparation:
			currentCooldown -= GameController.deltaTime;
			if(currentCooldown < 0)
			{
				for (int i = 0; i < shotAmmount; i++) {
					Shoot (initialAngle + ((randomIntervalAngle ? Random.Range (0,360): intervalAngle) * i),fury.GetOnFury ());
				}
				if(fury.GetOnFury())
					fury.ResetFury ();
				nextState = MonsterStates.Desync;
			}
			break;

		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;

		case MonsterStates.Inactive:
			attackCountDown -= GameController.deltaTime;
			if(attackCountDown < 0)
			{
				visual.SetActive (false);
				nextState = MonsterStates.Normal;
			}
			break;	

		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}

	void Shoot(float angle, bool onFury)
	{
		GameObject temp = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
		temp.SendMessage ("SetDirection",angle);
		gameController.addMonster (temp);	
		if(onFury)
		{
			temp.SendMessage ("OnFury",lvl5FuryProjectileUpgrade);
		}
	}
	
	void CalculateMovePosition()
	{		
		distancefromGoal = Vector2.Distance (transform.position,goalPosition.position);
		if(distancefromGoal < 1)
			angle = Random.Range (0,360);
		x_temp = player.position.x + (Mathf.Sin(angle) * (radiusSize));
		y_temp = player.position.y + (Mathf.Cos(angle) * (radiusSize));
		goalPosition.position = new Vector3(x_temp,y_temp,0);
	}
	
	void Velocity()
	{		
		if (Time.time - lastRepath > repathRate && seeker.IsDone()) {
			lastRepath = Time.time+ repathRate;
			seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);				
		}
		if (path == null || currentWaypoint >= path.vectorPath.Count) {
			velocity = goalPosition.position - transform.position;
		}         
		else
		{
			//Direction to the next waypoint
			velocity = (path.vectorPath[currentWaypoint]-transform.position);
			if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) 
				currentWaypoint++;
		}
			
		velocity.Normalize();
		if(monsterToughness != toughness.five)
			velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
		else
			velocity *= GameController.deltaTime * (speed + (fury.GetOnFury() ? fury.furySpeed * fury.maxFuryPoints : 0)) * (timeDistortion-magicSlowEffect);
	}
	
	void OnPathComplete (Path p) {
		p.Claim (this);
		if (!p.error) {
			if (path != null) path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		} else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
		}
	}

	void Charge()
	{
		velocity = dashingDirection * GameController.deltaTime * chargeSpeed;
			
	}
	
	void Move()
	{		
		controller.Move(velocity * isPlayerPushing);
	}

	Ray ray;
	RaycastHit hit;
	Vector3 direction, playerNow;
	void CheckForPlayerLineofSight()
	{
		this.direction = currentCursor.currentPosition - player.position;
		this.direction.z = 0;
		playerNow = player.position;
		playerNow.z = GetMonsterCenter ().z;
		ray = new Ray(playerNow,direction);
        Debug.DrawRay(playerNow, direction);
		if(capsuleCollider.Raycast (ray,out hit, 200f))
		{
			switch(monsterToughness)
			{
			case toughness.one:
				MakeADash (direction);
				break;
			case toughness.two:
				MakeADash (direction);
				if(!ImTheClone)
					ShootAtPlayer (false);
				break;
			case toughness.three:
			case toughness.four:
			case toughness.five:
				if(lvl3Clone.GetComponent<CapsuleCollider>().enabled)
				{
					MakeADash(direction);
					if(!ImTheClone || (cloneAlsoShoots && capsuleCollider.enabled))
						ShootAtPlayer (false);
				}
				else
				{
					joinDelay = 3;
					lvl3Clone.transform.position = transform.position;
					lvl3Clone.SendMessage ("RestartClone");
					bool dashMeInsteadOfTheClone = Random.Range (0,2) == 1 ? true : false;
					if(dashMeInsteadOfTheClone)
						MakeADash(direction);
					else
					{
						lvl3Clone.SendMessage("MakeADash",direction);
						currentChargeCooldown = chargeCooldown  * GetCooldownModifier ();
					}
				}
				break;
			}
		}
	}

	public void MakeADash(Vector3 d)
	{
		animator.SetBool ("dash",true);
		dashing = true;
		dashingDirection.x = d.y;
		dashingDirection.y = d.x;
		if(lastDashDirectionWasX)
			dashingDirection.y *= -1;
		else
			dashingDirection.x *= -1;
		dashingDirection.Normalize ();
		lastDashDirectionWasX = !lastDashDirectionWasX;
		currentChargeCooldown = chargeCooldown  * GetCooldownModifier ();
		currentChargeDuration = chargeDuration;
	}

	public void ShootAtPlayer(bool triggeredByClone)
	{
		float angle =  Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		GameObject temp = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
		temp.SendMessage ("SetDirection",angle);
		gameController.addMonster (temp);	
		if(monsterToughness == toughness.five)
		{
			if(fury.GetOnFury())
			{
				temp.SendMessage ("OnFury",lvl5FuryProjectileUpgrade);
				fury.ResetFury ();
			}
			else
			if(triggeredByClone)
			{
				oneSecond = 1.05f;
				getAFuryPoint++;	
			}

		}
	}
	Pumpkin father;
	public void SetImTheClone(Pumpkin father)
	{
		ImTheClone = true;
		this.father = father;
		nextState = MonsterStates.Inactive;
	}

	public void RestartClone()
	{
		GameObject FX = Instantiate (cloneMagicAura, transform.position, Quaternion.identity) as GameObject;
		FX.transform.parent = transform;
		currentLife = maxLife;
		gameObject.layer = originalLayer;
		nextState = MonsterStates.Normal;
		capsuleCollider.enabled = true;
		tag = Tags.Monster;
		monsterType = monsterTypes.pumpkin;
		visual.SetActive (true);
		controller.center = new Vector3(0,0,0);
		invulnerable = false;
		if(transform.localScale.x != father.transform.localScale.x && !cloneSizeIsDifferentThenOriginal)
		{
			transform.localScale = father.transform.localScale;			
		}
	}

	public void Disable()
	{
		capsuleCollider.enabled = false;
		controller.center = new Vector3(0,0,-3);
		invulnerable = true;
		tag = Tags.MonsterProjectile;
		monsterType = monsterTypes.projectile;
	}
	
	void OnTriggerEnter(Collider other)
	{		
		if(ImTheClone)
			return;
		switch(monsterToughness)
		{
		case toughness.one:
		case toughness.two:
		case toughness.three:
			return;		
		case toughness.four:
		case toughness.five:
			if(joinDelay < 0 && other.transform.tag == Tags.Monster && other.name == lvl3Clone.name)
			{
				lvl3Clone.GetComponent<MonsterGeneric>().nextState = MonsterStates.Inactive;
				UpgradeMonster (lvl4Upgrade);
				GameObject FX = Instantiate (absorbClone, transform.position, Quaternion.identity) as GameObject;
				FX.transform.parent = transform;
			}
			break;
		}		
		return;		
	}

	protected override void OnMonsterHit()
	{
	}

	public bool AmITheClone()
	{
		return ImTheClone;
	}
}
