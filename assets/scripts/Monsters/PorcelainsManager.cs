﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#pragma warning disable 0414 // private field assigned but not used.

[Prefab("PorcelainsManager")]
public class PorcelainsManager : Singleton<PorcelainsManager> {
       
	List<Porcelain> listOfDolls;
	CursorController currentCursor;
	protected Transform player;
	Ray ray;
	RaycastHit hit;
	Vector3 direction, playerNow;
	float distanceOfCloserDoll, errorManager;
	int numberOfCloserDoll, lastCloserDoll;
	bool lvl4Dolls;
	// Use this for initialization
	void Awake () {
		listOfDolls = new List<Porcelain> ();
		currentCursor = CursorController.Instance;
		player = GameObject.FindGameObjectWithTag("Player").transform;	
		lastCloserDoll = -1;
	}
	
	// Update is called once per frame
	void Update () {
		numberOfCloserDoll = -1;
		distanceOfCloserDoll = 1000000;
		for (int i = 0; i < listOfDolls.Count; i++) {
			CheckForPlayerLineofSight(i);
		}
		if(numberOfCloserDoll == -1)
		{
			errorManager--;
            if (errorManager < 0 && lastCloserDoll != -1)
			{
				listOfDolls[lastCloserDoll].PlayerNotLookingAtAnymore ();
				lastCloserDoll = -1;
				if(lvl4Dolls)
				{
					for (int i = 0; i < listOfDolls.Count; i++) {
						listOfDolls[i].Lvl4PlayerNotLookingAtAnyone();
					}
				}
			}
		}
		else
		{
			errorManager = 10;
            if (numberOfCloserDoll != lastCloserDoll)
			{                
                listOfDolls[numberOfCloserDoll].PlayerLookingAt();
                if (lastCloserDoll != -1)
                    listOfDolls[lastCloserDoll].PlayerNotLookingAtAnymore();
                lastCloserDoll = numberOfCloserDoll;
                if (lvl4Dolls)
                {
                    for (int i = 0; i < listOfDolls.Count; i++)
                    {
                        if (i != numberOfCloserDoll)
                            listOfDolls[i].Lvl4PlayerNotLookingAtThisDool();
                    }
                }
			}
		}
	}


	void CheckForPlayerLineofSight(int dollNumber)
	{
		this.direction = currentCursor.currentPosition - player.position;
		this.direction.z = 0;
		playerNow = player.position;
		playerNow.z = 30;
		ray = new Ray(playerNow,direction);
		Debug.DrawRay (playerNow, direction);
		if(listOfDolls[dollNumber].GetComponent<BoxCollider>().Raycast (ray,out hit, 200f))
		{
			float distance = Vector2.Distance (listOfDolls[dollNumber].transform.position,player.position);
			if(distance < distanceOfCloserDoll) 
			{
				numberOfCloserDoll = dollNumber;
				distanceOfCloserDoll = distance;                
			}
		}
	}

	public void SetLvl4()
	{
		lvl4Dolls = true;
	}

	public void RegisterDoll(Porcelain p)
	{
		listOfDolls.Add (p);
	}

	public void UnregisterDoll(Porcelain p)
	{
		if(listOfDolls.IndexOf(p) == lastCloserDoll)
		{
			lastCloserDoll = -1;
			listOfDolls.Remove (p);	
		}
		else
		{
			if(lastCloserDoll != -1)
			{
				Porcelain lastone = listOfDolls[lastCloserDoll];
				listOfDolls.Remove (p);	
				lastCloserDoll = listOfDolls.IndexOf(lastone);
			}
			else
				listOfDolls.Remove (p);	
		}
	}
}