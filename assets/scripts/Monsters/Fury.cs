﻿using UnityEngine;
using System.Collections;

public class Fury : MonoBehaviour{

	public int maxFuryPoints = 10;
	public float furySpeed = 0.1f;
	public float furyDamageAddition = 0.4f;
	public int randomFuryPoints = 10;
	public bool randomFury = false;
	protected float currentFuryTimer = 1;
	protected bool onFury;
	protected int currentFuryPoints = 0;
	SkillController skillcontroller;
	public GameObject FXFuryGathering, FxOnFury, FXEnterFury;
	void Start()
	{
		skillcontroller = (SkillController) FindObjectOfType(typeof(SkillController));
		if(randomFury)
			SetRandomFuryPoints();
		GameObject temp = Instantiate(FXFuryGathering,transform.position,FXFuryGathering.transform.rotation) as GameObject;
		temp.transform.parent = this.transform;
		temp.transform.localPosition = FXFuryGathering.transform.localPosition;
		FXFuryGathering = temp;
		temp = Instantiate(FxOnFury,transform.position,FxOnFury.transform.rotation) as GameObject;
		temp.transform.parent = this.transform;
		temp.transform.localPosition = FxOnFury.transform.localPosition;
		FxOnFury = temp;
		FXFuryGathering.SetActive (false);
		FxOnFury.SetActive (false);
	}

	void Update()
	{
	}

	public void CalculateFury()
	{
		if(onFury)
			return;
		if(!FXFuryGathering.activeSelf)
			FXFuryGathering.SetActive (true);
		currentFuryTimer -= GameController.deltaTime / skillcontroller.GetLoveandPeace ();
		if(currentFuryTimer < 0)
		{
			currentFuryPoints++;
			if(currentFuryPoints >= maxFuryPoints)
			{
				onFury = true;
				EnterFury();
				currentFuryPoints = 0;
			}
			else
				currentFuryTimer = 1;			
		}
	}

	public void StopAcumulatingFury()
	{
		FXFuryGathering.SetActive (false);
	}

	public bool GetOnFury()
	{
		return onFury;
	}

	public void SetOnFury(bool f)
	{
		onFury = f;
	}

	public void SetRandomFuryPoints()
	{
		maxFuryPoints = Random.Range (1,randomFuryPoints);
	}

	public void ResetFury()
	{
		onFury = false;
		currentFuryPoints = 0;
		FxOnFury.SetActive (false);
		FXFuryGathering.SetActive (true);
		float f = (furyDamageAddition * (float)maxFuryPoints);
		gameObject.GetComponent<MonsterGeneric>().monsterDamage -= (int)f;
		if(randomFury)
			SetRandomFuryPoints();
	}

	void OnTriggerStay(Collider other)
	{
		if(onFury && other.transform.tag == "PlayerHit") 
		{
			onFury = false;
			FxOnFury.SetActive (false);
			FXFuryGathering.SetActive (true);
			float f = (furyDamageAddition * (float)maxFuryPoints);
			gameObject.GetComponent<MonsterGeneric>().monsterDamage -= (int)f;
			if(randomFury)
				SetRandomFuryPoints();
		}
	}

	void EnterFury()
	{
		float f = (furyDamageAddition * (float)maxFuryPoints);
		gameObject.GetComponent<MonsterGeneric>().monsterDamage += (int)f;
		GameObject temp = Instantiate(FXEnterFury,transform.position,FXEnterFury.transform.rotation) as GameObject;
		temp.transform.parent = this.transform;
		temp.transform.localPosition = FXEnterFury.transform.localPosition;
		FxOnFury.SetActive (true);
		FXFuryGathering.SetActive (false);
	}

	public float GetFuryPoints()
	{
		if(onFury)
			return maxFuryPoints;
		else
			return currentFuryPoints;
	}
}
