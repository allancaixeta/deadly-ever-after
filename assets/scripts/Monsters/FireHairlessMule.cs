using UnityEngine;
using System.Collections;
using Pathfinding;

public class FireHairlessMule : MonsterGeneric {

	public float chargeRadius = 15;
	bool charging = false;
	public float chargeSpeed = 20;
	public float chargeCooldown = 0.5f;
	float currentChargeCooldown = 0;
	public float preparationTimer = 1;
	float currentPreparationTimer = 0;
	Vector3 direction;
	bool stoned;
	public float stoneDuration = 1;
	public int protectingArmor = 30;
	float currentStoneDuration = 0;
	public float startChargeAnimationDuration = 1;
	public float lvl3SpeedIncrease = 1;
	float currentSpeedIncrease, lastLineOfSight;
	bool sleeping = true;
	public float awakeDuration;
	float awakeTimer;
	public GameObject chargeImpact;
	public Vector3 chargeImpactPosition;
	public GameObject stone, firePath;
	public float firePathCooldown;
	float currentFirePathCooldown;
	bool playerHittedThisLoopControl = false;
	float dontHitWallToClose = 0;
	LineOfSight lineOfSight;

	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 1;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	public float repathRate = 0.5f;
	private float lastRepath = -9999;

	public override void SpecificMonsterAwake()
	{
		seeker = GetComponent<Seeker>();
		monsterType = MonsterGeneric.monsterTypes.firehairlessmule;
		currentChargeCooldown = chargeCooldown  * GetCooldownModifier ();
		lineOfSight = GetComponent<LineOfSight>();
		if (!lineOfSight) {
			gameObject.AddComponent <LineOfSight>();
			lineOfSight = GetComponent<LineOfSight>();
		}
		lvl3SpeedIncrease = lvl3SpeedIncrease * (1 - skills.GetAccel (false));
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			animator.SetBool("idle", true);
			nextState = MonsterStates.Normal;	
			if(monsterToughness != toughness.one)
			{
				armor = protectingArmor;		
				UpdateArmorBreakable ();
			}
			break;
		
		case MonsterStates.Inactive:	
			break;	
		
		case MonsterStates.Stun:
		case MonsterStates.Hook:
			if(charging && monsterToughness != MonsterGeneric.toughness.one)
				nextState = MonsterStates.Normal;
			break;
			
		case MonsterStates.Knockback:
			if(charging)
				nextState = MonsterStates.Normal;
			break;
			
		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
            if (stoned)
            {
                currentDesync = stoneDuration;
                animator.SetBool("stone", true);
                animator.SetBool("idle", false);
            }
            else
            {
                animator.SetBool("idle", true);
                knockbackType = KnockbackType.heavy;
            }
			break;

		case MonsterStates.Preparation:
			attackCountDown = startChargeAnimationDuration;
			animator.SetBool("precharge", true);
			animator.SetBool("idle", false);
			break;

		case MonsterStates.Attack:
			animator.SetBool ("precharge", false);
			playerHittedThisLoopControl = false;
			dontHitWallToClose = 0.25f;
			charging = true;
			lastToHit = null;
			gameObject.layer = MonsterAttack.giantLayer;
            Agressive();
			break;

		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		dontHitWallToClose -= GameController.deltaTime;
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			LifeRegen ();
			currentChargeCooldown -= GameController.deltaTime;
			if(sleeping)
			{
				animator.SetBool("sleeping", true);
				return;
			}
			else
			{
				awakeTimer -= GameController.deltaTime;
				if(awakeTimer < 0)
				{
					sleeping = true;
				}
				if(Random.value <= desyncChance && !charging)
					nextState = MonsterStates.Desync;
				switch(monsterToughness)
					{
					case toughness.one:
					case toughness.two:
						Velocity ();
						Move ();							
						if(Vector2.Distance (transform.position,player.position) <= chargeRadius && currentChargeCooldown < 0 && lineOfSight.hasLineOfSight)
							StartCharge();		
						break;
					case toughness.three:
					case toughness.four:
					case toughness.five:												
						Velocity ();
						MoveToPlayer ();
						bool near = Vector2.Distance(transform.position,player.position) <= chargeRadius;
						if(near && currentChargeCooldown < 0 && lineOfSight.hasLineOfSight)
						{
							StartCharge();
						}	
						if(!lineOfSight.hasLineOfSight)
						{
							lastLineOfSight += GameController.deltaTime;
							if(lastLineOfSight >= 1)
							{
								currentSpeedIncrease += lvl3SpeedIncrease;
								lastLineOfSight = 0;
							}
						}
						else
						{
							currentSpeedIncrease = 0;
							lastLineOfSight = 0;
						}
						break;
					}
			}
			break;
		
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
		
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
			{
				nextState = MonsterStates.Normal;
                animator.SetBool("stone", false);
                if(stoned)
                {
                    invulnerable = false;
                    knockbackType = KnockbackType.heavy;
                    stoned = false;
                }
                if (speed > 0)
					animator.SetBool("idle", false);                
            }
			break;

		case MonsterStates.Preparation:
			attackCountDown -= GameController.deltaTime;
			if(attackCountDown < 0)
			{
				nextState = MonsterStates.Attack;
			}
			break;

		case MonsterStates.Attack:		
			Charge ();
			Move ();	
			break;
		}
	}
	
	void Velocity()
	{		
		if (Time.time - lastRepath > repathRate && seeker.IsDone()) {
			lastRepath = Time.time+ repathRate;
			seeker.StartPath (transform.position,player.position, OnPathComplete);				
		}
		if (path == null || currentWaypoint >= path.vectorPath.Count) {
			velocity = player.position - transform.position;
		}         
		else
		{
			//Direction to the next waypoint
			velocity = (path.vectorPath[currentWaypoint]-transform.position);
			if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) 
				currentWaypoint++;
		}
			
		velocity.Normalize();
		velocity *= GameController.deltaTime * (speed + currentSpeedIncrease) * (timeDistortion-magicSlowEffect) ;				
	}

	void OnPathComplete (Path p) {
		p.Claim (this);
		if (!p.error) {
			if (path != null) path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		} else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
		}
	}

	void Charge()
	{
		velocity = direction * GameController.deltaTime * chargeSpeed;			
		if(monsterToughness == toughness.five)
		{
			currentFirePathCooldown -= GameController.deltaTime;
			if(currentFirePathCooldown < 0)
			{
				currentFirePathCooldown = firePathCooldown;
				GameObject temp = Instantiate (firePath, transform.position, Quaternion.identity) as GameObject;
				temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
				temp.SendMessage ("SetPassThrough");
				gameController.addMonster (temp);
			}
		}
	}

	void MoveToPlayer()
	{
		if (Vector2.Distance (player.position, transform.position) < stopMovingIfCloserThenThisDistance)
			return;
		Move ();
	}

	void Move()
	{		
		controller.Move(velocity * (charging ? 1 : isPlayerPushing));
	}

	void StartCharge()
	{
		currentChargeCooldown = chargeCooldown * GetCooldownModifier ();
		direction = lineOfSight.GetDirection ();
		this.direction.Normalize();
		nextState = MonsterStates.Preparation;
		currentFirePathCooldown = firePathCooldown;
		switch(monsterToughness)// != toughness.one)
		{
		default:
		case toughness.two:
		case toughness.three:
		case toughness.four:
		case toughness.five:
			knockbackType = KnockbackType.hulk;
			knockbackCountDown = -1;
			slowDuration = 0;
			if(magicallySlowed && slowdownObject)
				slowdownObject.MonsterDied(this);
			break;
		}
	}

	void StopCharge()
	{
		nextState = MonsterStates.Desync;
		charging = false;				
		gameObject.layer = MonsterAttack.nonHarmfulLayer;
        Passive();
	}

	Collider lastToHit;
	void OnControllerColliderHit(ControllerColliderHit hit) 
	{
		if(sleeping && hit.transform.tag == "Player")
			WakeUp ();        
        switch (monsterToughness)
		{
		case toughness.one:	
		case toughness.two:
		case toughness.three:
			if((hit.transform.tag == Tags.Lvl2Obstacle || hit.transform.tag == Tags.Lvl3Obstacle || hit.transform.tag == Tags.Wall) && charging && dontHitWallToClose < 0 && lastToHit != hit.collider)
			{
				StopCharge();	
				lastToHit = hit.collider;
                (Instantiate(chargeImpact, this.transform.position + chargeImpactPosition, Quaternion.Euler(0, 0, 90)) as GameObject).transform.parent = this.transform;
            }
			break;		
		case toughness.four:
		case toughness.five:
			if(hit.transform.tag == "PlayerHit")
				playerHittedThisLoopControl = true;
			if((hit.transform.tag == Tags.Lvl2Obstacle || hit.transform.tag == Tags.Lvl3Obstacle || hit.transform.tag == Tags.Wall) && charging && dontHitWallToClose < 0 && !stoned && lastToHit != hit.collider)
			{
				lastToHit = hit.collider;
                (Instantiate(chargeImpact, this.transform.position + chargeImpactPosition, Quaternion.Euler(0, 0, 90)) as GameObject).transform.parent = this.transform;
                if (!playerHittedThisLoopControl)
				{
					Stones ();
                }
                StopCharge();
			}
			break;	
		}		
	}

	void Stones()
	{	
		if(monsterToughness == toughness.four || monsterToughness == toughness.five)
		{
			GameObject rock = Instantiate (stone, player.position, Quaternion.identity) as GameObject;
			rock.SendMessage ("SetSide",EventController.sides.ontop);
			rock.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
			rock.SendMessage ("SetdontUsePositiveEffect");
			gameController.addEvent (rock);
		}
		stoned = true;
        invulnerable = true;
	}

	protected override void OnMonsterHit()
	{
		WakeUp ();
	}

	void WakeUp()
	{
		awakeTimer = awakeDuration;
		if(sleeping)
		{
			sleeping = false;
			animator.SetBool("sleeping", false);
		}
	}
}
