using UnityEngine;
using System.Collections;

public class Ghost : MonsterGeneric {

	[System.Serializable]
	public struct MovementData {
		[Tooltip("Ideal distance to stay from the target")]
		public float desiredDistance;
		
		[Tooltip("Should try to stay at least this distant from other entities of this same type")]
		public float minFriendlyDistance;
		
		[Tooltip("Normal movement max speed")]
		public float normalMoveMaxSpeed;
		
		[Tooltip("Size of each ring around the target, used to know circling direction")]
		public float circularRingSize;
		
		[Tooltip("Influence of the ring to make this pursuer to circle around the target")]
		public float ringPower;
		
		[Tooltip(
			"How much is this object repelled from the the target.\n" +
			"Equals to  attract at desired distance")
		 ]
		public float targetRepelPower;
		
		[TooltipAttribute("How much is the object attracted to target. Constant.")]
		public float targetAttractPower;
	}
	
	
	[System.Serializable]
	public struct AttackData {

		[Tooltip("Cosine of the angle of the attack-cone (positive is behind)")]
		public float attackCosine;
		
		[Tooltip("Cosine of the angle to retreat. Negative number if front")]
		public float retreatCosine;
		
		[Tooltip("Min time needed to stay inside angle tolerance, to attack")]
		public float minWaitTime;
		
		[Tooltip("If an attack roll fails, wait this seconds before rolling again")]
		public float waitTimeRollPenalty;
		
		[Tooltip("Success chance of an attack roll, to result in an attack")]
		public float chancePct;
		
		[Tooltip("Max speed to use when charging in to attack")]
		public float advanceMaxSpeed;
		
		[Tooltip("Max speed to use when retreating without lifting an attack")]
		public float retreatMaxSpeed;
		
		[System.NonSerialized]
		public float waitElapsed; // Time waited to attack
	}
	
	public MovementData movement;
	public AttackData attack;	

	// Caching
	protected Transform m_Transform;

	GhostManager ghostManager;

	public float initializationSpeedingTimer = 5;
	public float initializationSpeed = 5;
	public float runAsACowardDistance = 10;
	public float lvl2SuicideExtraDamage = 3;
	public GameObject suicidalExplosionFX;
	public int lvl3Armor = 3;
	bool cowardBehaviorOn;
	public GameObject firePath, sink;
	public float firePathCooldown;
	float currentFirePathCooldown;
	Transform cenarioLimits;
	Transform bigGhostEye;

	public override void SpecificMonsterAwake()
	{
		ghostManager = GhostManager.Instance;
		monsterType = MonsterGeneric.monsterTypes.ghost;
		m_Transform = this.transform;		
		attack.waitElapsed = -initializationSpeedingTimer;
	}
	
	public override bool StartState()
	{
		//verifica se eh um estado novo, e tem uma verificacao para nao trocar do estado death para outro (sem ressuscitar!)
		
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
				
		if(currentLife < 0)
			nextState = MonsterStates.Death;
		//efetiva a troca de estado
		currentState = nextState;
		
		switch(currentState)
		{
		//estado inicial, somente roda uma vez por monstro
		case MonsterStates.Start:
			currentFirePathCooldown = firePathCooldown;
			ghostManager.tList.Add(this);
			StageController stage = (StageController) FindObjectOfType(typeof(StageController));	
			cenarioLimits = stage.GetRoom().GetCenarioLImits ();
			break;
			
		//usavel para caso queira "desativar" o monstro por um tempo por algum motivo	
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Normal:	
			animator.SetBool ("normal",true);
            if (cowardBehaviorOn)
            { 
                gameObject.layer = MonsterGeneric.normalLayer;
                Agressive();
            }
            else
            {
                gameObject.layer = MonsterGeneric.nonHarmfulLayer;
                Passive();
            }
            break;	

		case MonsterStates.Preparation:			
			nextState = MonsterStates.Attack;
			break;	

		case MonsterStates.Attack:		
			animator.SetBool ("normal",false);
			animator.SetBool ("retreat",false);
			gameObject.layer = MonsterGeneric.normalLayer;
            Agressive();
            break;	

		case MonsterStates.Retreat:	
			animator.SetBool ("retreat",true);
			gameObject.layer = MonsterGeneric.nonHarmfulLayer;
            Passive();
            break;	
		
		//esse estado eh usado para dessincronizar os monstros, para q eles nao façam as acoes ao 
		//mesmo tempo q seus companheiros (por ex. para q monstros nao atirem todos ao mesmo tempo
		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			break;
	
		//vai para esse estado quando o monstro morre, e ativa a animacao de morte
		case MonsterStates.Death:
			if(iAmBigGhost)
			{
				ghostManager.DeathOfBigGhost (this.transform.position);
			}
			else
			{
				ghostManager.tList.Remove(this);
				ghostManager.EvaluateCowardNumber();
			}
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		//estado nulo inicial, sempre leva para o estado start
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;
			
		case MonsterStates.Start:
			initializationSpeedingTimer -= GameController.deltaTime;
			if(initializationSpeedingTimer < 0)
				nextState = MonsterStates.Normal;
			ExecuteNormalState();					
			break;

		//esse eh o estado em q mais acontece o acrescimo de codigo
		case MonsterStates.Normal:
			LifeRegen();//funcao q verifica se o monstro regenera vida, e ja regenera a vida. 
			if(cowardBehaviorOn)
			{
				switch(monsterToughness)
				{
				case toughness.one:
					Vector3 pos = m_Transform.position;
					if(Vector2.Distance (player.transform.position,pos) < runAsACowardDistance)
					{	
						Vector3 mov = pos - player.transform.position;
						mov = mov.normalized * movement.normalMoveMaxSpeed * speed * GameController.deltaTime * (timeDistortion-magicSlowEffect);				
						pos += mov;
						m_Transform.position = pos;
					}
					if(m_Transform.position.x < cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 ||
                       m_Transform.position.x > cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 ||
                       m_Transform.position.y < cenarioLimits.position.y - cenarioLimits.lossyScale.y/2 ||
                       m_Transform.position.y > cenarioLimits.position.y + cenarioLimits.lossyScale.y/2 )
						nextState = MonsterStates.Death;
					break;			
				case toughness.two:
				case toughness.three:
					ExecuteAttackState();
					break;
				case toughness.five:
				case toughness.four:
					if(!iAmBigGhost)
					{
						currentFirePathCooldown -= GameController.deltaTime;
						if(currentFirePathCooldown < 0)
						{
							currentFirePathCooldown = firePathCooldown;
							GameObject temp = Instantiate (firePath, transform.position, Quaternion.identity) as GameObject;
							temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
							temp.SendMessage ("SetPassThrough");
							gameController.addMonster (temp);
						}
					}
					else
					{
						float angle = Mathf.Atan2 (player.position.y - bigGhostEye.transform.position.y, player.position.x - bigGhostEye.transform.position.x) * Mathf.Rad2Deg;
						bigGhostEye.transform.rotation = Quaternion.Euler (0,0,angle+90);
					}
					ExecuteAttackState();
					break;
				}	

			}
			else
			{
				//Esse switch divide a logica do monstro em niveis.
				switch(monsterToughness)
				{
				case toughness.one:
				case toughness.two:
				case toughness.three:
				case toughness.four:
				case toughness.five:
					ExecuteNormalState();
					break;
				}
			}
			break;
		
		case MonsterStates.Attack:			
			//Esse switch divide a logica do monstro em niveis.
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				ExecuteAttackState();
				break;
			}
			break;

		case MonsterStates.Retreat:			
			//Esse switch divide a logica do monstro em niveis.
			switch(monsterToughness)
			{
			case toughness.one:
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				ExecuteRetreatState();
				break;
			}
			break;

		//estado usado para quando o monstro eh fiscado por uma mecanica q puxa ele
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		//estado de morte
		case MonsterStates.Death:
			Death();	
			break;
				
		//estado quando ele esta stunado		
		case MonsterStates.Stun:
			Stun();
			break;
		
		//estado para quando o monstro eh acertado por uma arma q empurra ele
		case MonsterStates.Knockback:
			Knock();
			break;
			
		//estado de desincronizacao
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}

	protected void ExecuteNormalState() {
		bool canAttack = false;
		
		if(attack.waitElapsed >= attack.minWaitTime) {
			float chance = Random.Range(0.0f, 1.0f);
			if(chance <= attack.chancePct) {
				canAttack = true;
			} else {
				attack.waitElapsed -= attack.waitTimeRollPenalty;
			}
		}
		
		if(canAttack) {
			Vector3 pos = m_Transform.position;
			Vector3 targetPos = player.position;
			
			// NOTE : CAN OPTMIZE BY USING DOT PRODUCTS OF QUATERNIONS
			
			Vector3 myDir = targetPos - pos;
			float targetAngle = PlayerController.angle * Mathf.Deg2Rad;
			Vector3 targetDir = new Vector3(
				Mathf.Cos(targetAngle),
				Mathf.Sin(targetAngle),
				0.0f
				);
			
			myDir.z = 0.0f;
			targetDir.z = 0.0f;
			myDir.Normalize();
			
			float cosine = Vector3.Dot(myDir, targetDir);
			if(cosine > attack.attackCosine) {
				canAttack = ghostManager.TryGetAttackPermission();
			} else {
				canAttack = false;
			}
		}
		
		if(canAttack) {
			nextState = MonsterStates.Preparation;
		} else {
			attack.waitElapsed += GameController.deltaTime;
			NormalMove();
		}
	}

	protected void ExecuteAttackState() {
		Vector3 pos = m_Transform.position;
		Vector3 otherPos = player.position;
		Vector3 dist = otherPos - pos;
		dist.z = 0.0f;
		
		// Check if target is looking at us.
		Vector2 dir = (Vector2) dist.normalized;
		float angle = PlayerController.angle;
		Vector2 targetDir = new Vector2(
			Mathf.Cos(angle * Mathf.Deg2Rad),
			Mathf.Sin(angle * Mathf.Deg2Rad)
			);
		
		float cos = Vector2.Dot(dir, targetDir);
		if(cos < attack.retreatCosine && !cowardBehaviorOn) {
			nextState = MonsterStates.Retreat;
			return;
		}				

		if(dist.sqrMagnitude > stopMovingIfCloserThenThisDistance) {
			Move(otherPos);
		}
	}

	protected void ExecuteRetreatState() {
		Vector3 pos = m_Transform.position;
		Vector3 otherPos = player.position;
		Vector3 dist = otherPos - pos;
		dist.z = 0.0f;
		
		// Check if target is looking at us.
		Vector2 dir = (Vector2) dist.normalized;
		float angle = PlayerController.angle;
		Vector2 targetDir = new Vector2(
			Mathf.Cos(angle * Mathf.Deg2Rad),
			Mathf.Sin(angle * Mathf.Deg2Rad)
			);
		
		float cos = Vector2.Dot(dir, targetDir);
		if(cos >= attack.retreatCosine) {
			// The player is not
			nextState = MonsterStates.Attack;
			return;
		} else {
			// Should give up from attacking?
			if(dist.magnitude > movement.desiredDistance * 0.9f) {
				ghostManager.LiftAttackPermission();
				nextState = MonsterStates.Normal;
				attack.waitElapsed = 0.0f;
				return;
			}
		}
		
		pos -= dist;
		Move(pos);
	}

	protected void NormalMove() {
		// Obtains (cache) the current position.
		Vector3 pos = m_Transform.position;
		
		// Distance to target and magnitude.
		Vector3 dist = player.transform.position - pos;
		dist.z = 0.0f;
		float distMag = dist.magnitude;
		
		
		// Attract to target
		Vector3 attract = dist.normalized * movement.targetAttractPower;
		
		
		// Add ring rotation to attrack power
		Vector2 normal = new Vector2(-dist.y, dist.x);
		normal.Normalize();
		
		float ringFactor = distMag / movement.circularRingSize; // Ring coordinate as float
		int rindDir =  1 - (2 * (Mathf.FloorToInt(ringFactor) % 2)); // Rotation to assume from current ring
		
		// normalizedRingPower is to make ring rotation power strongest at ring center,
		// and zero at its borders
		float normalizedRingPower = ringFactor - Mathf.FloorToInt(ringFactor);
		normalizedRingPower = Mathf.Clamp(normalizedRingPower, 0.0f, 1.0f);
		normalizedRingPower = LerpEquationTypes.SmoothCos.GetEquationTransformedFactor(
			normalizedRingPower
			) * movement.ringPower;
		
		attract += (Vector3) (normal * rindDir * normalizedRingPower);
		
		
		// Repel from target
		float repelPower = 0.0f;
		if(distMag < 1.0f) {
			repelPower = movement.targetRepelPower;
		} else if(dist.magnitude < movement.desiredDistance) {
			repelPower = Mathf.Lerp(
				movement.targetRepelPower, movement.targetAttractPower,
				(distMag * distMag) / (movement.desiredDistance * movement.desiredDistance)
				);
		}
		
		Vector3 repel = -dist.normalized * repelPower;
		
		
		// Repel from friendlies
		Vector3 friendlyRepel = Vector3.zero;
		
		for(int i = 0; i < ghostManager.tList.Count; ++i) {
			Ghost other = ghostManager.tList[i];
			if(other == this) continue;
			
			Transform otherTransform = other.transform;
			
			Vector3 otherPos = otherTransform.position;
			if((otherPos - pos).sqrMagnitude <= 0.01f) {
				otherPos.x += Random.Range(-1.0f, 1.0f);
				otherPos.y += Random.Range(-1.0f, 1.0f);
			}
			
			Vector3 otherDist = pos - otherPos;
			float otherMag = otherDist.magnitude;
			if(otherMag < movement.minFriendlyDistance) {
				float friendRepelPower = movement.minFriendlyDistance - otherMag;
				
				friendlyRepel += otherDist.normalized * friendRepelPower;
			}
		}
		
		
		// Final movement
		float z = pos.z;
		pos += attract + repel + friendlyRepel;
		pos.z = z;
		Move(pos);
	}
	
	protected void Move(Vector3 desiredPosition) {
		float maxSpeed = 0.0f;
		switch(currentState) {
		case MonsterStates.Start:       maxSpeed = movement.normalMoveMaxSpeed * initializationSpeed; break;
		case MonsterStates.Normal:      maxSpeed = movement.normalMoveMaxSpeed; break;
		case MonsterStates.Attack:      maxSpeed = attack.advanceMaxSpeed;      break;
		case MonsterStates.Retreat:     maxSpeed = attack.retreatMaxSpeed;      break;
		default:                        maxSpeed = 0.0f;                        break;
		}
		
		Vector3 pos = m_Transform.position;
		Vector3 delta = desiredPosition - pos;
		float deltaMag = delta.magnitude;
		Vector3 mov = delta;
		delta.Normalize();
		
		if(deltaMag > maxSpeed * GameController.deltaTime) {
			mov = delta * maxSpeed * speed * GameController.deltaTime * (timeDistortion-magicSlowEffect);;
		}
		
		pos += mov;
		m_Transform.position = pos;
	}

	void OnTriggerEnter(Collider other)
	{
		if (iAmBigGhost)
			return;
		if(other.transform.tag == "PlayerHit") 
		{
			if(cowardBehaviorOn && monsterToughness != toughness.one)
			{
				Instantiate (suicidalExplosionFX,transform.position,Quaternion.identity);
				switch(monsterToughness)
				{
				default:
					break;
				case toughness.four:
				case toughness.five:
					sink = Instantiate(sink,transform.position,Quaternion.identity) as GameObject;
					sink.SendMessage("StartEvent",StageController.StageNumber.stage1);
					sink.SendMessage ("SetdontUsePositiveEffect");
					gameController.addEvent (sink);
					sink.transform.position = transform.position;				
					break;
				}
				nextState = MonsterStates.Death;
			}
			if(currentState == MonsterStates.Attack)
			{
				nextState = MonsterStates.Normal;
				ghostManager.LiftAttackPermission();
				attack.waitElapsed = 0.0f;
			}

		}
	}

	//Funcao chamada quando o monstro eh acertado por uma arma. Essa funcao somente eh usada por monstros q reagem de alguma forma quando sao acertados.
	protected override void OnMonsterHit()
	{
	}

	public void ActivateCowardBehavior()
	{
		cowardBehaviorOn = true;
		if(CheckIfCurrentOrNextState (MonsterStates.Attack) || CheckIfCurrentOrNextState (MonsterStates.Retreat))
		{
			nextState = MonsterStates.Normal;
		}		
		if(monsterToughness != toughness.one)
		{
			monsterDamage += lvl2SuicideExtraDamage;	
			animator.SetTrigger ("suicide");
            gameObject.layer = MonsterGeneric.normalLayer;
            Agressive();
        }
	}
	bool lvl3armorOn;
	public void ChangeArmorLvl3(bool receiveArmor)
	{
		if(lvl3armorOn && !receiveArmor)
		{
			armor = 0;
			UpdateArmorInfinite ();
			lvl3armorOn = false;
		}
		if(!lvl3armorOn && receiveArmor)
		{
			armor = lvl3Armor;
			UpdateArmorInfinite ();
			lvl3armorOn = true;
		}
	}

	public void BecomePartOfBigGhost()
	{
		nextState = MonsterStates.Inactive;
		invulnerable = true;
	}

	public void GetOutBigGhost()
	{
		nextState = MonsterStates.Normal;
		invulnerable = false;
	}
	bool iAmBigGhost;
	public void ThisIsBigGhost()
	{
		iAmBigGhost = true;
		cowardBehaviorOn = true;
		nextState = MonsterStates.Normal;
		gameObject.layer = MonsterGeneric.normalLayer;
        Agressive();
        bigGhostEye = visual.transform.Find ("base/Global_CTRL/GravityCenter/BigGhost_Eye");
	}
}
