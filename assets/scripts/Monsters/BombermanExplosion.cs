using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BombermanExplosion : MonsterGeneric {
	
	ExplosionDetector up,left;
	public float sizeGrowth = 0.01f;
	bool growingUp,growingDown,growingLeft,growingRight;
	private List<string> hydrasGrowed;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.hydraexplosion;
		growingUp = true;
		growingDown = true;
		growingLeft = true;
		growingRight = true;
		hydrasGrowed = new List<string>();
		gameObject.tag = "MonsterProjectile";
		sizeGrowth = sizeGrowth * (1 - skills.GetAccel(true));
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;		
			up = visual.transform.Find("up").GetComponent<ExplosionDetector>();
			up.name = monsterDamage.ToString ();
			left = visual.transform.Find("left").GetComponent<ExplosionDetector>();
			left.name = monsterDamage.ToString ();
			break;
		
		case MonsterStates.Inactive:			
			break;	
		
		case MonsterStates.Desync:
			currentDesync = Random.Range (0,0);
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		if(magicallySlowed)
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}		
		switch(currentState)
		{
		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			if(growingUp && growingDown)
			{
				up.transform.localScale += new Vector3(sizeGrowth,0,0) * GameController.deltaTime * (timeDistortion-magicSlowEffect);
			}
			else
			{
				if(growingUp)
				{
					up.transform.localScale += new Vector3(sizeGrowth/2,0,0) * GameController.deltaTime * (timeDistortion-magicSlowEffect);
					up.transform.Translate (sizeGrowth/2 * GameController.deltaTime * (timeDistortion-magicSlowEffect),0,0);
				}
				else
				{
					if(growingDown)
					{
						up.transform.localScale += new Vector3(sizeGrowth/2,0,0) * GameController.deltaTime * (timeDistortion-magicSlowEffect);
						up.transform.Translate (-sizeGrowth/2 * GameController.deltaTime * (timeDistortion-magicSlowEffect),0,0);
					}
				}
			}
			if(growingLeft && growingRight)
			{
				left.transform.localScale += new Vector3(sizeGrowth,0,0) * GameController.deltaTime * (timeDistortion-magicSlowEffect);
			}
			else
			{
				if(growingLeft)
				{
					left.transform.localScale += new Vector3(sizeGrowth/2,0,0) * GameController.deltaTime * (timeDistortion-magicSlowEffect);
					left.transform.Translate (-sizeGrowth/2 * GameController.deltaTime * (timeDistortion-magicSlowEffect),0,0);
				}
				else
				{
					if(growingRight)
					{
						left.transform.localScale += new Vector3(sizeGrowth/2,0,0) * GameController.deltaTime * (timeDistortion-magicSlowEffect);
						left.transform.Translate (sizeGrowth/2 * GameController.deltaTime * (timeDistortion-magicSlowEffect),0,0);
					}
				}
			}
			if(!growingUp && !growingDown && !growingLeft && !growingRight)
			{
				DestroyItSelf ();
			}
			break;
		}
	}
	
	public void FoundAHydra(Hydra h)
	{		
		for (int i = 0; i < hydrasGrowed.Count; i++) {
			if(hydrasGrowed[i] == h.name)
				return;
		}
		h.Grow ();
		hydrasGrowed.Add(h.name);
	}
	
	public void HitSomething(bool isUp, bool higher)
	{
		if(isUp)
		{
			if(higher)
				growingUp = false;
			else
				growingDown = false;
		}
		else
		{
			if(higher)
				growingRight = false;
			else
				growingLeft = false;
		}
	}

	protected override void OnMonsterHit()
	{
	}
}
