using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tarrasque : MonsterGeneric {

	public float auraStartingRange = 10;
	public float auraMaxRange = 50;
	public float auraMinCooldownDecrease = 0.3f;
	public float gravityPull = 1;
	public float pullTimer = 2;
	public float minSleepTime = 1;
	public int lvl3ArmorIncrease = 10;
	public int lvl3ArmorLimit = 100;
	public GameObject blackHole;
	public AudioClip wakeUpSound;
	bool ImAlone, sleeping;
	RoomController room;
	List<MonsterGeneric> listOfMonsters;
	float currentSleepingTime, currentPullTime, lvl3ArmorIncreaseCooldown;
	public override void SpecificMonsterAwake()
	{
		monsterType = MonsterGeneric.monsterTypes.tarrasque;		
		listOfMonsters = new List<MonsterGeneric> ();
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			room = (RoomController) FindObjectOfType(typeof(RoomController));
            Agressive();
			if(room.GetNumberOfTarrasques() != 0)
			{
				DestroyItSelf ();
			}
			else
			{
				GetOutOfObject();
				nextState = MonsterStates.Normal;
				room.IncreaseATarrasque();
				player.GetComponent<PlayerController>().ATarrasqueIsBorn (transform,auraStartingRange,auraMaxRange,auraMinCooldownDecrease);
				sleeping = true;
				animator.SetBool("sleeping",true);
				armorCooldown = -1;
				UpdateArmorDestructible ();
				lvl3ArmorIncreaseCooldown = 5;
			}
			break;
			
		case MonsterStates.Inactive:			
			break;	
			
		case MonsterStates.Desync:
			if(lastState == MonsterStates.Start)
				currentDesync = Random.Range (1,GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin,desyncTimer) * GetCooldownModifier();
			FixZ();
			GetOutOfObject();
			break;
			
		case MonsterStates.Death:
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			room.DecreaseATarrasque();
			player.SendMessage ("ATarrasqueIsKilled");
			if(monsterToughness == toughness.five)
			{
				blackHole = Instantiate(blackHole,transform.position,Quaternion.identity) as GameObject;
				blackHole.SendMessage("StartEvent",StageController.StageNumber.stage1);
				blackHole.SendMessage ("SetdontUsePositiveEffect");
				gameController.addEvent (blackHole);
				blackHole.transform.position = transform.position;
			}
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		FixZ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		switch(currentState)
		{
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
			
		case MonsterStates.Normal:
			//if(Random.value <= desyncChance)
			//	nextState = monsterStates.desync;
			LifeRegen();
			switch(monsterToughness)
			{
			case toughness.one:
				break;
			case toughness.two:
			case toughness.three:
			case toughness.four:
			case toughness.five:
				lvl3ArmorIncreaseCooldown -= GameController.deltaTime;
				if(sleeping)
				{

					currentSleepingTime -= GameController.deltaTime;
					if(currentSleepingTime < 0)
					{
						listOfMonsters = gameController.GetMonsterList ();
						sleeping = false;
						for (int i = 0; i < listOfMonsters.Count; i++) {
							switch(listOfMonsters[i].GetMonsterType ())
							{
							default: 
								if(listOfMonsters[i].name != gameObject.name && listOfMonsters[i].knockbackType != KnockbackType.hulk && Vector2.Distance (transform.position,listOfMonsters[i].transform.position) < auraStartingRange && listOfMonsters[i].speed > 0)
								{
									sleeping = true;
									continue;
								}
								break;
							case MonsterGeneric.monsterTypes.ghost:
							case MonsterGeneric.monsterTypes.boss:
							case MonsterGeneric.monsterTypes.hydraexplosion:
							case MonsterGeneric.monsterTypes.projectile:
							case MonsterGeneric.monsterTypes.separatepiece:
								break;
							}
						}
						if(!sleeping)
						{
							gameController.MonsterAudioSource.PlayOneShot (wakeUpSound);
							animator.SetBool("sleeping",false);
							currentPullTime = pullTimer;
							for (int i = 0; i < listOfMonsters.Count; i++) {
								switch(listOfMonsters[i].GetMonsterType ())
								{
								default: 
									if(listOfMonsters[i].name != gameObject.name && listOfMonsters[i].speed > 0 && listOfMonsters[i].knockbackType != KnockbackType.hulk)
									{
										listOfMonsters[i].Stun (pullTimer);
									}
									break;
								case MonsterGeneric.monsterTypes.boss:
								case MonsterGeneric.monsterTypes.hydraexplosion:
								case MonsterGeneric.monsterTypes.projectile:
								case MonsterGeneric.monsterTypes.separatepiece:
									break;
								}
							}
							if(lvl3ArmorIncreaseCooldown < 0 && !ImAlone)
							{
								switch(monsterToughness)
								{
								case toughness.one:
								case toughness.two:
									break;
								case toughness.three:
								case toughness.four:
								case toughness.five:
									lvl3ArmorIncreaseCooldown = 5;
									armor += lvl3ArmorIncrease;
									if(armor > lvl3ArmorLimit)
										armor= lvl3ArmorLimit;
									UpdateArmorDestructible ();
									break;
								}
							}
						}
					}
				}
				else
				{
					currentPullTime -= GameController.deltaTime;
					if(currentPullTime < 0)
					{
						sleeping = true;
						animator.SetBool("sleeping",true);
						currentSleepingTime = minSleepTime;
						return;
					}
					for (int i = 0; i < listOfMonsters.Count; i++) {
						switch(listOfMonsters[i].GetMonsterType ())
						{
						default: 
							if(listOfMonsters[i].name != gameObject.name && listOfMonsters[i].speed > 0)
							{
								velocity = transform.position - listOfMonsters[i].transform.position;
									
								velocity.Normalize();
								velocity *= GameController.deltaTime * gravityPull;
								if(listOfMonsters[i].CheckIfCurrentOrNextState (MonsterStates.Stun))
									listOfMonsters[i].GetComponent<CharacterController>().Move (velocity);
							}
							break;
						case MonsterGeneric.monsterTypes.boss:
						case MonsterGeneric.monsterTypes.hydraexplosion:
						case MonsterGeneric.monsterTypes.projectile:
						case MonsterGeneric.monsterTypes.separatepiece:
							break;
						}
					}
				}



				/*if(currentSleepingTime < 0)
				{
					listOfMonsters = gameController.GetMonsterList ();
					sleeping = false;
					for (int i = 0; i < listOfMonsters.Count; i++) {
						switch(listOfMonsters[i].GetMonsterType ())
						{
						default: 
							if(listOfMonsters[i].name != gameObject.name && Vector2.Distance (transform.position,listOfMonsters[i].transform.position) < auraStartingRange && listOfMonsters[i].speed > 0)
							{
								if(!wasSleepingLastFrame)
									wentBackToSleep = true;
								sleeping = true;
								continue;
							}
							break;
						case MonsterGeneric.monsterTypes.boss:
						case MonsterGeneric.monsterTypes.hydraexplosion:
						case MonsterGeneric.monsterTypes.projectile:
						case MonsterGeneric.monsterTypes.separatepiece:
							break;
						}
					}
					if(!sleeping && wasSleepingLastFrame && lvl3ArmorIncreaseCooldown < 0)
					{
						switch(monsterToughness)
						{
						case toughness.one:
						case toughness.two:
							break;
						case toughness.three:
						case toughness.four:
						case toughness.five:
							lvl3ArmorIncreaseCooldown = 5;
							armor += lvl3ArmorIncrease;
							if(armor > lvl3ArmorLimit)
								armor= lvl3ArmorLimit;
							UpdateArmorDestructible ();
							break;
						}
					}
					wasSleepingLastFrame = sleeping;
					if(!sleeping)
					{
						for (int i = 0; i < listOfMonsters.Count; i++) {
							switch(listOfMonsters[i].GetMonsterType ())
							{
							default: 
								if(listOfMonsters[i].name != gameObject.name && listOfMonsters[i].speed > 0)
								{
									listOfMonsters[i].Stun (0.2f);
									velocity = transform.position - listOfMonsters[i].transform.position;
										
									velocity.Normalize();
									velocity *= GameController.deltaTime * gravityPull;
									if(listOfMonsters[i].CheckIfCurrentOrNextState (monsterStates.stun))
										listOfMonsters[i].GetComponent<CharacterController>().Move (velocity);
								}
								break;
							case MonsterGeneric.monsterTypes.boss:
							case MonsterGeneric.monsterTypes.hydraexplosion:
							case MonsterGeneric.monsterTypes.projectile:
							case MonsterGeneric.monsterTypes.separatepiece:
								break;
							}
						}
					}
					else
					{
						if(wentBackToSleep)
						{
							wentBackToSleep = false;
							currentSleepingTime = minSleepTime;
							for (int i = 0; i < listOfMonsters.Count; i++) {
								switch(listOfMonsters[i].GetMonsterType ())
								{
								default: 
									//if(listOfMonsters[i].name != gameObject.name)
									//	listOfMonsters[i].RemoveStun ();
									break;
								case MonsterGeneric.monsterTypes.boss:
								case MonsterGeneric.monsterTypes.hydraexplosion:
								case MonsterGeneric.monsterTypes.projectile:
								case MonsterGeneric.monsterTypes.separatepiece:
									break;
								}
							}
						}
					}
				}
				else
				{
					currentSleepingTime -= GameController.deltaTime;
				}/*/
				break;
			}
			if(!ImAlone && gameController.CountNumberOfMonstersInTheRoom () == 1)
			{
				//summon mini boss
				EventList eventList = (EventList) FindObjectOfType(typeof(EventList));
				GameObject aMiniBoss;
				switch(monsterToughness)
				{
				case toughness.one:
				case toughness.two:
				case toughness.three:
					aMiniBoss = Instantiate(eventList.GetAMiniBoss (),new Vector3(50,50,1.5f),Quaternion.identity) as GameObject;
					aMiniBoss.SendMessage("StartEvent",StageController.StageNumber.stage1);
					aMiniBoss.SendMessage ("SetdontUsePositiveEffect");
					gameController.addEvent (aMiniBoss);
					break;
				case toughness.four:
				case toughness.five:
					aMiniBoss = Instantiate(eventList.GetAMiniBoss (),new Vector3(50,50,1.5f),Quaternion.identity) as GameObject;
					aMiniBoss.SendMessage("StartEvent",StageController.StageNumber.stage1);
					aMiniBoss.SendMessage ("SetdontUsePositiveEffect");
					gameController.addEvent (aMiniBoss);
					break;
				}
				ImAlone = true;
			}
			break;
			
		case MonsterStates.Hook:
			controller.Move(velocity);					
			break;
			
		case MonsterStates.Death:
			Death();	
			break;
			
		case MonsterStates.Stun:
			Stun();
			break;
			
		case MonsterStates.Knockback:
			Knock();
			break;
			
		case MonsterStates.Desync:
			currentDesync -= GameController.deltaTime;
			if(currentDesync < 0)
				nextState = MonsterStates.Normal;
			break;
			
		}
	}
	
	void Velocity()
	{
		velocity = player.position - transform.position;
			
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{		
		controller.Move(velocity);
	}
	
	protected override void OnMonsterHit()
	{
	}
}
