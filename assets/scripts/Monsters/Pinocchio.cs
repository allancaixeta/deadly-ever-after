using UnityEngine;
using System.Collections;
using Pathfinding;

public class Pinocchio : MonsterGeneric
{
	
	public Transform goalPosition;
	public float radiusSize = 10;
	public float speedGainMultiplierIncreaseInDistance;
	public float cowardiceDuration = 1;
	public float cowardiceCooldown = 1;
	public int lvl4Armor = 5;
	int originalArmor;
	float currentCowardice = 1;
	float angle;
	float x_temp, y_temp;
	float defaultSpeed;
	bool insideRadius;
	Vector3 lastPosition;
	float maxStoppedTime = 0.7f;
	float currentStoppedTime;
	private float distancefromGoal;
	bool turningSides = false;
	Fury fury;
	Vector3 defaultScale, invertedScale;
	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 1;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	public float repathRate = 0.5f;
	private float lastRepath = -9999;

	public float attackTriggerRange = 3;

	public float preparationTimer = 1;
	float currentPreparationTimer = 0;

	public float attackDuration = 1;
	float currentAttackDuration = 1;
	public GameObject attackProjectile;

	public override void SpecificMonsterAwake ()
	{
		seeker = GetComponent<Seeker> ();
		goalPosition.position = player.position;		
		defaultSpeed = speed;
		monsterType = MonsterGeneric.monsterTypes.pinocchio;
		fury = GetComponent<Fury> ();
		gameController.RegisterAsInterestedOnPlayerShooting (gameObject);
	}

	public override bool StartState ()
	{
		lastState = currentState;
		if (currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;
		switch (currentState) {
		case MonsterStates.Start:
			originalArmor = armor;
			angle = Random.Range (0, 360);	
			nextState = MonsterStates.Desync;
			switch (monsterToughness) {
			case toughness.one:
				GetOutOfObject ();
				fury.enabled = false;
				break;
			case toughness.two:
			case toughness.three:			
			case toughness.four:
				controller.center = new Vector3 (0, 0, GameController.FlightPositionY);
				fury.enabled = false;
				break;		
			case toughness.five:
				controller.center = new Vector3 (0, 0, GameController.FlightPositionY);
				fury.enabled = true;
				break;
			}
			defaultScale = visual.transform.localScale;
			invertedScale = -defaultScale;
			if (goalPosition.position.x > transform.position.x)
				visual.transform.localScale = defaultScale;
			else
				visual.transform.localScale = invertedScale;
			switch (monsterToughness) {
			case toughness.one:
				break;
			case toughness.two:
				//animator.SetTrigger ("lvl2");
				break;
			default:
				//animator.SetTrigger ("lvl3");
				break;
			}
			//facingRight = anim.Sprite.FlipX;
			break;
			
		case MonsterStates.Normal:
			//anim.Play("Takeoff");
			// The delegate is used here to return to the previously
            // playing clip after the "hit" animation is done playing.
           // anim.AnimationCompleted = Walk;
			lastPosition = transform.position;
			break;
		
		case MonsterStates.Preparation:
			currentPreparationTimer = preparationTimer;
			animator.SetBool ("preparation",true);
			//toca animacao preparacao
			break;

		case MonsterStates.Attack:
			currentAttackDuration = attackDuration;
			animator.SetBool ("preparation",false);
			//toca animacao de ataque
			GameObject temp = Instantiate (attackProjectile, transform.position, Quaternion.identity) as GameObject;
			temp.transform.position = transform.position;
			//jumpDirection = Mathf.Atan2 (player.position.y - transform.position.y, player.position.x - transform.position.x) * Mathf.Rad2Deg;
			float angleAttack = Mathf.Atan2 (player.position.y - transform.position.y, player.position.x - transform.position.x) * Mathf.Rad2Deg;
			temp.SendMessage (MnstMsgs.SetToughness, monsterToughness);
			temp.SendMessage ("SetDirection",angleAttack);
			gameController.addMonster (temp);		
			if(fury.GetOnFury ())
			{
				temp.SendMessage ("OnFury");
				fury.ResetFury ();
			}
			break;

		case MonsterStates.Knockback:	
			break;	
			
		case MonsterStates.Desync:
			//anim.PlayFromFrame("Walkonce",anim.CurrentFrame);
			//anim.AnimationCompleted = Land;
			if (lastState == MonsterStates.Start)
				currentDesync = Random.Range (1, GameController.InitialMaxDesyncTime);
			else
				currentDesync = Random.Range (desyncTimeMin, desyncTimer) * GetCooldownModifier ();
			break;
		
		case MonsterStates.Death:
			deathCountDown = afterLife;
			animator.SetTrigger ("die");
			gameController.UnregisterAsInterestedOnPlayerShooting (gameObject);
			break;
		}
		return true;
	}

	public override void UpdateState ()
	{
		if (goalPosition.position.x > transform.position.x)
			visual.transform.localScale = defaultScale;
		else
			visual.transform.localScale = invertedScale;
		FixZ ();   //Set Z in the right default value
		Slow ();  //handle slowDuration counter
		Armor (); //handle breakable armor counter
		insideRadius = Vector2.Distance (transform.position, player.position) > radiusSize ? false : true;
		switch (currentState) {
		case MonsterStates.VOID:
			nextState = MonsterStates.Start;
			break;

		case MonsterStates.Start:
			nextState = MonsterStates.Normal;						
			break;
		
		case MonsterStates.Normal:
			//confere se o pinocchio parou de andar por 1s, ou seja, nao consegue chegar no alvo
			currentStoppedTime = Vector2.Distance (lastPosition, transform.position) > 0.02f ? 0 : currentStoppedTime + GameController.deltaTime;
			lastPosition = transform.position;
			if (currentStoppedTime > maxStoppedTime) {
				currentStoppedTime = 0;
				angle = Random.Range (0, 360);
			}
			switch (monsterToughness) {
			case toughness.one:
				CalculateMovePosition ();
				break;
			case toughness.two:		
				CalculateMovePosition ();
				break;	
					
			case toughness.three:
				if (insideRadius)
					speed = defaultSpeed;
				else
					speed = defaultSpeed + Vector2.Distance (transform.position, player.position) * speedGainMultiplierIncreaseInDistance;
				CalculateMovePosition ();
				break;
				
			case toughness.four:
				if (insideRadius) {
					speed = defaultSpeed;
				} else {
					speed = defaultSpeed + Vector2.Distance (transform.position, player.position) * speedGainMultiplierIncreaseInDistance;
				}
				CalculateMovePosition ();
				break;
				
			case toughness.five:
				if (fury.GetOnFury ()) {
					goalPosition.position = player.position;
					if (insideRadius) {
						speed = defaultSpeed + fury.furySpeed * fury.maxFuryPoints * defaultSpeed;
					} else {
						speed = defaultSpeed + fury.furySpeed * fury.maxFuryPoints * defaultSpeed + Vector2.Distance (transform.position, player.position) * speedGainMultiplierIncreaseInDistance;				
					}
				} else {
					if (insideRadius) {
						speed = defaultSpeed;
						fury.CalculateFury ();
					} else {
						speed = defaultSpeed + Vector2.Distance (transform.position, player.position) * speedGainMultiplierIncreaseInDistance;
						fury.StopAcumulatingFury ();
					}
					CalculateMovePosition ();
				}											
				break;
			}
			LifeRegen ();
			Velocity ();
			Move ();
			currentCowardice -= GameController.deltaTime;
			if (Vector2.Distance (player.position, transform.position) < attackTriggerRange)
				nextState = MonsterStates.Preparation;
			break;
		
		case MonsterStates.Hook:
			controller.Move (velocity);					
			break;
			
		case MonsterStates.Death:		
			Death ();
			break;
			
		case MonsterStates.Stun:
			Stun ();
			break;
		
		case MonsterStates.Knockback:			
			Knock ();
			break;
			
		case MonsterStates.Desync:					
			currentDesync -= GameController.deltaTime;
			if (currentDesync < 0) {
				nextState = MonsterStates.Normal;
			}
			break;
		
		case MonsterStates.Preparation:					
			currentPreparationTimer -= GameController.deltaTime;
			if (currentPreparationTimer < 0) {
				nextState = MonsterStates.Attack;
			}
			break;

		case MonsterStates.Attack:					
			currentAttackDuration -= GameController.deltaTime;
			if (currentAttackDuration < 0) {
				nextState = MonsterStates.Desync;
				//toca animacao de idle
			}
			break;

		case MonsterStates.Retreat:
			currentCowardice -= GameController.deltaTime;
			if (currentCowardice < 0) {
				nextState = MonsterStates.Normal;
				currentCowardice = cowardiceCooldown * GetCooldownModifier ();
				armor = originalArmor;
				UpdateArmorInfinite ();
			}
			break;
		}
	}

	void CalculateMovePosition ()
	{		
		distancefromGoal = Vector2.Distance (transform.position, goalPosition.position);
		if (distancefromGoal < 1)
			angle = Random.Range (0, 360);
		x_temp = player.position.x + (Mathf.Sin (angle) * (radiusSize));
		y_temp = player.position.y + (Mathf.Cos (angle) * (radiusSize));
		goalPosition.position = new Vector3 (x_temp, y_temp, 0);
		if (goalPosition.position.x > transform.position.x) {
			if (facingRight && !turningSides) {
				turningSides = true;
				//anim.Play("Land");	
				facingRight = false;
			}
		} else {
			if (!facingRight && !turningSides) {
				turningSides = true;
				//anim.Play("Land");		
				facingRight = true;
			}
		}
	}

	void Velocity ()
	{		
		if (Time.time - lastRepath > repathRate && seeker.IsDone ()) {
			lastRepath = Time.time + repathRate;
			seeker.StartPath (transform.position, goalPosition.position, OnPathComplete);				
		}
		if (monsterToughness != toughness.one || path == null || currentWaypoint >= path.vectorPath.Count) {
			velocity = goalPosition.position - transform.position;
		} else {
			//Direction to the next waypoint
			velocity = (path.vectorPath [currentWaypoint] - transform.position);
			if (Vector2.Distance (transform.position, path.vectorPath [currentWaypoint]) < nextWaypointDistance)
				currentWaypoint++;
		}
			
		velocity.Normalize ();
		velocity *= GameController.deltaTime * speed * (timeDistortion - magicSlowEffect);				
	}

	void OnPathComplete (Path p)
	{
		p.Claim (this);
		if (!p.error) {
			if (path != null)
				path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		} else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: " + p.errorLog);
		}
	}

	void Move ()
	{		
		controller.Move (velocity);
	}

	protected override void OnMonsterHit ()
	{
	
	}

	public void OnPlayerShooting (GameObject weapon)
	{
		if (currentCowardice < 0 && !CheckIfCurrentOrNextState (MonsterStates.Death) && !CheckIfCurrentOrNextState (MonsterStates.Stun)) {
			nextState = MonsterStates.Retreat;
			currentCowardice = Random.Range (cowardiceDuration, desyncTimer);
			if (monsterToughness == toughness.four || monsterToughness == toughness.five) {
				armor = lvl4Armor;
				UpdateArmorInfinite ();
			}
		}
	}
}