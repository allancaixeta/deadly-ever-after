﻿using UnityEngine;
using System.Collections;

public class KillMonsterThatTouch : MonoBehaviour {

	public bool applyHugeDamage = true;
	public bool affectPlayer = false;
	public GameObject FX;

	void Start()
	{
	}

	/*void OnCollisionEnter(Collision other)
	{		
		string monsterID = other.transform.name;
		if(other.transform.tag == Tags.Monster)
		{
			other.transform.GetComponent<MonsterGeneric>().Hit(1000,0,Vector3.zero);
			Instantiate(FX,other.transform.position,Quaternion.identity);
		}
	}*/

	void OnTriggerEnter(Collider other)
	{	
		//string monsterID = other.transform.name;
		if(other.transform.tag == Tags.Monster && other.transform.root.name != "Boss")
		{
			if(applyHugeDamage)
			{
				if(other.transform.GetComponent<MonsterGeneric>().Hit(1000))
				{
					Instantiate(FX,other.transform.position,Quaternion.identity);
				}
			}
			else
				other.transform.GetComponent<MonsterGeneric>().OneHitDeath();
		}
		else
		{
			if(affectPlayer && other.transform.tag == Tags.Player)
				other.transform.GetComponent<PlayerController>().OneHitDeath();
		}
	}

	void OnTriggerStay(Collider other){
		OnTriggerEnter (other);
	}	
}
