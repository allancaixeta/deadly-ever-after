﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public class GoldilocksRoom : BossRoomController {

	public TextAsset roomConfigFile;
	public GameObject goldilockPrefab;
	public float triggerMove = 5;
	public float phase3TriggerMove = 1;
	public int phase3NumberOfStraightMoves = 3;
	int currentPhase3StraightMove;
	public float timerToSummonMonsters = 10;
    public int limitOfCrowds = 10;
	public Vector2 playerStartPosition;
	float currentTimerToTriggerMove, currentTimerToSummonMonsters, originalorthographicSize;
	GoldilocksController goldilock;
	Vector2 maxSpot, minSpot;
	Transform cameraroot;
	public float initialX = 15.45f;
	public float initialY = 84.55f;
    GridGraph gg;
	public Vector2 cameraPosition;
	public Vector2 maxXAndYinStage;		// The maximum x and y coordinates the camera can have in the stage
	public Vector2 minXAndYinStage;		// The minimum x and y coordinates the camera can have in the sate
	GameObject []labyrhinthPlants = new GameObject[127];
	GameObject []labyrhinthPlantsBackup = new GameObject[20];
	MonsterGeneric.directions directionToMove;
	float distanceToMove;
	public float moveSpeed;
	public float moveSpeedPhase3;
	public GameObject killzone;
	float moveCounter;
	Vector3 velocity;
	//GameObject []pressureLevers = new GameObject[4];
	//public GameObject pressureLever;
	Transform furnitureTransform;
	StopPlayerfromGettingInside stopPlayerfromGettingInside;
	// Use this for initialization
	void Awake () {
		ReadSaveFile ();
		player = (PlayerController) FindObjectOfType(typeof(PlayerController));	
		stopPlayerfromGettingInside = GetComponent<StopPlayerfromGettingInside> ();
		gameController = GameController.Instance;	
		currentTimerToSummonMonsters = timerToSummonMonsters;
		gameController = GameController.Instance;	
		furnituresInTheGame = (FurnitureStorage) FindObjectOfType(typeof(FurnitureStorage));	
		allocatedSpots = new List<Vector2>();
		skillcontroller = (SkillController) FindObjectOfType(typeof(SkillController));
        GenerateFurniture();
    }
	
	public void GenerateFurniture()
	{
		furnitureTransform = transform.FindChild ("furniture");
		for (int i = 0; i < furnitures.Length; i++) {
			labyrhinthPlants[i] = Instantiate(furnituresInTheGame.GetFurniturePrefab (furnitures[i].furnituretype),
			                              new Vector3(initialX + StageController.adjustX * (furnitures[i].position.x -1),
			            initialY - StageController.adjustY * (furnitures[i].position.y - 1),0),
			                              Quaternion.identity) as GameObject;
			labyrhinthPlants[i].transform.parent = furnitureTransform;
            labyrhinthPlants[i].GetComponent<mud>().StartEvent(StageController.StageNumber.stage1);                        
            labyrhinthPlants[i].transform.Rotate(0, 0, -90);
        }
		for (int i = 0; i < 20; i++) {
			labyrhinthPlantsBackup[i] = Instantiate(furnituresInTheGame.GetFurniturePrefab (furnitures[0].furnituretype),
			                                  new Vector3(initialX + StageController.adjustX * (furnitures[0].position.x -1),
			            initialY - StageController.adjustY * (furnitures[0].position.y - 1),0),
			                                  Quaternion.identity) as GameObject;
			labyrhinthPlantsBackup[i].transform.parent = furnitureTransform;
			labyrhinthPlantsBackup[i].gameObject.SetActive (false);
			labyrhinthPlantsBackup[i].name = "Plantbackup"+i.ToString ();
            labyrhinthPlantsBackup[i].GetComponent<mud>().StartEvent(StageController.StageNumber.stage1);     
            labyrhinthPlantsBackup[i].transform.Rotate(0, 0, -90);
        }
		cenarioLimits = transform.FindChild("CenarioLimits");
		for (int i = 0; i < furnitures.Length; i++) {
			AllocatedFurnitureSpot(i);
		}
	}
	
	void ReadSaveFile()
	{
		ReadConfig file = new ReadConfig(roomConfigFile);
		furnitures = new furniture[127];
		string s;
		for (int i = 0; i < 127; i++) {
			s = file.GetString("Furniture"+(i+1).ToString ());
			furniture f = new furniture(new Vector2(int.Parse(s.Substring(0, 2)), int.Parse(s.Substring(3, 2))), furniture.typeOfFurniture.plantlabiryinth);
			furnitures[i] = f;
		}	
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		    case bossState.wait:
			
			    break;
		    case bossState.fight:                
                goldilock = (Instantiate(goldilockPrefab,goldilockPrefab.transform.position,Quaternion.identity) as GameObject).GetComponent<GoldilocksController>();
			    int tough = GameObject.Find ("StageController").GetComponent<StageController>().GetToughness();
			    goldilock.SendMessage (MnstMsgs.SetToughness,tough);
			    cenarioLimits = GameObject.FindGameObjectWithTag(Tags.Floor).transform;
			    cameraroot = GameObject.Find ("CameraControl").transform;
			    cameraroot.GetComponent<CameraFollow>().ChangeMargins(maxXAndYinStage,minXAndYinStage);
			    cameraroot.position= new Vector3(playerStartPosition.x,playerStartPosition.y,cameraroot.position.z);
			    player.transform.position = new Vector3(playerStartPosition.x,playerStartPosition.y,player.transform.position.z);
			    player.gameObject.SetActive (false);
                transform.Find("fixed").localPosition = new Vector3(59.5f, -58f, 0);
                killzone.SetActive (true);          
                transform.Find("furniture").localPosition = new Vector3(1.22f, -118.425f, 0);
                player.gameObject.SetActive (true);
			    player.dynamicGridPlayer.SetActive (true);
			    UpdateSpots();
			    currentTimerToTriggerMove = triggerMove;
			    /*pressureLevers[0] = (Instantiate(pressureLever,FindMeANewSpot (),Quaternion.identity) as GameObject);
			    pressureLevers[0].GetComponent<GoldiePressurelever>().SetDirection (MonsterGeneric.directions.D);
			    pressureLevers[0].name += "D";
			    pressureLevers[1] = (Instantiate(pressureLever,FindMeANewSpot (),Quaternion.identity) as GameObject);
			    pressureLevers[1].GetComponent<GoldiePressurelever>().SetDirection (MonsterGeneric.directions.U);
			    pressureLevers[1].name += "U";
			    pressureLevers[1].transform.Rotate (0,0,180);
			    pressureLevers[2] = (Instantiate(pressureLever,FindMeANewSpot (),Quaternion.identity) as GameObject);
			    pressureLevers[2].GetComponent<GoldiePressurelever>().SetDirection (MonsterGeneric.directions.R);
			    pressureLevers[2].name += "R";
			    pressureLevers[2].transform.Rotate (0,0,90);
			    pressureLevers[3] = (Instantiate(pressureLever,FindMeANewSpot (),Quaternion.identity) as GameObject);
			    pressureLevers[3].GetComponent<GoldiePressurelever>().SetDirection (MonsterGeneric.directions.L);
			    pressureLevers[3].name += "L";
			    pressureLevers[3].transform.Rotate (0,0,270);*/
			    //player.transform.Translate(Vector3.down*10);
			    // This creates a Grid Graph
			    gg = AstarPath.active.astarData.gridGraph;
			    // Setup a grid graph with some values
			    gg.depth = 125;
			    gg.width = 125;
			    gg.center.y = 110;
			    gg.center.x = 108.5f;
			    // Updates internal size from the above values
			    gg.UpdateSizeFromWidthDepth();	
			    // Scans all graphs, do not call gg.Scan(), that is an internal method
			    AstarPath.active.Scan();			
			    break;
		    case bossState.finish:
			    cameraroot.GetComponent<CameraFollow>().RestoreMargins();
			    gg.depth = 100;
			    gg.width = 100;
			    gg.center.y = 50;
			    gg.center.x = 50;
			    // Updates internal size from the above values
			    gg.UpdateSizeFromWidthDepth();	
			    player.dynamicGridPlayer.SetActive (false);
			    break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		switch(currentState)
		{
		    case bossState.wait:
			    break;
		    case bossState.fight:
			    goldilock.UpdateMonsterGeneric();
                for (int i = 0; i < furnitures.Length; i++)
                {
                    labyrhinthPlants[i].GetComponent<mud>().UpdateEvent();
                    labyrhinthPlants[i].GetComponent<MonsterGeneric>().UpdateMonsterGeneric();
                }
                for (int i = 0; i < labyrhinthPlantsBackup.Length; i++)
                {
                    labyrhinthPlantsBackup[i].GetComponent<mud>().UpdateEvent();
                    labyrhinthPlantsBackup[i].GetComponent<MonsterGeneric>().UpdateMonsterGeneric();
                }
                switch (phase)
			    {
			        case monsterPhase.phase1:
			        case monsterPhase.phase2:
			        case monsterPhase.phase3:
				        if(velocity != Vector3.zero)
				        {
                            furnitureTransform.position += velocity * GameController.deltaTime;                            
                            switch (directionToMove)
					        {
					        default:
					        case MonsterGeneric.directions.U:
						        if(furnitureTransform.position.y > distanceToMove)
						        {
							        velocity = Vector3.zero;
                                    furnitureTransform.position = new Vector3(furnitureTransform.position.x,distanceToMove, furnitureTransform.position.z);
							        for (int i = 0; i < 127; i++) {
                                        if(furnitures[i].position.x == 1)
								        {
									        labyrhinthPlants[i].transform.position = new Vector3(labyrhinthPlants[i].transform.position.x,
                                                                                                 initialY - StageController.adjustY * (19)
									                                                             ,labyrhinthPlants[i].transform.position.z);
								        }
							        }
						        }
						        break;
					        case MonsterGeneric.directions.D:
						        if(furnitureTransform.position.y < distanceToMove)
						        {			
							        velocity = Vector3.zero;
							        furnitureTransform.position = new Vector3(furnitureTransform.position.x,distanceToMove,furnitureTransform.position.z);
							        for (int i = 0; i < 127; i++) {
								        if(furnitures[i].position.x == 20)
								        {
									        labyrhinthPlants[i].transform.position = new Vector3(labyrhinthPlants[i].transform.position.x,
                                                                                                 initialY - StageController.adjustY * (0)
                                                                                                 , labyrhinthPlants[i].transform.position.z);

                                        }
							        }
						        }
						        break;
					        case MonsterGeneric.directions.L:
						        if(furnitureTransform.position.x < distanceToMove)
						        {
							        velocity = Vector3.zero;
							        furnitureTransform.position = new Vector3(distanceToMove,furnitureTransform.position.y,furnitureTransform.position.z);
							        for (int i = 0; i < 127; i++) {
								        if(furnitures[i].position.y == 20)
								        {
									        labyrhinthPlants[i].transform.position = new Vector3(initialX + StageController.adjustX * (19),
									                                                             labyrhinthPlants[i].transform.position.y
									                                                             ,labyrhinthPlants[i].transform.position.z);									
								        }
							        }
						        }
						        break;
					        case MonsterGeneric.directions.R:
						        if(furnitureTransform.position.x > distanceToMove)
						        {
							        velocity = Vector3.zero;
							        furnitureTransform.position = new Vector3(distanceToMove,furnitureTransform.position.y,furnitureTransform.position.z);
							        for (int i = 0; i < 127; i++)
                                    {
                                        if (furnitures[i].position.y == 1)
                                        {
                                            labyrhinthPlants[i].transform.position = new Vector3(initialX - StageController.adjustX * (0),
                                                                                                    labyrhinthPlants[i].transform.position.y,
                                                                                                    labyrhinthPlants[i].transform.position.z);
                                        }
                                    }
						        }
						        break;
					        }
					        if(velocity == Vector3.zero)
					        {
						        for (int i = 0; i < 20; i++) {
							        labyrhinthPlantsBackup[i].SetActive (false);
						        }
						        if(currentPhase3StraightMove == 0)
							        AstarPath.active.Scan();
						        //RepositionPressureLevers();
						        stopPlayerfromGettingInside.SwitchOffStopPlayerfromGettingInside ();
					        }
				        }
				        else
				        {
					        currentTimerToSummonMonsters -= GameController.deltaTime;
					        if(currentTimerToSummonMonsters < 0 && phase != monsterPhase.phase3 && gameController.CountNumberOfMonstersInTheRoom(MonsterGeneric.monsterCategory.crowd2) < limitOfCrowds)
					        {
						        currentTimerToSummonMonsters = timerToSummonMonsters;
						        SummonCrowds();
					        }
					        currentTimerToTriggerMove -= GameController.deltaTime;
					        if(currentTimerToTriggerMove < 0)
					        {
						        currentTimerToTriggerMove = phase == monsterPhase.phase3 ? phase3TriggerMove : triggerMove;
						        if(currentPhase3StraightMove != 0)
							        MoveButtonPressed (directionToMove);
						        else
						        {
							        switch (Random.Range(0,4))
                                    {
							        case 0:
								        MoveButtonPressed (MonsterGeneric.directions.U);
								        break;
							        case 1:
								        MoveButtonPressed (MonsterGeneric.directions.D);
								        break;
							        case 2:
								        MoveButtonPressed (MonsterGeneric.directions.R);
								        break;
							        case 3:
								        MoveButtonPressed (MonsterGeneric.directions.L);
								        break;
							        }
						        }
						        if(phase == monsterPhase.phase3)
						        {
							        currentPhase3StraightMove++;
							        if(currentPhase3StraightMove >= phase3NumberOfStraightMoves)
								        currentPhase3StraightMove = 0;
							        else
								        currentTimerToTriggerMove = 0;
						        }
					        }
				        }
				    break;			
			    }
			    break;
		    }
	}

	void SummonCrowds()
	{		
		List<Transform> list = gameController.SummonACrowd (minSpot,maxSpot);
		for (int i = 0; i < list.Count; i++) {
			list[i].GetComponent<CharacterController>().enabled = true;
			gameController.addMonster(list[i].gameObject);
		}
	}
	
	void UpdateSpots()
	{
		minSpot = new Vector2(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,cenarioLimits.position.y - cenarioLimits.lossyScale.y/2);
		maxSpot = new Vector2(cenarioLimits.position.x + cenarioLimits.lossyScale.x/2,cenarioLimits.position.y + cenarioLimits.lossyScale.y/2);
	}

	public void MoveButtonPressed(MonsterGeneric.directions direction)
	{
//		U=1
//		D=2
//		L=3
//		R=4
		if(velocity != Vector3.zero)
			return;
		directionToMove = direction;
		int j = 0;
		switch(direction)
		{
		    default:
		    case MonsterGeneric.directions.U:
			    velocity = Vector3.up * (phase == monsterPhase.phase3 ? moveSpeedPhase3 : moveSpeed);
			    distanceToMove = furnitureTransform.position.y + StageController.adjustY;
                for (int i = 0; i < 127; i++) {
				    furnitures[i].position.x++;
				    if(furnitures[i].position.x == 21)
				    {	                    				
					    labyrhinthPlantsBackup[j].transform.position = new Vector3(labyrhinthPlants[i].transform.position.x,
                                                                             initialY - StageController.adjustY * (furnitures[i].position.x-1)
					                                                         ,labyrhinthPlants[i].transform.position.z);
                        labyrhinthPlantsBackup[j].SetActive (true);                    
                        j++;
                        furnitures[i].position.x = 1;
                    }				
			    }
                break;
		    case MonsterGeneric.directions.D:
			    velocity = Vector3.down * (phase == monsterPhase.phase3 ? moveSpeedPhase3 : moveSpeed);
			    distanceToMove = furnitureTransform.position.y - StageController.adjustY; 
			    for (int i = 0; i < 127; i++) {
				    furnitures[i].position.x--;
				    if(furnitures[i].position.x == 0)
				    {	                    				
					    labyrhinthPlantsBackup[j].transform.position = new Vector3(labyrhinthPlants[i].transform.position.x,
                                                                             initialY - StageController.adjustY * (-1)
					                                                         ,labyrhinthPlants[i].transform.position.z);
                        labyrhinthPlantsBackup[j].SetActive (true);                    
                        j++;
                        furnitures[i].position.x = 20;
                    }				
			    }
			    break;
		    case MonsterGeneric.directions.L:
			    velocity = Vector3.left * (phase == monsterPhase.phase3 ? moveSpeedPhase3 : moveSpeed);
			    distanceToMove = furnitureTransform.position.x - StageController.adjustX; 
			    for (int i = 0; i < 127; i++) {
				    furnitures[i].position.y--;
				    if(furnitures[i].position.y == 0)
				    {					
					    labyrhinthPlantsBackup[j].transform.position = new Vector3(initialX + StageController.adjustX * (20),
					                                                               labyrhinthPlants[i].transform.position.y
					                                                               ,labyrhinthPlants[i].transform.position.z);
					    labyrhinthPlantsBackup[j].SetActive (true);
                        furnitures[i].position.y = 20;
                        j++;
				    }				
			    }
			    break;
		    case MonsterGeneric.directions.R:
			    velocity = Vector3.right * (phase == monsterPhase.phase3 ? moveSpeedPhase3 : moveSpeed);
			    distanceToMove = furnitureTransform.position.x + StageController.adjustX; 
			    for (int i = 0; i < 127; i++) {
				    furnitures[i].position.y++;
				    if(furnitures[i].position.y == 21)
				    {					
					    labyrhinthPlantsBackup[j].transform.position = new Vector3(initialX + StageController.adjustX * (-1), 
					                                                               labyrhinthPlants[i].transform.position.y,
					                                                               labyrhinthPlants[i].transform.position.z);
					    labyrhinthPlantsBackup[j].SetActive (true);
                        furnitures[i].position.y = 1;
                        j++;
				    }				
			    }
			    break;
		}
		/*for (int i = 0; i < labyrhinthPlants.Length; i++) 
		{
			labyrhinthPlants[i].GetComponent<PushEntityoutOfMe>().SetPushDirection(velocity);
		}
		for (int i = 0; i < labyrhinthPlantsBackup.Length; i++) 
		{
			labyrhinthPlantsBackup[i].GetComponent<PushEntityoutOfMe>().SetPushDirection(velocity);
		}
		stopPlayerfromGettingInside.SwitchOnStopPlayerfromGettingInside (direction,velocity);*/
	}

	Vector3 FindMeANewSpot()
	{
		Vector3 temp;
		temp = new Vector3(Random.Range(minSpot.x,maxSpot.x),Random.Range(minSpot.y,maxSpot.y),0);	
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,temp - upTemp);
		RaycastHit hit;
		int count = 0;
		if(Physics.SphereCast (ray,2f,out hit, 100)){			
			while(hit.collider.tag != Tags.Floor)
			{			
				count++;
				temp = new Vector3(Random.Range(minSpot.x,maxSpot.x),Random.Range(minSpot.y,maxSpot.y),0);	
				upTemp = temp + new Vector3(0,0,-10);
				ray = new Ray(upTemp,temp - upTemp);
				Debug.DrawRay(upTemp,temp - upTemp,Color.white,5);
				Physics.SphereCast (ray,2f,out hit, 100);
				if(count == 100)
					return Vector3.one * 100;
			}
		}
		return temp;
	}
	public int distancePositionToChooseTrent = 5;
	public GameObject Trent;
	int closerToPlayer = 0;
	public void SummonATrent()
	{
        /*		SortedList distanteToPlayerList = new SortedList ();       
                for (int i = 0; i < labyrhinthPlants.Length; i++) {
                    distanteToPlayerList.Add (Vector2.Distance (labyrhinthPlants[i].transform.position,player.transform.position),i);
                }
                closerToPlayer = (int)distanteToPlayerList.GetByIndex (distancePositionToChooseTrent);*/

        List<float> distanteToPlayerList = new List<float>();
        for (int i = 0; i < labyrhinthPlants.Length; i++)
        {
            distanteToPlayerList.Add(Vector2.Distance(labyrhinthPlants[i].transform.position, player.transform.position));
        }
        List<float> aux = new List<float>(distanteToPlayerList);
        distanteToPlayerList.Sort();
        for (int i = 0; i < labyrhinthPlants.Length; i++)
        {
            if (aux[i] == distanteToPlayerList[distancePositionToChooseTrent])
            {
                closerToPlayer = i;
                break;
            }
        }        
        labyrhinthPlants [closerToPlayer].SetActive (false);
		GameObject temp = Instantiate(Trent,labyrhinthPlants[closerToPlayer].transform.position,Quaternion.identity) as GameObject;			
		temp.SendMessage (MnstMsgs.SetToughness,MonsterGeneric.toughness.one);
		temp.SendMessage ("SetLabyrinthPosition",closerToPlayer);
		gameController.addMonster (temp);	
		AstarPath.active.Scan();
    }

	public void TrentDied(int labyrinthPosition)
	{
		labyrhinthPlants [labyrinthPosition].SetActive (true);
	}

	/*
	void RepositionPressureLevers()
	{
		for (int i = 0; i < 4; i++) 
		{
			pressureLevers[i].collider.enabled = false;
			pressureLevers[i].transform.position = FindMeANewSpot ();
			pressureLevers[i].collider.enabled = true;
		}
	}*/
}
