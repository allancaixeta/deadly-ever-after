using UnityEngine;
using System.Collections;

public class TinSoldierArms : MonsterGeneric {

	public bool rightArm;
	TinSoldierController target;
	public bool chest = false;
	int originalArmor;
	public Sprite vulnerableArm;
	public override void SpecificMonsterAwake()
	{
		target = (TinSoldierController) FindObjectOfType(typeof(TinSoldierController));
		monsterType = monsterTypes.separatepiece;
		//gameObject.tag = "BossWeapon";
	}

	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		switch(currentState)
		{
		case MonsterStates.Start:
            transform.rotation = Quaternion.Euler(0, 0, 90);
            break;
		case MonsterStates.Death:
			if(!chest)
			{
				target.ArmDestroyed(rightArm);
			}
			else
			{
				target.nextState = MonsterStates.Death;
			}
			break;
		}
		return true;
	}

	public override void UpdateState()
	{
		Armor (); //handle breakable armor counter
		LifeRegen();
		if(chest && currentLife < maxLife *0.1f)
			target.StartPhase3();
	}

	void OnTriggerEnter(Collider other)
	{
		if(chest)
			return;
		if(other.transform.tag == "PlayerHit")
			target.PlayerOnArms(rightArm);
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);
	}

	public void StopArmor()
	{
		animator.SetTrigger ("armorbreak");
		GetComponent<SpriteRenderer> ().sprite = vulnerableArm;
	}

	protected override void OnMonsterHit()
	{
	}
}
