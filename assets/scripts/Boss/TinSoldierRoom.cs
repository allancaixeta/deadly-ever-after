using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public class TinSoldierRoom : BossRoomController {

	public GameObject tinsoldierPrefab;
	public float timerToSummonMonsters = 10;
	float currentTimerToSummonMonsters, originalorthographicSize;
	TinSoldierController tinsoldier;
	Vector2 maxSpot, minSpot;
	List<Transform> monsterList;
	GridGraph gg;
	Transform cameraroot;
	// Use this for initialization
	void Awake () {
		player = (PlayerController) FindObjectOfType(typeof(PlayerController));	
		gameController = GameController.Instance;	
		currentTimerToSummonMonsters = timerToSummonMonsters;
		monsterList = new List<Transform> ();
	}
	
	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case bossState.wait:

			break;
		case bossState.fight:
			tinsoldier = (Instantiate(tinsoldierPrefab,tinsoldierPrefab.transform.position,Quaternion.identity) as GameObject).GetComponent<TinSoldierController>();
			int tough = GameObject.Find ("StageController").GetComponent<StageController>().GetToughness();
			tinsoldier.SendMessage (MnstMsgs.SetToughness,tough);
			cenarioLimits = GameObject.FindGameObjectWithTag(Tags.Floor).transform;
			cameraroot = GameObject.Find ("CameraControl").transform;
			cameraroot.GetComponent<CameraFollow>().FixPosition(true);
			cameraroot.parent = tinsoldier.transform.FindChild ("cameraposition");
			cameraroot.localPosition = Vector3.zero;
			originalorthographicSize = Camera.main.orthographicSize;
			Camera.main.orthographicSize = 1.5f * Camera.main.orthographicSize;
			cameraroot.FindChild("Camera/HUD").transform.localScale = new Vector3(1.5f,1.5f,1);
			player.transform.position = cameraroot.position + Vector3.right * 10;			
			UpdateSpots();
			// This creates a Grid Graph
			gg = AstarPath.active.astarData.gridGraph;
			// Setup a grid graph with some values
			gg.width = 400;
			gg.center.x = 0;
			// Updates internal size from the above values
			gg.UpdateSizeFromWidthDepth();	
			// Scans all graphs, do not call gg.Scan(), that is an internal method
			AstarPath.active.Scan();
			break;
		case bossState.finish:
			Transform camerarootz = GameObject.Find ("CameraControl").transform;
			camerarootz.GetComponent<CameraFollow>().FixPosition(false);
			Camera.main.orthographicSize = originalorthographicSize;
			cameraroot.FindChild("Camera/HUD").transform.localScale = Vector3.one;
			gg.width = 80;
			gg.center.x = 50;
			// Updates internal size from the above values
			gg.UpdateSizeFromWidthDepth();	
			break;
		}
		return true;
	}

	public override void UpdateState()
	{
		switch(currentState)
		{
		case bossState.wait:
			break;
		case bossState.fight:
			tinsoldier.UpdateMonsterGeneric();
			switch(phase)
			{
			case monsterPhase.phase1:
				for (int i = 0; i < monsterList.Count; i++) {
					if(w4.transform.position.x+2 > monsterList[i].position.x)
					{
						monsterList[i].GetComponent<CharacterController>().enabled = true;
						gameController.addMonster(monsterList[i].gameObject);
						//monsterList[i].GetComponent<MonsterGeneric>().nextState = MonsterGeneric.monsterStates.start;
						monsterList.RemoveAt(i);
					}
				}
				currentTimerToSummonMonsters -= GameController.deltaTime;
				if(currentTimerToSummonMonsters < 0)
				{
					currentTimerToSummonMonsters = timerToSummonMonsters;
					SummonCrowds();
				}
				break;
			case monsterPhase.phase2:
				break;
			}
			break;
		}
	}

	void SummonCrowds()
	{

		UpdateSpots();
		List<Transform> list = gameController.SummonACrowd (minSpot,maxSpot);
		for (int i = 0; i < list.Count; i++) {
			monsterList.Add (list[i]);
		}
		/*int currentMonsterCategory = 2;//chaser,crowd,shooter,muscle
		int monsterNumberinArray = Random.Range(0,gameController.monsterCrowd.Length);
		GameObject monster = gameController.GetCC(monsterNumberinArray, false);
		int monterQuantity = Random.Range (monster.GetComponent<MonsterGeneric>().monsterMin,monster.GetComponent<MonsterGeneric>().monsterMax+1);
		UpdateSpots();
		for (int k = 0; k < monterQuantity; k++) {
			int count = 0;
			bool loop = true;
			Vector2 spot = Vector2.one;
			spot = new Vector2(Random.Range (minSpot.x,maxSpot.x),Random.Range (minSpot.y,maxSpot.y));
			Vector3 position = new Vector3(spot.x,spot.y,1.5f);
			GameObject temp;
			bool tainted = Random.value < (chanceOfMonsterBecomingTainted / 100);
			if(tainted)
			{
				GameObject monstertainted = gameController.GetCC(monsterNumberinArray, true);
				temp = Instantiate(monstertainted,position,Quaternion.identity) as GameObject;
			}
			else
				temp = Instantiate(monster,position,Quaternion.identity) as GameObject;
			int tough = GetToughness();
			if(tainted)
				tough++;
			if(tough > 5)
				tough = 5;
			if(tough < 1)
				tough = 1;
			temp.SendMessage (MnstMsgs.SetToughness,tough);
			gameController.addMonster(temp);
			temp.GetComponent<MonsterGeneric>().nextState = MonsterGeneric.monsterStates.VOID;
			temp.GetComponent<CharacterController>().enabled = false;
			monsterList.Add (temp.transform);
		}*/
	}

	GameObject w1,w3,w4;

	void UpdateSpots()
	{

		w1 = GameObject.Find ("wall1");
		w3 = GameObject.Find ("wall3");
		w4 = GameObject.Find ("wall4");
        minSpot = new Vector2(w4.transform.position.x + 30, w1.transform.position.y + 2);
        maxSpot = new Vector2(w4.transform.position.x + 2, w3.transform.position.y - 2);
    }
}
	