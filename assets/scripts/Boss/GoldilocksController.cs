﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class GoldilocksController : MonsterGeneric {

	public enum modes
	{
		haslineofsight,
		donothaslineofsight,
		waitingforprey
	}
	modes currentMode;
	public float speedIncreaseWithRunningTime = 0.1f;
	float currentTimeRunning;
	public float timeNecessaryToLoosePlayer = 2;
    public float distanceNecessaryToLoosePlayer = 15;
    public float attackDistanceWhenWaitingForPrey = 5;
	public int ammountOfShots = 3;
	int currentShot;
	public float intervalBetweenShootsPhase1 = 1f;
	public float intervalBetweenShootsPhase2 = 0.5f;
	public GameObject projectile;
	public float timeToSummonTrent = 10;
	float currentTimeNecessaryToLoosePlayer, currentTimeToSummonTrent, currentTimeToLeaveATrap, currentTimeToLeaveAHole;
	public float phase2PercentageLife = 0.4f;
	public GameObject trap, hole, radar;
    public AudioClip radarLaugh;
	public float phase2TimeNecessaryToLoosePlayer = 2;
	public float timeToLeaveATrap = 10;
	public float trapSlowValue = 20;
	public float trapSlowTime = 3;
	public float timeToLeaveAHole = 15;
	public float phase3PercentageLife = 0.15f;
	public Vector2 phase3Position;
	public float phase3ShootingCooldown = 0.5f;
	public float angleSpeed = 11;
	float currentPhase3ShootingCooldown, currentAngleSpeed;
	float angleOfLastShot;
	BossRoomController bossRoom;
	BossRoomController.monsterPhase phase;
	Transform cenarioLimits, goalPosition;
	bool stopped = false;
	float maxStoppedTime = 2;
	float currentStoppedTime;
	Vector3 lastPosition;
	private float distancefromGoal;
	Seeker seeker;
	//The calculated path
	public Path path;
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	
	public float repathRate = 0.5f;
	private float lastRepath = -9999;
	float radiusCollider;
	LineOfSight lineOfSight;
	bool invalidPath;
	public float pathRecalcTime = 1;
	float pathRecalcTimer;
    SpriteRenderer eye;
    public LerpEquationTypes lerpEye;
    public override void SpecificMonsterAwake()
	{
		bossRoom = (BossRoomController) FindObjectOfType(typeof(BossRoomController));
		gameObject.name = "Boss";
		monsterType = monsterTypes.boss;
		seeker = GetComponent<Seeker>();
		goalPosition = new GameObject().transform;
		radiusCollider = GetComponent<CharacterController>().radius;
		lineOfSight = GetComponent<LineOfSight>();
		if (!lineOfSight) {
			gameObject.AddComponent <LineOfSight>();
			lineOfSight = GetComponent<LineOfSight>();
		}
	}

	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			StageController stage = (StageController) FindObjectOfType(typeof(StageController));	
			cenarioLimits = stage.GetRoom().GetCenarioLImits ();
			CreateNewGoal ();
			invalidPath = false;
			currentTimeRunning = 0;
			animator.SetBool ("invisible",true);
			tag = Tags.StunMonster;
			nextState = MonsterStates.Normal;
			currentTimeRunning = 50;
			currentMode = modes.donothaslineofsight;
			currentTimeNecessaryToLoosePlayer = 0;
            eye = visual.transform.Find("eye").GetComponent< SpriteRenderer>();
            eye.gameObject.SetActive(false);
            break;
		case MonsterStates.Normal:
			currentTimeToLeaveATrap = timeToLeaveATrap;
			currentTimeToLeaveAHole = timeToLeaveAHole;
			break;
		case MonsterStates.Preparation:
			GetOutOfObject ();
			animator.SetBool ("invisible",false);
			tag = Tags.BossMonster;
			nextState = MonsterStates.Attack;
			break;
		case MonsterStates.Attack:
			attackCountDown = phase == BossRoomController.monsterPhase.phase1 ? intervalBetweenShootsPhase1 : intervalBetweenShootsPhase2;
			currentShot = ammountOfShots;
			break;
		case MonsterStates.Death:
			bossRoom.nextState = BossRoomController.bossState.finish;
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		switch(currentState)
		{
		    case MonsterStates.Normal:
			    FixZ ();
			    switch(phase)
			    {
			        default:
			        case BossRoomController.monsterPhase.phase1:
			        case BossRoomController.monsterPhase.phase2:
				        switch(currentMode)
				        {
				            case modes.haslineofsight:
					            Velocity ();
					            Move ();
					            if(invalidPath)
					            {
						            pathRecalcTimer -= GameController.deltaTime;
						            if(pathRecalcTimer < 0)
						            {
							            invalidPath = false;
							            CreateNewGoal ();
							            pathRecalcTimer = 1;
						            }
					            }
					            distancefromGoal = Vector2.Distance (transform.position,goalPosition.position);
					            currentTimeRunning += GameController.deltaTime;
					            if(distancefromGoal < 3)
					            {
						            CreateNewGoal ();
					            }
                                if (!lineOfSight.hasLineOfSight && Vector2.Distance(player.position,transform.position) > distanceNecessaryToLoosePlayer)
                                {
                                    currentTimeNecessaryToLoosePlayer -= GameController.deltaTime;
                                    if (currentTimeNecessaryToLoosePlayer < 0)
                                    {
                                        currentMode = modes.donothaslineofsight;
                                        invalidPath = false;
                                        currentTimeRunning = 0;
                                        animator.SetBool("invisible", true);
                                        tag = Tags.StunMonster;
                                        eye.gameObject.SetActive(false);
                                    }
                                }
                                else
                                {
                                    currentTimeNecessaryToLoosePlayer = phase == BossRoomController.monsterPhase.phase2 ? phase2TimeNecessaryToLoosePlayer : timeNecessaryToLoosePlayer;
                                }
                                Color tmp = eye.color;
                                tmp.a = lerpEye.Lerp(0,1,1 - currentTimeNecessaryToLoosePlayer / (phase == BossRoomController.monsterPhase.phase2 ? phase2TimeNecessaryToLoosePlayer : timeNecessaryToLoosePlayer));
                                eye.color = tmp;
                                if (phase == BossRoomController.monsterPhase.phase2)
					            {
						            currentTimeToLeaveATrap -= GameController.deltaTime;
						            if(currentTimeToLeaveATrap < 0)
						            {
							            GameObject temp = Instantiate(trap,transform.position,Quaternion.identity) as GameObject;
							            //temp.GetComponent<DefaultProjectile>().SetSlowWhenHit (trapSlowValue, trapSlowTime);		
							            temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
							            gameController.addMonster (temp);	
							            currentTimeToLeaveATrap = timeToLeaveATrap;
						            }
					            }
					            break;

				            case modes.donothaslineofsight:
					            Velocity ();
					            Move ();
					            distancefromGoal = Vector2.Distance (transform.position,goalPosition.position);
					            if(distancefromGoal < stopMovingIfCloserThenThisDistance)
					            {
						            currentMode = modes.waitingforprey;
						            currentTimeToSummonTrent = timeToSummonTrent;
						            currentTimeRunning = 0;
						            currentTimeNecessaryToLoosePlayer = phase == BossRoomController.monsterPhase.phase2 ? phase2TimeNecessaryToLoosePlayer : timeNecessaryToLoosePlayer;
					            }
					            break;

				            case modes.waitingforprey:
					            currentTimeToSummonTrent -= GameController.deltaTime;
					            if(currentTimeToSummonTrent < 0)
					            {
						            bossRoom.GetComponent<GoldilocksRoom>().SummonATrent();
						            currentTimeToSummonTrent = timeToSummonTrent;
                                    Instantiate(radar, transform.position, Quaternion.identity);
                                    gameController.MonsterAudioSource.PlayOneShot(radarLaugh);
                                }
					            distancefromGoal = Vector2.Distance (transform.position,player.position); //the goal in these case is the player
					            if(distancefromGoal < attackDistanceWhenWaitingForPrey)
					            {
						            nextState = MonsterStates.Preparation;
					            }
            //					if(phase == BossRoomController.monsterPhase.phase2)
            //					{
            //						currentTimeToLeaveAHole -= GameController.deltaTime;
            //						if(currentTimeToLeaveAHole < 0)
            //						{
            //							GameObject temp = Instantiate(hole,transform.position,Quaternion.identity) as GameObject;
            //							temp.SendMessage("StartEvent",StageController.StageNumber.stage1);
            //							temp.SendMessage ("SetdontUsePositiveEffect");
            //							gameController.addEvent (temp);
            //							temp.transform.position = transform.position;
            //							currentTimeToLeaveAHole = timeToLeaveAHole;
            //						}
            //					}
					            break;
				        }

				        break;			
			        case BossRoomController.monsterPhase.phase3:
				        currentPhase3ShootingCooldown -= GameController.deltaTime;
				        if(currentPhase3ShootingCooldown < 0)
				        {
					        for (int i = 0; i < 8; i++) {
						        Shoot(currentAngleSpeed + 45*i);
					        }
					        currentAngleSpeed += angleSpeed;
					        currentPhase3ShootingCooldown = phase3ShootingCooldown;
				        }
				        break;
			        }
			    break;

		    case MonsterStates.Preparation:
			    nextState = MonsterStates.Attack;
			    break;

		    case MonsterStates.Attack:
			    attackCountDown -= GameController.deltaTime;
			    if(attackCountDown < 0)
			    {
				    angleOfLastShot = ShootAtPlayer();
				    Shoot (angleOfLastShot);
				    attackCountDown = phase == BossRoomController.monsterPhase.phase1 ? intervalBetweenShootsPhase1 : intervalBetweenShootsPhase2;
				    currentShot--;
				    if(currentShot <= 0)
				    {
					    nextState = MonsterStates.Normal;
					    currentMode = modes.haslineofsight;
                        eye.gameObject.SetActive(true);
                        currentTimeRunning = 0;
					    currentTimeNecessaryToLoosePlayer = phase == BossRoomController.monsterPhase.phase2 ? phase2TimeNecessaryToLoosePlayer : timeNecessaryToLoosePlayer;
				    }
			    }
			    break;
			
		    case MonsterStates.Stun:
			    Stun ();
			    break;
			
		    case MonsterStates.Death:
			    Death ();
			    break;
		}
	}

	Vector3 temp;
	void CreateNewGoal()
	{
		temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
		                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
		                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
		             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,temp - upTemp);
		RaycastHit hit;
		int count = 0;
		if(Physics.SphereCast (ray,radiusCollider,out hit, 100)){			
			while(hit.collider.tag != Tags.Floor)
			{			
				count++;
				temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
				                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
				upTemp = temp + new Vector3(0,0,-10);
				ray = new Ray(upTemp,temp - upTemp);
				Debug.DrawRay(upTemp,temp - upTemp,Color.white,5);
				Physics.SphereCast (ray,radiusCollider,out hit, 100);
				if(count == 10)
				{
					goalPosition.position = temp;		
					lastRepath = Time.time+ repathRate;
					seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);
					return;
				}
			}
		}
		goalPosition.position = temp;		
		lastRepath = Time.time+ repathRate;
		seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);
	}
	
	public void OnPathComplete (Path p) {
		p.Claim (this);
		if (!p.error) {
			if (path != null) path.Release (this);
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
			for (int i = currentWaypoint; i < path.vectorPath.Count; i++) {
				if (Vector2.Distance (player.position,path.vectorPath[i]) < stopMovingIfCloserThenThisDistance) 
				{
					invalidPath = true;
					return;
				}
			}
		} else {
			p.Release (this);
			Debug.Log ("Oh noes, the target was not reachable: "+p.errorLog);
		}
	}

	void Velocity()
	{  
		Vector2 directionToPlayer = player.position - transform.position;
		if (Time.time - lastRepath > repathRate && seeker.IsDone()) {
			lastRepath = Time.time+ repathRate;
			seeker.StartPath (transform.position,goalPosition.position, OnPathComplete);				
		}
		if (path == null || currentWaypoint >= path.vectorPath.Count) {
			velocity = goalPosition.position - transform.position;
			if (Vector2.Distance (velocity.normalized,directionToPlayer.normalized) < stopMovingIfCloserThenThisDistance/4 && currentMode == modes.haslineofsight) 
			{
				velocity = transform.position - player.position;
			}
		}         
		else
		{
			if(invalidPath)
			{
				velocity = transform.position - player.position;
				invalidPath = false;
				for (int i = currentWaypoint; i < path.vectorPath.Count; i++) {
					if (Vector2.Distance (player.position,path.vectorPath[i]) < stopMovingIfCloserThenThisDistance * 1.5f) 
					{
						invalidPath = true;
						break;
					}
				}	
			}
			else
			{
				//Direction to the next waypoint
				velocity = (path.vectorPath[currentWaypoint]-transform.position);			
				if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) 
					currentWaypoint++;
				if(currentMode == modes.haslineofsight)
				{
					for (int i = currentWaypoint; i < path.vectorPath.Count; i++) {
						if (Vector2.Distance (player.position,path.vectorPath[i]) < stopMovingIfCloserThenThisDistance) 
						{
							invalidPath = true;
							break;
						}
					}	
				}
			}
		}
		velocity.Normalize();
		velocity *= GameController.deltaTime * (speed + speedIncreaseWithRunningTime * currentTimeRunning) * (timeDistortion-magicSlowEffect);
	}
	
	void Move()
	{	
		controller.Move(velocity);           
	}

	float ShootAtPlayer()
	{
		float angle =  Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		return angle;
	}

	void Shoot(float angle)
	{
		GameObject temp = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;			
		temp.SendMessage (MnstMsgs.SetToughness,monsterToughness);
		temp.SendMessage ("SetDirection",angle);
		gameController.addMonster (temp);		
	}

	protected override void OnMonsterHit()
	{
		if(currentState == MonsterStates.Normal && currentMode != modes.haslineofsight)
			nextState = MonsterStates.Preparation;
		switch(phase)
		{
		case BossRoomController.monsterPhase.phase1:
		case BossRoomController.monsterPhase.phase2:
			if(currentLife < maxLife * phase2PercentageLife)
			{
				phase = BossRoomController.monsterPhase.phase2;		
				bossRoom.SetPhase (BossRoomController.monsterPhase.phase2);
			}
			if(currentLife < maxLife * phase3PercentageLife)
			{
				phase = BossRoomController.monsterPhase.phase3;
				currentPhase3ShootingCooldown = phase3ShootingCooldown;
				transform.position = new Vector2(phase3Position.x,phase3Position.y);
				bossRoom.SetPhase (BossRoomController.monsterPhase.phase3);
				controller.enabled = false;
			}
			break;
		}
	}

	void OnDestroy () {
		if(goalPosition)
			Destroy(goalPosition.gameObject);
	}

}
