using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TinSoldierController : MonsterGeneric {

	public enum attacks
	{
		hitarmL,
		hitarmR,
		groundattackL,
		groundattackR,
		groundattackBoth,
		laserbeginL,
		laserbeginR,
		laserbeginBoth,
		lasermidL,
		lasermidR,
		lasermidBoth,
		laserfarL,
		laserfarR,
		laserfarBoth,
		laserheart,
		charge,
		summon
	}

	MonsterGeneric handR, handL, chest;
	public GameObject destroyedbridgeFX;
	public Vector2 destroyingFXDistances; //x = initial X, y = distance from each effect
	List<string> monsterHitListID;
	List<float> monsterHitListDuration;
	public float damageOnMonster;
	public float damageOnPlayer;
	public float stunTimeForLosingArm = 0.5f;
	PlayerHitController playerhit;
	BossRoomController bossRoom;
	attacks attack;
	GameObject heart;

	public float timerToSpecial = 5;
	public float prepareTime = 0.5f;
	float currentPrepareTime;
	bool walking, armL, armR;
	public float endofPhase1, walkingTime, stopTime, groundAttackDuration, groundAttackTrigger, laserDuration,laserTrigger, chargeDuration, summonDuration, summonTrigger, chargeSpeed;
	bool specialTriggered, stunForArmDestroyed;
	float originalSpeed, originalY;
	public float chargeEffecteachY = 5;
	BoxCollider b;
	BossRoomController.monsterPhase phase;
	public GameObject stonesWithHole, stonesDefault;
	GameObject [] currentStones = new GameObject[2];
	public GameObject laserWeapon, laserheartWeapon;

	public override void SpecificMonsterAwake()
	{
		playerhit = PlayerHitController.Instance;
		bossRoom = (BossRoomController) FindObjectOfType(typeof(BossRoomController));
		monsterHitListID = new List<string>();
		monsterHitListDuration = new List<float>();
		originalSpeed = chargeSpeed;
		b = GetComponent<BoxCollider>();
        gameObject.name = "Boss";
		gameObject.tag = "BossWeapon";
		monsterType = monsterTypes.boss;
	}

	public override bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || currentState == MonsterStates.Death)
			return false;
		previousState = currentState;
		currentState = nextState;		
		switch(currentState)
		{
		case MonsterStates.Start:
			gameObject.name = "Boss";
			nextState = MonsterStates.Normal;
			handR = transform.FindChild("visual/base/tinsoldierarmR").GetComponent<MonsterGeneric>();
			handR.SetToughness (toughnessInNumber);
			handL = transform.FindChild("visual/base/tinsoldierarmL").GetComponent<MonsterGeneric>();
			handL.SetToughness (toughnessInNumber);
			chest = transform.FindChild("visual/base/chest").GetComponent<MonsterGeneric>();
			chest.SetToughness (toughnessInNumber);
			heart = transform.FindChild("visual/base/chest/heart").gameObject;
			chest.GetComponent<Collider>().enabled = false;
			chest.SetInvulnerability (true);
			handR.SetInvulnerability (true);
			handL.SetInvulnerability (true);
			heart.SetActive (false);
			//handR.SetToughness (toughness.one);
			///handL.SetToughness (toughness.one);
			//chest.SetToughness (toughness.one);
			StartPhase1();
			break;
		case MonsterStates.Normal:
			attackCountDown = timerToSpecial;
			chargeSpeed = 1;
			if(stunForArmDestroyed)
			{
				stunForArmDestroyed = false;
				StunBoss();
			}
			else
			{
				if(phase == BossRoomController.monsterPhase.phase1)
					animator.SetBool ("walk",true);
			}
			break;
		case MonsterStates.Attack:
			currentPrepareTime = prepareTime;
			specialTriggered = false;
			animator.SetBool ("walk",false);
			switch(attack)
			{
			case attacks.hitarmL:
				currentPrepareTime = -1;
				attackCountDown = 1f;
				animator.SetTrigger ("hitarmL");
				break;
			case attacks.hitarmR:
				currentPrepareTime = -1;
				attackCountDown = 1f;
				animator.SetTrigger ("hitarmR");
				break;
			case attacks.groundattackBoth:
			case attacks.groundattackL:
			case attacks.groundattackR:
				attackCountDown = groundAttackDuration;
				animator.SetTrigger ("groundpunch");
				break;
			case attacks.laserbeginBoth:
			case attacks.laserbeginL:
			case attacks.laserbeginR:
				attackCountDown = laserDuration;
				animator.SetBool ("laserbegin",true);
				break;
			case attacks.laserfarBoth:
			case attacks.laserfarL:
			case attacks.laserfarR:
				attackCountDown = laserDuration;
				animator.SetBool ("laserfar",true);
				break;
			case attacks.lasermidBoth:
			case attacks.lasermidL:
			case attacks.lasermidR:
				attackCountDown = laserDuration;
				animator.SetBool ("lasermid",true);
				break;
			case attacks.laserheart:
				attackCountDown = laserDuration;
				animator.SetBool ("laserheart",true);
				break;
			case attacks.summon:
				attackCountDown = summonDuration;
				animator.SetTrigger ("summon");
				break;
			case attacks.charge:
				attackCountDown = chargeDuration;
				originalY = transform.position.x + chargeEffecteachY;
				chargeSpeed = originalSpeed;
				animator.SetTrigger ("charge");
				break;
			}
			break;
		case MonsterStates.Death:
			bossRoom.nextState = BossRoomController.bossState.finish;
			animator.SetTrigger ("die");
			deathCountDown = afterLife;
			break;
		}
		return true;
	}
	
	public override void UpdateState()
	{
		CheckPlayerY();
		handR.UpdateMonsterGeneric ();
		handL.UpdateMonsterGeneric ();
		chest.UpdateMonsterGeneric ();
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
		switch(currentState)
		{
		case MonsterStates.Normal:
			switch(phase)
			{
			default:
			case BossRoomController.monsterPhase.phase1:
				Attacks();
				currentDesync -= GameController.deltaTime;
				if(walking)
				{
					Velocity ();
					Move ();
					if(currentDesync < 0)
					{
						walking = false;
						currentDesync = stopTime;
					}
				}
				else
				{
					if(currentDesync < 0)
					{
						walking = true;
						currentDesync = walkingTime;	
						if(attackCountDown > 0.25f)
							DestroyBridge();
					}
				}
				if(transform.position.x >= endofPhase1)
					StartPhase2();
				break;
			case BossRoomController.monsterPhase.phase2:
				Attacks();
				break;
			}
			break;
		case MonsterStates.Attack:
			currentPrepareTime -= GameController.deltaTime;
			if(currentPrepareTime < 0)
			{
				attackCountDown -= GameController.deltaTime;
				switch(attack)
				{
				case attacks.charge:
					Velocity ();
					Move ();
					if(transform.position.x >= endofPhase1)
					{	
						StartPhase2();
						nextState = MonsterStates.Normal;
						return;
					}
					if(transform.position.x < originalY - chargeEffecteachY)
					{
						DestroyBridge();
						originalY = transform.position.x;
					}
					break;
				case attacks.groundattackBoth:
				case attacks.groundattackL:
				case attacks.groundattackR:
					if(attackCountDown < groundAttackTrigger && !specialTriggered)
					{
						if(!armL)
							GroundAttack(0);
						if(!armR)
							GroundAttack(1);
						specialTriggered = true;
						GameObject.Find ("CameraControl").GetComponent<CameraFollow>().ShakeCamera (0.5f,1);
					}
					if(specialTriggered)
					{
						if(!armL)
							currentStones[0].GetComponent<EventController>().UpdateEvent();
						if(!armR)
							currentStones[1].GetComponent<EventController>().UpdateEvent();
					}
					if(attackCountDown < 0)
					{	
						if(!armL)
							currentStones[0].GetComponent<EventController>().StopEvent ();
						if(!armR)
							currentStones[1].GetComponent<EventController>().StopEvent ();
					}
					break;
				case attacks.summon:
					if(attackCountDown < summonTrigger && !specialTriggered)
					{
						SummonChaser();
						specialTriggered = true;
					}
					break;
				case attacks.laserbeginBoth:
				case attacks.laserbeginL:
				case attacks.laserbeginR:
				case attacks.lasermidBoth:
				case attacks.lasermidL:
				case attacks.lasermidR:
				case attacks.laserfarBoth:
				case attacks.laserfarL:
				case attacks.laserfarR:
					if(attackCountDown < laserTrigger && !specialTriggered)
					{
						if(!armL)
							Laser(0);
						if(!armR)
							Laser(1);
						specialTriggered = true;
					}
					if(attackCountDown < 0)
					{
						switch(attack)
						{
						default:
						case attacks.laserbeginBoth:
						case attacks.laserbeginL:
						case attacks.laserbeginR:
							animator.SetBool ("laserbegin",false);
							break;
						case attacks.lasermidBoth:
						case attacks.lasermidL:
						case attacks.lasermidR:
							animator.SetBool ("lasermid",false);
							break;
						case attacks.laserfarBoth:
						case attacks.laserfarL:
						case attacks.laserfarR:
							animator.SetBool ("laserfar",false);
							break;
						}
						if(!armL)
							if(laserprojectile[0])
								laserprojectile[0].SendMessage ("DestroyItSelf");
						if(!armR)
							if(laserprojectile[1])
								laserprojectile[1].SendMessage ("DestroyItSelf");
					}
					else
					{
						if(specialTriggered)
						{
							Vector3 pos;
							if(!armL)
							{
								pos = GameObject.Find ("laserL").transform.position;
								laserprojectile[0].transform.position = new Vector3(pos.x,pos.y,1.5f);
							}
							if(!armR)
							{
								pos = GameObject.Find ("laserR").transform.position;
								laserprojectile[1].transform.position = new Vector3(pos.x,pos.y,1.5f);
							}
						}
					}
					break;
				case attacks.laserheart:
					if(attackCountDown < laserTrigger && !specialTriggered)
					{
						LaserHeart();
						specialTriggered = true;
					}
					if(attackCountDown < 0)
					{
						animator.SetBool ("laserheart",false);
						if(laserprojectile[0])
							laserprojectile[0].SendMessage ("DestroyItSelf");
					}
					break;
				}
			}
			if(attackCountDown < 0)
				nextState = MonsterStates.Normal;
			break;

		case MonsterStates.Stun:
			Stun ();
			break;

		case MonsterStates.Death:
			Death ();
			break;
		}
	}

	void Attacks()
	{
		attackCountDown -= GameController.deltaTime;
		if(attackCountDown < 0)
		{
			GetSpecialAttack ();
			nextState = MonsterStates.Attack;
		}
	}
	void Velocity()
	{
		velocity = Vector3.right;
		velocity *= GameController.deltaTime * speed * chargeSpeed;
	}

	void Move()	{
        transform.position += new Vector3(velocity.x, velocity.y, 0);
	}

	void CheckPlayerY ()
	{        
		if(player.transform.position.x < b.bounds.max.x)
			player.transform.position = new Vector3 (b.bounds.max.x,player.transform.position.y,player.transform.position.z);
	}

	void StartPhase1()
	{
		invulnerable = true;
		//handR.SetInvulnerability (true);
		//handL.SetInvulnerability (true);
		//chest.SetInvulnerability (true);
	}

	void StartPhase2()
	{
		phase = BossRoomController.monsterPhase.phase2;
		bossRoom.SetPhase (phase);
		//invulnerable = false;
		handR.SetInvulnerability (false);
		handR.SendMessage ("StopArmor");
		handL.SetInvulnerability (false);
		handL.SendMessage ("StopArmor");
		GameObject w2 = GameObject.Find ("wall2");
		Destroy (w2.GetComponent<Rigidbody>());
		AstarPath.active.Scan();
	}

	public void ArmDestroyed(bool rightArm)
	{
		if(rightArm)
		{
			armR = true;
			handR.gameObject.SetActive (false);
			animator.SetTrigger ("destroyarmR");
		}
		else
		{
			armL = true;
			handL.gameObject.SetActive (false);
			animator.SetTrigger ("destroyarmL");
		}
		if(armL && armR)
		{
			chest.GetComponent<Collider>().enabled = true;
			chest.SetInvulnerability (false);
			heart.SetActive (true);
		}
		if(CheckIfCurrentOrNextState(MonsterStates.Attack))
		{
			stunForArmDestroyed = true;
			attackCountDown = 0.1f;
		}
		else
		{
			StunBoss();
		}
	}

	public void StartPhase3()
	{
		heart.GetComponent<Animator>().SetTrigger("lowlife");
	}

	void OnTriggerEnter(Collider other)
	{
		switch(other.transform.tag)
		{
		case "PlayerHit":
			playerhit.HitPlayer(damageOnPlayer,monsterName);
			break;
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);
	}
	
	bool OnMonsterHit(string monsterID, float speed)
	{
		for (int i = 0; i < monsterHitListID.Count; i++){
			if(monsterHitListID[i] == monsterID)
				return true;
		}
		monsterHitListID.Add(monsterID);
		monsterHitListDuration.Add(speed);	
		return false;
	}

	public void PlayerOnArms(bool rightArm)
	{
		if(playerhit.HitPlayer(damageOnPlayer, "Tin Soldier Arms"))
		{
			if(!CheckIfCurrentOrNextState (MonsterStates.Attack))
			{
				nextState = MonsterStates.Attack;
				if(rightArm)
					attack = attacks.hitarmR;
				else
					attack = attacks.hitarmL;					
			}
		}
	}

	void DestroyBridge()
	{
		float y;
		for (int i = 0; i < 8; i++) {
			y = destroyingFXDistances.x + destroyingFXDistances.y * i;
			Instantiate (destroyedbridgeFX,new Vector3(transform.position.x,y,0),Quaternion.identity);
		}	
	}

	void GroundAttack(int side)
	{
		switch(phase)
		{
		case BossRoomController.monsterPhase.phase1:
			currentStones[side] = Instantiate (stonesWithHole,this.transform.position,Quaternion.identity) as GameObject;
			currentStones[side].transform.parent = transform;
			currentStones[side].SendMessage ("SetSide",side);
			currentStones[side].GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
			break;
		default:
			currentStones[side] = Instantiate (stonesDefault,this.transform.position,Quaternion.identity) as GameObject;
			currentStones[side].transform.parent = transform;
			currentStones[side].SendMessage ("SetSide",side);
			currentStones[side].GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
			break;
		}
		//AstarPath.active.Scan ();
	}

	GameObject [] laserprojectile = new GameObject[2];
	void Laser(int side)
	{
		Vector3 pos;
		if(side == 1)
			pos = GameObject.Find ("laserR").transform.position;
		else
			pos = GameObject.Find ("laserL").transform.position;
		laserprojectile[side] = Instantiate(laserWeapon,new Vector3(pos.x,pos.y,1.5f),Quaternion.identity) as GameObject;
		laserprojectile[side].transform.parent = transform;
		if(side == 1)
			laserprojectile[side].transform.localScale = new Vector3(-laserprojectile[side].transform.localScale.x,laserprojectile[side].transform.localScale.y,laserprojectile[side].transform.localScale.z);
		laserprojectile[side].SendMessage (MnstMsgs.SetToughness,1);		
		laserprojectile[side].SendMessage ("SetPassThrough");			
		gameController.addMonster (laserprojectile[side]);
			
	}

	void LaserHeart()
	{
		Vector3 pos;
		pos = GameObject.Find ("laserHeart").transform.position;
		laserprojectile[0] = Instantiate(laserheartWeapon,new Vector3(pos.x,pos.y,1.5f),Quaternion.identity) as GameObject;
		laserprojectile[0].transform.parent = transform;
		laserprojectile[0].SendMessage (MnstMsgs.SetToughness,1);		
		laserprojectile[0].SendMessage ("SetPassThrough");			
		gameController.addMonster (laserprojectile[0]);
	}

	void GetSpecialAttack()
	{
		int specialtactics;
		if(phase == BossRoomController.monsterPhase.phase1)
		{
            specialtactics = Random.Range (0,7);
			switch(specialtactics)
			{
			default:
			case 0:
				attack = attacks.charge;
				break;
			case 1:
				attack = attacks.groundattackBoth;
				break;
			case 2:
				attack = attacks.summon;
				break;
			case 3:
				attack = attacks.laserbeginBoth;
				break;
			case 4:
				attack = attacks.lasermidBoth;
				break;
			case 5:
				attack = attacks.laserfarBoth;
				break;						
			case 6:
				attack = attacks.laserheart;
				break;
			}
		}
		else
		{
			if(armL && armR)
			{
				specialtactics = Random.Range (0,2);
				switch(specialtactics)
				{
				case 0:
					attack = attacks.laserheart;
					break;
				case 1:
					attack = attacks.summon;
					break;
				}
			}
			else
			if(armL)
			{
				specialtactics = Random.Range (0,6);
				switch(specialtactics)
				{
				default:
				case 0:
					attack = attacks.laserheart;
					break;
				case 1:
					attack = attacks.summon;
					break;
				case 2:
					attack = attacks.groundattackR;
					break;
				case 3:
					attack = attacks.laserbeginR;
					break;
				case 4:
					attack = attacks.laserfarR;
					break;
				case 5:
					attack = attacks.lasermidR;
					break;
				}
			}
			else
			if(armR)
			{
				specialtactics = Random.Range (0,6);
				switch(specialtactics)
				{
				default:
				case 0:
					attack = attacks.laserheart;
					break;
				case 1:
					attack = attacks.summon;
					break;
				case 2:
					attack = attacks.groundattackL;
					break;
				case 3:
					attack = attacks.laserbeginL;
					break;
				case 4:
					attack = attacks.laserfarL;
					break;
				case 5:
					attack = attacks.lasermidL;
					break;
				}
			}
			else
			{
				specialtactics = Random.Range (0,6);
				switch(specialtactics)
				{
				default:
				case 0:
					attack = attacks.laserheart;
					break;
				case 1:
					attack = attacks.summon;
					break;
				case 2:
					attack = attacks.groundattackBoth;
					break;
				case 3:
					attack = attacks.laserbeginBoth;
					break;
				case 4:
					attack = attacks.laserfarBoth;
					break;
				case 5:
					attack = attacks.lasermidBoth;
					break;
				}
			}
		}
	}

	void SummonChaser()
	{
		//int currentMonsterCategory = 1;//chaser,crowd,shooter,muscle
		int monsterNumberinArray = Random.Range(0,gameController.monsterChaser.Length);
		GameObject monster = gameController.GetChaser(monsterNumberinArray, false);
		int monsterQuantity = Random.Range (monster.GetComponent<MonsterGeneric>().monsterMin,monster.GetComponent<MonsterGeneric>().monsterMax+1);
		if(phase != BossRoomController.monsterPhase.phase1)
			monsterQuantity /= 2;
		UpdateSpots();
		for (int k = 0; k < monsterQuantity; k++) {
			Vector2 spot = Vector2.one;
			spot = new Vector2(Random.Range (minSpot.x,maxSpot.x),Random.Range (minSpot.y,maxSpot.y));
			Vector3 position = new Vector3(spot.x,spot.y,1.5f);
			GameObject temp;
			temp = Instantiate(monster,position,Quaternion.identity) as GameObject;
			int tough = GameObject.Find ("StageController").GetComponent<StageController>().GetToughness();
			temp.SendMessage (MnstMsgs.SetToughness,tough);
			gameController.addMonster(temp);
			temp.GetComponent<MonsterGeneric>().nextState = MonsterStates.Start;
		}
	}
	Vector2 minSpot,maxSpot;
	void UpdateSpots()
	{
		GameObject w1,w3,w4;
		w1 = GameObject.Find ("wall1");
		w3 = GameObject.Find ("wall3");
		w4 = GameObject.Find ("wall4");
		minSpot = new Vector2(w4.transform.position.x, w1.transform.position.y + 2);
		maxSpot = new Vector2(w4.transform.position.x-2, w3.transform.position.y - 2);
	}

	void StunBoss()
	{
		animator.enabled = false;
		currentStunTime = stunTimeForLosingArm;
		nextState = MonsterStates.Stun;
		saveStateBeforeStun = currentState;
	}

	protected override void OnMonsterHit()
	{
	}
}