﻿using UnityEngine;
using System.Collections;



public static class LerpEquationTypesExtensionMethods {
	public static float GetEquationTransformedFactor(
		this LerpEquationTypes lerpEquation, float crudeFactor
	) {
		crudeFactor = Mathf.Clamp(crudeFactor, 0.0f, 1.0f);
		
		switch(lerpEquation) {
		case LerpEquationTypes.Quadratic:
			crudeFactor = crudeFactor * crudeFactor;
			break;
			
		case LerpEquationTypes.SmoothCos:
			crudeFactor = (Mathf.Cos(crudeFactor * Mathf.PI) * -0.5f) + 0.5f;
			break;

		case LerpEquationTypes.InverseQuadratic:
			crudeFactor = 1 - ((1 - crudeFactor) * (1 - crudeFactor));
			break;
		}
		
		return(crudeFactor);
	}



	public static Vector3 Lerp(
		this LerpEquationTypes lerpEquation, Vector3 start, Vector3 end, float crudeFactor
	) {
		return(Vector3.Lerp(start, end, lerpEquation.GetEquationTransformedFactor(crudeFactor)));
	}



	public static float Lerp(
		this LerpEquationTypes lerpEquation, float start, float end, float crudeFactor
	) {
		return(Mathf.Lerp(start, end, lerpEquation.GetEquationTransformedFactor(crudeFactor)));
	}
}
