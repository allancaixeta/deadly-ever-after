using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WallTextureUtility : MonoBehaviour
{
	//Variables that controls mesh generation behavior
	public bool GenerateTop = true;
	public bool GenerateBottom = true;
	public bool GenerateLeft = true;
	public bool GenerateRight = true;
	public bool GenerateFront = true;
	public bool GenerateBack = true;
	
	public float GenerateWidth = 1;
	public float GenerateHeight = 1;
	public float GenerateDepth = 1;
	
	
	//Variables that controls UV mapping
	public enum TextureNormalization { Clamp, RepeatFixedHeight, RepeatFixedWidth }
	public enum TextureOrientation { Normal, InvertedHorizontal, InvertedVertical, InvertedBoth }
	
	public TextureNormalization LeftTextureWrapping;
	public TextureOrientation LeftTextureOrientation;
	public Vector2 LeftStartUV = new Vector2(0, 0);
	public Vector2 LeftEndUV = new Vector2(1, 1);
	public float LeftFixedRatio = 1;
	
	public TextureNormalization RightTextureWrapping;
	public TextureOrientation RightTextureOrientation;
	public Vector2 RightStartUV = new Vector2(0, 0);
	public Vector2 RightEndUV = new Vector2(1, 1);
	public float RightFixedRatio = 1;
	
	public TextureNormalization FrontTextureWrapping;
	public TextureOrientation FrontTextureOrientation;
	public Vector2 FrontStartUV = new Vector2(0, 0);
	public Vector2 FrontEndUV = new Vector2(1, 1);
	public float FrontFixedRatio = 1;
	
	public TextureNormalization BackTextureWrapping;
	public TextureOrientation BackTextureOrientation;
	public Vector2 BackStartUV = new Vector2(0, 0);
	public Vector2 BackEndUV = new Vector2(1, 1);
	public float BackFixedRatio = 1;
	
	public TextureNormalization TopTextureWrapping;
	public TextureOrientation TopTextureOrientation;
	public Vector2 TopStartUV = new Vector2(0, 0);
	public Vector2 TopEndUV = new Vector2(1, 1);
	public float TopFixedRatio = 1;
	
	public TextureNormalization BottomTextureWrapping;
	public TextureOrientation BottomTextureOrientation;
	public Vector2 BottomStartUV = new Vector2(0, 0);
	public Vector2 BottomEndUV = new Vector2(1, 1);
	public float BottomFixedRatio = 1;
	
	public Vector3 MeshOrigin = new Vector3(0, 0, 0);
	
	
	//Box collider of this object
	private BoxCollider box_collider;
	
	
	//Used to know what face we are editing
	private enum FaceTypes { FrontBack, TopBottom, Sides }
	

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	
	public void GenerateNewMesh()
	{
		//Create our needed lists for calculations
		List<Vector3> VerticesList = new List<Vector3>();
		List<Vector2> UVList = new List<Vector2>();
		List<Vector2> UV2List = new List<Vector2>();
		List<int> TrianglesList = new List<int>();
		
		
		//Generate those faces
		if(GenerateTop)
		{
			GenerateNewFace
			(
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				TopStartUV, TopEndUV, TopTextureWrapping, TopFixedRatio, TopTextureOrientation,
				ref VerticesList, ref TrianglesList, ref UVList, ref UV2List, FaceTypes.TopBottom
			);
		}
		
		if(GenerateBottom)
		{
			GenerateNewFace
			(
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				BottomStartUV, BottomEndUV, BottomTextureWrapping, BottomFixedRatio, BottomTextureOrientation,
				ref VerticesList, ref TrianglesList, ref UVList, ref UV2List, FaceTypes.TopBottom
			);
		}
		
		if(GenerateLeft)
		{
			GenerateNewFace
			(
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				LeftStartUV, LeftEndUV, LeftTextureWrapping, LeftFixedRatio, LeftTextureOrientation,
				ref VerticesList, ref TrianglesList, ref UVList, ref UV2List, FaceTypes.Sides
			);
		}
		
		if(GenerateRight)
		{
			GenerateNewFace
			(
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				RightStartUV, RightEndUV, RightTextureWrapping, RightFixedRatio, RightTextureOrientation,
				ref VerticesList, ref TrianglesList, ref UVList, ref UV2List, FaceTypes.Sides
			);
		}
		
		if(GenerateFront)
		{
			GenerateNewFace
			(
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, -GenerateDepth*0.5f - MeshOrigin.z),
				FrontStartUV, FrontEndUV, FrontTextureWrapping, FrontFixedRatio, FrontTextureOrientation,
				ref VerticesList, ref TrianglesList, ref UVList, ref UV2List, FaceTypes.FrontBack
			);
		}
		
		if(GenerateBack)
		{
			GenerateNewFace
			(
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, -GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(-GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				new Vector3(GenerateWidth*0.5f - MeshOrigin.x, GenerateHeight*0.5f - MeshOrigin.y, GenerateDepth*0.5f - MeshOrigin.z),
				BackStartUV, BackEndUV, BackTextureWrapping, BackFixedRatio, BackTextureOrientation,
				ref VerticesList, ref TrianglesList, ref UVList, ref UV2List, FaceTypes.FrontBack
			);
		}
		
		
		//Save the mesh
		MeshFilter mesh_filter = this.gameObject.GetComponent<MeshFilter>();
		if(mesh_filter == null)
		{
			mesh_filter = this.gameObject.AddComponent<MeshFilter>();
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = VerticesList.ToArray();
		mesh.triangles = TrianglesList.ToArray();
		mesh.uv = UVList.ToArray();
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		CalculateMeshTangents.calculateMeshTangents(mesh);
		;
		
		mesh_filter.mesh = mesh;
		
		
		//Get the box collider back again
		regenerateBoxCollider();
	}
	
	
	
	private void GenerateNewFace
	(
		Vector3 BottomLeftVertex, Vector3 BottomRightVertex, Vector3 TopRightVertex, Vector3 TopLeftVertex,
		Vector2 StartUV, Vector2 EndUV, TextureNormalization TextureWrapping, float FixedRatio, TextureOrientation Orientation,
		ref List<Vector3> VerticesList, ref List<int> TrianglesList,
		ref List<Vector2> UVList, ref List<Vector2> UV2List, FaceTypes FaceType
	)
	{
		//Add the vertices
		VerticesList.Add(BottomLeftVertex);
		VerticesList.Add(BottomRightVertex);
		VerticesList.Add(TopRightVertex);
		VerticesList.Add(TopLeftVertex);
		
		
		//Get vertices position
		int blvi = VerticesList.Count - 4;
		int brvi = VerticesList.Count - 3;
		int trvi = VerticesList.Count - 2;
		int tlvi = VerticesList.Count - 1;
		
		
		//Create the face (triangles)
		TrianglesList.Add(blvi);
		TrianglesList.Add(tlvi);
		TrianglesList.Add(trvi);
		
		TrianglesList.Add(trvi);
		TrianglesList.Add(brvi);
		TrianglesList.Add(blvi);
		
		
		//Do the UV mapping
		Vector2[] uv = new Vector2[4];
		
		switch(TextureWrapping)
		{
		case TextureNormalization.Clamp:
		{
			uv[0] = new Vector2(StartUV.x, StartUV.y);
			uv[1] = new Vector2(EndUV.x, StartUV.y);
			uv[2] = new Vector2(EndUV.x, EndUV.y);
			uv[3] = new Vector2(StartUV.x, EndUV.y);
		} break;
			
		case TextureNormalization.RepeatFixedHeight:
		{
			float side_height = 1;
			float horizontal_distance = 1;
			
			switch(FaceType)
			{
			case FaceTypes.FrontBack:
				side_height = Mathf.Abs(TopLeftVertex.y - BottomLeftVertex.y);
				horizontal_distance = Mathf.Abs(BottomRightVertex.x - BottomLeftVertex.x);
			break;
				
			case FaceTypes.Sides:
				side_height = Mathf.Abs(TopLeftVertex.y - BottomLeftVertex.y);
				horizontal_distance = Mathf.Abs(TopRightVertex.z - TopLeftVertex.z);
			break;
				
			case FaceTypes.TopBottom:
				side_height = Mathf.Abs(TopLeftVertex.z - BottomLeftVertex.z);
				horizontal_distance = Mathf.Abs(TopRightVertex.x - TopLeftVertex.x);
				break;
			}
				
			float horizontal_distance_factor = (horizontal_distance / side_height) * FixedRatio;
			uv[0] = new Vector2(StartUV.x, StartUV.y);
			uv[1] = new Vector2(StartUV.x + horizontal_distance_factor, StartUV.y);
			uv[2] = new Vector2(StartUV.x + horizontal_distance_factor, EndUV.y);
			uv[3] = new Vector2(StartUV.x, EndUV.y);
		} break;
			
			
		case TextureNormalization.RepeatFixedWidth:
		{
			float side_distance = 1;
			float vertical_distance = 1;
			
			switch(FaceType)
			{
			case FaceTypes.FrontBack:
				side_distance = Mathf.Abs(BottomRightVertex.x - BottomLeftVertex.x);
				vertical_distance = Mathf.Abs(TopLeftVertex.y - BottomLeftVertex.y);
			break;
				
			case FaceTypes.Sides:
				side_distance = Mathf.Abs(TopRightVertex.z - TopLeftVertex.z);
				vertical_distance = Mathf.Abs(TopLeftVertex.y - BottomLeftVertex.y);
			break;
				
			case FaceTypes.TopBottom:
				side_distance = Mathf.Abs(TopRightVertex.x - TopLeftVertex.x);
				vertical_distance = Mathf.Abs(TopLeftVertex.z - BottomLeftVertex.z);
			break;
			}
				
			float vertical_distance_factor = (vertical_distance / side_distance) * FixedRatio;
			uv[0] = new Vector2(StartUV.x, StartUV.y);
			uv[1] = new Vector2(EndUV.x, StartUV.y);
			uv[2] = new Vector2(EndUV.x, StartUV.y + vertical_distance_factor);
			uv[3] = new Vector2(StartUV.x, StartUV.y + vertical_distance_factor);
		} break;
		}
		
		
		if(Orientation == TextureOrientation.InvertedHorizontal || Orientation == TextureOrientation.InvertedBoth)
		{
			Vector2 aux = uv[1];
			uv[1] = uv[0];
			uv[0] = aux;
			
			aux = uv[2];
			uv[2] = uv[3];
			uv[3] = aux;
		}
		
		if(Orientation == TextureOrientation.InvertedVertical || Orientation == TextureOrientation.InvertedBoth)
		{
			Vector2 aux = uv[0];
			uv[0] = uv[2];
			uv[2] = aux;
			
			aux = uv[1];
			uv[1] = uv[3];
			uv[3] = aux;
		}
		
		UVList.Add(uv[0]);
		UVList.Add(uv[1]);
		UVList.Add(uv[2]);
		UVList.Add(uv[3]);
		
		
		
		//Create the second uv mapping
		//Do the UV mapping
		Vector2[] uv2 = new Vector2[4];
		
		uv2[0] = uv[0];
		uv2[1] = uv[1];
		uv2[2] = uv[2];
		uv2[3] = uv[3];
		
		UV2List.Add(uv[0]);
		UV2List.Add(uv[1]);
		UV2List.Add(uv[2]);
		UV2List.Add(uv[3]);
	}
	
	
	
	private void regenerateBoxCollider()
	{
		if(GetComponent<Collider>() != null)
		{
			box_collider = GetComponent<Collider>() as BoxCollider;
			if(box_collider == null)
			{
				Debug.LogWarning("Warning [WallTextureUtility]: deleting collider on this object to create it as a BoxCollider.", this.gameObject);
				GameObject.Destroy(GetComponent<Collider>());
				this.gameObject.AddComponent<BoxCollider>();
			}
		} else {
			this.gameObject.AddComponent<BoxCollider>();
			box_collider = GetComponent<Collider>() as BoxCollider;
		}
		
		
		//Set the box collider values
		box_collider.size = new Vector3(GenerateWidth, GenerateHeight, GenerateDepth);
		box_collider.center = -MeshOrigin;
	}
}
