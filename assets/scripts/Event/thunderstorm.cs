﻿using UnityEngine;
using System.Collections;

public class thunderstorm : EventController {
	
	public enum waterStates{
		VOID,
		start,
		normal
	}
	
	public float timeForPlayerToMoveBeforeTheLightning = 1.5f;
	public GameObject laser, target, thunder;
	
	Transform player;

	Vector3 lastPlayerPosition;
	float currentCountdown, currentShootCooldown, lastTimePlayerMoved;
	SpriteRenderer targetinner, targetouter;
	
	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		target = Instantiate (target, player.position, Quaternion.identity) as GameObject;
		targetinner = target.transform.Find ("thundertargetinner").GetComponent<SpriteRenderer>();
		blackWithAlpha = targetinner.color;
		whiteWithAlpha = Color.white;
		whiteWithAlpha.a = targetinner.color.a;
		targetouter = target.transform.Find ("thundertargetouter").GetComponent<SpriteRenderer>();
		target.SetActive (false);
	}
	
	public override void SpecificEventUpdate()
	{
		CheckPlayerMoved ();
		if(lastTimePlayerMoved > timeForPlayerToMoveBeforeTheLightning)
			ShootLaser();
	}

	void ShootLaser()
	{
		GameObject temp = Instantiate(laser,player.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,MonsterGeneric.toughness.one);
		//temp.SendMessage ("SetDirection",angle);
		temp.SendMessage ("SetPassThrough");
		GameObject.Find ("GameController").SendMessage("addMonster", temp);
		target.SetActive (false);
		temp = Instantiate (thunder, new Vector3(player.position.x-15,player.position.y,player.position.z) , thunder.transform.rotation) as GameObject;
		temp.transform.localRotation = Quaternion.Euler (0,-temp.transform.localRotation.eulerAngles.y,0);
		ResetPlayerMoved ();
	}

	public void ResetPlayerMoved()
	{
		lastTimePlayerMoved = 0;
		targetinner.color = blackWithAlpha;
		targetouter.color = blackWithAlpha;
	}
	Color blackWithAlpha, whiteWithAlpha;
	void CheckPlayerMoved()
	{
		if(Vector2.Distance (player.position,lastPlayerPosition)> 0.1f)
		{
			ResetPlayerMoved ();
			target.SetActive (false);
		}
		else
		{
			target.SetActive (true);
			lastTimePlayerMoved += GameController.deltaTime;
			float percentageToShootLaser = 0;
			percentageToShootLaser = 1 - lastTimePlayerMoved/timeForPlayerToMoveBeforeTheLightning;
			target.transform.position = player.position;
			if(percentageToShootLaser < 0.5f)
				targetinner.color = Color.Lerp (whiteWithAlpha,blackWithAlpha,percentageToShootLaser * 2);
			else
				targetouter.color = Color.Lerp (whiteWithAlpha,blackWithAlpha,(percentageToShootLaser - 0.5f) * 2);
		}
		lastPlayerPosition = player.position;
	}
}