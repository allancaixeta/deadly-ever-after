﻿using UnityEngine;
using System.Collections;

public class laserturner : EventController {
	
	public enum laserturnerType{
		limited,
		spin,
		stopped
	}

	public float spinSpeed;
	public laserturnerType laserType;
	public int additionLaserQuantity;
	public GameObject laserTurner;
	public float limitAngle = 30;
	public float creationTime = 1;
	public float stopFromMiddleDistance = 5;
	public bool sync;
	public float startAngle = -1;
	public Collider mycollider;
	float up, down, left, right, invertMovement, startDelayTimer, downLimit, upperLimit;
	Transform player;
	Vector3 targetRotateAround;
	bool isUp, isVertical;
	bool creatingLaser = true;
	
	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		mycollider.enabled = false;
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		spinSpeed = spinSpeed * (1 - skills.GetAccel (false));

		transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 + stopFromMiddleDistance,
		                                              cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 - stopFromMiddleDistance),
		                                 Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2 + stopFromMiddleDistance,
		             					cenarioLimits.position.y + cenarioLimits.lossyScale.y/2 - stopFromMiddleDistance),1.5f);
		while(CheckCollisionWithFurniture (transform.position) || Vector2.Distance (transform.position,player.position) < GameController.minPlayerDistanceToSpawn)
		{
			transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 + stopFromMiddleDistance,
			                                              cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 - stopFromMiddleDistance),
			                                 Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2 + stopFromMiddleDistance,
			             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2 - stopFromMiddleDistance),1.5f);
		}
		while(additionLaserQuantity != 0)
		{
			CreateAnotherLaser();
			additionLaserQuantity--;
		}
		invertMovement = 1;
		targetRotateAround = transform.position;
		switch(laserType)
		{
		case laserturnerType.spin:
			invertMovement = Random.value < 0.5f ? 1 : -1;
			if(startAngle != -1)
				transform.RotateAround (targetRotateAround,Vector3.forward,startAngle);
			if(!sync)
				transform.RotateAround (targetRotateAround,Vector3.forward,Random.Range (0,360));
			break;
		case laserturnerType.limited:
			invertMovement = 1;
			if(startAngle != -1)
				transform.RotateAround (targetRotateAround,Vector3.forward,startAngle);
			else
				startAngle = transform.rotation.eulerAngles.z;
			upperLimit = (startAngle + limitAngle > 360 ? ((startAngle + limitAngle) - 360) : startAngle + limitAngle);
			downLimit = (startAngle - limitAngle < 0 ? (360 + (startAngle - limitAngle)) : startAngle - limitAngle);
			break;
		case laserturnerType.stopped:
			transform.RotateAround (targetRotateAround,Vector3.forward,Random.Range(0,360));
			break;
		}
	}
	
	public override void SpecificEventUpdate()
	{
		if(creatingLaser)
		{
			creationTime -= GameController.deltaTime;
			if(creationTime < 0)
				creatingLaser = false;
			mycollider.enabled = true;
		}
		switch(laserType)
		{
		case laserturnerType.spin:
			transform.RotateAround (targetRotateAround,Vector3.forward,invertMovement * spinSpeed * GameController.deltaTime);
			break;
		case laserturnerType.limited:
			transform.RotateAround (targetRotateAround,Vector3.forward,invertMovement * spinSpeed * GameController.deltaTime);
			if(upperLimit > downLimit)
			{
				if(invertMovement == 1 && Mathf.DeltaAngle (transform.rotation.eulerAngles.z,upperLimit) < 0)
					invertMovement = -1;
				else
				{
					if(invertMovement == -1 && Mathf.DeltaAngle (transform.rotation.eulerAngles.z,downLimit) > 0)
						invertMovement = 1;
				}
			}
			else
			{
				if(!(transform.rotation.eulerAngles.z < upperLimit || transform.rotation.eulerAngles.z > downLimit))
					invertMovement = -invertMovement;
			}
			break;
		}		
	}
	

	void CreateAnotherLaser()
	{
		GameObject temp = Instantiate (laserTurner, transform.position, Quaternion.identity) as GameObject;
		if(dontUsePositiveEffect)
			temp.SendMessage ("SetdontUsePositiveEffect");
		temp.SendMessage ("ResetQuantity");
		temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		GameObject.Find ("GameController").SendMessage("addEvent",temp);
	}

	bool CheckCollisionWithFurniture(Vector3 position)
	{
		int layerMask = 132096;
		Vector3 upTemp = position + new Vector3(0,0,-100);
		Ray ray = new Ray(upTemp,position - upTemp);
		if(Physics.SphereCast(ray,GetComponent<SphereCollider>().radius,1000,layerMask))
			return true;
		else
			return false;
	}

	public void ResetQuantity()
	{
		additionLaserQuantity = 0;
	}

	public void SetPosition(Vector3 position, float angle)
	{
		transform.position = position;
		targetRotateAround = position;
		transform.rotation = Quaternion.Euler (0,0,angle);
		startAngle = angle;
		downLimit = (startAngle - limitAngle < 0 ? (360 + (startAngle - limitAngle)) : startAngle - limitAngle);
		upperLimit = (startAngle + limitAngle > 360 ? ((startAngle + limitAngle) - 360) : startAngle + limitAngle);
	}
}