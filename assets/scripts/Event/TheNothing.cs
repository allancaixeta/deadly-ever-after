﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheNothing : EventController
{
    public float num;
    public override void SpecificEventStart(StageController.StageNumber stage)
    {
        transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x / 2, cenarioLimits.position.x + cenarioLimits.lossyScale.x / 2),
            Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y / 2, cenarioLimits.position.y + cenarioLimits.lossyScale.y / 2),0);        
    }

    public override void SpecificEventUpdate()
    {
        GameObject[] vec = GameObject.FindGameObjectsWithTag(Tags.FurnitureVisual);
        for (int i = 0; i < vec.Length; i++)
        {
            vec[i].transform.position = new Vector3(vec[i].transform.position.x, vec[i].transform.position.y, num);
        }
    }    
}
