﻿using UnityEngine;
using System.Collections;

#pragma warning disable 0414 // private field assigned but not used.

public class quicksand : EventController {
	
	floordetector.hazards hazardType;
	floordetector.modus modusOperandi;
	PlayerController player;
	float currentCountdown;
	public float sinkingDeathTime;
	public float lifeTime;
	public GameObject vortex;
	bool playerInside;
	float currentSinkingDeathTime;
	// Use this for initialization
	public override void SpecificEventStart(StageController.StageNumber stage)
	{		
		player = (PlayerController) FindObjectOfType(typeof(PlayerController));
		vortex.SetActive (false);
		switch(modusOperandi)
		{			
		case floordetector.modus.permanent:
			break;
		}
		currentSinkingDeathTime = sinkingDeathTime;
	}
	
	public override void SpecificEventUpdate()
	{                                             
		switch(modusOperandi)
		{			
		case floordetector.modus.permanent:
			break;
		case floordetector.modus.temporary:
			lifeTime -= GameController.deltaTime;
			if(lifeTime < 0)
			{
				transform.parent.SendMessage ("DestroyItSelf");
			}
			break;
		}
		if(playerInside)
		{
			currentSinkingDeathTime -= GameController.deltaTime;
			if(currentSinkingDeathTime < 0)
			{
				player.OneHitDeath ();
			}
			vortex.transform.position = player.transform.position;
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		switch(modusOperandi)
		{			
		default:
		case floordetector.modus.permanent:
			Active(other);
			break;
		}
	}
	
	void Active(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Monster:
			//other.GetComponent<MonsterGeneric>().Hit(monsterDamage,0,transform.position);
			break;
		case "PlayerHit":
			playerInside = true;
			vortex.SetActive (true);
			break;
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);	
	}

	void OnTriggerExit(Collider other)
	{
		switch(modusOperandi)
		{			
		default:
		case floordetector.modus.permanent:
			Deactive(other);
			break;
		}
	}

	void Deactive(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Monster:
			//other.GetComponent<MonsterGeneric>().Hit(monsterDamage,0,transform.position);
			break;
		case "PlayerHit":
			playerInside = false;
			currentSinkingDeathTime = sinkingDeathTime;
			vortex.SetActive (false);
			break;
		}
	}

	public void SetModus(floordetector.modus m)
	{
		modusOperandi = m;
	}
	
	public void SetHazardType(floordetector.hazards h)
	{
		hazardType = h;
	}
	
	void TurnOn()
	{
		GetComponent<Animator>().SetBool("on",true);
	}
	
	void TurnOff()
	{
		GetComponent<Animator>().SetBool("on",false);
	}
}