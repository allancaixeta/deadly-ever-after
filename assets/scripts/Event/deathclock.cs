﻿using UnityEngine;
using System.Collections;

public class deathclock : EventController {

	float currentClock = 100;
	public TextMesh text1, text2, text3;
	bool personalclock;
	// Use this for initialization
	public override void SpecificEventStart(StageController.StageNumber stage)
	{	
		transform.parent = GameObject.Find ("deathclockpos").transform;
		transform.localPosition = Vector3.zero;
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{                                             
		currentClock -= GameController.deltaTime;
		string time = ((int)currentClock).ToString ("000");
		text1.text = currentClock / 100 > 1 ? time.Substring(0,1) : "";
		text2.text = currentClock / 10 > 1 ? time.Substring(1,1) : "0";
		text3.text = time.Substring(2,1);
		if(currentClock < 0)
			GameController.Instance.GameOver();
	}
	
	public void SetasPersonalClock(bool personalclock)
	{
		this.personalclock = personalclock;
	}
	
	public void ClockTimer(float timer)
	{
		currentClock = timer;
	}
}