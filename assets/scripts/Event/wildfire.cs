﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class wildfire : EventController {

	public float expandTime = 10;
	public float distanceFromOriginal = 2;
	public GameObject controller;
	float currentExpandTime;
	public float timeSinceBorn = 1;
	float currentTimeSinceBorn;
	List<string> monsterHitListID;
	List<float> monsterHitListDuration;
	GameObject father;
	wildfireController wildfireControl;
	bool surrounded;
	int surroundCount;

	void Awake()
	{
	}
	// Use this for initialization
	public override void SpecificEventStart(StageController.StageNumber stage)
	{		
		wildfireControl = (wildfireController) FindObjectOfType(typeof(wildfireController));
		if(wildfireControl == null)
		{
			GameObject temp = Instantiate (controller) as GameObject;
			wildfireControl = (wildfireController) FindObjectOfType(typeof(wildfireController));
			temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
			GameObject.Find ("GameController").SendMessage("addEvent",temp);
			transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
			                                              cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
			                                 Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
			             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
			while(CheckCollisionWithFurniture (transform.position) || Vector2.Distance (transform.position,GameObject.FindGameObjectWithTag("Player").transform.position) < GameController.minPlayerDistanceToSpawn)
			{
				transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
				                                              cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				                                 Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
			}
		}
		wildfireControl.InsertFire(this.transform);
		monsterHitListID = new List<string> ();
		monsterHitListDuration = new List<float> ();
		currentExpandTime = expandTime + Random.Range (0,timeSinceBorn/2);
		currentTimeSinceBorn = timeSinceBorn;
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{   
		DecreaseMonsterHitListDuration();
		if(!surrounded)
		{
			currentTimeSinceBorn -= GameController.deltaTime;
			currentExpandTime -= GameController.deltaTime;
			if(currentExpandTime < 0)
			{
				Expand ();
			}
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Monster:
			for (int i = 0; i < monsterHitListID.Count; i++){
				if(monsterHitListID[i] == other.transform.name)
					return;
			}	
			monsterHitListID.Add(other.transform.name);
			monsterHitListDuration.Add(1);
			other.GetComponent<MonsterGeneric>().Hit(monsterDamage);
			if(currentTimeSinceBorn < 0)
				currentExpandTime = -1;
			break;
		case "PlayerHit":
			if(currentTimeSinceBorn < 0)
				currentExpandTime = -1;
			break;
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);	
	}
	
	void DecreaseMonsterHitListDuration()
	{
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
	}

	void Expand()
	{
		surroundCount = 0;
		Vector2 currentPosition;
		currentPosition = new Vector2(transform.position.x, transform.position.y);
		CreateFires (currentPosition + new Vector2 (0, distanceFromOriginal));//U
		CreateFires (currentPosition + new Vector2 (distanceFromOriginal, distanceFromOriginal));//UR
		CreateFires (currentPosition + new Vector2 (distanceFromOriginal, 0));//R
		CreateFires (currentPosition + new Vector2 (distanceFromOriginal, -distanceFromOriginal));//DR
		CreateFires (currentPosition + new Vector2 (0, -distanceFromOriginal));//D
		CreateFires (currentPosition + new Vector2 (-distanceFromOriginal, -distanceFromOriginal));//DL
		CreateFires (currentPosition + new Vector2 (-distanceFromOriginal, 0));//L
		CreateFires (currentPosition + new Vector2 (-distanceFromOriginal, distanceFromOriginal));//UL
		surrounded = true;
	}

	bool CheckCollisionWithFurniture(Vector3 position)
	{
		int layerMask = 132096;
		Vector3 upTemp = position + new Vector3(0,0,-100);
		Ray ray = new Ray(upTemp,position - upTemp);
		if(Physics.SphereCast(ray,GetComponent<SphereCollider>().radius,1000,layerMask))
			return true;
		else
			return false;
	}

	void CreateFires(Vector2 position)
	{
		GameObject temp;
		if(!CheckCollisionWithFurniture (position) && wildfireControl.CheckSpot(position))
		{
			temp = Instantiate (this.gameObject,new Vector3(position.x,position.y,transform.position.z),Quaternion.identity) as GameObject;
			temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
			GameObject.Find ("GameController").SendMessage("addEvent",temp);
			temp.name = this.name;
			//wildfireControl.InsertFire (temp.transform);
		}
		else
			surroundCount++;
	}
}