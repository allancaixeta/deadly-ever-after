﻿using UnityEngine;
using System.Collections;

public class fog : EventController {

	GameObject father;
	bool haveAFather;
	int insideCheck;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
		                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
		                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
		             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
		transform.Rotate (0, 0, 90);
	}

	public override void SpecificEventUpdate()
	{
		if(insideCheck > 0)
		{
			insideCheck--;
			if(insideCheck <= 0)
			{
				if(haveAFather)
				{
					father.SendMessage("PlayerLeavingFog");	
				}
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(haveAFather && other.transform.tag == "PlayerHit")
		{
			father.SendMessage("PlayerInsideFog");	
			insideCheck = 10;
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter(other);
	}

	void OnTriggerExit(Collider other)
	{
		if(haveAFather && other.transform.tag == "PlayerHit")
		{
			father.SendMessage("PlayerLeavingFog");	
		}
	}

	void SetFather(GameObject f)
	{
		father = f;
		haveAFather = true;
	}

	public void SkipVisual()
	{
		
	}
}
