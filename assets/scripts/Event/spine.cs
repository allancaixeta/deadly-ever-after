﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class spine : EventController {

	floorhazard.hazards hazardType;
	Vector2 size;
	public GameObject spineVisual;
	public Vector2 distanceAdjust = new Vector2(1,1);
	public Vector2 minSize = new Vector2(1,1);
	public Vector2 sizeModifier = new Vector2(1,1);
	public Vector2 centerModifier = new Vector2(1,1);
 	floorhazard.modus modusOperandi;
	GameObject [] spines;
	PlayerHitController playerHit;
	public bool alsoHitMonster = false;
	public float pressureTimeToTrigger, pressureTimeActive, pressureWaitTime;
	public float timedTimeOn, timedTimeOff;
	public float timeToStartAnimationBeforeTriggerHarm = 0.5f;
	public GameObject spanwAfterOneTimeOnly;
	bool spinesOn, oneTimeOnly, turnedOfforoneTimeOnly;
	float currentCountdown;
	List<string> monsterHitListID;
	List<float> monsterHitListDuration;
	public MonsterGeneric.monsterTypes immuneMonsterType;
	// Use this for initialization
	public override void SpecificEventStart(StageController.StageNumber stage)
	{		
		monsterHitListID = new List<string> ();
		monsterHitListDuration = new List<float> ();
		playerHit = PlayerHitController.Instance;
		BoxCollider b = gameObject.GetComponent<BoxCollider> ();
		switch(hazardType)
		{
		case floorhazard.hazards.spines:
			b.size = new Vector3 (minSize.x + (size.x-1)*sizeModifier.x ,minSize.y + (size.y-2)*sizeModifier.y,b.size.z);
			b.center = new Vector3 (b.center.x + (size.x-1)*centerModifier.x ,b.center.y + (size.y-2)*centerModifier.y,b.center.z);
			break;
		case floorhazard.hazards.blocks:
		default:
			b.size = new Vector3 (minSize.x + (size.x-1)*sizeModifier.x ,minSize.y + (size.y-1)*sizeModifier.y,b.size.z);
			b.center = new Vector3 (b.center.x + (size.x-1)*centerModifier.x ,b.center.y + (size.y-1)*centerModifier.y,b.center.z);
			break;
		}
		spines = new GameObject[(int)(size.x*size.y)];
		int count = 0;
		for (int i = 0; i < size.y; i++) 
		{
			for (int j = 0; j < size.x; j++) {
				Vector3 position = new Vector3((transform.position.x - size.x * distanceAdjust.x/2) + (j * distanceAdjust.x),
				                               (transform.position.y - size.y * distanceAdjust.y/2) + (i * distanceAdjust.y),transform.position.z);
				spines[count]= Instantiate(spineVisual,position,Quaternion.identity) as GameObject;
				spines[count].transform.parent = this.transform;
				count++;
			}
		}
		switch(modusOperandi)
		{			
		case floorhazard.modus.permanent:
			spinesOn = true;
			TurnOn ();
			break;
		case floorhazard.modus.pressure:
			currentCountdown = -1;
			break;
		case floorhazard.modus.timed:
			currentCountdown = timedTimeOff;
			break;
		}
	}

	public override void SpecificEventUpdate()
	{   
		if(alsoHitMonster)
			DecreaseMonsterHitListDuration();
		switch(modusOperandi)
		{			
		case floorhazard.modus.permanent:
			break;
		case floorhazard.modus.pressure:
			if(currentCountdown > 0)
			{
				currentCountdown -= GameController.deltaTime;
				if(spinesOn)
				{
					if(currentCountdown < 0)
					{
						currentCountdown = 0;
						spinesOn = false;
						TurnOff ();
						if(turnedOfforoneTimeOnly)
							for (int i = 0; i < spines.Length; i++) {
								spines[i].GetComponent<Animator>().SetTrigger("off");
							}
					}
				}
				else
				{
					if(turnedOfforoneTimeOnly)
						return;
					if(currentCountdown < timeToStartAnimationBeforeTriggerHarm)
						TurnOn ();
					if(currentCountdown < 0)
					{
						currentCountdown = pressureTimeActive;
						spinesOn = true;
						if(oneTimeOnly)
						{
							turnedOfforoneTimeOnly = true;
							if(spanwAfterOneTimeOnly)
							{
								Instantiate (spanwAfterOneTimeOnly,transform.position,Quaternion.identity);
							}
						}
					}
				}
			}
			else
				currentCountdown -= GameController.deltaTime;
			break;
		case floorhazard.modus.timed:
			currentCountdown -= GameController.deltaTime;
			if(spinesOn)
			{
				if(currentCountdown < 0)
				{
					currentCountdown = timedTimeOff;
					spinesOn = false;
					TurnOff ();
				}
			}
			else
			{
				if(turnedOfforoneTimeOnly)
					return;
				if(currentCountdown < timeToStartAnimationBeforeTriggerHarm)
					TurnOn ();
				if(currentCountdown < 0)
				{
					currentCountdown = timedTimeOn;
					spinesOn = true;
					if(oneTimeOnly)
					{
						turnedOfforoneTimeOnly = true;
						if(spanwAfterOneTimeOnly)
						{
							Instantiate (spanwAfterOneTimeOnly,transform.position,Quaternion.identity);
						}
					}
				}
			}
			break;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		switch(modusOperandi)
		{			
		case floorhazard.modus.permanent:
			Active(other);
			break;
		case floorhazard.modus.pressure:
			if(spinesOn)
			{
				Active(other);
				if(currentCountdown < 0.1f && !turnedOfforoneTimeOnly)
					currentCountdown = pressureTimeActive;
			}
			else
			{
				if(turnedOfforoneTimeOnly)
					return;
				if(other.transform.tag == "PlayerHit" && currentCountdown < pressureWaitTime)
				{
					currentCountdown = pressureTimeToTrigger;
				}
			}
			break;
		case floorhazard.modus.timed:
			if(spinesOn)
				Active(other);
			break;
		}
	}

	void Active(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Monster:
			if(alsoHitMonster && other.transform.root != transform.root && other.GetComponent<MonsterGeneric>().GetMonsterType () != immuneMonsterType)
			{
				for (int i = 0; i < monsterHitListID.Count; i++){
					if(monsterHitListID[i] == other.transform.name)
						return;
				}	
				other.GetComponent<MonsterGeneric>().Hit(monsterDamage);
				monsterHitListID.Add(other.transform.name);
				monsterHitListDuration.Add(1);
			}
			break;
		case "PlayerHit":
			playerHit.HitPlayer(damage,eventName);
			break;
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);	
	}

	public void SetSize(Vector2 s)
	{
		size = s;
	}

	public void SetModus(floorhazard.modus m)
	{
		modusOperandi = m;
	}

	public void SetHazardType(floorhazard.hazards h)
	{
		hazardType = h;
	}

	public void SetOneTimeOnly(bool o)
	{
		oneTimeOnly = o;
	}

	void TurnOn()
	{
		for (int i = 0; i < spines.Length; i++) {
			if(spines[i])
				spines[i].GetComponent<Animator>().SetBool("on",true);
		}
	}

	void TurnOff ()
	{
		for (int i = 0; i < spines.Length; i++) {
			if(spines[i])
				spines[i].GetComponent<Animator>().SetBool("on",false);
		}
	}

	void DecreaseMonsterHitListDuration()
	{
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
	}
}
