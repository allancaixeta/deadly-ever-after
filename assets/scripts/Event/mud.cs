﻿using UnityEngine;
using System.Collections;

#pragma warning disable 0414 // private field assigned but not used.

public class mud : EventController {
	
	floordetector.hazards hazardType;
	floordetector.modus modusOperandi;
	PlayerController player;
	float currentCountdown;
	public float lifeTime;
	bool playerInside;
	// Use this for initialization
	public override void SpecificEventStart(StageController.StageNumber stage)
	{		
		player = (PlayerController) FindObjectOfType(typeof(PlayerController));
		switch(modusOperandi)
		{			
		case floordetector.modus.permanent:
			break;
		}
	}
	
	public override void SpecificEventUpdate()
	{                                             
		switch(modusOperandi)
		{			
		case floordetector.modus.permanent:
			break;
		case floordetector.modus.temporary:
			lifeTime -= GameController.deltaTime;
			if(lifeTime < 0)
			{
				if(playerInside)
					player.LeaveTimeDistortion();
				transform.parent.SendMessage ("DestroyItSelf");
			}
			break;
		}
		
	}
	
	void OnTriggerEnter(Collider other)
	{
		switch(modusOperandi)
		{			
		default:
		case floordetector.modus.permanent:
			Active(other);
			break;
		}
	}
	
	void Active(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Monster:
            if(monsterDamage > 0)
			    other.GetComponent<MonsterGeneric>().SlowDown(monsterDamage,0.1f);
			break;
		case "PlayerHit":
			player.EnterTimeDistortion((float)damage);
			playerInside = true;
			break;
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);	
	}

	void OnTriggerExit(Collider other)
	{
		switch(modusOperandi)
		{			
		default:
		case floordetector.modus.permanent:
			Deactive(other);
			break;
		}
	}

	void Deactive(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Monster:
			//other.GetComponent<MonsterGeneric>().Hit(monsterDamage,0,transform.position);
			break;
		case "PlayerHit":
			player.LeaveTimeDistortion();
			playerInside = false;
			break;
		}
	}

	public void SetModus(floordetector.modus m)
	{
		modusOperandi = m;
	}
	
	public void SetHazardType(floordetector.hazards h)
	{
		hazardType = h;
	}
	
	void TurnOn()
	{
		GetComponent<Animator>().SetBool("on",true);
	}
	
	void TurnOff()
	{
		GetComponent<Animator>().SetBool("on",false);
	}
}