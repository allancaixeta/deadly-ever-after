﻿using UnityEngine;
using System.Collections;

public class Lighty : EventController {
	
	public enum lightStates{
		VOID,
		start,
		hold,
		teleporting
	}

	public GameObject warning, laserWarning, laserWall, laserTurner1, laserTurner2;
	public float countDownToTeleport = 5;
	public float teleportingDuration = 0.5f;
	public float foresseTimer = 1;
	public float warningvisualSizeMultiplier;
	public float laserTurnerAdjust = 5;
	float currentCountdown;
	PlayerController playerController;
	lightStates currentState, nextState;
	Vector3 goalPosition, lastPosition;
	
	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		playerController = PlayerController.Instance;
		nextState = lightStates.start;
		warning = Instantiate (warning,transform.position,Quaternion.identity) as GameObject;
		warning.SetActive (false);
		laserWarning = Instantiate (laserWarning,transform.position,Quaternion.identity) as GameObject;
		laserWarning.SetActive (false);
		laserWall = Instantiate (laserWall, transform.position, Quaternion.identity) as GameObject;
		laserWall.GetComponent<laserwall>().SetSpawnPoints(transform.position,transform.position);
		laserWall.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		laserWall.SendMessage ("SetdontUsePositiveEffect");
		GameObject.Find ("GameController").SendMessage("addEvent",laserWall);

		laserTurner1 = Instantiate (laserTurner1, transform.position, Quaternion.identity) as GameObject;
		laserTurner1.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		laserTurner1.GetComponent<laserturner>().SetPosition(transform.position,0);
		laserTurner1.SendMessage ("SetdontUsePositiveEffect");
		GameObject.Find ("GameController").SendMessage("addEvent",laserTurner1);

		laserTurner2 = Instantiate (laserTurner2, transform.position, Quaternion.identity) as GameObject;
		laserTurner2.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		laserTurner2.GetComponent<laserturner>().SetPosition(transform.position,180);
		laserTurner2.SendMessage ("SetdontUsePositiveEffect");
		GameObject.Find ("GameController").SendMessage("addEvent",laserTurner2);

		lastPosition = transform.position;
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}
	
	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case lightStates.start:
			nextState = lightStates.hold;	

			break;
			
		case lightStates.hold:
			currentCountdown = countDownToTeleport * GetCooldownModifier ();
			warning.SetActive (false);
			laserWarning.SetActive (false);
			break;

		case lightStates.teleporting:
			laserWall.GetComponent<laserwall>().SetSpawnPoints(transform.position,transform.position);
			Vector2 tv = playerController.GetCurrentSpeed();
			if (tv == Vector2.zero)
				goalPosition = playerController.transform.position;
			else
				goalPosition = new Vector2(playerController.transform.position.x + tv.x * foresseTimer,playerController.transform.position.y + tv.y * foresseTimer);	
			currentCountdown = teleportingDuration;
			warning.SetActive (true);
			warning.transform.position = goalPosition;
			laserWarning.SetActive (true);
			Vector3 begin, end;
			begin = transform.position;
			end = goalPosition;
			laserWarning.transform.position = (begin + end) / 2;
			laserWarning.transform.localScale = new Vector3(laserWarning.transform.localScale.x, Vector2.Distance(begin,end) * warningvisualSizeMultiplier,1);
			laserWarning.transform.localRotation = Quaternion.Euler(0,0,(-Mathf.Atan2((begin.x - end.x),(begin.y - end.y)) * Mathf.Rad2Deg));
			lastPosition = transform.position;
			break;
		}
		return true;
	}
	
	public void UpdateState()
	{
		switch(currentState)
		{
		case lightStates.start:
			nextState = lightStates.hold;	
			break;
			
		case lightStates.hold:
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				nextState = lightStates.teleporting;
			}
			break;

		case lightStates.teleporting:
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				laserWall.GetComponent<laserwall>().SetSpawnPoints(lastPosition,goalPosition);
				transform.position = goalPosition;
				laserTurner1.GetComponent<laserturner>().SetPosition(transform.position,laserWall.transform.eulerAngles.z+90);
				laserTurner2.GetComponent<laserturner>().SetPosition(transform.position,laserWall.transform.eulerAngles.z-90);
				nextState = lightStates.hold;
			}
			break;
		}
	}	

	void OnDestroy () {
		if(warning)
			Destroy(warning.gameObject);
	}

	void OnDisable () {
		if(warning)
			Destroy(warning.gameObject);
	}
}