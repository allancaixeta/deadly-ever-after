﻿using UnityEngine;
using System.Collections;

public class Wood : EventController {
	
	public enum woodStates{
		VOID,
		start,
		normal
	}
	
	public float speed;
	
	public GameObject blackHole;
	woodStates currentState, nextState;
	Vector3 velocity;
	
	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		nextState = woodStates.start;
		blackHole = Instantiate (blackHole, transform.position, blackHole.transform.rotation) as GameObject;
		blackHole.SendMessage ("SetdontUsePositiveEffect");
		blackHole.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		GameObject.Find ("GameController").SendMessage("addEvent",blackHole);
		speed = speed * (1 - skills.GetAccel (false));
	}
	
	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}
	
	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case woodStates.start:
			nextState = woodStates.normal;	
			break;
			
		case woodStates.normal:
			break;
		}
		return true;
	}
	
	public void UpdateState()
	{
		switch(currentState)
		{
		case woodStates.start:
			nextState = woodStates.normal;	
			break;
			
		case woodStates.normal:
			break;
		}
	}
	
}