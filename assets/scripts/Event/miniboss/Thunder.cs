﻿using UnityEngine;
using System.Collections;

public class Thunder : EventController {
	
	public enum thunderStates{
		VOID,
		start,
		normal,
		shooting
	}
	
	public float speed;
	public float alternateAt = 5;
	public GameObject blast, laser;
	public float shootCooldown = 3;
	public float blastLifeTime = 5;
	public float timeForPlayerToMoveBeforeTheLightning = 1.5f;
	public GameObject target, thunder;

	GameObject broter;
	Transform player;
	thunderStates currentState, nextState;
	Vector3 velocity, lastPlayerPosition;
	bool functional = true;
	float currentCountdown, currentShootCooldown, lastTimePlayerMoved;
	CharacterController controller;
	SpriteRenderer targetinner, targetouter;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		controller = GetComponent<CharacterController>();
		nextState = thunderStates.start;
		if(functional)
		{
			broter = Instantiate (this.gameObject,transform.position + new Vector3(Random.Range(-3,3),Random.Range(-3,3),0) ,Quaternion.identity) as GameObject;
			broter.SendMessage ("SetUnfunctional");
			broter.SendMessage ("SetBrother",this.gameObject);
			broter.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
			GameObject.Find ("GameController").SendMessage("addEvent",broter);		
		}
		speed = speed * (1 - skills.GetAccel (false));
		target = Instantiate (target, player.position, Quaternion.identity) as GameObject;
		targetinner = target.transform.Find ("thundertargetinner").GetComponent<SpriteRenderer>();
		blackWithAlpha = targetinner.color;
		whiteWithAlpha = Color.white;
		whiteWithAlpha.a = targetinner.color.a;
		targetouter = target.transform.Find ("thundertargetouter").GetComponent<SpriteRenderer>();
		target.SetActive (false);
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}
	
	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case thunderStates.start:
			nextState = thunderStates.normal;	
			currentCountdown = alternateAt;
			blast = Instantiate(blast,new Vector3(-10,-10,1.5f),Quaternion.identity) as GameObject;
			blast.SendMessage (MnstMsgs.SetToughness,MonsterGeneric.toughness.one);
			GameObject.Find ("GameController").SendMessage("addMonster", blast);
			break;
			
		case thunderStates.normal:

			break;
		}
		return true;
	}
	
	public void UpdateState()
	{
		switch(currentState)
		{
		case thunderStates.start:
			nextState = thunderStates.normal;	

			break;
			
		case thunderStates.normal:
			currentShootCooldown -= GameController.deltaTime;
			if(functional)
			{
				Move ();
				Velocity ();
				currentCountdown -= GameController.deltaTime;
				if(currentCountdown < 0)
				{
					functional = false;
					broter.SendMessage ("SetFunctional");
				}
			}
			else
			{
				CheckPlayerMoved();
				if(lastTimePlayerMoved > timeForPlayerToMoveBeforeTheLightning)
					ShootLaser();
			}
			break;
		}
	}

	void Velocity()
	{
		velocity = player.position - transform.position;
		
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed;	
	}
	
	void Move()
	{	
		controller.Move(velocity);
	}

	public void SetUnfunctional()
	{
		functional = false;
		target.SetActive (false);
	}

	public void SetFunctional()
	{
		functional = true;
		currentCountdown = alternateAt;
	}

	public void SetBrother(GameObject b)
	{
		broter = b;
	}

	public void ShootOnPlayer()
	{
		currentShootCooldown = shootCooldown;
		blast.transform.position = transform.position;
		float angle =  Mathf.Atan2 (player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
		blast.SendMessage (MnstMsgs.SetDirection,angle);
		blast.SendMessage ("SetNewLife",blastLifeTime);
	}

	void ShootLaser()
	{
		GameObject temp = Instantiate(laser,player.position,Quaternion.identity) as GameObject;		
		temp.SendMessage (MnstMsgs.SetToughness,MonsterGeneric.toughness.one);
		//temp.SendMessage ("SetDirection",angle);
		temp.SendMessage ("SetPassThrough");
		GameObject.Find ("GameController").SendMessage("addMonster", temp);
		target.SetActive (false);
		Instantiate (thunder, transform.position, thunder.transform.rotation);
		temp = Instantiate (thunder, new Vector3(player.position.x-15,player.position.y,player.position.z) , thunder.transform.rotation) as GameObject;
		temp.transform.localRotation = Quaternion.Euler (0,-temp.transform.localRotation.eulerAngles.y,0);
		ResetPlayerMoved();
	}

	public void ResetPlayerMoved()
	{
		lastTimePlayerMoved = 0;
		targetinner.color = blackWithAlpha;
		targetouter.color = blackWithAlpha;
	}

	Color blackWithAlpha, whiteWithAlpha;
	void CheckPlayerMoved()
	{
		if(Vector2.Distance (player.position,lastPlayerPosition)> 0.1f)
		{
			ResetPlayerMoved();
			target.SetActive (false);
		}
		else
		{
			target.SetActive (true);
			lastTimePlayerMoved += GameController.deltaTime;
			float percentageToShootLaser = 0;
			percentageToShootLaser = 1 - lastTimePlayerMoved/timeForPlayerToMoveBeforeTheLightning;
			target.transform.position = Vector3.Lerp (player.position,this.transform.position,percentageToShootLaser);
			if(percentageToShootLaser < 0.5f)
				targetinner.color = Color.Lerp (whiteWithAlpha,blackWithAlpha,percentageToShootLaser * 2);
			else
				targetouter.color = Color.Lerp (whiteWithAlpha,blackWithAlpha,(percentageToShootLaser - 0.5f) * 2);
		}
		lastPlayerPosition = player.position;
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.transform.tag == Tags.Weapon)
		{
			ShootOnPlayer ();
			broter.SendMessage ("ShootOnPlayer");
		}
	}

	void OnDestroy()
	{
		Destroy (target);
	}
}