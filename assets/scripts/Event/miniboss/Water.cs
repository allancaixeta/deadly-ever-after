﻿using UnityEngine;
using System.Collections;

public class Water : EventController {
	
	public enum waterStates{
		VOID,
		start,
		normal
	}
	
	public float speed;
	public float playerDistanceSpeedMultiplier = -0.5f;
	public float mudPathCooldown;
	float currentmudPathCooldown;
	public GameObject rain, mud;
	Transform player;
	waterStates currentState, nextState;
	Vector3 velocity, goalPosition;
	CharacterController controller;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		nextState = waterStates.start;
		controller = GetComponent<CharacterController>();
		speed = speed * (1 - skills.GetAccel (false));
		GameObject temp = Instantiate (rain, new Vector3(50,50,0), Quaternion.identity) as GameObject;
		temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		temp.SendMessage ("SetdontUsePositiveEffect");
		GameObject.Find ("GameController").SendMessage("addEvent",temp);
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}
	
	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case waterStates.start:
			nextState = waterStates.normal;	
			currentmudPathCooldown = mudPathCooldown;
			break;
			
		case waterStates.normal:
			CreateNewGoal();
			break;
		}
		return true;
	}
	
	public void UpdateState()
	{
		switch(currentState)
		{
		case waterStates.start:
			nextState = waterStates.normal;	
			break;
			
		case waterStates.normal:
			Velocity ();
			Move ();
			if(Vector2.Distance (goalPosition,transform.position) < 1)
				CreateNewGoal ();
			currentmudPathCooldown -= GameController.deltaTime;
			if(currentmudPathCooldown < 0)
			{
				currentmudPathCooldown = mudPathCooldown;
				GameObject temp = Instantiate (mud, transform.position, Quaternion.identity) as GameObject;
				temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
				temp.SendMessage ("SetdontUsePositiveEffect");
				GameObject.Find ("GameController").SendMessage("addEvent",temp);
			}
			break;
		}
	}

	void Velocity()
	{
		velocity = goalPosition - transform.position;
		
		velocity.Normalize();
		velocity *= GameController.deltaTime * (speed + Vector2.Distance (player.position,transform.position) * playerDistanceSpeedMultiplier);
	}
	
	void Move()
	{	
		controller.Move(velocity);
	}

	Vector3 temp;
	void CreateNewGoal()
	{
		temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
		                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
		                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
		             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
		goalPosition = temp;		
	}
}