﻿using UnityEngine;
using System.Collections;

public class Fire : EventController {
	
	public enum fireStates{
		VOID,
		start,
		attack,
		hold
	}
	
	public float speed;
	public float attackTime;
	public float holdTime;
	public float lavaDuration = 3;
	public float speedIncreasePerWildfire = 1.03f;
	public float firePathCooldown;
	float currentFirePathCooldown;
	public GameObject lava, wildfire;
	Transform player;
	fireStates currentState, nextState;
	Vector3 direction, velocity;
	float currentCountdown, dontChangeDirectionForTheNextSeconds, reloaddontChangeDirectionForTheNextSeconds;
	CharacterController controller;
	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		controller = GetComponent<CharacterController>();
		nextState = fireStates.start;
		dontChangeDirectionForTheNextSeconds = 0.2f;
		reloaddontChangeDirectionForTheNextSeconds = dontChangeDirectionForTheNextSeconds;
		speed = speed * (1 - skills.GetAccel (false));
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}
	
	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case fireStates.start:
			nextState = fireStates.hold;	
			break;
			
		case fireStates.attack:
			currentCountdown = attackTime;			
			direction = player.position - transform.position;
			direction.z = 0;
			direction.Normalize();
			currentFirePathCooldown = firePathCooldown;
			break;

		case fireStates.hold:
			currentCountdown = holdTime;
			break;
		}
		return true;
	}
	
	public void UpdateState()
	{
		switch(currentState)
		{
		case fireStates.start:
			nextState = fireStates.attack;	
			break;
			
		case fireStates.attack:
			Velocity ();
			Move ();
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				nextState = fireStates.hold;
			}
			dontChangeDirectionForTheNextSeconds -= GameController.deltaTime;
			currentFirePathCooldown -= GameController.deltaTime;
			if(currentFirePathCooldown < 0)
			{
				currentFirePathCooldown = firePathCooldown;
				GameObject temp = Instantiate (lava, transform.position, Quaternion.identity) as GameObject;
				temp.GetComponent<lava>().SetActionWhenKillingMonster (this.gameObject,"MonsterKilledByLava");
				temp.GetComponent<lava>().SetTimedLava(lavaDuration);
				temp.GetComponent<lava>().SetdontUsePositiveEffect ();
				temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
				temp.SendMessage ("SetdontUsePositiveEffect");
				GameObject.Find ("GameController").SendMessage("addEvent",temp);
			}
			break;

		case fireStates.hold:
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				nextState = fireStates.attack;
			}
			break;
		}
	}

	void Velocity()
	{
		velocity = direction * (GameController.deltaTime * speed);	
	}
	
	void Move()
	{	
		controller.Move(velocity);
	}

	void OnControllerColliderHit(ControllerColliderHit hit)
	{	
		if(dontChangeDirectionForTheNextSeconds > 0)
			return;
		dontChangeDirectionForTheNextSeconds = reloaddontChangeDirectionForTheNextSeconds;
		if(hit.collider.tag == Tags.Wall && currentState == fireStates.attack)
			nextState = fireStates.hold;
	}	

	public void MonsterKilledByLava(Vector3 position)
	{
		print ("Killed by lava");
		wildfireController wildfireControl = (wildfireController) FindObjectOfType(typeof(wildfireController));
		if(wildfireControl != null)
		{
			if(!wildfireControl.CheckSpot(new Vector2(position.x,position.y)))
				return;
		}
		GameObject temp = Instantiate (wildfire, position, Quaternion.identity) as GameObject;
		temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		temp.SendMessage ("SetdontUsePositiveEffect");
		GameObject.Find ("GameController").SendMessage("addEvent",temp);
		temp.transform.position = position;
		speed = speed * (1 - skills.GetAccel (false)) * speedIncreasePerWildfire;
 	}

}