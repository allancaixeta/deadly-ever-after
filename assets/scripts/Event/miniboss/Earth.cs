﻿using UnityEngine;
using System.Collections;

public class Earth : EventController {
	
	public enum earthStates{
		VOID,
		start,
		burrow,
		hold,
		shooting,
		biting
	}
	
	public float speed;
	public float burrowTime = 5;
	public float holdTime = 0.5f;
	public float shootingTime = 0.5f;
	public float bitingTime = 0.5f;
	public float timeToMoveinDefinedDirection = 1;
	float currentTimeToMoveinDefinedDirection;

	public GameObject projectile, quicksand;
	earthStates currentState, nextState;
	Vector3 velocity;
	float currentCountdown, direction;
	GameController gameController;
	CharacterController controller;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		gameController = GameController.Instance;
		nextState = earthStates.start;
		controller = GetComponent<CharacterController>();
		speed = speed * (1 - skills.GetAccel (false));
		quicksand = Instantiate (quicksand, transform.position, Quaternion.identity) as GameObject;
		quicksand.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		quicksand.SendMessage ("SetdontUsePositiveEffect");
		GameObject.Find ("GameController").SendMessage("addEvent",quicksand);
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}
	
	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case earthStates.start:
			nextState = earthStates.shooting;	
			break;
			
		case earthStates.burrow:
			currentCountdown = burrowTime * GetCooldownModifier ();
			transform.Find ("Visual").gameObject.SetActive (false);
			break;

		case earthStates.hold:
			currentCountdown = holdTime;
			transform.Find ("Visual").gameObject.SetActive (true);
			break;

		case earthStates.shooting:
			currentCountdown = shootingTime;
			GameObject temp = Instantiate(projectile,transform.position,Quaternion.identity) as GameObject;		
			temp.SendMessage (MnstMsgs.SetToughness,MonsterGeneric.toughness.one);		
			gameController.addMonster (temp);	
			break;

		case earthStates.biting:
			currentCountdown = bitingTime;
			transform.Find ("Visual").gameObject.SetActive (true);
			break;
		}
		return true;
	}
	
	public void UpdateState()
	{
		switch(currentState)
		{
		case earthStates.start:
			nextState = earthStates.shooting;	
			break;
			
		case earthStates.burrow:
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				if(!CheckCollisionWithFurniture())
					nextState = earthStates.hold;
			}
			currentTimeToMoveinDefinedDirection -= GameController.deltaTime;
			if(currentTimeToMoveinDefinedDirection < 0)
			{
				ChangeDirection ();
			}
			Velocity ();
			Move ();
			break;

		case earthStates.hold:
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				nextState = earthStates.shooting;
			}
			break;

		case earthStates.shooting:
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				nextState = earthStates.burrow;
			}
			break;
			
		case earthStates.biting:
			currentCountdown -= GameController.deltaTime;
			if(currentCountdown < 0)
			{
				nextState = earthStates.burrow;
			}
			break;
		}
	}

	void Velocity()
	{
		velocity = new Vector3(Mathf.Cos(Mathf.Deg2Rad * direction),Mathf.Sin(Mathf.Deg2Rad * direction),0);
		velocity.Normalize();
		velocity *= GameController.deltaTime * speed;
	}
		
	void Move()
	{		
		controller.Move(velocity);
	}

	void ChangeDirection()
	{
		currentTimeToMoveinDefinedDirection = timeToMoveinDefinedDirection;
		direction = Random.Range (0,360);
	}

	bool CheckCollisionWithFurniture()
	{
		int layerMask = 1024;
		Vector3 upTemp = transform.position + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,transform.position - upTemp);
		if(Physics.SphereCast(ray,controller.radius,1000,layerMask))
			return true;
		else
			return false;
	}

	void OnTriggerEnter(Collider other)
	{
		if(currentState != earthStates.burrow)
			return;
		if(other.transform.tag == "PlayerHit")
		{
			nextState = earthStates.biting;
		}
	}	
}