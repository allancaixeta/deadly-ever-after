﻿using UnityEngine;
using System.Collections;

public class Darkness : EventController {
	
	public enum darknessStates{
		VOID,
		start,
		normal
	}
	
	public float blackHoleSpeed;

	public GameObject blackHole, thenothing, nightfall;
	Transform player;
	darknessStates currentState, nextState;
	Vector3 velocity;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		RoomController room = (RoomController) FindObjectOfType(typeof(RoomController));	
		room.ThereIsADarkness ();

		player = GameObject.FindGameObjectWithTag ("Player").transform;
		nextState = darknessStates.start;
		blackHole = Instantiate (blackHole, transform.position, blackHole.transform.rotation) as GameObject;
		blackHole.SendMessage ("SetdontUsePositiveEffect");
		blackHole.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		blackHoleSpeed = blackHoleSpeed * (1 - skills.GetAccel (false));
		GameObject.Find ("GameController").SendMessage("addEvent",blackHole);

		thenothing = Instantiate (thenothing, transform.position, thenothing.transform.rotation) as GameObject;
		thenothing.SendMessage ("SetdontUsePositiveEffect");
		thenothing.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		GameObject.Find ("GameController").SendMessage("addEvent",thenothing);
		thenothing.transform.position = transform.position;

		nightfall = Instantiate (nightfall, transform.position, thenothing.transform.rotation) as GameObject;
		nightfall.SendMessage ("SetdontUsePositiveEffect");
		nightfall.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		GameObject.Find ("GameController").SendMessage("addEvent",nightfall);
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}
	
	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case darknessStates.start:
			nextState = darknessStates.normal;	
			break;
			
		case darknessStates.normal:
			break;
		}
		return true;
	}
	
	public void UpdateState()
	{
		switch(currentState)
		{
		case darknessStates.start:
			nextState = darknessStates.normal;	
			break;
			
		case darknessStates.normal:
			BlackHoleMovement();
			break;
		}
	}

	void BlackHoleMovement()
	{
		if(blackHole)
		{
			velocity = player.position - blackHole.transform.position;
			velocity.Normalize();
			velocity *= GameController.deltaTime * blackHoleSpeed;
			if (Vector2.Distance (player.position, blackHole.transform.position) < 1)
				return;
			blackHole.transform.Translate (velocity,Space.World);
		}
	}
}