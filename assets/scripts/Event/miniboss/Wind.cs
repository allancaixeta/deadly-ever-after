﻿using UnityEngine;
using System.Collections;

public class Wind : EventController {

	public enum windStates{
		VOID,
		start,
		normal
	}

	public float speed;
	public float speedVertical = 10;
	public float speedHorizontal = 1;
	public float horizontalRange = 0.1f;
	public float growthSize = 4;
	public float growthRate = 0.01f;
	public float timePlayerIsInsideFogBeforeAttack = 1;
	public GameObject fog, sink;
	CharacterController controller;
	float directionH = 1.4f;
	float directionV = 1;
	float angle, currentTimePlayerInsideFog;
	int goingright = 1;
	bool changeDirection = false;
	public Vector3 spwPoint = new Vector3(16,51,0);
	Transform goalPosition;
	Vector3 velocity, originalCenter;
	bool playerInsideFog, expanding;
	bool growing = false;
	Vector3 originalScale;
	PlayerHitController playerHitController;
	float currentGrowth;
	enum movingstates
	{
		up,
		down
	}
	movingstates move = movingstates.up;
	windStates currentState, nextState;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		goalPosition = new GameObject().transform;
		controller = GetComponent<CharacterController>();
		playerHitController = (PlayerHitController) FindObjectOfType(typeof(PlayerHitController));
		sink = Instantiate (sink, transform.position, sink.transform.rotation) as GameObject;
		sink.SendMessage ("SkipVisual");
		sink.SendMessage ("SetdontUsePositiveEffect");
		sink.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		sink.transform.parent = this.transform;
		sink.transform.localPosition = new Vector3 (0,0,-6.5f);
		GameObject.Find ("GameController").SendMessage("addEvent",sink);
		originalScale = sink.transform.localScale;

		fog = Instantiate (fog, transform.position, fog.transform.rotation) as GameObject;
		fog.SendMessage ("SetFather",this.gameObject);
		fog.SendMessage ("SkipVisual");
		fog.SendMessage ("SetdontUsePositiveEffect");
		fog.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		fog.transform.parent = sink.transform;
		fog.transform.localPosition = new Vector3 (0,0,-0.1f);
		GameObject.Find ("GameController").SendMessage("addEvent",fog);

		nextState = windStates.start;
		expanding = true;
		speed = speed * (1 - skills.GetAccel (false));
	}

	public override void SpecificEventUpdate()
	{
		if(!StartState())
			UpdateState();
	}

	public bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case windStates.start:
			nextState = windStates.normal;	
			break;
			
		case windStates.normal:
			break;
		}
		return true;
	}

	public void UpdateState()
	{
		switch(currentState)
		{
		case windStates.start:
			nextState = windStates.normal;	
			break;
			
		case windStates.normal:
			CloudMovement();
			Velocity ();
			Move();
			if(expanding)
			{
				if(currentGrowth == 0)
					growing = true;
				if(growing)
				{
					currentGrowth += growthRate;
					if(currentGrowth > 1)
						growing = false;
					sink.transform.localScale = Vector3.Lerp(originalScale,originalScale*growthSize,currentGrowth);
				}
			}
			else
			{
				if(growing)
				{
					currentGrowth -= growthRate*10;
					if(currentGrowth <= 0)
					{
						growing = false;
						currentGrowth = 0;
						expanding = true;
					}
					sink.transform.localScale = Vector3.Lerp(originalScale,originalScale*growthSize,currentGrowth);
				}
				else
					if(sink.transform.localScale != originalScale)
					{
						growing = true;
					}
			}
			if(playerInsideFog)
			{
				currentTimePlayerInsideFog -= GameController.deltaTime;
				if(currentTimePlayerInsideFog < 0)
				{
					if(playerHitController.HitPlayer(damage,eventName))
					{
						currentTimePlayerInsideFog = timePlayerIsInsideFogBeforeAttack;
						expanding = false;
					}
				}
			}
			else
				currentTimePlayerInsideFog = timePlayerIsInsideFogBeforeAttack;
			break;
		}
	}

	bool returningToSpawnPos = false;
	void Velocity()
	{
		if(returningToSpawnPos)
		{
			velocity = cenarioLimits.position - transform.position;
			
			velocity.Normalize();
			velocity *= GameController.deltaTime * speed;
			if(Vector2.Distance (cenarioLimits.position,transform.position) < 2)
			{
				returningToSpawnPos = false;
				move = movingstates.up;
				directionH = 1.4f;   //directionH == 2 ? 2 : -1;
				directionV = 1;
				angle = 0;
				goingright = 1;
			}
		}
		else
		{
			//velocity = new Vector3(speedHorizontal*direction*goingright,Mathf.Sin(Mathf.Deg2Rad * angle * goingright)*speedVertical*direction,0);
			velocity = new Vector3(speedHorizontal*directionH,Mathf.Sin(Mathf.Deg2Rad * angle)*speedVertical*directionV,0);
				
			velocity.Normalize();
			velocity *= GameController.deltaTime * speed;
			if(Vector2.Distance (cenarioLimits.position,transform.position) > 100)
				returningToSpawnPos = true;
		}
	}

	void Move()
	{		
		controller.Move(velocity);
	}
	
	void CloudMovement()
	{
		switch(move)
		{
		case movingstates.up:
			angle += horizontalRange;
			if(angle > 360)
			{
				move = movingstates.down;
				angle = 0;
				directionV = -1;
				//direction = direction == 1 ? -1 : 1;
				//speedHorizontal /= 2;
				if(changeDirection)
				{
					goingright = goingright == 1 ? -1 : 1;
					changeDirection = false;
				}
				if(goingright == 1)
					directionH = -1;
				else
					directionH = -1.4f;
			}
			break;
		case movingstates.down:
			angle += horizontalRange;
			if(angle > 360)
			{
				move = movingstates.up;
				//speedHorizontal *= 2;
				//direction = direction == 1 ? -1 : 1;
				angle = 0;
				//directionH = directionH == 1 ? -1 : 1; //directionH == 2 ? -1 : 2;
				directionV = 1;
				if(changeDirection)
				{
					goingright = goingright == 1 ? -1 : 1;
					changeDirection = false;
				}
				if(goingright == 1)
					directionH = 1.4f;
				else
					directionH = 1;
			}
			break;
		}
		if(transform.position.x < cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 && goingright == -1)
		{
			changeDirection = true;
		}
		if(transform.position.x > cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 && goingright == 1)
		{
			changeDirection = true;	
		}
	}
	
	void OnDestroy () {
		if(goalPosition)
			Destroy(goalPosition.gameObject);
	}
	
	public void PlayerInsideFog()
	{
		playerInsideFog = true;
	}

	public void PlayerLeavingFog()
	{
		playerInsideFog = false;
	}
}
