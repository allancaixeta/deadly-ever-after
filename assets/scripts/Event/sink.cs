﻿using UnityEngine;
using System.Collections;

public class sink : EventController {

	public float stopFromMiddleDistance = 20;
	public int additionQuantity = 2;
	public GameObject Sink;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 + stopFromMiddleDistance,
		                                              cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 - stopFromMiddleDistance),
		                                 Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2 + stopFromMiddleDistance,
		             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2 - stopFromMiddleDistance),1.5f);
		while(additionQuantity != 0)
		{
			CreateAnotherSink();
			additionQuantity--;
		}
		transform.Rotate (0, 0, 90);
	}

	public override void SpecificEventUpdate()
	{
	}

	void OnTriggerEnter(Collider other)
	{
		switch(other.transform.tag)
		{
		default:
			break;
		case Tags.Weapon:
			other.SendMessage("EnterSink");
			break;
		//case Tags.Monster:
		case Tags.MonsterProjectile:
		case Tags.StunMonster:
			if(other.GetComponent<MonsterGeneric>().GetMonsterType() == MonsterGeneric.monsterTypes.projectile)
				other.SendMessage("OneHitDeath");
			break;
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter(other);
	}

	void CreateAnotherSink()
	{
		GameObject temp = Instantiate (Sink, transform.position, Quaternion.identity) as GameObject;
		if(dontUsePositiveEffect)
			temp.SendMessage ("SetdontUsePositiveEffect");
		temp.SendMessage ("ResetQuantity");
		temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		GameObject.Find ("GameController").SendMessage("addEvent",temp);
	}
	
	public void ResetQuantity()
	{
		additionQuantity = 0;
	}

	public void SkipVisual()
	{

	}
}
