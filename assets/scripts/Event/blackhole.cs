﻿using UnityEngine;
using System.Collections;

public class blackhole : EventController {

	PlayerController playerController;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		playerController = PlayerController.Instance;
		transform.position = new Vector3 (50,50,1.5f);
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.transform.tag == Tags.PlayerHit)
			playerController.OneHitDeath();
		if(other.transform.tag == Tags.Monster)
			other.SendMessage("OneHitDeath");
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter(other);
	}
	
	public void SkipVisual()
	{
		
	}
}
