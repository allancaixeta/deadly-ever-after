﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class lava : EventController {
	
	public int quantity;
	PlayerHitController playerHit;
	List<string> monsterHitListID;
	List<float> monsterHitListDuration;
	private List<MonsterGeneric> listOfMonstersBurning;
	private List<float> listOfMonstersBurningCountdownToHit;
	private List<int> listOfMonstersBurningCycle;
	float playerBurningCountdownToHit, playerBurningCycle, lifeTime;
	GameObject father;
	string functionToCallWhenKillingMonster;
	// Use this for initialization
	public override void SpecificEventStart(StageController.StageNumber stage)
	{		
		monsterHitListID = new List<string> ();
		monsterHitListDuration = new List<float> ();
		listOfMonstersBurning = new List<MonsterGeneric>();
		listOfMonstersBurningCountdownToHit = new List<float>();
		listOfMonstersBurningCycle = new List<int>();
		playerHit = PlayerHitController.Instance;
		if(quantity > 0)
		{
			for (int i = 0; i < quantity; i++) {
				GameObject temp = Instantiate (this.gameObject, transform.position, Quaternion.identity) as GameObject;
				temp.GetComponent<lava>().quantity = 0;
				if(father)
					temp.GetComponent<lava>().SetActionWhenKillingMonster (father,"MonsterKilledByLava");
				temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
				GameObject.Find ("GameController").SendMessage("addEvent",temp);
			}
		}
		if(!father)
		{
			transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
		                                              cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
		                                 Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
		             						cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
			while(Vector2.Distance (transform.position,GameObject.FindGameObjectWithTag("Player").transform.position) < GameController.minPlayerDistanceToSpawn)
			{
				transform.position = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
				                                              cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				                                 Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
			}
		}
		transform.Rotate (0, 0, 90);
	}
	
	public override void SpecificEventUpdate()
	{   
		DecreaseMonsterHitListDuration();
		if(playerBurningCycle > 0)
		{
			playerBurningCountdownToHit -= GameController.deltaTime;
			if(playerBurningCountdownToHit < 0)
			{
				playerHit.HitPlayer(damage/3,eventName);
				playerBurningCountdownToHit = 1;
				playerBurningCycle--;
			}
		}
		for (int i = 0; i < listOfMonstersBurning.Count; i++) {
			listOfMonstersBurningCountdownToHit[i] -= GameController.deltaTime;
			if(!listOfMonstersBurning[i])
			{
				listOfMonstersBurning.RemoveAt (i);
				listOfMonstersBurningCountdownToHit.RemoveAt (i);
				listOfMonstersBurningCycle.RemoveAt (i);
			}
			else
			{
				if(listOfMonstersBurningCountdownToHit[i] < 0)
				{
					listOfMonstersBurning[i].Hit (monsterDamage/3);
					listOfMonstersBurningCountdownToHit[i] = 1;
					listOfMonstersBurningCycle[i]++;
					if(listOfMonstersBurningCycle[i] >= 3)
					{
						listOfMonstersBurning.RemoveAt (i);
						listOfMonstersBurningCountdownToHit.RemoveAt (i);
						listOfMonstersBurningCycle.RemoveAt (i);
					}
				}
			}
		}
		if(lifeTime > 0)
		{
			lifeTime -= GameController.deltaTime;
			if(lifeTime < 0)
				DestroyItSelf ();
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Monster:
			for (int i = 0; i < monsterHitListID.Count; i++){
				if(monsterHitListID[i] == other.transform.name)
					return;
			}	
			monsterHitListID.Add(other.transform.name);
			monsterHitListDuration.Add(1);
			if(father)
			{
				if(other.GetComponent<MonsterGeneric>().Hit(monsterDamage))
					father.SendMessage (functionToCallWhenKillingMonster, other.transform.position);
			}
			else
				other.GetComponent<MonsterGeneric>().Hit(monsterDamage);
			for (int i = 0; i < listOfMonstersBurning.Count; i++) {
				if(listOfMonstersBurning[i].name == other.name)
				{
					listOfMonstersBurningCountdownToHit[i] = 1;
					listOfMonstersBurningCycle[i] = 0;
					return;
				}
			}
			listOfMonstersBurning.Add (other.GetComponent<MonsterGeneric>());
			listOfMonstersBurningCountdownToHit.Add (1);
			listOfMonstersBurningCycle.Add (0);
			break;
		case "PlayerHit":
			playerHit.HitPlayer(damage,eventName);
			playerBurningCycle = 3;
			playerBurningCountdownToHit = 1.05f;
			break;
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);	
	}
		
	void DecreaseMonsterHitListDuration()
	{
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
	}

	public void SetActionWhenKillingMonster(GameObject f, string m)
	{
		father = f;
		functionToCallWhenKillingMonster = m;
	}

	public void SetTimedLava(float t)
	{
		lifeTime = t;
	}
}
