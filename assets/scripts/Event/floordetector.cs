﻿using UnityEngine;
using System.Collections;

public class floordetector : EventController {
	
	public enum hazards
	{
		mud,
		quicksand
	}
	public hazards hazardType;
	
	//public Vector2 lvl1size,lvl2size,lvl3size,lvl4size,lvl5size;
	public int quantity;
	public float phisycalOccupationRadius = 1;
	public EventController.sides sideOfStage;
	public GameObject mudPrefab;
	EventController [] elements;
	
	public enum modus
	{
		permanent,
		temporary
	}	
	
	public modus modusOperandi = modus.permanent;
	
	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		BuildHazards(quantity);
	}
	
	void BuildHazards(int quantity)
	{
		elements = new EventController[quantity];
		for (int i = 0; i < quantity; i++) {
			elements[i] = (Instantiate(mudPrefab,CreateNewPosition(),Quaternion.identity) as GameObject).GetComponent<EventController>();
			elements[i].SendMessage ("SetModus",modusOperandi);
			elements[i].SendMessage ("SetHazardType",hazardType);
			elements[i].SpecificEventStart(StageController.StageNumber.stage1);
			elements[i].transform.parent = this.transform;
			elements [i].transform.Rotate (0, 0, 90);
		}
	}
	
	public override void SpecificEventUpdate()
	{
		for (int i = 0; i < elements.Length; i++) {
			elements[i].SpecificEventUpdate();
		}
	}
	
	Vector3 CreateNewPosition()
	{
		Vector3 temp = GetPosition ();
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,temp - upTemp);
		RaycastHit hit;
		int count = 0;

		if(Physics.SphereCast (ray,phisycalOccupationRadius,out hit, 100)){			
			while(hit.collider.tag != Tags.Floor)
			{			
				count++;
				temp = GetPosition ();
				upTemp = temp + new Vector3(0,0,-10);
				ray = new Ray(upTemp,temp - upTemp);
				Debug.DrawRay(upTemp,temp - upTemp,Color.white,5);
				Physics.SphereCast (ray,phisycalOccupationRadius,out hit, 100);
				if(count == 10)
				{
					return temp;
				}
			}
		}
		return temp;
	}

	Vector3 GetPosition()
	{
		Vector3 temp;
		switch(sideOfStage)//0 left; 1 right; 2 both; 3 on top of itself
		{
		case sides.left:
			temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
			                                cenarioLimits.position.x),
			                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
			             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
			while(Vector2.Distance (temp,GameObject.FindGameObjectWithTag("Player").transform.position) < GameController.minPlayerDistanceToSpawn)
			{
				temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
				                                cenarioLimits.position.x),
				                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);
			}
			break;
		case sides.right:
			temp = new Vector3(Random.Range(cenarioLimits.position.x,
			                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
			                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
			             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
			while(Vector2.Distance (temp,GameObject.FindGameObjectWithTag("Player").transform.position) < GameController.minPlayerDistanceToSpawn)
			{
				temp = new Vector3(Random.Range(cenarioLimits.position.x,
				                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
			}
			break;
		default:
		case sides.both:
			temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
			                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
			                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
			             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
			while(Vector2.Distance (temp,GameObject.FindGameObjectWithTag("Player").transform.position) < GameController.minPlayerDistanceToSpawn)
			{
				temp = new Vector3(Random.Range(cenarioLimits.position.x - cenarioLimits.lossyScale.x/2,
				                                cenarioLimits.position.x + cenarioLimits.lossyScale.x/2),
				                   Random.Range(cenarioLimits.position.y - cenarioLimits.lossyScale.y/2,
				             cenarioLimits.position.y + cenarioLimits.lossyScale.y/2),1.5f);	
			}
			break;
		case sides.ontop:
			temp = transform.position;
			break;
		}
		return temp;
	}
	
	public void SetSide(sides side)//0 left; 1 right; 2 both
	{
		this.sideOfStage = side;
	}
}
