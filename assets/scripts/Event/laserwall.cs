﻿using UnityEngine;
using System.Collections;

public class laserwall : EventController {
	
	public enum laserwallType{
		cross,
		x,
		predefined
	}
	public laserwallType laserType;
	public GameObject laserWall;
	public float speed, visualSizeMultiplier, stopFromMiddleDistance, stopFromEdgeDistance;
	
	public float creationTime = 1;
	float up, down, left, right, invertMovement;
	Vector3 velocity, begin, end;
	bool isUp, isVertical;
	bool creatingLaser = true;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		GetComponent<BoxCollider>().center = new Vector3(0,0,10);
		SetBases ();
		speed = speed * (1 - skills.GetAccel (false));
		invertMovement = 1;
	}
	
	public override void SpecificEventUpdate()
	{
		if(creatingLaser)
		{
			creationTime -= GameController.deltaTime;
			if(creationTime < 0)
			{
				creatingLaser = false;
				GetComponent<BoxCollider>().center = new Vector3(0,0,-6.5f);
			}
		}
		switch(laserType)
		{
		case laserwallType.cross:
			if(isVertical)
			{
				if(isUp)
				{
					begin -= new Vector3(invertMovement * GameController.deltaTime * speed,0,0);//L
					end -= new Vector3(invertMovement * GameController.deltaTime * speed,0,0);//L
					//if(begin.x > right || end.x < 50 + stopFromMiddleDistance || begin.x < 50 + stopFromMiddleDistance || end.x > right)
					//invertMovement = -invertMovement;
					if(invertMovement == 1 && begin.x < 50 + stopFromMiddleDistance)
						invertMovement = -1;
					else
						if(invertMovement == -1 && begin.x > right - stopFromEdgeDistance)
							invertMovement = 1;
				}
				else
				{
					begin += new Vector3(invertMovement * GameController.deltaTime * speed,0,0);//R
					end += new Vector3(invertMovement * GameController.deltaTime * speed,0,0);//R
					//if(begin.x > 50 - stopFromMiddleDistance || end.x < left || begin.x < left || end.x > 50 - stopFromMiddleDistance)
					//invertMovement = -invertMovement;
					if(invertMovement == 1 && begin.x > 50 - stopFromMiddleDistance)
						invertMovement = -1;
					else
						if(invertMovement == -1 && begin.x < left + stopFromEdgeDistance)
							invertMovement = 1;
				}
				transform.position = new Vector3(begin.x,50,1.5f);
			}
			else
			{
				if(isUp)
				{
					begin -= new Vector3(0,invertMovement * GameController.deltaTime * speed,0);//D
					end -= new Vector3(0,invertMovement * GameController.deltaTime * speed,0);//D
					//if(begin.y > up || end.y < 50 + stopFromMiddleDistance || begin.y < 50 + stopFromMiddleDistance || end.y > up)
					//invertMovement = -invertMovement;
					if(invertMovement == 1 && begin.y < 50 + stopFromMiddleDistance)
						invertMovement = -1;
					else
						if(invertMovement == -1 && begin.y > up - stopFromEdgeDistance)
							invertMovement = 1;
				}
				else
				{
					begin += new Vector3(0,invertMovement * GameController.deltaTime * speed,0);//U
					end += new Vector3(0,invertMovement * GameController.deltaTime * speed,0);//U
					//if(begin.y > 50 - stopFromMiddleDistance || end.y < down || begin.y < down || end.y > 50 - stopFromMiddleDistance)
					if(invertMovement == 1 && begin.y > 50 - stopFromMiddleDistance)
						invertMovement = -1;
					else
						if(invertMovement == -1 && begin.y < down + stopFromEdgeDistance)
							invertMovement = 1;
				}
				transform.position = new Vector3(50,begin.y,1.5f);
			}
			break;
		case laserwallType.x:
			transform.localScale = new Vector3(transform.localScale.x, Vector2.Distance(begin,end) * visualSizeMultiplier,1);
			transform.localRotation = Quaternion.Euler(0,0,(-Mathf.Atan2((begin.x - end.x),(begin.y - end.y)) * Mathf.Rad2Deg));
			if(isVertical)
			{
				begin += new Vector3(invertMovement * GameController.deltaTime * speed,0,0);//R
				end -= new Vector3(invertMovement * GameController.deltaTime * speed,0,0);//L
				if(invertMovement == 1 && (begin.x > right || end.x < left))
				{
					isVertical =!isVertical;
				}
				else
					if(invertMovement == -1 && (begin.x < left || end.x > right))
					{
						isVertical =!isVertical;						
					}
			}
			else
			{
				begin += new Vector3(0,invertMovement * GameController.deltaTime * speed,0);//U
				end -= new Vector3(0,invertMovement * GameController.deltaTime * speed,0);//D
				if(invertMovement == 1 && (begin.y > up || end.y < down))
				{
					invertMovement = -1;
					isVertical =!isVertical;
				}
				else
					if(invertMovement == -1 && (begin.y < down || end.y > up))
					{
						isVertical =!isVertical;
						invertMovement = 1;
					}
			}
			break;
		case laserwallType.predefined:
			break;
		}

	}

	public void SetSpawnPoints(Vector3 begin, Vector3 end)
	{
		this.begin = begin;
		this.end = end;
		transform.localScale = new Vector3(transform.localScale.x, Vector2.Distance(begin,end) * visualSizeMultiplier,1);
		transform.localRotation = Quaternion.Euler(0,0,(-Mathf.Atan2((begin.x - end.x),(begin.y - end.y)) * Mathf.Rad2Deg));
		Vector3 aux = (begin + end) / 2;
		aux.z = 1.5f;
		transform.position = aux;
	}

	public void SetBases()
	{
		up = cenarioLimits.position.y + cenarioLimits.lossyScale.y/2 - 1;
		down = cenarioLimits.position.y - cenarioLimits.lossyScale.y/2 + 1;
		left = cenarioLimits.position.x - cenarioLimits.lossyScale.x/2 + 1;
		right = cenarioLimits.position.x + cenarioLimits.lossyScale.x/2 - 1;
		switch(laserType)
		{
		case laserwallType.cross:
			if(isUp)
			{
				if(isVertical)
				{
					begin = new Vector3(right,up,1.5f);//UR
					end = new Vector3(right,down,1.5f);//DR
					transform.position = new Vector3(right,50,1.5f);
				}
				else
				{
					begin = new Vector3(left,up,1.5f);//UL
					end = new Vector3(right,up,1.5f);//UR
					transform.position = new Vector3(50,up,1.5f);
				}
			}
			else
			{
				Vector2 playerposition = GameObject.FindGameObjectWithTag("Player").transform.position;
				isVertical  = playerposition.x == cenarioLimits.position.x ? true : false;
				if(isVertical)
				{
					begin = new Vector3(left,up,1.5f);//UL
					end = new Vector3(left,down,1.5f);//DL
					transform.position = new Vector3(left,50,1.5f);
				}
				else
				{
					begin = new Vector3(left,down,1.5f);//DL
					end = new Vector3(right,down,1.5f);//DR
					transform.position = new Vector3(50,down,1.5f);
				}
				CreateAnotherLaser ();
			}
			transform.localScale = new Vector3(transform.localScale.x, Vector2.Distance(begin,end) * visualSizeMultiplier,1);
			if(!isVertical)
				transform.localRotation = Quaternion.Euler(0,0,(-Mathf.Atan2((begin.x - end.x),(begin.y - end.y)) * Mathf.Rad2Deg));
			break;
		case laserwallType.x:
			begin = new Vector3(left,up,1.5f);//UL
			end = new Vector3(right,down,1.5f);//DR
			//isVertical  = Random.value < 0.5f ? true : false;
			transform.position = new Vector3(50,50,1.5f);
			invertMovement = -1;
			break;
		case laserwallType.predefined:
			break;
		}
	}

	public void SetIsUp(bool isVertical)
	{
		isUp = true;
		this.isVertical = isVertical;
	}

	public void SetlaserwallType(laserwallType l)
	{
		laserType = l;
	}

	void CreateAnotherLaser()
	{
		GameObject temp = Instantiate (laserWall, transform.position, Quaternion.identity) as GameObject;
		if(dontUsePositiveEffect)
			temp.SendMessage ("SetdontUsePositiveEffect");
		temp.SendMessage ("SetIsUp",isVertical);
		temp.SendMessage ("SetlaserwallType",laserType);
		temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		GameObject.Find ("GameController").SendMessage("addEvent",temp);
	}

}