﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class floorhazard : EventController {

	public enum hazards
	{
		spines,
		blocks,
		lava,
		mine
	}
	public hazards hazardType;
	public EventController.sides sideOfStage;
	public Vector2 size;
	public int quantity;
	public float phisycalOccupationRadius = 1;
	public GameObject spinePrefab;
	EventController [] elements;
	public int height = 2;//0 left; 1 right; 2 both
	public enum modus
	{
		permanent,
		timed,
		pressure
	}	
	
	public modus modusOperandi = modus.permanent;
	public bool oneTimeOnly;

	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		BuildHazards(quantity,size);
	}

	void BuildHazards(int quantity, Vector2 size)
	{
		elements = new EventController[quantity];
		for (int i = 0; i < quantity; i++) {
			elements[i] = (Instantiate(spinePrefab,CreateNewPosition(),Quaternion.identity) as GameObject).GetComponent<EventController>();
			elements[i].SendMessage ("SetSize",size,SendMessageOptions.DontRequireReceiver);
			elements[i].SendMessage ("SetModus",modusOperandi,SendMessageOptions.DontRequireReceiver);
			elements[i].SendMessage ("SetHazardType",hazardType,SendMessageOptions.DontRequireReceiver);
			elements[i].SendMessage ("SetOneTimeOnly",oneTimeOnly,SendMessageOptions.DontRequireReceiver);
			elements[i].SpecificEventStart(StageController.StageNumber.stage1);
			elements[i].transform.parent = this.transform;
			elements [i].transform.Rotate (0, 0, 90);
		}
	}

	public override void SpecificEventUpdate()
	{
		for (int i = 0; i < elements.Length; i++) {
			elements[i].SpecificEventUpdate();
		}
	}

	Vector3 CreateNewPosition()
	{
		Vector3 temp = GetPosition ();
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,temp - upTemp);
		RaycastHit hit;
		int count = 0;
		int layermask = 136704;
		if(Physics.SphereCast (ray,phisycalOccupationRadius,out hit, 100, layermask)){
			while(hit.collider.tag != Tags.Floor)
			{			
				count++;
				temp = GetPosition ();
				upTemp = temp + new Vector3(0,0,-10);
				ray = new Ray(upTemp,temp - upTemp);
				Debug.DrawRay(upTemp,temp - upTemp,Color.white,5);
				Physics.SphereCast (ray,phisycalOccupationRadius,out hit, 100, layermask);
				if(count == 10)
				{
					return temp;
				}
			}
		}
		return temp;
	}

	Vector3 GetPosition()
	{
		Vector3 temp;
		float minX, maxX, minY, maxY;
		maxX = 0;
		switch(height)//0 up; 1 down; 2 both;
		{
		case 0:
			minY = cenarioLimits.position.y;
			maxY = cenarioLimits.position.y + cenarioLimits.lossyScale.y/2;
			break;
		case 1:
			minY = cenarioLimits.position.y - cenarioLimits.lossyScale.y/2;
			maxY = cenarioLimits.position.y;
			break;
		default:
		case 2:
			minY = cenarioLimits.position.y - cenarioLimits.lossyScale.y/2;
			maxY = cenarioLimits.position.y + cenarioLimits.lossyScale.y/2;
			break;
		}
		switch(sideOfStage)//0 left; 1 right; 2 both; 3 on top of itself
		{
		case sides.left:
			minX = cenarioLimits.position.x - cenarioLimits.lossyScale.x/2;
			maxX = cenarioLimits.position.x;
			break;
		case sides.right:
			minX = cenarioLimits.position.x;
			maxX = cenarioLimits.position.x + cenarioLimits.lossyScale.x/2;
			break;
		default:
		case sides.both:
			minX = cenarioLimits.position.x - cenarioLimits.lossyScale.x/2;
			maxX = cenarioLimits.position.x + cenarioLimits.lossyScale.x/2;
			break;
		case sides.ontop:
			temp = transform.position;
			return temp;
		}
		temp = new Vector3(Random.Range(minX,maxX),Random.Range(minY,maxY),1.5f);	
		while(Vector2.Distance (temp,GameObject.FindGameObjectWithTag("Player").transform.position) < GameController.minPlayerDistanceToSpawn)
		{
			temp = new Vector3(Random.Range(minX,maxX),Random.Range(minY,maxY),1.5f);		
		}
		return temp;
	}

	public void SetSide(int side)//0 left; 1 right; 2 both
	{
		switch(side)//0 left; 1 right; 2 both; 3 on top of itself
		{
		case 0:
			this.sideOfStage = sides.left;
			break;
		case 1:
			this.sideOfStage = sides.right;
			break;
		default:
		case 2:
			this.sideOfStage = sides.both;
			break;
		case 3:
			this.sideOfStage = sides.ontop;
			break;
		}
	}

	public void SetSide(sides side)//0 left; 1 right; 2 both
	{
		this.sideOfStage = side;
	}

	public void SetHeight(int height)//0 up; 1 down; 2 both
	{
		this.sideOfStage = sides.both;
		this.height = height;
	}
}
