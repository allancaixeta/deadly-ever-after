﻿using UnityEngine;
using System.Collections;

public class rain : EventController {
	
	public float speedReduced = 0.2f;
	public bool snow;
	PlayerController playercontroller;
	public override void SpecificEventStart(StageController.StageNumber stage)
	{
		if(!snow)
			skills.StartRaining (speedReduced);
		else
			skills.StartSnow (speedReduced);
		playercontroller = PlayerController.Instance;
		playercontroller.Raining (speedReduced);
	}
	
	public override void SpecificEventUpdate()
	{

	}

	void OnDestroy()
	{
		if(!snow)
			skills.StopRaining();
		else
			skills.StopSnow ();
		playercontroller.Raining (0);
	}
}