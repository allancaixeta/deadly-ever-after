﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class wildfireController : EventController {

	List<Transform> fires;

	// Use this for initialization
	void Awake () {
		fires = new List<Transform> ();
	}
	
	public override void SpecificEventStart(StageController.StageNumber stage)
	{

	}

	public override void SpecificEventUpdate()
	{
	}

	public void Reset()
	{
		fires = new List<Transform> ();
	}

	public void InsertFire(Transform fire)
	{
		fires.Add (fire);
	}

	public bool CheckSpot(Vector2 position)
	{
		for (int i = 0; i < fires.Count; i++) {
			if(Vector2.Distance (position,fires[i].position) < 2)
				return false;
		}
		return true;
	}
}
