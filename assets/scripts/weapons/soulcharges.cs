﻿using UnityEngine;
using System.Collections;

public class soulcharges : WeaponController {

	public float afterHitSurvival = 0.5f;
	public float afterHitSizeIncrease = 2;
	float soulCount;
	public GameObject FXsoulPlayer, FXsoulMonster;
	Ray ray;
	RaycastHit hit;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.soulcharges;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			countDown = LifeTime;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.normal:

			float speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			ray = new Ray(transform.position,direction);
			int layerMask = 2321408;
			if(Physics.SphereCast(ray, speed, out hit, 200, layerMask)){
				switch(hit.collider.tag)
				{
				case Tags.Wall:
				case Tags.Unmovable:
				case Tags.Lvl3Obstacle:
				case Tags.HarmEvent:
					Instantiate(onHitEffect,hit.point,Quaternion.identity);
					player.GetComponent<PlayerController>().ResetSoulChargeCount();
					break;
					
				case Tags.Monster:
				case Tags.BossMonster:	
					CheckForHit(hit.collider.GetComponent<MonsterGeneric>(),hit.collider.name,hit.collider.ClosestPointOnBounds(transform.position));	
					break;
				}
			}
			DestroyItSelf ();
			break;
			
		case weaponControllerStates.finish:
			Instantiate(onHitEffect,transform.position,Quaternion.identity);
			player.GetComponent<PlayerController>().ResetSoulChargeCount();
			transform.FindChild ("Shadow").gameObject.SetActive (false);
			weaponVisual.gameObject.SetActive (false);
			GetComponent<CapsuleCollider>().radius *= afterHitSizeIncrease;
			countDown = afterHitSurvival;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			MoveForward();
			CheckIfInsideRoom ();
			break;			
		case weaponControllerStates.finish:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				DestroyItSelf();
			break;
		}
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		return;
		/*string monsterID = other.name;
		switch(other.tag)
		{
		case Tags.Unmovable:
		case Tags.Lvl3Obstacle:
			nextState = weaponControllerStates.finish;
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:	
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
			break;
		}*/
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;	
		HitMonster ((int)(damage + ((float)player.GetComponent<PlayerController> ().GetandIncreaseSoulCount (monster.name) * AttackSpeed)), monster, transform.position);
		GameObject temp1 = Instantiate(FXsoulPlayer,player.transform.position,FXsoulPlayer.transform.rotation) as GameObject;
		temp1.transform.parent = player.transform;
		GameObject temp2 = Instantiate(FXsoulMonster,monster.transform.position,FXsoulMonster.transform.rotation) as GameObject;
		temp2.transform.parent = monster.transform;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		//DestroyItSelf ();
	}
}
