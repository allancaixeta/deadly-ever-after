﻿using UnityEngine;
using System.Collections;

public class OneTimeDamage : WeaponController {

	public override void SpecificWeaponAwake()
	{
		
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
			
		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}

	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.normal:	
			float speed = ProjectileSpeed;
			transform.Translate(direction * GameController.deltaTime * (speed) * timeDistortion,Space.Self);
			DecreaseLifeTime ();
			break;	
		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}

	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case Tags.PlayerHit:
			break;
			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.Wall:	
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,transform.position);	
			break;
		}
	}

	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		DestroyItSelf ();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
