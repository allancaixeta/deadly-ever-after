﻿using UnityEngine;
using System.Collections;

public class homemissile : WeaponController {
	public float afterHitSurvival = 0.5f;
	public float afterHitSizeIncrease = 2;
	public float damageDivisionIfNotReleasedMouseButton = 0.7f;
	bool missileReleased = false;
	float accumulatedDamage = 0;
	GameObject weaponVisual2;
	public float speedIncrement = 4;

	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.homemissile;
		weaponVisual2 = transform.FindChild("weaponVisual2").gameObject;
		weaponVisual2.SetActive (false);
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			GetComponent<SphereCollider>().radius *= afterHitSizeIncrease;
			countDown = afterHitSurvival;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:
			if(missileReleased)
			{
				MoveStraight();	
			}
			else
			{
				MoveToMouse();
				//transform.Translate(new Vector3(-1,0,0)* GameController.deltaTime * ProjectileSpeed * timeDistortion);
				float angleDifferenceBetweenWeaponAndCursor = 
					(-Mathf.Atan2((this.transform.position.x - currentCursor.currentPosition.x), 
					              (this.transform.position.y - currentCursor.currentPosition.y)) * Mathf.Rad2Deg);
				float angleAdjust2 = (angleDifferenceBetweenWeaponAndCursor < 0) ? -angleDifferenceBetweenWeaponAndCursor + 90 : -angleDifferenceBetweenWeaponAndCursor + 90;
				transform.rotation = Quaternion.Euler(new Vector3(0,0,-angleAdjust2));				
				accumulatedDamage += AttackSpeed * GameController.deltaTime;
				transform.localScale += Vector3.one * auxVariable * GameController.deltaTime;
			}
			CheckIfInsideRoom();
			break;		
		case weaponControllerStates.finish:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				DestroyItSelf();
			break;
		}
	}

	void MoveToMouse()
	{
		float speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
		direction = currentCursor.currentPosition - transform.position;
		direction.Normalize ();
		direction *= GameController.deltaTime * speed * timeDistortion;
		direction.z = 0;
		Move ();
	}

	void MoveStraight()
	{
		float speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
		direction.Normalize ();
		direction *= GameController.deltaTime * speed * timeDistortion;
		direction.z = 0;
		Move ();
	}

	void Move()
	{
		transform.Translate(direction,Space.World);
	}

	public void MouseReleased()
	{
		missileReleased = true;
		ProjectileSpeed = ProjectileSpeed + (int)((float)ProjectileSpeed * speedIncrement);
		weaponVisual2.SetActive (true);
		weaponVisual.gameObject.SetActive (false);
	}
	
	public override void DestroyItSelf()
	{
		if(missileReleased)
		{
			weaponVisual2.transform.parent = null;
			weaponVisual2.GetComponent<ParticleSystem>().Stop ();
		}
		else
		{
			player.GetComponent<PlayerController>().HomeMissileAccidentalHit();
			weaponVisual.parent = null;
			weaponVisual.GetComponent<ParticleSystem>().Stop ();
		}
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.HarmEvent:
			nextState = weaponControllerStates.finish;
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:		
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{	
		if(OnMonsterHit(monsterID,LifeTime))
			return;	
		damage += (int)(accumulatedDamage * (missileReleased ? 1: damageDivisionIfNotReleasedMouseButton));
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		DestroyItSelf();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
