﻿using UnityEngine;
using System.Collections;

public class movetokill : WeaponController {
	public float afterHitSurvival = 0.5f;
	public float afterHitSizeIncrease = 2;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.movetokill;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.normal:
			break;

		case weaponControllerStates.finish:
			Instantiate(onHitEffect,transform.position,onHitEffect.transform.rotation);
			weaponVisual.gameObject.SetActive (false);
			GetComponent<CapsuleCollider>().radius *= afterHitSizeIncrease;
			countDown = afterHitSurvival;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			break;

		case weaponControllerStates.normal:	
			MoveForward ();
			CheckIfInsideRoom();
			break;			
		case weaponControllerStates.finish:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				DestroyItSelf();
			break;
		}
	}

	public override void DestroyItSelf()
	{
		Instantiate(onHitEffect,transform.position,onHitEffect.transform.rotation);
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:
		case Tags.HarmEvent:
			nextState = weaponControllerStates.finish;		
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:		
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{	
		if(OnMonsterHit(monsterID,LifeTime))
			return;	
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		Instantiate(onHitEffect,transform.position,onHitEffect.transform.rotation);
		DestroyItSelf();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}