using UnityEngine;
using System.Collections;

public class frozenorb : WeaponController {

	public GameObject shard;
	public float intervalBetweenShots = 0.1f;
	public float angleDifferenceBetweenShots = 10;
	public float shardOriginDistance = 1;
	float currentIntervalBetweenShots;
	int shotNumber;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.frozenorb;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			currentIntervalBetweenShots = intervalBetweenShots;
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			SetDirection (currentCursor.currentPosition - transform.position);
			MoveForward();
			DecreaseMonsterHitListDuration ();
			currentIntervalBetweenShots -= GameController.deltaTime;
			if(currentIntervalBetweenShots < 0)
			{
				Shoot (shotNumber * angleDifferenceBetweenShots);
				shotNumber++;
				currentIntervalBetweenShots = intervalBetweenShots;
			}
			break;			
		}
	}

	void Shoot(float angle)
	{
		GameObject temp = Instantiate(shard,transform.position+(new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad),Mathf.Sin(angle * Mathf.Deg2Rad),0) * shardOriginDistance),Quaternion.Euler (0,0,angle)) as GameObject;
		temp.SendMessage("SetStrenght",playerStrenght);
		temp.SendMessage("SetDirection",Vector3.right);
		gameController.addWeapon(temp);		
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case "PlayerHit":
			break;
			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.Wall:	
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit (other.GetComponent<MonsterGeneric>(),monsterID,transform.position);
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,AttackSpeed))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
