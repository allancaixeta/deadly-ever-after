﻿using UnityEngine;
using System.Collections;

#pragma warning disable 0414 // private field assigned but not used.

public class disablejumble : WeaponController {

	GameObject monsterTarget;
	bool targetIsPlayer = false;
	Ray ray;
	RaycastHit [] hits;
	int layerMask;
	bool hitted = false;
	int raycastFailed = 0;
	int raycastFailedLimit = 7;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.disablejumble;
		layerMask = 512;
	}

	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {

		switch(currentState)
		{
		case weaponControllerStates.start:			
			break;
		case weaponControllerStates.normal:	
			/*ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			hits = Physics.RaycastAll(ray, 1000,layerMask);
			hitted = false;
			if(hits.Length == 0)
				raycastFailed++;
			else
			{
				for (int i = 0; i < hits.Length; i++) {
					if(hits[i].transform.name == this.name)
					{
						hitted = true;
						raycastFailed = 0;
					}
				}
				if(!hitted)
					raycastFailed++;
			}
			if(raycastFailed > raycastFailedLimit)
			{
				player.SendMessage ("DisableJumbleOff",isRight ? 1: 0);
				DestroyItSelf();
			}*/
			if(targetIsPlayer)
			{
				DecreaseMonsterHitListDuration();
			}
			else
			{
				Vector3 tempPos = transform.position;
				tempPos.z = player.transform.position.z;
				if(Vector2.Distance (tempPos,player.transform.position)*2 > (transform.localScale.x))
				{
					player.SendMessage ("DisableJumbleOff",isRight ? 1: 0);
					DestroyItSelf();
				}
				if(!monsterTarget)
				{
					player.SendMessage ("DisableJumbleOff",isRight ? 1: 0);
					gameController.removeWeapon(this.gameObject);
					Destroy(this.gameObject);
				}
			}
			break;			
		}
	}

	public void DisableJumbleOn(GameObject monster)
	{
		monsterTarget = monster;
		monster.SendMessage ("Stun",100);
		//transform.parent = monster.transform;
		transform.position = monster.transform.position;
		transform.localScale = new Vector3(ProjectileSpeed,ProjectileSpeed,ProjectileSpeed);
	}

	public void DisableJumbleOnPlayer()
	{
		targetIsPlayer = true;
		transform.localScale = new Vector3(ProjectileSpeed,ProjectileSpeed,ProjectileSpeed);
	}

	public override void DestroyItSelf()
	{
		if(!targetIsPlayer)
			monsterTarget.SendMessage ("RemoveStun");
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		if(targetIsPlayer)
		{
			string monsterID = other.name;
			switch(other.transform.tag)
			{
			case Tags.Monster:
			case Tags.BossMonster:
				CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
				break;
			}
		}
		else
			return;
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,AttackSpeed))
			return;		
		HitMonster(damage,monster,position);
		return;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		return;		
	}
	
	public override void LeaveTimeDistortion()
	{
		return;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem

			DestroyItSelf ();
	}
}
