using UnityEngine;
using System.Collections;

#pragma warning disable 0414 // private field assigned but not used.

public class portalgun : WeaponController {

	GameObject target;
	Ray ray;
	RaycastHit [] hits;
	int layerMask = 515072;
	bool failed = false;
	float radius;
	bool targetIsMonster = false;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.portalgun;
		countDown = AttackSpeed;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:

			break;
		case weaponControllerStates.normal:	
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{		
				ray = Camera.main.ScreenPointToRay((Input.mousePosition));
				hits =Physics.SphereCastAll(ray,radius,1000,layerMask);
				if(hits.Length == 0)
				{
					player.SendMessage("PortalGunFailed",isRight ? 1:0);
					DestroyItSelf ();
					return;
				}
				else
				{
					for (int i = 0; i < hits.Length; i++) {
						switch(hits[i].collider.transform.tag)
						{
						case Tags.Lvl2Obstacle:
						case Tags.Lvl3Obstacle:
						case Tags.Unmovable:	
						case Tags.Wall:	
							failed = true;
							break;
						case Tags.Monster:
						case Tags.BossMonster:
							if(hits[i].transform.name != target.name)
								failed = true;
							break;
						}
					}
				}
				if(failed)
				{
					player.SendMessage("PortalGunFailed",isRight ? 1:0);
					if(target != player)
						target.SendMessage ("TeleportOff");
					DestroyItSelf ();
				}
				else
				{
					target.SendMessage("Teleport",Camera.main.ScreenToWorldPoint((Input.mousePosition)));
					DestroyItSelf ();
				}
			}	
			if(!target)
			{
				player.SendMessage ("PortalGunOff",isRight ? 1: 0);
				gameController.removeWeapon(this.gameObject);
				Destroy(this.gameObject);
			}
			break;			
		}
	}

	public void PortalGunOn(GameObject monster)
	{
		target = monster;
		monster.SendMessage ("Stun",AttackSpeed);
		transform.position = monster.transform.position;
		radius = monster.GetComponent<CharacterController>().radius;
		transform.localScale = new Vector3(radius + 1,radius + 1,radius + 1);
		targetIsMonster = true;
	}

	public void TeleportOn()
	{
		target = player;
		transform.position = player.transform.position;
		radius = player.GetComponent<CharacterController>().radius;
		transform.localScale = new Vector3(radius + 1,radius + 1,radius + 1);
	}

	public void ActivatePortalGun()
	{
		countDown = 0;
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		//string monsterID = other.name;
		switch(other.transform.tag)
		{
		case "PlayerHit":
			break;
			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.Wall:	
			break;
			
		case Tags.Monster:		
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		DestroyItSelf();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		return;
	}
}