﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class chainlightning : WeaponController {

	public float distanceDecrement = 2;
	[Range(0, 1)]
	public float primaryTargetDamage= 0.5f;
	public int damageIncreasePerTarget = 2;
	public GameObject chainlinks;
	List<MonsterGeneric> targetsHitList;
	List<Transform> closeMonstersHitList;
	List<GameObject> chainLinksList;
	bool spreadingStage;
	int numberOfJumps;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.chainlightning;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			targetsHitList = new List<MonsterGeneric>();
			chainLinksList = new List<GameObject>();
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.normal:	
			numberOfJumps = 0;
			TraceNewTargets();
			break;

		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:	
			MoveForward ();
			if(CheckIfInsideRoomandReturn())
				countDown -= 4*GameController.deltaTime;
			break;		

		case weaponControllerStates.normal:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				TraceNewTargets();
			for (int i = 0; i < targetsHitList.Count; i++) 
			{
				if(!targetsHitList[i])
				{
					targetsHitList.RemoveAt (i);
					if(i == 0)
					{
						nextState = weaponControllerStates.finish;
						return;
					}
					if(initialTargetFoxNextLoop == 1)
						initialTargetFoxNextLoop = 2;
					if(targetsHitList.Count > 1)
						for (int j = targetsHitList.Count-1; j >= initialTargetFoxNextLoop -1; j--) {
							targetsHitList.RemoveAt (j);
						}
					FinishChainLightning ();
				}
			}
			break;
		}
	}
	int initialTargetFoxNextLoop;
	int numberOfNewAcquiredTargets = 0;
	public void TraceNewTargets()
	{
		numberOfNewAcquiredTargets = 0;
		countDown = auxVariable * (1 - skills.GetFasterBoom ());
		if (numberOfJumps != 0)
		{
			int aux = targetsHitList.Count;
			for (int i = initialTargetFoxNextLoop; i < aux; i++) 
			{
				closeMonstersHitList = gameController.FindAllMonsterWithinDistance (targetsHitList[i].transform.position, AttackSpeed / (numberOfJumps * distanceDecrement));
				FilterNewTargetsAmongExistents (targetsHitList[i].transform);
			}
			initialTargetFoxNextLoop = aux;
		}
		else 
		{
			closeMonstersHitList = gameController.FindAllMonsterWithinDistance (targetsHitList[0].transform.position, AttackSpeed);
			FilterNewTargetsAmongExistents (targetsHitList[0].transform);
			initialTargetFoxNextLoop = 1;
		}
		numberOfJumps++;
		if (numberOfNewAcquiredTargets == 0)
			FinishChainLightning ();
	}

	public void FilterNewTargetsAmongExistents (Transform currentGuySearchingForTargets)
	{
		bool deny;
		for (int i = 0; i < closeMonstersHitList.Count; i++) 
		{
			deny = false;
			for (int j = 0; j < targetsHitList.Count; j++) {
				if(closeMonstersHitList[i] == targetsHitList[j].transform)
					deny = true;
			}
			if(!deny)
			{
				GameObject tempWeapon = Instantiate(chainlinks,currentGuySearchingForTargets.position,Quaternion.identity) as GameObject;
				tempWeapon.SendMessage("SetTarget",closeMonstersHitList[i]);
				tempWeapon.SendMessage("SetFather",currentGuySearchingForTargets);
				tempWeapon.SendMessage("SetGrowth",countDown);
				gameController.addWeapon(tempWeapon);
				chainLinksList.Add (tempWeapon);
				targetsHitList.Add (closeMonstersHitList[i].GetComponent<MonsterGeneric>());
				numberOfNewAcquiredTargets++;
			}
		}
	}

	public void FinishChainLightning()
	{
		nextState = weaponControllerStates.finish;
		print (nextState);
		damage = damage + (damageIncreasePerTarget * targetsHitList.Count);
		HitMonster((int)(damage * primaryTargetDamage),targetsHitList[0],targetsHitList[0].transform.position);
		int secondaryTargetDamage = (int)(damage * (1 - primaryTargetDamage) / (targetsHitList.Count - 1));
		for (int i = 1; i < targetsHitList.Count; i++) 
		{
			HitMonster(secondaryTargetDamage,targetsHitList[i],targetsHitList[0].transform.position);
		}

	}

//	void Shoot(Vector3 pos)
//	{
//		GameObject temp = Instantiate(fragments,pos,Quaternion.identity) as GameObject;
//		temp.SendMessage("SetDamage",damage);
//		temp.SendMessage("SetProjectileSpeed",LifeTime);
//		temp.SendMessage("SetShotgun",this);
//		gameController.addWeapon(temp);	
//	}

	public override void DestroyItSelf()
	{
		player.SendMessage ("ChainLightningFinished");
		for (int i = 0; i < chainLinksList.Count; i++) {
			if(chainLinksList[i])
				chainLinksList[i].SendMessage ("DestroyItSelf");
		}
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		//string monsterID = other.name;
		switch(other.transform.tag)
		{
		case "PlayerHit":
			break;
						
		case Tags.Monster:
		case Tags.BossMonster:
			if(targetsHitList.Count == 0)
			{
				targetsHitList.Add (other.GetComponent<MonsterGeneric>());
				nextState = weaponControllerStates.normal;
			}
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		return;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		if(currentState  != weaponControllerStates.normal)
			DestroyItSelf ();
	}
}