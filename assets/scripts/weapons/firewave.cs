﻿using UnityEngine;
using System.Collections;

public class firewave : WeaponController {

	public bool damageAmplifies = false;
	public float sizeIncrement = 0.2f;
	float monsterHitCount = 0;
	public int numberOfHitsMax =  10;

	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.firewave;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			DestroyItSelf ();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			MoveForward();
			if(CheckIfInsideRoomandReturn())
				countDown -= 4*GameController.deltaTime;
			break;			
		}
	}

	public override void DestroyItSelf()
	{
		player.SendMessage ("FireWaveOver",isRight ? 1 : 0);
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;
		int tempDamage = (damageAmplifies ? 1:-1) * (int)(monsterHitCount * AttackSpeed * damage);
		HitMonster(damage + tempDamage,monster,transform.position);//TODO: verificar posiçao do Knockback
		/*if(damageAmplifies)
			damage = damage + (int)((float)damage * AttackSpeed);
		else
		*	damage = damage - (int)((float)damage * AttackSpeed);
		*/
		monsterHitCount++;
		knockBack *= (1-AttackSpeed);
		transform.localScale = new Vector3(transform.localScale.x + sizeIncrement, transform.localScale.y + sizeIncrement, transform.localScale.z + sizeIncrement);
		if(monsterHitCount >= numberOfHitsMax)
				DestroyItSelf ();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
