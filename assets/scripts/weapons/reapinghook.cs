using UnityEngine;
using System.Collections;

public class reapinghook :  WeaponController {
	
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.reapinghook;
		countDown = 0;
		UI = InterfaceController.Instance;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			weaponVisual = transform.FindChild("WeaponVisual");	
			PlaySound(audioClip,false);	
			countDown = AttackSpeed;
			float speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			transform.Translate(direction * speed,Space.World);
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			break;
		case weaponControllerStates.normal:	
			countDown -= GameController.deltaTime;
			if(countDown < 0)				
				DestroyItSelf ();
			break;			
		}
	}

	public override void DestroyItSelf()
	{

		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case Tags.Monster:
		case Tags.BossMonster:			
			if(OnMonsterHit(monsterID,Mathf.Abs(AttackSpeed)))
				return;		
			HitMonster((int)GetDamage (other.GetComponent<MonsterGeneric>(),other.ClosestPointOnBounds(transform.position)),other.GetComponent<MonsterGeneric>(),other.ClosestPointOnBounds(transform.position));

			break;
		}
	}

	float GetDamage(MonsterGeneric m,Vector3 closestpointonbounds)
	{
		if((Vector2.Distance (transform.position, m.GetMonsterCenter ()) - 0.1f) > Vector2.Distance (closestpointonbounds, m.GetMonsterCenter ())) 
			return damage * auxVariable;
		else
			return damage;
	}

	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		return;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
