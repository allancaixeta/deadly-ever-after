using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class energymines : WeaponController {

	public GameObject energymineOnePiece;
	List<Transform> minesList;

	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.energymines;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			minesList = new List<Transform>();
			DeployMine ();
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			
			break;			
		}
	}

	public void DeployMine()
	{
		GameObject tempWeapon = Instantiate(energymineOnePiece,player.transform.position,Quaternion.identity) as GameObject;
		tempWeapon.SendMessage("SetStrenght",playerStrenght);
		tempWeapon.SendMessage("SetFather",this);
		gameController.addWeapon(tempWeapon);
		minesList.Add (tempWeapon.transform);
		RetargetMinesField ();
	}

	public void DepleteMine(GameObject depletedmine)
	{
		minesList.Remove (depletedmine.transform);		
		RetargetMinesField ();
	}

	public void RetargetMinesField ()
	{
		if(minesList.Count > 1)
		{
			if(minesList.Count == 2)
			{
				minesList[0].SendMessage("SetTarget",minesList[1]);
				minesList[1].SendMessage("SetTarget",minesList[0]);
			}
			else
			{
				for (int i = 0; i < minesList.Count; i++) {
					if(i == minesList.Count - 1)
						minesList[i].SendMessage("SetTarget",minesList[0]);
					else
						minesList[i].SendMessage("SetTarget",minesList[i+1]);
				}
			}
		}
		else
		{
			if(minesList.Count == 1)
				minesList[0].SendMessage("SetTarget",minesList[0]);
		}
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		return;
		//string monsterID = other.name;
//		switch(other.transform.tag)
//		{
//		case "PlayerHit":
//			break;
//			
//		case Tags.Lvl3Obstacle:
//		case Tags.Unmovable:	
//		case Tags.Wall:	
//			break;
//			
//		case Tags.Monster:
//		case Tags.BossMonster:
//			//CheckForHit (other.GetComponent<MonsterGeneric>(),monsterID,transform.position);
//			break;
//		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		DestroyItSelf();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
	}
}
