using UnityEngine;
using System.Collections;

public class skylaser : WeaponController {

	public Collider myCollider;
	float speed, forgivenessCountDown;

	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.skylaser;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			countDown = AttackSpeed;
			forgivenessCountDown = auxVariable;
			speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			transform.localScale = new Vector3(speed,speed,1);
			break;

		case weaponControllerStates.normal:		
			Instantiate(onHitEffect,transform.position,Quaternion.identity);
			myCollider.enabled = true;
			countDown = LifeTime;
			break;

		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				nextState = weaponControllerStates.normal;
			if(Vector2.Distance (transform.position,currentCursor.currentPosition) > speed)
			{
				forgivenessCountDown -= GameController.deltaTime;
				if(forgivenessCountDown < 0)
				{
					player.SendMessage ("MissedSkyLaser");
					DestroyItSelf ();
				}
			}
			break;
		case weaponControllerStates.normal:	
			DecreaseLifeTime ();
			break;			
		}
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case Tags.PlayerHit:
			break;
			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.Wall:	
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,transform.position);	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
