﻿using UnityEngine;
using System.Collections;

public class fraggrenade : WeaponController {

	Vector3 goalPosition;
	public float distance = 0.2f;
	public int fragsQuantity = 8;
	public GameObject fragments;
	int numberOfShots;
	float lastDistanceToGoal;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.fraggrenade;
		countDown = 100;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			goalPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			goalPosition.z = transform.position.z;
			lastDistanceToGoal = Vector2.Distance (transform.position,goalPosition);
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.VOID:			
			weaponVisual.gameObject.SetActive (false);
			break;

		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			MoveForward();
			if(Vector2.Distance (transform.position,goalPosition) < distance || Vector2.Distance (transform.position,goalPosition)>= lastDistanceToGoal)
				OnHit();
			lastDistanceToGoal = Vector2.Distance (transform.position,goalPosition);
			break;

		case weaponControllerStates.normal:	
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{						
				for (int i = 0; i < fragsQuantity; i++) {
					Shoot (0 + (360/fragsQuantity*i));
				}
				nextState = weaponControllerStates.VOID;
				GetComponent<Collider>().enabled = false;
				countDown = auxVariable;
			}	
			transform.localScale += transform.localScale * AttackSpeed * GameController.deltaTime;
			break;	

		case weaponControllerStates.VOID:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{	
				DestroyItSelf();
			}
			break;
		}
	}

	public void OnHit()
	{
		nextState = weaponControllerStates.normal;
		countDown = LifeTime;
	}

	void Shoot(float angle)
	{
		GameObject temp = Instantiate(fragments,transform.position,Quaternion.identity) as GameObject;
		temp.SendMessage ("SetDirection",angle);
		temp.SendMessage("SetFraGrenade",this);
		temp.SendMessage("SetStrenght",playerStrenght);
		gameController.addWeapon(temp);		
	}

	public bool CheckForMonsterHitForFrags(string monsterID)
	{
		return OnMonsterHit(monsterID,5);
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case Tags.Monster:	
		case Tags.BossMonster:
			if(currentState == weaponControllerStates.start && currentState == weaponControllerStates.start)
				OnHit ();
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(CheckForMonsterHitForFrags(monsterID))
			return;	
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		player.SendMessage ("SinkSwallow");
		DestroyItSelf ();
	}
}