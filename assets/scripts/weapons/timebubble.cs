﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class timebubble : WeaponController {
	
	public float timeEffect = 50;//força do efeito de slowdown de 0 a 100%
	protected List<MonsterGeneric> monsterControllerHitList;

	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.timebubble;
		monsterControllerHitList = new List<MonsterGeneric>();
		countDown = LifeTime;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,true);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:			
			break;
		case weaponControllerStates.normal:	
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{
				for (int i = 0; i < monsterControllerHitList.Count; i++){
					monsterControllerHitList[i].SlowDown(0,-1,this);
				}
				DestroyItSelf();
			}
			break;			
		}
	}

	public override void DestroyItSelf()
	{
		if(isRight)
			gameController.RightWeaponAudioSource.Stop();
		else
			gameController.LeftWeaponAudioSource.Stop();
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.tag)
		{
		case Tags.Monster:
		case Tags.MonsterProjectile:
		case Tags.StunMonster:
			for (int i = 0; i < monsterControllerHitList.Count; i++){
				if(!monsterControllerHitList[i])
				{
					monsterControllerHitList.RemoveAt(i);
					i--;
				}
				else
					if(monsterControllerHitList[i].gameObject.name == monsterID)
						return;
			}
			other.GetComponent<MonsterGeneric>().SlowDown(timeEffect,countDown,this);
			monsterControllerHitList.Add(other.GetComponent<MonsterGeneric>());
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		return;
	}

	protected override void OnTriggerExit(Collider other)
	{
		switch(other.tag)
		{
		case Tags.Monster:
		case Tags.MonsterProjectile:
		case Tags.StunMonster:
			other.GetComponent<MonsterGeneric>().SlowDown(0,-1,this);
			for (int i = 0; i < monsterControllerHitList.Count; i++){
				if(monsterControllerHitList[i] != null && monsterControllerHitList[i].gameObject.name == other.name)
					monsterControllerHitList.RemoveAt(i);
			}
			break;
		}
	}

	public void MonsterDied(MonsterGeneric m)
	{
		monsterControllerHitList.Remove (m);
	}

	public override void EnterTimeDistortion(float rate)
	{
		return;		
	}
	
	public override void LeaveTimeDistortion()
	{
		return;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
