using UnityEngine;
using System.Collections;

public class rumbleult : WeaponController {

	public Collider myCollider;
	float growthRate;
	float speed;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.rumbleult;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			float angle = Mathf.Atan2 (currentCursor.currentPosition.y - transform.position.y, currentCursor.currentPosition.x - transform.position.x) * Mathf.Rad2Deg;				
			transform.localEulerAngles = new Vector3 (0,0,angle);
			growthRate += GameController.deltaTime * speed;
			transform.localScale = new Vector3 (growthRate,transform.localScale.y,transform.localScale.z);
			break;
		case weaponControllerStates.normal:	
			DecreaseLifeTime ();
			DecreaseMonsterHitListDuration ();
			break;			
		}
	}

	public void ActivateRumbleUlt()
	{
		myCollider.enabled = true;
		nextState = weaponControllerStates.normal;
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case Tags.PlayerHit:
			break;
			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.Wall:	
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,transform.position);	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,AttackSpeed))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
	}
}
