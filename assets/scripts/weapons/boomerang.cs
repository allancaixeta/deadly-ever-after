using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class boomerang : WeaponController {
	
	//AttackSpeed: 1 projétil  que viaja reto por 3 segundos 
	// Lifetime: Depois de efetuar o disparo o jogador pode segurar o mouse por ate 10 segundos
	PlayerController.boomerang boomerangState = PlayerController.boomerang.going;
	public Material visualOnHold;
	float tempAngleCalc ;
	float originalSpeed;
	float delay = 0;
	public float initialYSize = 3;
	public float growthTime = 0.5f;
	public float returnDiminishDelay = 0.5f;
	float currentgrowthTime;
	public LerpEquationTypes lerpTypeGoing, lerpTypeReturning;

	float originalY;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.boomerang;
		countDown = LifeTime;	
		originalSpeed = ProjectileSpeed;
		currentgrowthTime = growthTime;
		originalY = transform.localScale.y;
		transform.localScale = new Vector3 (transform.localScale.x,initialYSize,transform.localScale.y);

	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			weaponVisual = transform.FindChild("WeaponVisual");	
			normalMaterial = weaponVisual.GetComponent<Renderer>().material;
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			DecreaseMonsterHitListDuration();
			switch(boomerangState)
			{
			case PlayerController.boomerang.going:
				//transform.Translate(direction * GameController.deltaTime * ProjectileSpeed * timeDistortion);
				transform.position += direction * GameController.deltaTime * ProjectileSpeed * (1 + skills.GetFasterBoom ()) * timeDistortion;
				//transform.LookAt (player.transform.position);	
				tempAngleCalc = Mathf.Atan2 (transform.position.y - player.transform.position.y, transform.position.x - player.transform.position.x) * Mathf.Rad2Deg;
				if(delay > 0)
					delay -= GameController.deltaTime;
				else
					transform.rotation = Quaternion.Euler(0,0,tempAngleCalc);
				countDown -= GameController.deltaTime;
				if(countDown < 0)
				{
					boomerangState = PlayerController.boomerang.hold;
					weaponVisual.GetComponent<Renderer>().material = visualOnHold;
					gameObject.layer = 4;
				}
				if(currentgrowthTime > 0)
				{
					currentgrowthTime -= GameController.deltaTime;
					transform.localScale = new Vector3 (transform.localScale.x,lerpTypeGoing.Lerp (originalY,initialYSize,currentgrowthTime / growthTime),transform.localScale.z);
				}
				break;
			case PlayerController.boomerang.hold:
				break;
			case PlayerController.boomerang.returning:					
				Vector3 vectorDifference2 = transform.position - player.transform.position;
				vectorDifference2.z = 0;
				vectorDifference2.Normalize();	
				transform.position -= vectorDifference2 * GameController.deltaTime * originalSpeed * (1 + skills.GetFasterBoom ()) * timeDistortion;
				//transform.LookAt (player.transform.position);
				//transform.Rotate (0,0,90);
				tempAngleCalc = Mathf.Atan2 (transform.position.y - player.transform.position.y, transform.position.x - player.transform.position.x) * Mathf.Rad2Deg;
				transform.rotation = Quaternion.Euler(0,0,tempAngleCalc);
				if(Vector2.Distance(transform.position,player.transform.position) < auxVariable)
					DestroyItSelf();
				if(currentgrowthTime > 0)
				{
					currentgrowthTime -= GameController.deltaTime;
					transform.localScale = new Vector3 (transform.localScale.x,lerpTypeReturning.Lerp (initialYSize,originalY,currentgrowthTime / growthTime),transform.localScale.z);
				}
				break;
			}				
			break;			
		}
	}

	public void BoomerangReturn()
	{
		boomerangState = PlayerController.boomerang.returning;
		currentgrowthTime = growthTime + returnDiminishDelay;
		weaponVisual.GetComponent<Renderer>().material = normalMaterial;
		gameObject.layer = 9;
		monsterHitListID = new List<string>();
		monsterHitListDuration = new List<float>();
	}
		
	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case "PlayerHit":
			if(boomerangState == PlayerController.boomerang.returning)
				DestroyItSelf();	
			break;
			
		case Tags.Monster:	
		case Tags.BossMonster:
			if(OnMonsterHit(monsterID,LifeTime+AttackSpeed))
				return;		
			HitMonster(damage,other.GetComponent<MonsterGeneric>(),other.ClosestPointOnBounds(transform.position));
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		return;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		return;		
	}
	
	public override void LeaveTimeDistortion()
	{
		return;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		player.SendMessage ("SinkSwallow");
		DestroyItSelf ();
	}
}
