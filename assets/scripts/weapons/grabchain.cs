﻿using UnityEngine;
using System.Collections;

public class grabchain : WeaponController {

	public float contortionSpeed = 1; //velocidade que a corrente se contorce
	public float maxContortion = 4; //contorção maxima da corrente, vai de x a -x
	public float tillingRate = 2; //tilling * tillingRate é igual a escala, portanto quanto maior menor vai ser os pedaços da corrente
	float currentContortion;
	bool contortionUp = true;
	bool hitMonster = false;
	bool hitObject = false;
	Vector2 ChainTileMatrix;
	Vector3 finalPosition;
	Transform weaponVisual2;	
	Vector3 towardsTarget;
	Transform target;
	bool functional = false;
	float chainScaleDivider =  0.935f;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.grabchain;
		//transform.Rotate (0,90,0);
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:
			//transform.Translate(new Vector3(0,-2,0));	
			weaponVisual2 = transform.FindChild("WeaponVisual2");
			weaponVisual2.GetComponent<Renderer>().enabled = true;
			weaponVisual2.position = player.transform.position;
			PlaySound(audioClip,false);	
//			direction = (transform.localPosition + new Vector3(1,0,0)) - transform.localPosition;
//			direction.z = 0;
//			direction.Normalize();
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			nextState = weaponControllerStates.normal;
			break;
		case weaponControllerStates.normal:	
			grabChain();
			break;			
		}
	}

	void grabChain()
	{
		if(functional)
		{
			if(hitMonster)
			{
				transform.Translate(direction * GameController.deltaTime * ProjectileSpeed * timeDistortion,Space.World);
				MoveGrab();
				weaponVisual.localScale = new Vector3(Vector2.Distance(transform.position,finalPosition) * chainScaleDivider,
				                                      currentContortion,weaponVisual.localScale.z);
				weaponVisual.position = new Vector3(finalPosition.x + (transform.position.x - finalPosition.x)/2,
				                                    finalPosition.y + (transform.position.y - finalPosition.y)/2,weaponVisual.position.z);
				weaponVisual2.position = finalPosition;
				if(Vector2.Distance(transform.position,finalPosition)<2.6f)
					DestroyItSelf();
			}
			else 
			{
				if(hitObject)
				{
					MoveGrab();
					ResizeChain();
				}
			}
		}
		else
		{
			if(hitMonster || hitObject)
			{
				ResizeChain();
			}
			else
			{
				transform.Translate(direction * GameController.deltaTime * ProjectileSpeed * timeDistortion,Space.World);
				MoveGrab();
				ResizeChain();
			}
		}		
	}

	void ResizeChain()
	{
		weaponVisual.localScale = new Vector3(Vector2.Distance(transform.position,player.transform.position) * chainScaleDivider,
		                                      currentContortion,weaponVisual.localScale.z);
		//weaponVisual.localPosition = (new Vector3(-1,0,0) * Vector3.Distance(transform.position,player.transform.position) * 0.102f);
		weaponVisual.position = new Vector3(player.transform.position.x + (transform.position.x - player.transform.position.x)/2,
		                                    player.transform.position.y + (transform.position.y - player.transform.position.y)/2, weaponVisual.position.z);
		weaponVisual2.position = player.transform.position;
		float angle = (-Mathf.Atan2((player.transform.position.x - this.transform.position.x), 
		                            (player.transform.position.y - this.transform.position.y)) * Mathf.Rad2Deg);
		weaponVisual.rotation = Quaternion.Euler(0,0,angle+90);	
	}
	
	void MoveGrab()
	{
		if(contortionUp)
		{
			currentContortion += contortionSpeed * GameController.deltaTime * timeDistortion;
			if(currentContortion > maxContortion)
			{
				contortionUp = false;
			}
		}
		else {
			currentContortion -= contortionSpeed * GameController.deltaTime * timeDistortion;
			if(currentContortion < -maxContortion)
			{
				contortionUp = true;
			}
		}					
		ChainTileMatrix = weaponVisual.GetComponent<Renderer>().material.mainTextureScale;
		ChainTileMatrix.x = weaponVisual.localScale.x / tillingRate;
		weaponVisual.GetComponent<Renderer>().material.mainTextureScale = ChainTileMatrix;	
	}

	public void ActivateChain()
	{
		functional = true;
		if(hitMonster)
			SetTargetPos ();
	}

	void SetTargetPos()
	{
		direction = weaponVisual2.position - transform.position;
		direction.z = 0;
		direction.Normalize();
		target.GetComponent<MonsterGeneric>().Hooked();
		target.parent = this.transform;
		finalPosition = player.transform.position;
	}

	public override void DestroyItSelf()
	{
		player.GetComponent<PlayerController>().ChainCanceled();
		if(hitMonster && target)
			target.GetComponent<MonsterGeneric>().UnHooked();
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		//string monsterID = other.name;
		if(hitObject || hitMonster)
			return;
		switch(other.transform.tag)
		{		
		case Tags.Wall:	
			target = other.transform;
			if(Vector2.Distance(transform.position,weaponVisual2.position)<5)
			{			
				DestroyItSelf();
				return;
			}
			direction = weaponVisual2.position - transform.position;
			direction.z = 0;
			direction.Normalize();
			player.GetComponent<PlayerController>().GrabChainWall();
			hitObject = true;
			break;
			
		case Tags.Monster:
			return; //remove essa linha para permitir que a corrente pegue monstros
			/*target = other.transform;
			if(other.GetComponent<MonsterGeneric>().knockbackType == MonsterGeneric.KnockbackType.hulk)
			{
				DestroyItSelf();
				return;
			}
			if(functional)
				SetTargetPos ();
			else
				other.GetComponent<MonsterGeneric>().Stun(AttackSpeed);
			hitMonster = true;
			player.GetComponent<PlayerController>().GrabChainMonster();
			break;*/
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		return;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf();
	}
}
