﻿using UnityEngine;
using System.Collections;

public class shotgunfrags : WeaponController {
	
	float angle;
	bool allowedToHit = false; //small delay necessary, so that all shots don't hit the same target
	shotgun shotgunFather;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.shotgun;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			countDown = AttackSpeed;
			break;
			
		case weaponControllerStates.normal:
			allowedToHit = true;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

			
		case weaponControllerStates.finish:
			DestroyItSelf ();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{
				nextState = weaponControllerStates.normal;
				countDown = LifeTime;
			}
			transform.Translate(new Vector3(0,-1,0)* GameController.deltaTime * ProjectileSpeed * (1 + skills.GetFasterBoom ()) * timeDistortion);
			if(CheckIfInsideRoomandReturn())
				countDown -= 2*GameController.deltaTime;
			break;
		case weaponControllerStates.normal:	
			transform.Translate(new Vector3(0,-1,0)* GameController.deltaTime * ProjectileSpeed * (1 + skills.GetFasterBoom ()) * timeDistortion);
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				DestroyItSelf();
			else
				if(CheckIfInsideRoomandReturn())
					countDown -= 2*GameController.deltaTime;
			break;			
		}
	}
	
	public void SetDamage(int d)
	{
		damage = d;
	}
	
	public void SetProjectileSpeed(int p)
	{
		ProjectileSpeed = p;
	}

	public void SetShotgun(shotgun s)
	{
		shotgunFather = s;
	}
	
	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.tag)
		{			
		case Tags.Monster:
		case Tags.BossMonster:
			if(allowedToHit)
				CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{	
		//if(shotgunFather.CheckForMonsterHitForFrags(monsterID))
		//	return;	
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		//DestroyItSelf();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
