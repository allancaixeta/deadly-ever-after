﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class kendo : WeaponController {

	public float chargeTime, HoldTime;
	public GameObject FX;
	public float visualUpdate = 1;
	float timeCount = 0;
	float speed;
	List<GameObject> allFXs;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.kendo;
		//transform.Translate(new Vector3(0,-1,0)* (transform.localScale.x/2 + auxVariable));
		//transform.Rotate (0,0,90)
		allFXs = new List<GameObject> ();
		/*Vector3 adjustedCenter = GetComponent<BoxCollider>().center ;
		adjustedCenter.y = -(Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad));
		adjustedCenter.x = Mathf.InverseLerp(-1,1,(Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad)));
		adjustedCenter.x = Mathf.Lerp (-0.4f, 0.2f , adjustedCenter.x);
		GetComponent<BoxCollider>().center = adjustedCenter;*/
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			break;

		case weaponControllerStates.normal:		
			countDown = AttackSpeed;
			timeCount = visualUpdate;
			speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			weaponVisual.parent = null;
			break;

		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			countDown = LifeTime;
			weaponVisual.parent = transform;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.normal:	
			timeCount -= GameController.deltaTime;
			if(timeCount < 0)
			{
				Vector3 tempAngle = Vector3.zero;// transform.rotation.eulerAngles;
				//tempAngle.y = 0;
				GameObject temp = Instantiate(FX,weaponVisual.transform.position,Quaternion.Euler (0,0,90)) as GameObject;

				allFXs.Add (temp);
				//temp.transform.parent = this.transform;
				timeCount = visualUpdate;
			}
			transform.localScale += new Vector3(speed/2,0,0) * GameController.deltaTime * (timeDistortion);
			transform.Translate (speed/4 * GameController.deltaTime * (timeDistortion),0,0);
			weaponVisual.transform.Translate (speed/2 * GameController.deltaTime * (timeDistortion),0,0);
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{		
				nextState = weaponControllerStates.finish;		
			}	
			break;

		case weaponControllerStates.finish:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				DestroyItSelf();
			break;
		}
	}

	public void StartKendo(float d)
	{
		damage = (int)(d * damage);
		if(damage <= 0)
			damage = 1;
		nextState = weaponControllerStates.normal;
	}
	
	public override void DestroyItSelf()
	{
		for (int i = 0; i < allFXs.Count; i++) {
			Destroy (allFXs[i]);
		}
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:
		case Tags.HarmEvent:
			nextState = weaponControllerStates.finish;			
			break;

		case Tags.Monster:
		case Tags.BossMonster:		
			CheckForHit (other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(player.transform.position));
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime+AttackSpeed))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}

	public float GetChargeTime()
	{
		return chargeTime;
	}

	public float GetHoldTime()
	{
		return HoldTime;
	}
}
