using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class remotebombermine : WeaponController {

	public GameObject mineOnePiece;
	GameObject[] minesList;
	int currentMine;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.remotebombermine;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			currentMine = 0;
			minesList = new GameObject[ProjectileSpeed];
			InstantiateMines ();
			DeployMine();
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			
			break;			
		}
	}

	public void InstantiateMines()
	{
		for (int i = 0; i < ProjectileSpeed; i++) {
			GameObject tempWeapon = Instantiate(mineOnePiece,player.transform.position,Quaternion.identity) as GameObject;
			tempWeapon.SendMessage("SetStrenght",playerStrenght);
			gameController.addWeapon(tempWeapon);
			minesList[i] = tempWeapon;
			minesList [i].SetActive (false);
		}
		currentMine = 0;
	}

	public void DeployMine()
	{
		minesList [currentMine].SetActive (true);
		minesList [currentMine].transform.position = player.transform.position;
		currentMine++;
	}

	public void ExplodeMines()
	{
		for (int i = 0; i < minesList.Length; i++) 
		{
			minesList[i].GetComponent<remotemine>().Explode();
		}
		currentMine = 0;
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		return;
		//string monsterID = other.name;
//		switch(other.transform.tag)
//		{
//		case "PlayerHit":
//			break;
//			
//		case Tags.Lvl3Obstacle:
//		case Tags.Unmovable:	
//		case Tags.Wall:	
//			break;
//			
//		case Tags.Monster:
//		case Tags.BossMonster:
//			//CheckForHit (other.GetComponent<MonsterGeneric>(),monsterID,transform.position);
//			break;
//		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		DestroyItSelf();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
	}
}
