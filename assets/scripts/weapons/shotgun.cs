﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class shotgun : WeaponController {

	public GameObject fragments;
	Transform weaponVisual2;
	public int maxSpreadAngle = 90; //Angulo entre as balas
    public int startSpreadAngle = 60;
    public float openingSpeedBoost = 1.2f;
	public LerpEquationTypes lerp;
	bool isShotgunBulletPar;
	float currentSpreadAngle = 0;
	int angleGrowing = 1;
	List<Transform> closeMonstersHitList;
	float speed;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.shotgun;
		weaponVisual2 = transform.FindChild ("WeaponVisual2");
		//transform.Rotate (0,0,90);
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			currentSpreadAngle = startSpreadAngle;
			speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.normal:	
			weaponVisual.gameObject.SetActive (false);
			weaponVisual2.gameObject.SetActive (false);
			if(closeMonstersHitList.Count == 0)
				nextState = weaponControllerStates.VOID;
			else
				countDown = LifeTime;
			break;

		case weaponControllerStates.VOID:	
			for (int i = 0; i < closeMonstersHitList.Count; i++) 
			{
				Destroy (closeMonstersHitList[i].gameObject);
			}
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:	
			currentSpreadAngle += GameController.deltaTime * AttackSpeed * angleGrowing * (angleGrowing > 0 ? openingSpeedBoost : 1);
			if((currentSpreadAngle < 0 && angleGrowing < 0 ) || (currentSpreadAngle> maxSpreadAngle && angleGrowing > 0))
			{
				angleGrowing = -angleGrowing;
			}
			weaponVisual.localEulerAngles = new Vector3(0,0,currentSpreadAngle);
			weaponVisual2.localEulerAngles = new Vector3(0,0,-currentSpreadAngle);
			break;		

		case weaponControllerStates.normal:
			countDown -= GameController.deltaTime;
			for (int i = 0; i < closeMonstersHitList.Count; i++) 
			{
                if(closeMonstersHitList[i])
				    closeMonstersHitList[i].position = lerp.Lerp (player.transform.position,closeMonstersHitList[i].position,countDown/LifeTime);
			}
			if(countDown < 0)
				nextState = weaponControllerStates.VOID;
			break;

		case weaponControllerStates.VOID:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{	
				float cd = CoolDown * (1 - auxVariable * closeMonstersHitList.Count);
				if(cd <= 0)
					cd = 0.1f;
				player.GetComponent<PlayerController>().StopShotgun (cd);
				DestroyItSelf();
			}
			break;
		}
	}

	public void StartShotgun()
	{
		closeMonstersHitList = new List<Transform>();
		List<Transform> closeMonstersList = gameController.FindAllMonsterWithinDistance (transform.position, speed);
		if(closeMonstersList != null)
		{
			float angleMax = transform.parent.rotation.eulerAngles.z + currentSpreadAngle;
			if(angleMax > 360)
				angleMax -= 360;
			float angleMin = transform.parent.rotation.eulerAngles.z - currentSpreadAngle;
			if(angleMin < 0)
				angleMin += 360;
			bool maxIsHigher = angleMax > angleMin;
			for (int i = 0; i < closeMonstersList.Count; i++) {
				float angleMonster = Mathf.Atan2 (closeMonstersList[i].position.y - transform.position.y, closeMonstersList[i].position.x - transform.position.x) * Mathf.Rad2Deg;				
				if(angleMonster < 0)
					angleMonster = 360 + angleMonster;
				bool targetInRange = false;
				if(maxIsHigher)
				{
					if(angleMonster < angleMax && angleMonster > angleMin)
						targetInRange = true;
				}
				else
				{
					if(angleMonster < angleMax || angleMonster > angleMin)
						targetInRange = true;
				}
				if(targetInRange)
				{
					GameObject temp = Instantiate(fragments,closeMonstersList[i].position,Quaternion.identity) as GameObject;
					closeMonstersHitList.Add (temp.transform);
					HitMonster(damage,closeMonstersList[i].GetComponent<MonsterGeneric>(),temp.transform.position);
				}
			}
		}
		nextState = weaponControllerStates.normal;
	}

	void Shoot(Vector3 pos)
	{
		GameObject temp = Instantiate(fragments,pos,Quaternion.identity) as GameObject;
		temp.SendMessage("SetDamage",damage);
		temp.SendMessage("SetProjectileSpeed",LifeTime);
		temp.SendMessage("SetShotgun",this);
		gameController.addWeapon(temp);	
	}

	public bool CheckForMonsterHitForFrags(string monsterID)
	{
		return OnMonsterHit(monsterID,5);
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		//string monsterID = other.name;
		switch(other.transform.tag)
		{
		case "PlayerHit":
			break;
						
		case Tags.Monster:
		case Tags.BossMonster:			
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		return;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
	}
}