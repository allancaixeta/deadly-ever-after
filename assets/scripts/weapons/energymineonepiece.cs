using UnityEngine;
using System.Collections;

public class energymineonepiece : WeaponController {

	energymines father;
	public bool updatableEnergyBean;
	public LerpEquationTypes lerp;
	Transform updatableTargetBegin, updatableTargetEnd;
	float speed, growtDuration, currentGrowth;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.constantdamage;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			father.DepleteMine(gameObject);
			DestroyItSelf();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			if(updatableEnergyBean)
			{
				if(updatableTargetBegin && updatableTargetEnd)
				{
					transform.position = updatableTargetBegin.position;
					float angle = Mathf.Atan2 (updatableTargetEnd.position.y - transform.position.y, updatableTargetEnd.position.x - transform.position.x) * Mathf.Rad2Deg;				
					transform.localEulerAngles = new Vector3 (0,0,angle);
					if(currentGrowth > 0)
					{
						currentGrowth -= GameController.deltaTime;
						transform.localScale = new Vector3 (lerp.Lerp (Vector2.Distance(updatableTargetEnd.position,transform.position),0,currentGrowth/growtDuration),transform.localScale.y,transform.localScale.z);
					}
					else
						transform.localScale = new Vector3 (Vector2.Distance(updatableTargetEnd.position,transform.position),transform.localScale.y,transform.localScale.z);
				}
				else
					DestroyItSelf ();
			}
			else
			{
				countDown -= GameController.deltaTime;
				DecreaseMonsterHitListDuration ();
				if(countDown < 0)
					nextState = weaponControllerStates.finish;
			}
			break;			
		}
	}

	public void SetTarget(Transform target)
	{
		if (updatableEnergyBean)
			updatableTargetEnd = target;
		float angle = Mathf.Atan2 (target.transform.position.y - transform.position.y, target.position.x - transform.position.x) * Mathf.Rad2Deg;				
		transform.localEulerAngles = new Vector3 (0,0,angle);
		transform.localScale = new Vector3 (updatableEnergyBean ? 0 : Vector2.Distance(target.position,transform.position),transform.localScale.y,transform.localScale.z);
	}

	public void SetFather(Transform t)
	{
		updatableTargetBegin = t;
	}

	public void SetGrowth(float g)
	{
		growtDuration = g;
		currentGrowth = growtDuration;
	}

	public void SetFather(energymines f)
	{
		father = f;
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case "PlayerHit":
			break;
			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.Wall:	
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit (other.GetComponent<MonsterGeneric>(),monsterID,transform.position);
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,speed))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		father.DepleteMine(gameObject);
		DestroyItSelf();
	}
}
