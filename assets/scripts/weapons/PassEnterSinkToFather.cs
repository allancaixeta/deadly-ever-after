﻿using UnityEngine;
using System.Collections;

public class PassEnterSinkToFather : MonoBehaviour {

	public void EnterSink()
	{
		transform.SendMessageUpwards(MnstMsgs.DestroyItself);
	}
}
