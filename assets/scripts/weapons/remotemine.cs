using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class remotemine : WeaponController {

	public Transform horizontal, vertical;
	float growthRate, speed;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.constantdamage;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			growthRate = 0;
			countDown = LifeTime;
			monsterHitListID = new List<string>();
			monsterHitListDuration = new List<float>();
			speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.normal:
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			DecreaseMonsterHitListDuration ();
			break;
		case weaponControllerStates.normal:	
			growthRate += GameController.deltaTime * speed;
			horizontal.localScale = new Vector3 (growthRate,horizontal.localScale.y,horizontal.localScale.z);
			vertical.localScale = new Vector3 (vertical.localScale.x,growthRate,vertical.localScale.z);
			DecreaseLifeTime ();
			DecreaseMonsterHitListDuration ();
			break;			
		}
	}

	public void Explode()
	{
		nextState = weaponControllerStates.normal;
	}

	public override void DestroyItSelf()
	{
		horizontal.localScale = new Vector3 (horizontal.localScale.y,horizontal.localScale.y,horizontal.localScale.z);
		vertical.localScale = new Vector3 (vertical.localScale.x,vertical.localScale.x,vertical.localScale.z);
		nextState = weaponControllerStates.start;
		gameObject.SetActive (false);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case Tags.PlayerHit:
			break;
			
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:	
		case Tags.Wall:	
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,transform.position);	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,AttackSpeed))
			return;
		float multiplier = (currentState == weaponControllerStates.start) ? auxVariable : 1;
		HitMonster((int)(damage * multiplier),monster,transform.position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}