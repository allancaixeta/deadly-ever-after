﻿using UnityEngine;
using System.Collections;

public class voidray : WeaponController {

	public LerpEquationTypes lerp;
	public float sphereCastRadius = 1;
	bool hasATarget = false;
	public float lightningSize = 1; //grossura do raio
	int lightningRealSize; //grossura real do raio
	public float lightningSpeed = 1f; // velocidade do ruido do raio
	public float amplitude = 1;
	public float timeToEaseTheUse = 5;
	float currentTimeEaseTheUse;
	public float speedUpVariable = 0.2f;
	float loopsOfEaseToUse = 0;
	Transform lightningTarget, finalTarget;
	float defaultDistance;
	enum targetType{
		nothing,
		monster,
		objects		
	}
	float [] targetConsistencyCheck = new float [3];
	targetType targetControl = targetType.nothing;
	float targetDistance;
	Transform target;
	float tempDistance;
	Perlin noise;
	float oneOverLightningSize;
	int lightningLength;
	ParticleSystem.Particle[] particles;
	float currentChargeTime;
	Vector3 voidrayDirection;
	GameObject monsterTarget;
	Ray ray;
	RaycastHit hit;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.voidray;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,true);
			lightningTarget = transform.FindChild("lightningTarget");
			finalTarget = transform.FindChild ("FinalTarget");	
			float speed = ProjectileSpeed * (1 + skills.GetFasterBang ());
			targetDistance = speed;
			defaultDistance = targetDistance;
			tempDistance = targetDistance;		
			finalTarget.localPosition = new Vector3(0,-defaultDistance,0);
			noise = new Perlin();
			//transform.Translate(new Vector3(0,-2,0));
			currentTimeEaseTheUse = timeToEaseTheUse;
			nextState = weaponControllerStates.normal;
			break;

		case weaponControllerStates.normal:
			RecreateLightningParticles();				
			LightningVoidRayGun();
			currentChargeTime = AttackSpeed;
			UI = InterfaceController.Instance;
			transform.position = new Vector3 (transform.position.x, transform.position.y,auxVariable);
			break;

		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			break;
		case weaponControllerStates.normal:	
			currentTimeEaseTheUse -= GameController.deltaTime;
			if(currentTimeEaseTheUse < 0)
			{
				loopsOfEaseToUse += speedUpVariable;
				currentTimeEaseTheUse = timeToEaseTheUse;
			}
			voidrayDirection = finalTarget.position - transform.position;
			voidrayDirection.z = 0;
			ray = new Ray(transform.position,voidrayDirection);
			int layerMask = 2321408;
			if(Physics.SphereCast(ray, sphereCastRadius, out hit, Vector2.Distance (finalTarget.position,this.transform.position)+0.5f, layerMask)){
				if(hit.collider.name == finalTarget.name)
				{
					if(targetControl == targetType.nothing)
					{
						ResetConsistensys();
						target = finalTarget;
						targetDistance = defaultDistance;							
					}
					else
					{
						targetConsistencyCheck[0] += GameController.deltaTime;
						if(targetConsistencyCheck[0] > LifeTime)
						{
							targetControl = targetType.nothing;
							currentChargeTime = AttackSpeed;
						}
					}
				}
				else
				{						
					target = hit.collider.transform;
					targetDistance = hit.distance;
					if(target.tag == Tags.Monster)
					{	
						if(targetControl == targetType.monster)
						{
							ResetConsistensys();
							if(monsterTarget)
							{
								if(monsterTarget.name == hit.collider.name)
								{
									currentChargeTime -= GameController.deltaTime + loopsOfEaseToUse;
									UI.InterfaceCharge(0,currentChargeTime/AttackSpeed);
									if(currentChargeTime < 0)
									{
										hasATarget = true;
										nextState = weaponControllerStates.VOID;
										player.SendMessage("StopVoidRay",false);
										return;
									}
								}
								else
								{
									currentChargeTime = AttackSpeed;
									monsterTarget = hit.collider.gameObject;
								}
							}
							else
							{
								monsterTarget = hit.collider.gameObject;
							}
						}
						else
						{
							targetConsistencyCheck[1] += GameController.deltaTime;
							if(targetConsistencyCheck[1] > LifeTime)
							{
								targetControl = targetType.monster;
								monsterTarget = hit.collider.gameObject;
								currentChargeTime = AttackSpeed;
							}
						}
					}
					else
					{
						if(targetControl == targetType.objects)
						{
							ResetConsistensys();
						}
						else
						{
							targetConsistencyCheck[2] += GameController.deltaTime;
							if(targetConsistencyCheck[2] > LifeTime)
							{
								targetControl = targetType.objects;
								currentChargeTime = AttackSpeed;
							}
						}
					}
				}
			}
			else 
			{
				Debug.LogError("Void ray com defeito!");
			}
			LightningVoidRayGun();	
			break;	

		//estado usado quando a arma ja obteve seu alvo e somente precisar acertar ele independente de posiçao
		case weaponControllerStates.VOID:
			targetDistance = Vector2.Distance (target.position,this.transform.position);
			LightningVoidRayGun();	
			break;
		}
	}

	void LightningVoidRayGun()
	{
		lightningTarget.localPosition = new Vector3(0,-(targetDistance + sphereCastRadius),0);
		if(Mathf.Abs(tempDistance-targetDistance) > 1)
		{
			RecreateLightningParticles();	
			tempDistance = targetDistance;
		}
		float timex = Time.time * lightningSpeed * 0.1365143f;
		float timey = Time.time * lightningSpeed * 1.21688f;
		for (int i=0; i < lightningLength; i++)
		{
			Vector3 position;
			if(currentState == weaponControllerStates.VOID)
			{
				position = Vector3.Lerp(this.transform.position, target.position, oneOverLightningSize * (float)i);
			}
			else
			{
				position = Vector3.Lerp(this.transform.position, lightningTarget.position, oneOverLightningSize * (float)i);
			}
			Vector3 offset = new Vector3(noise.Noise(timex + position.x, timex + position.y, timex + position.z) * lerp.Lerp(0,1,currentChargeTime/AttackSpeed),
			                             noise.Noise(timey + position.x, timey + position.y, timey + position.z) * lerp.Lerp(0,1,currentChargeTime/AttackSpeed),0);
			position += (offset /** ((float)i * oneOverLightningSize)*/) * (Random.value + (amplitude * lerp.Lerp(0,1,currentChargeTime/AttackSpeed)));		
			particles[i].position = position;
			particles[i].color = Color.Lerp(Color.white,Color.blue,1-currentChargeTime/AttackSpeed);
//			switch(currentParticleLevel)
//			{
//			case 0:
//				particles[i].color = new Color(0.33f,0.33f,0.33f,1);
//				break;
//			case 1:
//				particles[i].color = new Color(0.66f,0.66f,0.66f,1);
//				break;
//			case 2:
//			default:
//				particles[i].color = Color.white;
//				break;
//			}
			particles[i].remainingLifetime = 1;
		}
		weaponVisual.GetComponent<ParticleSystem>().SetParticles(particles,lightningLength);		
	}

	void RecreateLightningParticles()
	{
		lightningRealSize = (int)(targetDistance * 4 * lightningSize);
		oneOverLightningSize = 1f / (float)lightningRealSize;		
		particles = new ParticleSystem.Particle[lightningRealSize];
		weaponVisual.GetComponent<ParticleSystem>().enableEmission = false;	
		weaponVisual.GetComponent<ParticleSystem>().Emit(lightningRealSize);
		//weaponVisual.particleSystem.Simulate(1);
		lightningLength = weaponVisual.GetComponent<ParticleSystem>().GetParticles(particles);
	}

	void ResetConsistensys()
	{
		targetConsistencyCheck[0] = 0;
		targetConsistencyCheck[1] = 0;
		targetConsistencyCheck[2] = 0;
	}

	public void VoidRayStop()
	{
		//TODO: animaçao de fim do uso da arma
		if(hasATarget)
			HitMonster(damage,monsterTarget.GetComponent<MonsterGeneric>(),transform.position);
		DestroyItSelf();
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		return;
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		return;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		return;	
	}
	
	public override void LeaveTimeDistortion()
	{
		return;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
	}
}
