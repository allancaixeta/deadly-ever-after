﻿using UnityEngine;
using System.Collections;

public class jump : WeaponController {

	public float distanceFromPlayer = 1;
	float stun;
	bool turnedON = false;
	public GameObject FX;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.jump;

	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			transform.Translate(new Vector3(0,-1,0)* distanceFromPlayer,Space.World);
			break;

		case weaponControllerStates.normal:
			break;

		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			break;
		case weaponControllerStates.normal:	
			DecreaseLifeTime();
			break;			
		}
	}

	public void HammerStunOn(float s)
	{
		stun = s;
		nextState = weaponControllerStates.normal;
		turnedON = true;
		GameObject temp = Instantiate(FX,weaponVisual.position,Quaternion.Euler (45,0,0)) as GameObject;
		temp.transform.parent = this.transform;
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		if(!turnedON)
			return;
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case "PlayerHit":
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:	
			CheckForHit (other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,LifeTime))
			return;
		HitMonster(damage,monster,transform.position);//TODO: verificar posiçao do Knockback
		monster.Stun(stun);
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
	}
}