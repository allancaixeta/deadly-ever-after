﻿using UnityEngine;
using System.Collections;

public class fragments : WeaponController {

	float angle;
	bool allowedToHit = false; //small delay necessary, so that all shots don't hit the same target
	fraggrenade fraggrenadeFather;
	bool attached = false;
	MonsterGeneric monster;
	Vector3 relativePositionToTarget;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.fraggrenade;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			countDown = AttackSpeed;
			break;

		case weaponControllerStates.normal:
			allowedToHit = true;
			break;

		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
			
		case weaponControllerStates.finish:
			DestroyItSelf ();
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{
				nextState = weaponControllerStates.normal;
				countDown = LifeTime;
			}
			Move ();
			break;
		case weaponControllerStates.normal:	
			if(!attached)
				Move ();
			else
			{
				if(!monster)
					DestroyItSelf ();
				else
					transform.position = monster.transform.position + relativePositionToTarget;
			}
			countDown -= GameController.deltaTime;
			if(countDown < 0)
			{
				if(attached && monster)
					HitMonster(damage,monster,transform.position);
				DestroyItSelf ();
			}
			break;			
		}
	}

	void Move()
	{
		direction = new Vector3(Mathf.Sin(Mathf.Deg2Rad * angle),-Mathf.Cos(Mathf.Deg2Rad * angle),0);
		direction.Normalize();
		direction *= GameController.deltaTime * ProjectileSpeed * (1 + skills.GetFasterBoom ()) * timeDistortion;
		direction.z = 0;	
		transform.Translate(direction,Space.World);
	}

	public void SetDirection(float a)
	{
		angle = a;
	}

	public void SetDamage(int d)
	{
		damage = d;
	}

	public void SetProjectileSpeed(int p)
	{
		ProjectileSpeed = p;
	}

	public void SetFraGrenade(fraggrenade f)
	{
		fraggrenadeFather = f;
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		if(attached)
			return;
		string monsterID = other.name;
		switch(other.transform.tag)
		{
		case Tags.Monster:
		case Tags.BossMonster:
			if(allowedToHit)
				CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(transform.position));	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		attached  = true;
		this.monster = monster;
		relativePositionToTarget = transform.position - monster.transform.position;
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
