﻿using UnityEngine;
using System.Collections;

public class deathrain : WeaponController {

	public GameObject FXbloodpool;
	public float bloodSpawnTimer = 0.5f;
	float counter;
	float maxX, minX, maxY, minY;
	Transform target;
	bool targetAcquired;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.deathrain;
		maxX = transform.position.x + transform.localScale.x/2;
		minX = transform.position.x - transform.localScale.x/2;
		maxY = transform.position.y + transform.localScale.y/2;
		minY = transform.position.y - transform.localScale.y/2;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			if(!targetAcquired)
			{
				target = gameController.FindClosestMonster(transform.position,"");
				if(target && Vector2.Distance (target.position,transform.position) < auxVariable)
				{
					targetAcquired = true;
				}
			}
			else
			{
				if(!target)
				{
					targetAcquired = false;
				}
				else
				{
					if(Vector2.Distance (target.position,transform.position) > 0.25f)
					{
						Vector3 vectorDifference2 = transform.position - target.position;
						vectorDifference2.z = 0;
						vectorDifference2.Normalize();	
						transform.position -= vectorDifference2 * GameController.deltaTime * ProjectileSpeed * (1 + skills.GetFasterBoom ()) * timeDistortion;
					}
				}
			}
			counter -= GameController.deltaTime;
			if(counter < 0)
			{
				Vector3 pos = new Vector3(Random.Range (minX,maxX), Random.Range(minY,maxY), transform.position.z);
				Instantiate(FXbloodpool,pos,Quaternion.identity);
				counter = bloodSpawnTimer;
			}
			DecreaseLifeTime ();
			DecreaseMonsterHitListDuration();
			break;			
		}
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,other.transform.position);	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,AttackSpeed))
			return;
		HitMonster(damage,monster,position);//TODO: verificar posiçao do Knockback
	}

	protected override void OnTriggerExit(Collider other)
	{
		return;
	}

	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem

	}
}