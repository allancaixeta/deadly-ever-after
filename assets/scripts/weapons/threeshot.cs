using UnityEngine;
using System.Collections;

public class threeshot : WeaponController {

	public float afterHitSurvival = 0.5f;
	public float afterHitSizeIncrease = 2;
	bool miss = true;
	bool thirdShot;
	string targetName;
	public float thirdShotDamageIncrease = 3;
	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.threeshot;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;

		case weaponControllerStates.finish:
			Instantiate(onHitEffect,transform.position,onHitEffect.transform.rotation);
			transform.FindChild ("Shadow").gameObject.SetActive (false);
			weaponVisual.gameObject.SetActive (false);
			GetComponent<CapsuleCollider>().radius *= afterHitSizeIncrease;
			countDown = afterHitSurvival;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.normal:	
			MoveForward();
			CheckIfInsideRoom();
			break;	
		case weaponControllerStates.finish:
			countDown -= GameController.deltaTime;
			if(countDown < 0)
				DestroyItSelf();
			break;
		}
	}

	public void ArmThirdShot(string name)
	{
		thirdShot = true;
		targetName = name;
	}

	public override void DestroyItSelf()
	{
		if(miss)
			player.SendMessage ("ThreeShotMiss");
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.tag)
		{
		case Tags.Lvl3Obstacle:
		case Tags.Unmovable:
		case Tags.HarmEvent:
			nextState = weaponControllerStates.finish;			
			break;
			
		case Tags.Monster:
		case Tags.BossMonster:
			CheckForHit(other.GetComponent<MonsterGeneric>(),monsterID,transform.position);	
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{	
		if(OnMonsterHit(monsterID,LifeTime))
			return;	
		miss = false;
		if(thirdShot && targetName == monster.name)
		{
			int tempDamage = (int)((float) damage * thirdShotDamageIncrease);
			HitMonster(tempDamage,monster,position);
		}
		else
			HitMonster(damage,monster,position);
		player.SendMessage ("ThreeShotHit",monster.name);
		Instantiate(onHitEffect,transform.position,onHitEffect.transform.rotation);
		DestroyItSelf();
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	

	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}
