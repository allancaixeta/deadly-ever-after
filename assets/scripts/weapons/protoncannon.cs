﻿using UnityEngine;
using System.Collections;

public class protoncannon : WeaponController {

	public float damageMultiplier = 0.5f;

	public override void SpecificWeaponAwake()
	{
		weaponSpecial = WeaponController.WeaponSpecial.protoncannon;
		countDown = AttackSpeed;
	}
	
	// Use this for initialization
	protected override bool StartState(){
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
		case weaponControllerStates.start:	
			PlaySound(audioClip,false);	
			nextState = weaponControllerStates.normal;
			break;
			
		case weaponControllerStates.pause:			
			saveState = lastState;
			break;
		}
		return true;
	}
	
	// Update is called once per frame
	protected override void UpdateState() {
		switch(currentState)
		{
		case weaponControllerStates.start:
			
			break;
		case weaponControllerStates.normal:	
			DecreaseLifeTime();
			break;			
		}
	}

	public void StartCannon(float d)
	{
		damage = damage + (int)(damageMultiplier * d);
	}

	public override void DestroyItSelf()
	{
		gameController.removeWeapon(this.gameObject);
		Destroy(this.gameObject);
	}
	
	protected override void OnTriggerEnter(Collider other)
	{		
		string monsterID = other.name;
		switch(other.transform.tag)
		{			
		case Tags.Monster:
		case Tags.BossMonster:		
			CheckForHit (other.GetComponent<MonsterGeneric>(),monsterID,other.ClosestPointOnBounds(player.transform.position));
			break;
		}
	}
	
	protected override void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position)
	{		
		if(OnMonsterHit(monsterID,AttackSpeed))
			return;
		int distance = (int)(Vector2.Distance (player.transform.position,monster.transform.position)/10);
		HitMonster(distance == 0 ? damage : damage / distance,monster,position);//TODO: verificar posiçao do Knockback
	}
	
	protected override void OnTriggerExit(Collider other)
	{
		return;
	}
	
	public override void EnterTimeDistortion(float rate)
	{
		timeDistortion = 1 - rate / 100;		
	}
	
	public override void LeaveTimeDistortion()
	{
		timeDistortion = 1;
	}	
	
	public override void EnterSink()
	{
		//TODO: animaçao de ser engolida pela nuvem
		DestroyItSelf ();
	}
}