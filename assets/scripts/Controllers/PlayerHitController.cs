using UnityEngine;
using System.Collections;

[Prefab("PlayerHitController")]
public class PlayerHitController : Singleton<PlayerHitController>
{
	
	float currentBlinkDuration;
	public float blinkDuration = 3;
	PlayerController playerController;
	SkillController skillcontroller;
	InterfaceController UI;
	void Start()
	{
		playerController = PlayerController.Instance;
		skillcontroller = (SkillController) FindObjectOfType(typeof(SkillController));
		UI = InterfaceController.Instance;
	}
	
	void Update()
	{
		if(currentBlinkDuration >= 0)
		{
			currentBlinkDuration -=GameController.deltaTime;
			if(currentBlinkDuration < 0)
				playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("hit",false);
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		float damage;
		switch(other.transform.tag)
		{
		case Tags.Monster:
		case Tags.BossMonster:
		//case "GiantMonster":
		//case "GhostMonster":
		case Tags.MonsterProjectile:
		case Tags.Explosion:
			if(currentBlinkDuration < 0)
			{
				damage = other.GetComponent<MonsterGeneric>().monsterDamage;
				if(skillcontroller.GetDFENCE() > 0)
				{
					playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("armor",true);
					damage -= skillcontroller.GetDFENCE();
					if(damage <=0)
						return;
				}
				playerController.HitPlayer(damage);	
				currentBlinkDuration = blinkDuration + skillcontroller.GetBlinky ();
				playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("hit",true);
				UI.OnPlayerHit (other.GetComponent<MonsterGeneric>().monsterName, other.GetComponent<MonsterGeneric>().monsterToughness.ToString(), other.GetComponent<MonsterGeneric>().monsterDamage.ToString ());
			}
			break;
		/*case Tags.Weapon:
			if(other.GetComponent<WeaponController>().rocketlauncherActivate && currentBlinkDuration < 0)
			{
				playerController.HitPlayer(other.GetComponent<WeaponController>().damage);	
				currentBlinkDuration = blinkDuration;
			}
			break;*/
//		case "Explosion":
//			if(currentBlinkDuration < 0)
//			{
//				damage = int.Parse(other.name);
//				if(skillcontroller.GetDFENCE() > 0)
//				{
//					playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("armor",true);
//					damage -= skillcontroller.GetDFENCE();
//					if(damage <=0)
//						return;
//				}
//				playerController.HitPlayer(damage);		
//				currentBlinkDuration = blinkDuration + skillcontroller.GetBlinky ();
//				playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("hit",true);
//				UI.OnPlayerHit ("Explosion", " ", other.name);
//			}
//			break;
		case "HarmEvent":
			if(currentBlinkDuration < 0)
			{
				damage = other.GetComponent<EventController>().damage;
				if(skillcontroller.GetDFENCE() > 0)
				{
					playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("armor",true);
					damage -= skillcontroller.GetDFENCE();
					if(damage <=0)
						return;
				}
				playerController.HitPlayer(damage);		
				currentBlinkDuration = blinkDuration + skillcontroller.GetBlinky ();
				playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("hit",true);
				UI.OnPlayerHit (other.GetComponent<EventController>().eventName, other.GetComponent<EventController>().level.ToString(), other.GetComponent<EventController>().damage.ToString ());
			}
			break;
		default:
			break;
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter (other);	
	}

	public bool HitPlayer(float damage, string monsterName)
	{
		if(currentBlinkDuration < 0)
		{
			if(skillcontroller.GetDFENCE() > 0)
			{
				playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("armor",true);
				damage -= skillcontroller.GetDFENCE();
				if(damage <=0)
					return true;
			}
			playerController.HitPlayer(damage);	
			currentBlinkDuration = blinkDuration + skillcontroller.GetBlinky ();
			playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("hit",true);
			UI.OnPlayerHit (monsterName, " ", damage.ToString ());
			return true;
		}
		return false;
	}

	public bool HitPlayer(float damage)
	{
		if(currentBlinkDuration < 0)
		{
			if(skillcontroller.GetDFENCE() > 0)
			{
				playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("armor",true);
				damage -= skillcontroller.GetDFENCE();
				if(damage <=0)
					return true;
			}
			playerController.HitPlayer(damage);	
			currentBlinkDuration = blinkDuration + skillcontroller.GetBlinky ();
			playerController.transform.FindChild("PlayerVisual").GetComponent<Animator>().SetBool("hit",true);
			return true;
		}
		return false;
	}

	public void SetcurrentBlinkDuration(float b)
	{
		if(currentBlinkDuration < b)
			currentBlinkDuration = b;
	}
}
