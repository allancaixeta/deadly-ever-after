using UnityEngine;
using System.Collections;

public class SimpleButton : MonoBehaviour {
	
	//Material pra usar se o tipo do button for Press
	public Material MaterialNormal;
	public Material MaterialPressed;
	
	//Time to remain pressed
	public float UnpressDelay = 0.13f;
	
	//Timer to count how long ago the button has been pressed and then released
	private float delay = 0.0f;
	
	//Objeto alvo para onde voce vai mandar uma mensagem quando o button for apertado
	public GameObject target;
	
	//Nome da função a ser chamada quando o button for apertado
	public string message;
	
	// Use this for initialization
	void Start () {
		//gameObject.tag = "Button";  O ideal é que a interfaces esteja em um layer e tag unico dela
	}
	
	// Update is called once per frame
	void Update () {
		if(delay > 0)
		{
			delay -= GameController.deltaTime;
			if(delay < 0)
			{
				delay = 0;
				this.GetComponent<Renderer>().material = MaterialNormal;
				if (target != null && !(message.Trim() == "")) {
					target.SendMessage(message);
				}
			}
		}
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			int layermask = 1000;
			if(Physics.Raycast (ray, out hit, 1000, layermask)){
				if(hit.collider.transform == this.transform)
				{
					delay = UnpressDelay;
					this.GetComponent<Renderer>().material = MaterialPressed;				
				}
			}
		}
	}
}
