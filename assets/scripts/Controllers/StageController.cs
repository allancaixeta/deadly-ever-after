using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class RoomDifficulty{
	public MonsterVariations [] monsters;
}

[System.Serializable]
public class MonsterVariations{
	public monstervariation [] variations;
	public enum monstervariation{
		favorite,
		nonfavorite,
		plus1level,
		minus1level,
		onetainted
	}
}

[System.Serializable]
public class BossAssignment{
	public bosses [] bosslist;
	public enum bosses{
		redhood,
		tinsoldier,
		hansel,
		goldilocks,
		cinder,
		hamelin,
		bluebeard,
		redshoes,
		sleepingbeauty
	}
}

[Prefab("StageController")]
public class StageController : Singleton<StageController>
{
		
	/*doorSide{
		0 left
		1 up
		2 right
		3 down
	}*/

	//ajuste para o x e y da configuracao dos monstros e moveis encaixar examente na Unity
	public static float initialPos = 15.45f;
	public static float adjustPos = 4.6076f;
    public static float initialX = 15.45f;
    public static float initialY = 84.55f;
    public static float adjustX = 4.6076f;
    public static float adjustY = 4.6076f;

    public enum StageNumber
	{
		stage1, 
		stage2,
		stage3,
		stage4, 
		stage5,
		stage6,
		stage7,
		stage8,
		stage9
	}

	public enum RoomType
	{
		normal,
		treasure,
		boss
	}

	public enum RoomCenario
	{
		capela,
		cemiterio,
		estrada,
		laboratorio,
		adega,
		dungeon,
		quarto,
		refeitorio,
		sala,
		saladearmas,
		salao,
		campo,
		floresta,
		jardim
	}

	public StageNumber nextStageNumber = StageNumber.stage1;
	StageNumber currentStageNumber = StageNumber.stage9;
	GameController gameController;
	PlayerController player;
	EventList eventList;
	public Vector3 leftDoorSpwn, upDoorSpwn, rightDoorSpwn, downDoorSpwn;
	
	public StageNumber AOEWeaponStageAppearence = StageNumber.stage1;
	public StageNumber defenseWeaponStageAppearence = StageNumber.stage2;
	public StageNumber crushWeaponStageAppearence = StageNumber.stage3;
    public GameObject treasureRoom;
    public GameObject []capelaRooms = new GameObject[5];
	public GameObject []cemiterioRooms = new GameObject[5];
	public GameObject []estradaRooms = new GameObject[5];
	public GameObject []laboratorioRooms = new GameObject[5];
	public GameObject []adegaRooms = new GameObject[5];
	public GameObject []dungeonRooms = new GameObject[5];
	public GameObject []quartoRooms = new GameObject[5];
	public GameObject []refeitorioRooms = new GameObject[5];
	public GameObject []salaRooms = new GameObject[5];
	public GameObject []saladearmasRooms = new GameObject[5];
	public GameObject []salaoRooms = new GameObject[5];
	public GameObject []campoRooms = new GameObject[5];
	public GameObject []florestaRooms = new GameObject[5];
	public GameObject []jardimRooms = new GameObject[5];
	public GameObject []bossRooms = new GameObject[9];
	public RoomDifficulty []difficultysStage1 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage2 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage3 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage4 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage5 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage6 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage7 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage8 = new RoomDifficulty[10];
	public RoomDifficulty []difficultysStage9 = new RoomDifficulty[10];
    public RoomCenario[] roomCenarioSequence = new RoomCenario[10];


    [Range(0, 100)]
	public int []chanceOfMonsterBecomingTaintedByStage;

	public BossAssignment [] matchAssigmentofBosses;
	BossRoomController bossroom;
	bool []roomCleared = new bool[12];
	GameObject []currentRooms = new GameObject[12];
	public StageConfiguration [] maps = new StageConfiguration[19];
	public GameObject roomClearedAnimation;
	GameObject currentRoomClearedAnimation;
	int currentStageConfiguration, currentRoomNumber;
	GameObject []rats;	
	InterfaceController UI;
	int weaponAOEChosen, weaponDefenseChosen, weaponCrushChosen;
	private EventController currentEvent;
	PrototipoController prot;
	[System.Serializable]
	public class door{
		public int doorSide;
		public int doorDestinationRoom;
	}
	
	// Use this for initialization
	void Start () {
		gameController = GameController.Instance;	
		eventList = (EventList) FindObjectOfType(typeof(EventList));	
		prot = (PrototipoController) FindObjectOfType(typeof(PrototipoController));	
		player = (PlayerController) FindObjectOfType(typeof(PlayerController));		
		UI = InterfaceController.Instance;
	}
	
	// Update is called once per frame
	public void UpdateStageController() {
		if(!StartState())
			UpdateState();
	}

	bool StartState()
	{
		if(currentStageNumber == nextStageNumber)
			return false;
		currentStageNumber = nextStageNumber;
		switch(currentStageNumber)
		{
		case StageNumber.stage1:
            maps[currentStageConfiguration].LoadStageConfiguration();
			UI.MiniMapFirstRoom ((int)maps[currentStageConfiguration].allRooms[0].x, (int)maps[currentStageConfiguration].allRooms[0].y);
			for (int i = 0; i < 10; i++) {
				GenerateRoom(i,RoomType.normal,0);
				if(i!=0)
				{
					currentRooms[i].SetActive (false);
				}
			}
			GenerateRoom(10,RoomType.treasure,0);
			currentRooms[10].SendMessage("LockDoors");
			currentRooms[10].SetActive (false);
			GenerateBossRoom(GetBossRoom(matchAssigmentofBosses[0].bosslist[0]),0);
			currentRooms[11].SetActive (false);
			MoveToRoom(0,1);
			break;
		case StageNumber.stage2:
			break;
		case StageNumber.stage3:
			break;
		case StageNumber.stage4:
			break;
		case StageNumber.stage5:
			break;
		case StageNumber.stage6:
			break;
		case StageNumber.stage7:
			break;
		case StageNumber.stage8:
			break;
		case StageNumber.stage9:
			break;
		}
		return true;
	}

	void UpdateState()
	{
		switch(currentStageNumber)
		{
		case StageNumber.stage1:
			if(currentEvent)
				currentEvent.UpdateEvent();
			if(currentRoomNumber == 11)
				bossroom.UpdateBossRoom();
			break;
		case StageNumber.stage2:
			break;
		case StageNumber.stage3:
			break;
		case StageNumber.stage4:
			break;
		case StageNumber.stage5:
			break;
		case StageNumber.stage6:
			break;
		case StageNumber.stage7:
			break;
		case StageNumber.stage8:
			break;
		case StageNumber.stage9:
			break;
		}
	}

	// Para cada mudança de fase
	public void SwitchStage(StageNumber nextStage)
	{	
		//prot.StartGame ();
		nextStageNumber = nextStage;
		currentStageConfiguration = Random.Range (0, maps.Length);
		currentRoomNumber = 0;
		switch(nextStage)
		{
            case StageNumber.stage1:
                weaponAOEChosen = Random.Range(0, 10); //prot.weaponChosenAOE;
                weaponDefenseChosen = 1;// prot.weaponChosenDefense;
			    weaponCrushChosen = Random.Range(0, 9);//prot.weaponChosenCrush;
                UI.AddWeapon (gameController.weaponCrush[weaponCrushChosen]);
			    UI.VisualWeapon (0,gameController.weaponCrush[weaponCrushChosen].weaponAppearenceUP,
			                     gameController.weaponCrush[weaponCrushChosen].weaponAppearenceDown);
			    gameController.SetCrush(weaponCrushChosen);
			    UI.AddWeapon (gameController.weaponAOE[weaponAOEChosen]);
			    UI.VisualWeapon (1,gameController.weaponAOE[weaponAOEChosen].weaponAppearenceUP,
			                     gameController.weaponAOE[weaponAOEChosen].weaponAppearenceDown);
			    gameController.SetAOE(weaponAOEChosen);
			    UI.AddWeapon (gameController.weaponDefense[weaponDefenseChosen]);
			    UI.VisualWeapon (2,gameController.weaponDefense[weaponDefenseChosen].weaponAppearenceUP,
			                     gameController.weaponDefense[weaponDefenseChosen].weaponAppearenceDown);
			    gameController.SetDefense(weaponDefenseChosen);
			    break;
		    case StageNumber.stage2:
			    break;
		    case StageNumber.stage3:
			    break;
		    case StageNumber.stage4:
			    break;
		    case StageNumber.stage5:
			    break;
		    case StageNumber.stage6:
			    break;
		    case StageNumber.stage7:
			    break;
		    case StageNumber.stage8:
			    break;
		    case StageNumber.stage9:
			    break;
		}	
	}

	public void MoveToRoom(int nextRoom, int nextDoorSide)
	{
		GameObject [] effectsToDelete = GameObject.FindGameObjectsWithTag ("FxTemporaire");
		for (int i = 0; i < effectsToDelete.Length; i++) {
			Destroy (effectsToDelete[i]);
		}
		gameController.ClearEventsInRoom ();
		//gameController.ClearListofInterestedOnPlayerShooting ();
		currentRooms [currentRoomNumber].SendMessage ("LockDoors");
		player.SendMessage ("ChangeRoom",DoorPosition(nextDoorSide));
		currentRooms[currentRoomNumber].SetActive(false);
		//TODO: inicia evento aleatorio da sala     gameController.StartRandomEvent();
		currentRooms[nextRoom].SetActive(true);
		currentRoomNumber = nextRoom;
		UpdateAstar();
		GameObject porcelainsManager = GameObject.Find ("PorcelainsManager(Clone)");
		if(porcelainsManager)
			Destroy (porcelainsManager);
		GameObject GhostsManager = GameObject.Find ("GhostManager");
		if(GhostsManager)
			Destroy (GhostsManager);
		if(!roomCleared[currentRoomNumber])
		{
			switch(currentRoomNumber)
			{
			default:
				currentEvent = (Instantiate(eventList.GetNewEvent(GetLevel(),currentRoomNumber),new Vector3(50,50,1.5f),Quaternion.identity) as GameObject).GetComponent<EventController>();
				currentEvent.StartEvent(currentStageNumber);
				currentRooms[currentRoomNumber].GetComponent<RoomController>().GenerateMonsters(difficultysStage1[currentRoomNumber],currentStageNumber);
				gameController.StartMonsters();
				SetNeighboursRooms(currentRoomNumber);
				break;
			case 10:
				SetNeighboursRooms(currentRoomNumber);
				break;
			case 11:
				bossroom.nextState = BossRoomController.bossState.fight;
				break;
			}
		}
		else
			currentRooms[currentRoomNumber].SendMessage("OpenDoors");
		UI.ChangeSelectedRoom(currentRoomNumber);
		if(currentRoomClearedAnimation)
			Destroy (currentRoomClearedAnimation);
		currentRooms[currentRoomNumber].transform.position = new Vector3(50,50,0);
		Time.timeScale = 1;
		UI.CancelSkillSelection ();
	}

	void SetNeighboursRooms(int roomNumber)
	{
		for (int i = 0; i < 4; i++) {
			if(maps[currentStageConfiguration].room[roomNumber].doors[i].doorSide != 0)
			{
				door d = new door();
				d.doorSide = i+1;
				d.doorDestinationRoom = maps[currentStageConfiguration].room[roomNumber].doors[i].doorDestinationRoom;
				UI.SetNeighboursRooms(d.doorDestinationRoom);		
			}
		}
	}

	void GenerateRoom(int roomNumber, RoomType roomType, int stageNumber)
	{
		Vector3 position = new Vector3(50,50,0);
		GameObject room;
        RoomCenario roomCenario = RoomCenario.capela;
        if (roomNumber < 10)
            roomCenario = roomCenarioSequence[roomNumber];
        switch (roomType)
		{
		default:
		case RoomType.normal:
			switch(roomCenario)
			{
			default:
			case RoomCenario.adega:
				room = adegaRooms[Random.Range (0, adegaRooms.Length)];
                break;
            case RoomCenario.campo:
                room = campoRooms[Random.Range(0, campoRooms.Length)];
                break;
            case RoomCenario.capela:
                room = capelaRooms[Random.Range(0, capelaRooms.Length)];
                break;
            case RoomCenario.cemiterio:
                room = cemiterioRooms[Random.Range(0, cemiterioRooms.Length)];
                break;
            case RoomCenario.dungeon:
                room = dungeonRooms[Random.Range(0, dungeonRooms.Length)];
                break;
            case RoomCenario.estrada:
                room = estradaRooms[Random.Range(0, estradaRooms.Length)];
                break;
            case RoomCenario.floresta:
                room = florestaRooms[Random.Range(0, florestaRooms.Length)];
                break;
            case RoomCenario.jardim:
                room = jardimRooms[Random.Range(0, jardimRooms.Length)];
                break;
            case RoomCenario.laboratorio:
                room = laboratorioRooms[Random.Range(0, laboratorioRooms.Length)];
                break;
            case RoomCenario.quarto:
                room = quartoRooms[Random.Range(0, quartoRooms.Length)];
                break;
            case RoomCenario.refeitorio:
                room = refeitorioRooms[Random.Range(0, refeitorioRooms.Length)];
                break;
            case RoomCenario.sala:
                room = salaRooms[Random.Range(0, salaRooms.Length)];
                break;
            case RoomCenario.saladearmas:
                room = saladearmasRooms[Random.Range(0, saladearmasRooms.Length)];
                break;
            case RoomCenario.salao:
                room = salaoRooms[Random.Range(0, salaoRooms.Length)];
                break;
                }
			break;
		case RoomType.treasure:
            room = treasureRoom;
            UI.SetTreasureRoom(roomNumber);
			break;
		}
		currentRooms[roomNumber] = Instantiate(room,position,Quaternion.identity) as GameObject;
		currentRooms[roomNumber].SendMessage("SetRoomNumber",roomNumber);
		currentRooms[roomNumber].SendMessage("SetChanceOfMonsterBecomingTainted",chanceOfMonsterBecomingTaintedByStage[stageNumber]);
		for (int i = 0; i < 4; i++) {
			if(maps[currentStageConfiguration].room[roomNumber].doors[i].doorSide != 0)
			{
				door d = new door();
				d.doorSide = i+1;
				d.doorDestinationRoom = maps[currentStageConfiguration].room[roomNumber].doors[i].doorDestinationRoom;
				//UI.SetRoomPosFromDoor(roomNumber,d.doorSide,d.doorDestinationRoom);
				currentRooms[roomNumber].SendMessage ("SetDoor",d);			
			}
		}
        UI.SetRoomPos(roomNumber, maps[currentStageConfiguration].allRooms[roomNumber].x, maps[currentStageConfiguration].allRooms[roomNumber].y);
        currentRooms[roomNumber].SendMessage("LockDoors");
		currentRooms [roomNumber].transform.Rotate (0, 0, 90);
	}

	Vector3 DoorPosition(int doorSide)
	{
		switch(doorSide)
		{
		default:
		case 1:
			return leftDoorSpwn;
		case 2:
			return upDoorSpwn;
		case 3:
			return rightDoorSpwn;
		case 4:
			return downDoorSpwn;
		}
	}

	public void RoomCleared()
	{
		if(roomCleared[currentRoomNumber] || currentRoomNumber == 11)
			return;
		currentRooms[currentRoomNumber].SendMessage("OpenDoors");
		roomCleared[currentRoomNumber] = true;
		if (currentRoomNumber < 10)
		{
			UI.LevelUp ();
		}
		currentRoomClearedAnimation = Instantiate(roomClearedAnimation,player.transform.position+Vector3.back*5,Quaternion.identity) as GameObject;
		currentRoomClearedAnimation.transform.parent = player.transform;
		GameObject [] effectsToDelete = GameObject.FindGameObjectsWithTag ("FxTemporaire");
		for (int i = 0; i < effectsToDelete.Length; i++) {
			Destroy (effectsToDelete[i]);
		}
		gameController.ClearEventsInRoom ();
		currentEvent.StopEvent ();
	}

	void GenerateBossRoom(GameObject room, int stageNumber)
	{
		int roomNumber = 11;
		Vector3 position = new Vector3(50,50,0);
		UI.SetBossRoom(roomNumber);
        UI.SetRoomPos(roomNumber, maps[currentStageConfiguration].allRooms[roomNumber].x, maps[currentStageConfiguration].allRooms[roomNumber].y);
        currentRooms[roomNumber] = Instantiate(room,position,Quaternion.identity) as GameObject;
		currentRooms[roomNumber].SendMessage("SetChanceOfMonsterBecomingTainted",chanceOfMonsterBecomingTaintedByStage[stageNumber]);
		bossroom = currentRooms[roomNumber].GetComponent<BossRoomController>();
		bossroom.SetStage (currentStageNumber);
		currentRooms [roomNumber].transform.Rotate (0, 0, 90);
	}

	GameObject GetBossRoom(BossAssignment.bosses b)
	{
		switch(b)
		{
		default:
		case BossAssignment.bosses.tinsoldier:
			return bossRooms[0];
		case BossAssignment.bosses.goldilocks:
			return bossRooms[1];
		}
	}

	public void UpdateAstar()
	{
		AstarPath.active.Scan();
	}

	public void SetEvent(EventController e)
	{
		currentEvent = e;
	}

	public int GetCurrentRoom()
	{
		return currentRoomNumber;
	}

	public RoomController GetRoom()
	{
		return currentRooms[currentRoomNumber].GetComponent<RoomController>();
	}

	bool hardcore;

	public void SetHardcore()
	{
		hardcore = true;
	}

	public int GetToughness()
	{
		if(hardcore)
		{
            return 5;//disable  this when you want to play the old way
			switch(currentRoomNumber)
			{
			default:
			case 1:
			case 2:
				return 1;
			case 3:
			case 4:
				return 2;
			case 5:
			case 6:		
				return 3;
			case 7:
			case 8:		
				return 4;
			case 9:
			case 10:
				return 5;
			}
		}
		else
		{
            switch (currentRoomNumber)
            {
                default:
                case 1:
                case 2:
                    return 1;
                case 3:
                case 4:
                    return 2;
                case 5:
                case 6:
                    return 3;
                case 7:
                case 8:
                    return 4;
                case 9:
                case 10:
                    return 5;
            }
            switch (currentStageNumber)
			{
			default:
			case StageController.StageNumber.stage1:
			case StageController.StageNumber.stage2:
				return 1;
			case StageController.StageNumber.stage3:
			case StageController.StageNumber.stage4:
				return 2;
			case StageController.StageNumber.stage5:
			case StageController.StageNumber.stage6:
				return 3;
			case StageController.StageNumber.stage7:
			case StageController.StageNumber.stage8:
				return 4;
			case StageController.StageNumber.stage9:
				return 5;
			}
		}
	}

	public EventController.eventlvl GetLevel()
	{
		if(hardcore)
		{
			switch(currentRoomNumber)
			{
			default:
			case 1:
			case 2:
			case 3:
				return EventController.eventlvl.one;			
			case 4:
			case 5:
			case 6:		
				return EventController.eventlvl.two;
			case 7:
			case 8:		
			case 9:
			case 10:
				return EventController.eventlvl.three;
			}
		}
		else
		{
			switch(currentStageNumber)
			{
			default:
			case StageController.StageNumber.stage1:
			case StageController.StageNumber.stage2:
			case StageController.StageNumber.stage3:
				return EventController.eventlvl.one;
			case StageController.StageNumber.stage4:
			case StageController.StageNumber.stage5:
			case StageController.StageNumber.stage6:
				return EventController.eventlvl.two;
			case StageController.StageNumber.stage7:
			case StageController.StageNumber.stage8:
			case StageController.StageNumber.stage9:
				return EventController.eventlvl.three;
			}
		}
	}
}
