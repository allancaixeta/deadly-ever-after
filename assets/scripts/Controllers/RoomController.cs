﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

[System.Serializable]
public class furniture{
	public Vector2 position;
	public typeOfFurniture furnituretype;
	public enum typeOfFurniture
	{
		bonesH = 0,
		bonesV = 1,
		cristalball =2,
		torch =3,
		grave = 4,
        religiousstatue = 5,
		stonealtarH =6,
		stonealtarV =7,
        stonecolumn = 8,
		magiccircle =9,
		chair =10,
		woodchairH=11,
		woodchairV=12,
		ironchain=13,
        crow=14,
        rat = 15,
        firefly = 16,
        pumpkin = 17,
        fire = 18,
        barrel = 19,
        box = 20,
        table = 21,
        cask = 22,
        tree = 23,
        straw = 24,
        target = 25,
        road = 26,
        bodiesH = 27,
        bodiesV = 28,
        fenceH = 29,
        fenceV = 30,
        tortureH = 31,
        tortureV = 32,
        flowerH = 33,
        flowerV = 34,
        jailH = 35,
        jailV = 36,
        pileH = 37,
        pileV = 38,
        groceryH = 39,
        groceryV = 40,
        dinnertable = 41,
        bed = 42,
        hallowen = 43,
        plantlabiryinth = 100
	}

	public furniture(Vector2 pos,typeOfFurniture type)
	{
		this.position = pos;
		this.furnituretype = type;
	}

	static public typeOfFurniture GetTypeOfFurniture(string s)
	{
		switch(s)
		{
		default:
			return typeOfFurniture.crow;
		case "bonesH":
            return typeOfFurniture.bonesH;
        case "bonesV":
            return typeOfFurniture.bonesV;            
        case "bodiesH":
            return typeOfFurniture.bodiesH;
        case "bodiesV":
			return typeOfFurniture.bodiesV;
		case "cristalball":
			return typeOfFurniture.cristalball;
		case "torch":
			return typeOfFurniture.torch;       
        case "grave":
			return typeOfFurniture.grave;
		case "stonecolumn":            
             return typeOfFurniture.stonecolumn;
        case "religiousstatue":
             return typeOfFurniture.religiousstatue;
		case "stonealtarH":
			return typeOfFurniture.stonealtarH;
		case "stonealtarV":
			return typeOfFurniture.stonealtarV;
		case "magiccircle":
            return typeOfFurniture.magiccircle;
        case "chair":
			return typeOfFurniture.chair;
		case "woodchairH":
			return typeOfFurniture.woodchairH;
		case "woodchairV":
			return typeOfFurniture.woodchairV;
		case "ironchain":
			return typeOfFurniture.ironchain;
        case "crow":
            return typeOfFurniture.crow;
        case "rat":
            return typeOfFurniture.rat;
        case "firefly":
            return typeOfFurniture.firefly;
        case "fire":
            return typeOfFurniture.fire;
        case "box":
            return typeOfFurniture.box;
        case "barrel":
            return typeOfFurniture.barrel;
        case "table":
            return typeOfFurniture.table;
        case "cask":
            return typeOfFurniture.cask;
        case "tree":
            return typeOfFurniture.tree;
        case "straw":
            return typeOfFurniture.straw;
        case "target":
            return typeOfFurniture.target;
        case "road":
            return typeOfFurniture.road;
        case "fenceH":
            return typeOfFurniture.fenceH;
        case "fenceV":
            return typeOfFurniture.fenceV;
        case "tortureH":
            return typeOfFurniture.tortureH;
        case "tortureV":
            return typeOfFurniture.tortureV;
        case "flowerH":
            return typeOfFurniture.flowerH;
        case "flowerV":
            return typeOfFurniture.flowerV;
        case "jailH":
            return typeOfFurniture.jailH;
        case "jailV":
            return typeOfFurniture.jailV;
        case "pileH":
            return typeOfFurniture.pileH;
        case "pileV":
            return typeOfFurniture.pileV;
        case "groceryH":
            return typeOfFurniture.groceryH;
        case "groceryV":
            return typeOfFurniture.groceryV;
        case "dinnertable":
            return typeOfFurniture.dinnertable;
        case "bed":
            return typeOfFurniture.bed;
        case "hallowen":
            return typeOfFurniture.hallowen;
        case "plantlabiryinth":
		return typeOfFurniture.plantlabiryinth;
		}
	}
}


public class RoomController : MonoBehaviour {

	Vector2 []chaser = new Vector2[8];
	Vector2 []crowdcontrol = new Vector2[5];
	Vector2 []shooter = new Vector2[6];
	Vector2 []muscle = new Vector2[4];
	public Vector2 miniboss;
	protected furniture[] furnitures;
	protected List<Vector2> allocatedSpots;
	public GameObject []doors = new GameObject[4];
	public Sprite floor,walls;
	protected float chanceOfMonsterBecomingTainted;
	protected int chosenSpot = -1;
	protected int roomNumber;
	protected int currentMonsterCategory;
	protected GameController gameController;
	protected FurnitureStorage furnituresInTheGame;
	protected Transform cenarioLimits;
	protected StageController.StageNumber currentStage;
	protected int numberOfDucksInRoom, numberOfTarrasques;
	public enum roomtype
	{
		abandon,
		basic,
		central,
		baggins,
		labyrinth
	}
	public roomtype currentRoomType;
	public StageController.RoomCenario roomCenario;
	GDERoomData currentRoom;
	protected SkillController skillcontroller;
	// Use this for initialization
	void Awake () {
		gameController = GameController.Instance;	
		furnituresInTheGame = (FurnitureStorage) FindObjectOfType(typeof(FurnitureStorage));	
		allocatedSpots = new List<Vector2>();
		skillcontroller = (SkillController) FindObjectOfType(typeof(SkillController));
	}

	public void SetRoomNumber(int r)
	{        
		GDEDataManager.DataDictionary.TryGetCustom(roomCenario.ToString()+currentRoomType.ToString(), out currentRoom);
		furnitures = new furniture[currentRoom.FurniturePosition.Count];
		for (int i = 0; i < furnitures.Length; i++) {
			furnitures [i] = new furniture (currentRoom.FurniturePosition[i],furniture.GetTypeOfFurniture(currentRoom.FurnitureName[i]));
		}
		for (int i = 0; i < chaser.Length; i++) {
			chaser [i] = currentRoom.Chasers [i];
		}
		for (int i = 0; i < crowdcontrol.Length; i++) {
			crowdcontrol [i] = currentRoom.Crowds [i];
		}
		for (int i = 0; i < shooter.Length; i++) {
			shooter[i] = currentRoom.Shooters[i];
		}
		for (int i = 0; i < muscle.Length; i++) {
			muscle[i] = currentRoom.Muscles[i];
		}
		roomNumber = r;
		Transform fix = transform.FindChild("fixed").GetComponent<ObjectInstantiator>().StartNow();
		for (int i = 0; i < 4; i++) {			 
			doors[i] = fix.FindChild("door"+(i+1)).gameObject;
		}
		Transform parent = transform.FindChild ("furniture");
		for (int i = 0; i < furnitures.Length; i++) {
            GameObject temp = Instantiate(furnituresInTheGame.GetFurniturePrefab(furnitures[i].furnituretype),
                                          new Vector3(StageController.initialX + StageController.adjustX * (furnitures[i].position.x - 1),
                                            StageController.initialY - StageController.adjustY * (furnitures[i].position.y - 1), 0),
                                          Quaternion.identity) as GameObject;
            temp.transform.parent = parent;
		}
		fix.FindChild("floor").GetComponent<SpriteRenderer>().sprite = floor;
		fix.FindChild("walls").GetComponent<SpriteRenderer>().sprite = walls;
		cenarioLimits = fix.FindChild("CenarioLimits");
		for (int i = 0; i < furnitures.Length; i++) {
			AllocatedFurnitureSpot(i);
		}
	}

	protected void AllocatedFurnitureSpot(int i)
	{
		switch(furnitures[i].furnituretype)
		{
		default: //nao usa espacco (obstaculo nivel 1)
			break;
        //1x1
		case furniture.typeOfFurniture.torch:
		case furniture.typeOfFurniture.cristalball:
		case furniture.typeOfFurniture.chair:
		case furniture.typeOfFurniture.grave:
		case furniture.typeOfFurniture.ironchain:
		case furniture.typeOfFurniture.religiousstatue:
		case furniture.typeOfFurniture.stonecolumn:
		case furniture.typeOfFurniture.plantlabiryinth:
        case furniture.typeOfFurniture.barrel:
        case furniture.typeOfFurniture.box:
        case furniture.typeOfFurniture.table:
        case furniture.typeOfFurniture.pumpkin:
        case furniture.typeOfFurniture.straw:
        case furniture.typeOfFurniture.target:
        case furniture.typeOfFurniture.hallowen:
                allocatedSpots.Add(furnitures[i].position);
			break;
        //2x1
        case furniture.typeOfFurniture.bodiesH:
        case furniture.typeOfFurniture.bed:
        case furniture.typeOfFurniture.fenceH:
        case furniture.typeOfFurniture.tortureH:
        case furniture.typeOfFurniture.flowerH:
        case furniture.typeOfFurniture.pileH:
        case furniture.typeOfFurniture.groceryH:
        case furniture.typeOfFurniture.jailH:
        case furniture.typeOfFurniture.bonesH:
		case furniture.typeOfFurniture.woodchairH:
		case furniture.typeOfFurniture.stonealtarH:
			allocatedSpots.Add(furnitures[i].position);
			allocatedSpots.Add(new Vector2(furnitures[i].position.x+1,furnitures[i].position.y));
			break;
        //1x2
        case furniture.typeOfFurniture.bonesV:
		case furniture.typeOfFurniture.stonealtarV:
		case furniture.typeOfFurniture.woodchairV:
        case furniture.typeOfFurniture.bodiesV:            
        case furniture.typeOfFurniture.fenceV:
        case furniture.typeOfFurniture.tortureV:
        case furniture.typeOfFurniture.flowerV:
        case furniture.typeOfFurniture.pileV:
        case furniture.typeOfFurniture.groceryV:
        case furniture.typeOfFurniture.jailV:
                allocatedSpots.Add(furnitures[i].position);
			allocatedSpots.Add(new Vector2(furnitures[i].position.x,furnitures[i].position.y+1));
			break;
            //2x2	
        case furniture.typeOfFurniture.tree:
        case furniture.typeOfFurniture.fire:
        case furniture.typeOfFurniture.dinnertable:
        case furniture.typeOfFurniture.cask:
            allocatedSpots.Add(furnitures[i].position);
            allocatedSpots.Add(new Vector2(furnitures[i].position.x, furnitures[i].position.y + 1));
            allocatedSpots.Add(new Vector2(furnitures[i].position.x + 1, furnitures[i].position.y));
            allocatedSpots.Add(new Vector2(furnitures[i].position.x + 1, furnitures[i].position.y + 1));
                break;
        }
	}

	public int GetRoomNumber()
	{
		return roomNumber;
	}

	public void SetChanceOfMonsterBecomingTainted(int c)
	{
		chanceOfMonsterBecomingTainted = (float)c;
	}

	public float  GetChanceOfMonsterBecomingTainted(int c)
	{
		return chanceOfMonsterBecomingTainted;
	}

	public Transform GetCenarioLImits()
	{
		return cenarioLimits;
	}

	public void OpenDoors()
	{
		for (int i = 0; i < 4; i++) {
			doors[i].SetActive (true);
			doors[i].SendMessage("OpenDoor");
		}
	}

	public void LockDoors()
	{
		for (int i = 0; i < 4; i++) {
			doors[i].SetActive (false);
		}
	}

	public void SetDoor(StageController.door d)
	{
		doors[d.doorSide-1].SendMessage("SetDestinationRoom",d.doorDestinationRoom);
	}

	public void GenerateMonsters(RoomDifficulty difficultyConfiguration, StageController.StageNumber s)
	{
		currentStage = s;
		bool favorite, nonfavorite, plus1level, minusonelevel, onetainted;
		skillcontroller.ResetEZMode();
		skillcontroller.ResetRealDebuff();
		for (int i = 0; i < difficultyConfiguration.monsters.Length; i++) {
			favorite = false;
			nonfavorite = false;
			plus1level = false;
			minusonelevel = false;
			onetainted = false;
			for (int j = 0; j < difficultyConfiguration.monsters[i].variations.Length ; j++) {
				switch(difficultyConfiguration.monsters[i].variations[j])
				{
				case MonsterVariations.monstervariation.favorite:
					favorite = true;
					break;
				case MonsterVariations.monstervariation.nonfavorite:
					nonfavorite = true;
					break;
				case MonsterVariations.monstervariation.plus1level:
					plus1level = true;
					break;
				case MonsterVariations.monstervariation.minus1level:
					minusonelevel = true;
					break;
				case MonsterVariations.monstervariation.onetainted:
					onetainted = true;
					break;
				}
			}
			if(favorite)
			{
				GetCategoryFavorite(true);
			}
			else if(nonfavorite)
			{
				GetCategoryFavorite(false);
			}
			else
				currentMonsterCategory = Random.Range(1,5);//chaser,crowd,shooter,muscle
			int monsterNumberinArray = ChooseTheMonster ();
			GameObject monster = GetThisMonster(monsterNumberinArray,false);
			if(monster.GetComponent<MonsterGeneric>().GetMonsterType() == MonsterGeneric.monsterTypes.tarrasque && numberOfTarrasques != 0)
			{
				while(monster.GetComponent<MonsterGeneric>().GetMonsterType() == MonsterGeneric.monsterTypes.tarrasque)
				{
					monsterNumberinArray = ChooseTheMonster ();
					monster = GetThisMonster(monsterNumberinArray,false);
				}
			}
			int monsterQuantity = Random.Range (monster.GetComponent<MonsterGeneric>().monsterMin,monster.GetComponent<MonsterGeneric>().monsterMax+1);

			bool checkSkillEZMode = true;
			while(checkSkillEZMode)
			{
				monsterQuantity -= skillcontroller.GetEZMode ();
				if(monsterQuantity <= 1 || skillcontroller.GetEZModeUses() == 0)
					checkSkillEZMode = false;
			}

			for (int k = 0; k < monsterQuantity; k++) {
				int count = 0;
				bool loop = true;
				Vector2 spot = Vector2.one;
				if(monster.GetComponent<MonsterGeneric>().category == MonsterGeneric.monsterCategory.crowd2)
				{
					spot = FindMeARandomSpot();
				}
				else
				{
					while (loop)
					{
						chosenSpot = Random.Range (0,GetCategorySpotQuantity());
						spot = GetSpot();
						loop = false;
						for (int l = 0; l < allocatedSpots.Count; l++) {
							if(spot == allocatedSpots[l])
							{
								loop = true;
								continue;
							}
						}
						count++;
						if(count > 20)
						{
							spot = FindMeARandomSpot();
							loop = false;
						}
					}
				}
				allocatedSpots.Add (spot);
				Vector3 position = new Vector3(StageController.initialPos + StageController.adjustPos * (spot.y - 1),StageController.initialPos + StageController.adjustPos * (spot.x - 1),1.5f);
				GameObject temp;
				bool tainted = darkness || Random.value < (chanceOfMonsterBecomingTainted / 100) || onetainted;
				if(tainted)
				{
					GameObject monstertainted = GetThisMonster(monsterNumberinArray,tainted);
					temp = Instantiate(monstertainted,position,Quaternion.identity) as GameObject;
				}
				else
					temp = Instantiate(monster,position,Quaternion.identity) as GameObject;
				int tough = GameObject.Find ("StageController").GetComponent<StageController>().GetToughness();
				if((tough > 1 && !tainted) || tough > 2)
					tough -= skillcontroller.GetRealDebuff ();
				if(tainted)
					tough++;
				if(plus1level)
					tough++;
				if(minusonelevel)
					tough--;
				if(tough > 5)
					tough = 5;
				if(tough < 1)
					tough = 1;
				temp.SendMessage (MnstMsgs.SetToughness,tough);
				gameController.addMonster(temp);
				onetainted = false;
			}
		}
	}

	void GetCategoryFavorite(bool favoriteornot)
	{
		if(favoriteornot)
		{
			switch(currentRoomType)
			{
			case roomtype.abandon: //boas para shooter e muscle
			case roomtype.baggins:
			case roomtype.basic:
				currentMonsterCategory = Random.Range(3,5);
				break;
			case roomtype.central://boas para crowd e chaser
			case roomtype.labyrinth:
				currentMonsterCategory = Random.Range(1,3);
				break;
			}
		}
		else
		{
			switch(currentRoomType)
			{
			case roomtype.abandon: //boas para shooter e muscle
			case roomtype.baggins:
			case roomtype.basic:
				currentMonsterCategory = Random.Range(2,4);
				break;
			case roomtype.central://boas para crowd e chaser
			case roomtype.labyrinth:
				int random = Random.Range(0,2);
				currentMonsterCategory = random == 0 ? 1 : 4;
				break;
			}
		}
	}

	int GetCategorySpotQuantity()
	{
		switch(currentMonsterCategory)
		{
		default:
		case 1:
			return chaser.Length;
		case 2:
			return crowdcontrol.Length;
		case 3:
			return shooter.Length;
		case 4:
			return muscle.Length;
		case 5:
			return 1;
		}
	}

	GameObject GetThisMonster(int monsterNumber, bool tainted)
	{
		switch(currentMonsterCategory)
		{
		default:
		case 1:
			return gameController.GetChaser(monsterNumber, tainted);
		case 2:
			return gameController.GetCC(monsterNumber, tainted);
		case 3:
			return gameController.GetShooter(monsterNumber, tainted);
		case 4:
			return gameController.GetMuscle(monsterNumber, tainted);
		}
	}

	int ChooseTheMonster()
	{
		switch(currentMonsterCategory)
		{
		default:
		case 1:
			return Random.Range(0,gameController.monsterChaser.Length);
		case 2:
			return Random.Range(0,gameController.monsterCrowd.Length);
		case 3:
			return Random.Range(0,gameController.monsterShooter.Length);
		case 4:
			return Random.Range(0,gameController.monsterMuscle.Length);
		}
	}

	Vector2 GetSpot()
	{
		switch(currentMonsterCategory)
		{
		default:
		case 1:
			return new Vector2(chaser[chosenSpot].x,chaser[chosenSpot].y);
		case 2:
			return new Vector2(crowdcontrol[chosenSpot].x,crowdcontrol[chosenSpot].y);
		case 3:
			return new Vector2(shooter[chosenSpot].x,shooter[chosenSpot].y);
		case 4:
			return new Vector2(muscle[chosenSpot].x,muscle[chosenSpot].y);
		}
	}	 

	Vector2 FindMeARandomSpot()
	{
		Vector2 returnvalue = Vector2.one;
		bool loop = true;
		int i, j;
		int count = 256;
		while(loop)
		{
			i = Random.Range (1,17);
			j = Random.Range (1,17);
			loop = false;
			returnvalue = new Vector2(i,j);
			for (int k = 0; k < allocatedSpots.Count; k++) {
				if(returnvalue == allocatedSpots[k])
					loop = true;
			}
            //StageController.initialPos + StageController.adjustPos * (spot.y - 1),StageController.initialPos + StageController.adjustPos * (spot.x - 1)
            //if(Vector2.Distance (new Vector2(StageController.initialPos + StageController.adjustPos * (returnvalue.x - 1),
            //							StageController.initialPos + StageController.adjustPos * (returnvalue.y - 1)),
            if (Vector2.Distance(new Vector2(StageController.initialPos + StageController.adjustPos * (returnvalue.y - 1),
                                         StageController.initialPos + StageController.adjustPos * (returnvalue.x - 1)),
                                         GameObject.FindGameObjectWithTag("Player").transform.position) < GameController.minPlayerDistanceToSpawn)
            {
                loop = true;
            }
			count--;
			if(count<0)
				loop = false;
		}
		return returnvalue;
	}

	Vector2 FindMeANewSpot()
	{
		Vector2 returnvalue = Vector2.one;
		bool loop = true;
		int i, j;
		i = 1;
		j = 1;
		while(loop)
		{
			loop = false;
			returnvalue = new Vector2(i,j);
			for (int k = 0; k < allocatedSpots.Count; k++) {
				if(returnvalue == allocatedSpots[k])
					loop = true;
			}
			i++;
			if(i > 16)
			{
				i = 1;
				j++;
			}
			if(j > 16)
				loop = false;
		}
		return returnvalue;
	}

	public void IncreaseADuck()
	{
		numberOfDucksInRoom++;
	}

	public void DecreaseADuck()
	{
		numberOfDucksInRoom--;
	}

	public int GetNumberOfDucks()
	{
		return numberOfDucksInRoom;
	}

	public void IncreaseATarrasque()
	{
		numberOfTarrasques++;
	}
	
	public void DecreaseATarrasque()
	{
		numberOfTarrasques--;
	}
	
	public int GetNumberOfTarrasques()
	{
		return numberOfTarrasques;
	}
	bool darkness;
	public void ThereIsADarkness()
	{
		darkness = true;
	}

	public bool IsThereADarkness()
	{
		return darkness;
	}
}
