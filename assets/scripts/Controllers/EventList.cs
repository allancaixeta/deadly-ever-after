﻿using UnityEngine;
using System.Collections;

public class EventList : MonoBehaviour{

	public GameObject []listStageOneToThree;
	public GameObject []listStageFourToSix;
	public GameObject []listStageSevenToNine;
	public GameObject []listOfMiniBoss;
	public GameObject firstEvent;
	public GameObject GetNewEvent(EventController.eventlvl level, int roomNumber)
	{
		switch(level)
		{
		default:
		case EventController.eventlvl.one:
			if(roomNumber == 0)
				return firstEvent;			
			if(roomNumber == 5)
				return listOfMiniBoss[Random.Range(0,listOfMiniBoss.Length)];
			return listStageOneToThree[Random.Range(0,listStageOneToThree.Length)];
		case EventController.eventlvl.two:
			if(roomNumber == 5)
				return listOfMiniBoss[Random.Range(0,listOfMiniBoss.Length)];
			return listStageFourToSix[Random.Range(0,listStageFourToSix.Length)];
		case EventController.eventlvl.three:
			if(roomNumber == 5)
				return listOfMiniBoss[Random.Range(0,listOfMiniBoss.Length)];
			return listStageSevenToNine[Random.Range(0,listStageSevenToNine.Length)];
		}
	}

	public GameObject GetAMiniBoss()
	{
		return listOfMiniBoss[Random.Range(0,listOfMiniBoss.Length)];
	}
}
