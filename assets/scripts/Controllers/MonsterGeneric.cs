using UnityEngine;
using System.Collections;

public abstract class MonsterGeneric : MonoBehaviour, iMonster {
	public enum toughness
	{
		one=0,
		two=1,
		three=2,
		four=3,
		five=4
	}		
		
	public enum KnockbackType
	{
		light=0,
		medium=1,
		heavy=2,
		hulk=3
	}
	
	public enum directions{
		U=0,
		D=1,
		L=2,
		R=3,
		UR=4,
		UL=5,
		DR=6,
		DL=7
	}

	[Header("Monsters general")]

	public string monsterName;
	public monsterCategory category;
	public bool isTainted = false;
	public int monsterMin = 1;
	public int monsterMax = 3;
	public toughness monsterToughness = toughness.one;
	public string UniqueID = "Não mexa aqui";
	public float speed = 10;
	public int maxLife = 1;
	protected int currentLife;
	public float monsterDamage = 1;
	public AudioClip hitSound;
	protected MonsterStates currentState = MonsterStates.VOID;
	public MonsterStates nextState = MonsterStates.Start;
	protected MonsterStates lastState, previousState;
	protected MonsterStates saveState, saveStateBeforeStun;
	public KnockbackType knockbackType = KnockbackType.light;
	public float afterLife = 0.5f;
	public float desyncTimer = 0.2f;
	public float desyncTimeMin = 0;
	public float desyncChance = 0.001f;
	protected float currentDesync;
	public int lifeRegen;
	public float stopMovingIfCloserThenThisDistance = 0.25f;
	protected float lifeRegenTimer = 1;
	protected Vector2 velocity;
	protected Transform player;
	protected GameController gameController;
	protected SkillController skills;
	protected float timeDistortion = 1;//multiplique velocidade de andar e bater dos monstros por essa variavel, que pode deixar eles mais lentos
	protected float magicSlowEffect;
	protected bool magicallySlowed = false;
	protected float slowDuration;
	protected float bleeding = 0;//vulnerability
	protected Vector2 saveVelocity;
	protected Vector2 hookDirection;
	protected float currentStunTime;
	protected float animationDuration;	
	protected Vector2 hitDirection;
	protected GameObject floatingDamage;
	protected float attackCountDown, knockbackCountDown, deathCountDown;		
	public int armor = 0;
	public float armorCooldown = 0;
	protected float armorCurrentCooldown;
	protected bool armorON = true;
	protected CharacterController controller;
	protected bool facingRight = true;
	protected GameObject visual;
	public bool visualIsPerLevel;
	public GameObject [] visualsPrefab = new GameObject[5];
	public GameObject visualPrefab;
	Transform armorPosition;
	protected bool invulnerable = false;
	protected int originalLayer;
	protected Animator animator, armorAnimator;
	public enum monsterTypes
	{
		boss =0,
		separatepiece=1,
		projectile=2,
		spintop=3,
		pinocchio=4,
		hydra=5,
		hydraexplosion=6,
		beanstalk=7,
		firehairlessmule=8,
		puck=9,
		bat=10,
		egg=11,
		briar=12,
		connected=13,
		grimreaper=14,
		ciclops=15,
		pumpkin=16,
		porcelain=17,
		mandrake=18,
		swan=19,
		tarrasque=20,
		trent =21,
		ghost =22,
        grimreaperScythe = 23,
        dummy = 1000
    }

	public enum monsterCategory
	{
		chaser,
		crowd1,
		crowd2,
		shooter,
		muscle,
		miniboss
	}
	protected monsterTypes monsterType;

	protected float originalZ;

	public bool monsterIsScalableWithLevel = false;
	public int scalableMaxLife;
	public float scalableDamage,scalableSpeed, scalableLifeRegen, scalableArmor;
	public static float speedCap = 25;
	protected SpriteRenderer [] listOfAllSprites;
	Material [] materialStarting;
	float sizeGrowthByLevel = 0.05f;
	GameObject armorInstanciated;
    SpriteRenderer shadow;
	public const int deadLayer = 13;
	public const int giantLayer = 14;
	public const int nonHarmfulLayer = 21;
	public const int normalLayer = 11;
	public const int immaterialLayer = 5;
    public static readonly Color passiveShadow = new Color(0, 0, 0, 0.5f);
    public static readonly Color agressiveShadow = new Color(0.7f, 0, 0, 0.5f);

    protected int isPlayerPushing = 1; //0 when player is pushing, 1 when is not. Used to stop monster when player is pushing then.
	[Header("Specifics")]
	[SerializeField] protected HealthBehavior m_Health;


	// Use this for initialization
	void Awake () {
		gameObject.tag = Tags.Monster;
		gameController = GameController.Instance;
		skills = (SkillController) FindObjectOfType(typeof(SkillController));
		player = GameObject.FindGameObjectWithTag(Tags.Player).transform;
        floatingDamage = Resources.Load<GameObject>("UIdamage");
		controller = GetComponent<CharacterController>();
		originalZ = transform.position.z;
		originalLayer = gameObject.layer;
		currentLife = (int)(maxLife * skills.GetDebuff ());
		SpecificMonsterAwake();
		if(monsterType != monsterTypes.boss && monsterType != monsterTypes.separatepiece)
		{
			System.TimeSpan now;
			now = System.DateTime.Now - new System.DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
			UniqueID = monsterName + (now.TotalSeconds*100000).ToString();
		} else {
			UniqueID = gameObject.name;
		}
		gameObject.name = UniqueID;
		if(monsterType == monsterTypes.projectile) invulnerable = true;
		speed = speed * (1 - skills.GetAccel (monsterType == monsterTypes.projectile));
		armor = (int)((float)armor * skills.GetArmorless());
		transform.rotation = Quaternion.Euler (0, 0, 90);
	}



	protected void OnEnable() {
		if(m_Health) m_Health.RegisterToDeathEvent(OnDeath, false);
	}



	protected void OnDisable() {
		if(m_Health) m_Health.UnregisterFromDeathEvent(OnDeath);
	}


	
	public abstract void SpecificMonsterAwake ();
	
	public void UpdateMonsterGeneric() {
		if(!StartState())
			UpdateState();
	}

	//para testes de monstros usar o codigo abaixo
	void Start()
	{
		/*if(!visual)
		{
			SetToughness(monsterToughness);
		}*/
	}
	
	// Update is called once per frame
	public abstract bool StartState ();
	
	public abstract void UpdateState();	
	
	public void Pause()
	{
		nextState = MonsterStates.Inactive;
		saveState = currentState;
		animator.speed = 0;
		armorAnimator.speed = 0;
	}
	
	public void UnPause()
	{
		nextState = saveState;
		animator.speed = 1;
		armorAnimator.speed = 1;
	}
	
	public void SetUniqueID(string id)
	{
		UniqueID = id;	
	}
	
	public bool Hit(int damage)//usado por outros monstros, eventos, etc
	{
		if(m_Health != null) {
			return(HitWithHealthBehavior(damage));
		}
		if(invulnerable || CheckIfCurrentOrNextState(MonsterStates.Death) || currentLife < 0)
			return false;
		if(damage < 0)
			damage = 0;
		if(armorON)
		{
			armorCurrentCooldown = armorCooldown;
			if(armorCurrentCooldown > 0)			
			{	
				armorAnimator.SetBool("armorbreak",true);			
				armorON = false;
			}
			else
			{
				if(armorCurrentCooldown < 0)			
				{	
					if(armor > damage)
					{
						armor -= damage;
						UpdateArmorDestructible();
						return false;
					}
					else
					{
						damage -= 10 * armor;
						armor = 0;
						armorAnimator.SetBool("armorDestructible",false);			
						armorON = false;
						UpdateArmorDestructible();
					}
				}
			}
			damage -= 10 * armor;
			if(damage < 1)
				damage = 1;
		}
		damage = (int)(damage * (1 + bleeding));
		currentLife -= damage;
		if(damage > 0)
		{	
			GameObject temp = Instantiate(floatingDamage,GetMonsterCenter(),Quaternion.Euler (0,0,0)) as GameObject;	
			temp.SendMessage ("SetDamage",damage);
		}
		if(currentLife <= 0)
		{
			nextState = MonsterStates.Death;
			deathCountDown = 0.5f; // TODO : Get Rid of Magic Number
			gameObject.layer = 13; // TODO : Get Rid of Magic Number
			return true;
		}
		OnMonsterHit ();
		return false;
	}



	protected bool HitWithHealthBehavior(int damage) {
		if(invulnerable || CheckIfCurrentOrNextState(MonsterStates.Death) || m_Health.IsDead) {
			return false;
		}

		if(damage < 0) damage = 0;

		if(armorON) {
			armorCurrentCooldown = armorCooldown;
			if(armorCurrentCooldown > 0) {	
				armorAnimator.SetBool("armorbreak",true);			
				armorON = false;
			} else {
				if(armorCurrentCooldown < 0) {	
					if(armor > damage) {
						armor -= (int)damage;
						UpdateArmorDestructible();
						return false;
					} else {
						damage -= 10 * armor;
						armor = 0;
						armorAnimator.SetBool("armorDestructible",false);			
						armorON = false;
						UpdateArmorDestructible();
					}
				}
			}
			damage -= 10 * armor;
			if(damage < 1) damage = 1;
		}


		damage = (int)(damage * (1 + bleeding));
		if(damage > 0) {	
			GameObject temp = Instantiate(
				floatingDamage, GetMonsterCenter(),Quaternion.Euler (0,0,0)
			) as GameObject;	
			temp.SendMessage("SetDamage",damage);
		}

		if(m_Health.ApplyDamage(damage)) return true;

		OnMonsterHit();
		return false;
	}



	protected void OnDeath() {
		if(currentState != MonsterStates.Death && nextState != MonsterStates.Death) {
			nextState = MonsterStates.Death;
			deathCountDown = 0.5f; // TODO : Get Rid of Magic Number
			gameObject.layer = 13; // TODO : Get Rid of Magic Number
		}
	}



	WeaponController doublechanceLastWeapon;
	public bool Hit(int damage, bool IsWeaponCrush, WeaponController w)//usado pelo jogador
	{
		if(invulnerable || CheckIfCurrentOrNextState(MonsterStates.Death) || currentLife < 0)
        {
            if (monsterType == monsterTypes.connected)
            {
                if(!CheckIfCurrentOrNextState(MonsterStates.Attack))
                    nextState = MonsterStates.Death;
            }
            else
            {
                if (currentLife < 0 && !CheckIfCurrentOrNextState(MonsterStates.Death))
                    nextState = MonsterStates.Death;
            }
            return false;
        }			
		if(damage < 0)
			damage = 0;
		float accurate, knockback, burn, cold, lifesteal, dragonslayer, sneakattack, repel, freeze, sizematters, hpregeneration, poison, bleed,
		antiheavy, peacemaker, finalblow, fightingthebigguy, doublechance;
		int piercing;
		Vector2 knockbackPosition;
		if(IsWeaponCrush)
		{
			piercing = skills.GetPiercing ();
			accurate = skills.GetAccurate ();
			knockback = skills.GetKnockback () + w.knockBack;
			burn = skills.GetBurn ();
			cold = skills.GetCold ();
			lifesteal = skills.GetLifeSteal ();
			dragonslayer = skills.GetDragonslayer ();
			if(dragonslayer > 0)
				damage = (int)(damage * (1 + dragonslayer * Mathf.Sqrt(GetHittableArea())));
			antiheavy = skills.GetAntiheavy (armor);
			if(antiheavy > 0)
				damage = (int)(damage * (1 + antiheavy));
			peacemaker = skills.GetPeacemaker();
			if(peacemaker > 0 && GetComponent<Fury>())
				damage = (int)(damage * (1 + (peacemaker * GetComponent<Fury>().GetFuryPoints ())));
			finalblow = skills.GetFinalblow ();
			sneakattack = 0;
			repel = 0;
			freeze = 0;
			sizematters = 0;
			hpregeneration = 0;
			poison = 0;
			bleed = 0;
			fightingthebigguy = 0;
			doublechance = 0;
		}
		else
		{
			sneakattack = skills.GetSneakattack();
			repel = skills.GetRepel() + w.knockBack;
			freeze = skills.GetFreeze ();
			sizematters = skills.GetSizeMatters();
			if(sizematters > 0)
				damage = (int)(damage * (1 + sizematters / GetHittableArea()));
			hpregeneration = skills.GetHPRegeneration (name);
			if(hpregeneration > 0 && knockbackType == KnockbackType.light)
				player.GetComponent<PlayerController>().LifeRecover (hpregeneration);
			poison = skills.GetPoison();
			bleed = skills.GetBleed ();
			fightingthebigguy = skills.GetFightingthebigguy ();
			if(fightingthebigguy > 0 && category == monsterCategory.muscle)
				damage = (int)(damage * (1 + fightingthebigguy));
			doublechance = skills.GetDoublechance ();
			piercing = 0;
			accurate = 0;
			knockback =0;
			burn = 0;
			cold = 0;
			lifesteal = 0;
			dragonslayer = 0;
			antiheavy = 0;
			peacemaker = 0;
			finalblow = 0;
		}
		int tempArmor = armor;
		if(armorON && piercing == 0)
		{
			armorCurrentCooldown = armorCooldown;
			if(armorCurrentCooldown > 0)			
			{	
				armorAnimator.SetBool("armorbreak",true);			
				armorON = false;
			}
			else
			{
				if(armorCurrentCooldown < 0)			
				{	
					if(armor > damage)
					{
						armor -= damage;
						UpdateArmorDestructible();
						return false;
					}
					else
					{
						damage -= tempArmor;
						tempArmor = 0;
						armorAnimator.SetBool("armorDestructible",false);			
						armorON = false;
						UpdateArmorDestructible();
					}
				}
			}
			damage -= tempArmor;
			if(damage < 1)
				damage = 1;
		}
		damage = (int)(damage * (1 + bleeding));
		currentLife -= damage;
		if(damage > 0)
		{	
			GameObject temp = Instantiate(floatingDamage,GetMonsterCenter(),Quaternion.Euler (0,0,0)) as GameObject;	
			temp.SendMessage ("SetDamage",damage);
		}
		if(doublechance >= Random.value && !IsWeaponCrush && !doublechanceLastWeapon)
			doublechanceLastWeapon = w;
		else
			doublechanceLastWeapon = null;
		StartCoroutine(ChangeMaterialForHit ());
		if (currentLife <= 0 || ((float)currentLife/maxLife < finalblow && (monsterType != monsterTypes.boss && monsterType != monsterTypes.separatepiece))) {
			nextState = MonsterStates.Death;
			deathCountDown = 0.5f;
			gameObject.layer = 13;
			switch(monsterType)
			{
			case monsterTypes.connected:
				if(GetComponent<Connected>().IsBrotherDead ())
					return true;
				else
					return false;
			case monsterTypes.pumpkin:
				if(GetComponent<Pumpkin>().AmITheClone ())
					return false;
				else
					return true;
			default:
				return true;
			}
		}
		if(IsWeaponCrush)
		{
			if(accurate > 0)
			{
				Stun (accurate);
			}
			if(knockback > 0)
			{
				if(!CheckIfCurrentOrNextState(MonsterStates.Knockback) && knockbackType != KnockbackType.hulk)
				{
					if(CheckIfCurrentOrNextState(MonsterStates.Stun))
						saveState = MonsterStates.Stun;
					else
						saveState = currentState;
				}
				knockbackPosition = GetMonsterCenter()- w.transform.position;
				knockbackPosition.Normalize();
				Knockback(knockback,knockbackPosition);	
			}
			if(cold > 0)
				SlowDown (cold,3);
			if(lifesteal > 0)
				player.GetComponent<PlayerController>().LifeRecover (lifesteal * damage /10);
			if(burn > 0)
				skills.RegisterAsBurningMonster (this);
		}
		else
		{
			if(sneakattack > 0)
			{
				Stun (sneakattack);
			}
			if(repel > 0)
			{
				if(!CheckIfCurrentOrNextState(MonsterStates.Knockback) && knockbackType != KnockbackType.hulk)
				{
					if(CheckIfCurrentOrNextState(MonsterStates.Stun))
						saveState = MonsterStates.Stun;
					else
						saveState = currentState;
				}
				knockbackPosition = GetMonsterCenter () - player.position;
				knockbackPosition.Normalize();
				Knockback(repel,knockbackPosition);		
			}
			if(freeze > 0)
				SlowDown (freeze,2);
			if(poison > 0)
				skills.RegisterAsPoisonedMonster (this);
			if(bleed > 0)
				skills.RegisterAsBleedingMonster (this);
		}
		gameController.MonsterAudioSource.PlayOneShot(hitSound);
		OnMonsterHit ();
		return false;
	}

	protected abstract void OnMonsterHit ();

	public void Knockback(float knockback, Vector2 knockbackPosition)
	{		
		switch(knockbackType)
		{
		case KnockbackType.hulk:
			return;
		default:
			switch(currentState)
			{
			default:
				if(nextState!= MonsterStates.Hook)
				{
					velocity = knockbackPosition;
					knockbackCountDown = 0.2f;
					switch(knockbackType)
					{
					case KnockbackType.light:
						velocity *= GameController.deltaTime * knockback * (timeDistortion-magicSlowEffect);
						break;
					case KnockbackType.medium:
						velocity *= GameController.deltaTime * knockback * (timeDistortion-magicSlowEffect) * 0.85f;
						break;
					case KnockbackType.heavy:
						velocity *= GameController.deltaTime * knockback * (timeDistortion-magicSlowEffect) * 0.7f;
						break;
					}
					nextState = MonsterStates.Knockback;
				}
				else
					print ("devia dar erro");
				break;
			case MonsterStates.Hook:
			case MonsterStates.Death:
				break;
			/*case monsterStates.jump:
				velocity = velocity * ((100-5*knockback)/100);
				break;*/
			}
			break;
		}				
	}
	
	// Funcao chamada para fazer o monstro morrer
	public void DestroyItSelf()
	{
		if(magicallySlowed && slowdownObject)
			slowdownObject.MonsterDied(this);
		gameController.removeMonster(this.gameObject);
		Destroy(this.gameObject);
	}
		
	public void Hooked()
	{
		nextState = MonsterStates.Hook;
		gameObject.layer = 13;
	}
	
	public void UnHooked()
	{
		nextState = MonsterStates.Normal;
		gameObject.layer = originalLayer;
		transform.parent = null;
		GetOutOfObject();
	}

	protected void GetOutOfObject()
	{

		float radius = transform.GetComponent<CharacterController>().radius;
		Ray ray;
		RaycastHit [] hits;
		int layerMask = 136192;
		int attempt = 0;
		float area = 0.5f;

		bool outside = true;
		Vector3 temp = transform.position;
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		ray = new Ray(upTemp,temp - upTemp);
		hits = Physics.SphereCastAll(ray,radius,1000,layerMask);
		if(hits.Length == 0)
			outside= false;
		else
		{
			for (int i = 0; i < hits.Length; i++) {
				switch(hits[i].collider.transform.tag)
				{
				case Tags.Unmovable:
				case Tags.Lvl2Obstacle:
				case Tags.Lvl3Obstacle:
				case Tags.Wall:	
					outside = false;
					break;
				}
			}
		}
		while(!outside)
		{
			temp = new Vector3(transform.position.x + Random.Range (-area,area),transform.position.y + Random.Range (-area,area),0);
			upTemp = temp + new Vector3(0,0,-10);
			ray = new Ray(upTemp,temp - upTemp);
			hits = Physics.SphereCastAll(ray,radius,1000,layerMask);
			if(hits.Length != 0)
			{
				bool failed = false;
				for (int i = 0; i < hits.Length; i++) {
					switch(hits[i].collider.transform.tag)
					{
					case Tags.Unmovable:
					case Tags.Lvl2Obstacle:
					case Tags.Lvl3Obstacle:
					case Tags.Wall:	
						failed = true;
						break;
					}
				}
				if(!failed)
				{
					iTween.MoveTo(gameObject,new Vector3(temp.x,temp.y,originalZ),0.4f);
					return;
				}
			}
			attempt++;
			if(attempt > 5)
			{
				attempt = 0;
				area+=0.1f;
			}
		}
	}

	ParticleSystem [] arrayOfAllParticlesFromMonster;
	public void Stun(float stun)
	{
		switch(knockbackType)
		{
		case KnockbackType.light:
		case KnockbackType.medium:
		case KnockbackType.heavy:
			if(CheckIfCurrentOrNextState (MonsterStates.Death))
				return;
			if(!CheckIfCurrentOrNextState (MonsterStates.Stun))
			{
				if(CheckIfCurrentOrNextState (MonsterStates.Knockback))
				{
					if(saveState != MonsterStates.Stun)
						saveStateBeforeStun = saveState;
					saveState = MonsterStates.Stun;
				}
				else
				{
					nextState = MonsterStates.Stun;
					saveStateBeforeStun = currentState;
				}
			}
			if(currentStunTime <= 0)
			{
				animator.enabled = false;
				arrayOfAllParticlesFromMonster = transform.GetComponentsInChildren<ParticleSystem>();
				for (int i = 0; i < arrayOfAllParticlesFromMonster.Length; i++) {
					arrayOfAllParticlesFromMonster[i].Pause ();
				}
			}
			currentStunTime = stun;
			break;
		case KnockbackType.hulk:
			//TODO: error sound?
			break;
		}
	}

	public void RemoveStun()
	{
		currentState = saveStateBeforeStun;
		nextState = currentState;
		animator.enabled = true;
		currentStunTime = -1;
		if(arrayOfAllParticlesFromMonster != null)
		{
			for (int i = 0; i < arrayOfAllParticlesFromMonster.Length; i++) {
				if(arrayOfAllParticlesFromMonster[i])
					arrayOfAllParticlesFromMonster[i].Play ();
			}
		}
	}

	protected timebubble slowdownObject;

	public void SlowDown(float effect, float duration, timebubble t)
	{		
		if(knockbackType == KnockbackType.hulk)
		{
			//TODO: erro pois nao afeta mula sem cabeça, tocar som
			return;
		}
		this.magicSlowEffect = effect / 100;
		if(duration == -1)
			magicallySlowed = false;
		else
			magicallySlowed = true;
		slowDuration = duration;
		slowdownObject = t;
	}	

	public void SlowDown(float effect, float duration)
	{		
		if(knockbackType == KnockbackType.hulk)
		{
			//TODO: erro pois nao afeta mula sem cabeça, tocar som
			return;
		}
		if(this.magicSlowEffect < effect)
			this.magicSlowEffect += effect;
		if(this.magicSlowEffect > 0.9f)
			this.magicSlowEffect = 0.9f;
		if(duration == -1)
			magicallySlowed = false;
		else
			magicallySlowed = true;
		slowDuration = slowDuration < duration ? duration : slowDuration;	
	}	

	protected void Death()
	{
		deathCountDown -= GameController.deltaTime;
		if(deathCountDown < 0)
			DestroyItSelf();
	}
	
	protected void Stun()
	{
		currentStunTime -= GameController.deltaTime;
		if(currentStunTime < 0)
		{
			RemoveStun();
		}	
	}

	protected void Knock()
	{
		knockbackCountDown -= GameController.deltaTime;
		controller.Move(velocity);
		if(knockbackCountDown < 0)
		{
			currentState = saveState;
			nextState = currentState;
		}
	}

	public void Pushed(bool beingPushed)
	{
		if (beingPushed)
			isPlayerPushing = 0;
		else
			isPlayerPushing = 1;
	}

	public void Teleport(Vector2 pos)
	{
		if(knockbackType == KnockbackType.hulk || nextState == MonsterStates.Death || currentState == MonsterStates.Death)
			return;		
		transform.position = pos;
		nextState = MonsterStates.Normal;
	}

	public void TeleportOff()
	{
		if(knockbackType == KnockbackType.hulk || nextState == MonsterStates.Death || currentState == MonsterStates.Death)
			return;
		nextState = MonsterStates.Normal;
	}
	
	public void SetToughness(int level)
	{
		/*if(monsterType == monsterTypes.separatepiece)
		{
			if(monsterIsScalableWithLevel)
				ScaleMonster();
			return;
		}*/
		if(!visualIsPerLevel)
			visual = Instantiate (visualPrefab,transform.position,Quaternion.identity) as GameObject;
		else
			visual = Instantiate (visualsPrefab[level-1],transform.position,Quaternion.identity) as GameObject;
		visual.transform.parent = this.transform;
		visual.transform.localPosition = visualPrefab.transform.position;
		visual.transform.localScale = Vector3.one;
		visual.transform.rotation = transform.rotation;
		visual.name = "visual";
        //SpriteRenderer aux = visual.transform.Find("shadowcontrol/shadow").GetComponent<SpriteRenderer>();
        if (visual.transform.Find("shadowcontrol") != null && visual.transform.Find("shadowcontrol/shadow").GetComponent<SpriteRenderer>()!= null)
        {
            shadow = visual.transform.Find("shadowcontrol/shadow").GetComponent<SpriteRenderer>();
            shadow.material = Resources.Load<Material>("shadow");
            shadow.color = passiveShadow;
        }
		armorPosition = visual.transform.Find ("armorposition");
		listOfAllSprites = visual.gameObject.GetComponentsInChildren<SpriteRenderer>(true);
		animator = visual.GetComponent<Animator>();
		switch(monsterType)
		{
		default:
			InstantiateArmor ();
			switch(level)
			{
			case 1:
			default:
				monsterToughness = toughness.one;
				animator.SetTrigger("lvl1");
				toughnessInNumber = 1;			
				break;
			case 2:
				monsterToughness = toughness.two;
				animator.SetTrigger("lvl2");
				toughnessInNumber = 2;
				break;
			case 3:
				monsterToughness = toughness.three;
				animator.SetTrigger("lvl3");
				toughnessInNumber = 3;
				break;
			case 4:
				monsterToughness = toughness.four;
				animator.SetTrigger("lvl4");
				toughnessInNumber = 4;
				break;
			case 5:
				monsterToughness = toughness.five;
				toughnessInNumber = 5;
				break;
			}
			transform.localScale = new Vector3(transform.localScale.x * (1 + ((toughnessInNumber - 1) * sizeGrowthByLevel)),
			                                   transform.localScale.y * (1 + ((toughnessInNumber - 1) * sizeGrowthByLevel)),
			                                   transform.localScale.z);
			visual.transform.localPosition = visualPrefab.transform.position;
			GetAllMaterials();
			break;

		case monsterTypes.projectile:
			visual.transform.localScale = visualPrefab.transform.localScale;
            if(shadow)
                shadow.color = agressiveShadow;
			//visual.transform.localRotation = Quaternion.Euler (0,0,180);
			switch(level)
			{
			case 1:
			default:
				monsterToughness = toughness.one;
				toughnessInNumber = 1;			
				break;
			case 2:
				monsterToughness = toughness.two;
				toughnessInNumber = 2;
				break;
			case 3:
				monsterToughness = toughness.three;
				toughnessInNumber = 3;
				break;
			case 4:
				monsterToughness = toughness.four;
				toughnessInNumber = 4;
				break;
			case 5:
				monsterToughness = toughness.five;
				toughnessInNumber = 5;
				break;
			}
			break;

		case monsterTypes.hydraexplosion:		
			break;

		case monsterTypes.separatepiece:
			InstantiateArmor ();
			GetAllMaterials();
			break;

		case monsterTypes.boss:
			GetAllMaterials();
			break;
		}
		if(monsterIsScalableWithLevel)
			ScaleMonster();

		if(armor > 0)//start armor
		{
			if(armorCooldown == 0)
			{
				armorAnimator.SetBool ("armorInfinite",true);
				armorON = true;
				UpdateArmorInfinite ();
			}
			else
			{
				if(armorCooldown < 0)
				{
					armorAnimator.SetBool ("armorDestructible",true);
					armorON = true;
					UpdateArmorDestructible ();
				}
				else
				{
					armorON = true;
					armorAnimator.SetBool ("armorBreakable",true);
					UpdateArmorBreakable ();
				}
			}
		}
	}
	protected int toughnessInNumber = 1;
	public void SetToughness(toughness level)
	{
		monsterToughness = level;		
		switch(monsterToughness)
		{
		case toughness.one:
		default:
			SetToughness (1);
			break;
		case toughness.two:
			SetToughness (2);
			break;
		case toughness.three:
			SetToughness (3);
			break;
		case toughness.four:
			SetToughness (4);
			break;
		case toughness.five:
			SetToughness (5);
			break;
		}
	}

	void InstantiateArmor()
	{
		armorVisual = Instantiate(Resources.Load<GameObject>("ArmorVisual")) as GameObject;// visual.transform.Find ("base/armor").gameObject;
		armorVisual.transform.parent = armorPosition;
		armorVisual.transform.localPosition = Vector3.zero;
		armorAnimator = armorVisual.GetComponent<Animator>();
	}

	public monsterTypes GetMonsterType()
	{
		return monsterType;
	}

    public void SetMonsterType(monsterTypes m)
    {
        monsterType = m;
    }

    public monsterCategory GetMonsterCategory()
	{
		return category;
	}
	
	public void SetDamage(float d)
	{
		monsterDamage = d;
	}
	
	public void LifeRegen()
	{
		if(currentLife >= maxLife || lifeRegen <= 0)
			return;
		lifeRegenTimer -= GameController.deltaTime;
		if(lifeRegenTimer < 0)
		{
			lifeRegenTimer = 1;
			currentLife += lifeRegen;
			GameObject temp = Instantiate(floatingDamage,transform.position,Quaternion.Euler (0,0,0)) as GameObject;	
			temp.SendMessage ("SetRegen",lifeRegen);
			if(currentLife > maxLife)
				currentLife = maxLife;				
		}	
	}

	GameObject armorVisual;

	public void Armor()
	{
		if(armor > 0 && armorCurrentCooldown > 0)
		{
			armorCurrentCooldown -= GameController.deltaTime; 
			if (armorCurrentCooldown < 0) 
			{
				armorON = true;
				armorAnimator.SetBool ("armorbreak",false);
			}
		}
	}

	protected void UpdateArmorBreakable()
	{
		if(armor <= 0)
		{
			armorAnimator.SetBool ("armorBreakable",false);
			armorON = false;
		}
		else 
		{
			armorAnimator.SetBool ("armorBreakable", true);
			armorON = true;
			float alpha;
			if(armor> 500)
				alpha = 1;
			else
				alpha = 0.2f + (armor) * 0.016f;
			Color c = armorVisual.transform.Find ("armorBreakable").GetComponent<SpriteRenderer>().material.color;
			c.a = alpha;
			armorVisual.transform.Find ("armorBreakable").GetComponent<SpriteRenderer>().material.color = c;
			SpriteRenderer [] armorSpritesToUpdate = armorVisual.transform.Find ("broken").GetComponentsInChildren<SpriteRenderer>();
			for (int i = 0; i < armorSpritesToUpdate.Length; i++) {
				c = armorSpritesToUpdate[i].material.color;
				c.a = alpha;
				armorSpritesToUpdate[i].material.color = c;
			}
		}
	}
	
	protected void UpdateArmorInfinite()
	{
		if (armor <= 0) 
		{
			armorAnimator.SetBool ("armorInfinite", false);
			armorON = false;
		} 
		else 
		{
			armorAnimator.SetBool ("armorInfinite", true);
			armorON = true;
			float alpha;
			if(armor> 500)
				alpha = 1;
			else
				alpha = (armor) * 0.02f;
			Color c = armorVisual.transform.Find ("armorPermanent").GetComponent<ParticleSystem>().startColor;
			c.a = alpha;
			armorVisual.transform.Find ("armorPermanent").GetComponent<ParticleSystem>().startColor = c;
			float FXPlaySpeed;
			if(armor> 500)
				FXPlaySpeed = 2;
			else
				FXPlaySpeed = 0.3f + (armor) * 0.034f;
			ParticleSystem [] armorFxUpdate = armorVisual.transform.Find ("armorPermanent").GetComponentsInChildren<ParticleSystem>();
			for (int i = 0; i < armorFxUpdate.Length; i++) {
				armorFxUpdate[i].playbackSpeed = FXPlaySpeed;
			}
		}
	}
	
	protected void UpdateArmorDestructible()
	{
		if(armor <= 0)
		{
			armorAnimator.SetBool ("armorDestructible",false);
			armorON = false;
		}
		else 
		{
			armorAnimator.SetBool ("armorDestructible", true);
			armorON = true;
			float alpha;
			if(armor> 500)
				alpha = 1;
			else
				alpha = (armor) * 0.02f;
			Color c = armorVisual.transform.Find ("armorDestructible").GetComponent<ParticleSystem>().startColor;
			c.a = alpha;
			armorVisual.transform.Find ("armorDestructible").GetComponent<ParticleSystem>().startColor = c;
			c = armorVisual.transform.Find ("armorDestructible/CFX3 Aura").GetComponent<ParticleSystem>().startColor;
			c.a = alpha;
			armorVisual.transform.Find ("armorDestructible/CFX3 Aura").GetComponent<ParticleSystem>().startColor = c;
			float FXPlaySpeed;
			if(armor> 500)
				FXPlaySpeed = 2;
			else
				FXPlaySpeed = 0.3f + (armor) * 0.034f;
			ParticleSystem [] armorFxUpdate = armorVisual.transform.Find ("armorDestructible").GetComponentsInChildren<ParticleSystem>();
			for (int i = 0; i < armorFxUpdate.Length; i++) {
				armorFxUpdate[i].playbackSpeed = FXPlaySpeed;
			}
		}
	}

	public void Slow()
	{
		if(magicallySlowed)//como nao tem mta coisa que pode diminuir a velocidade de um monstro, todo o codigo de diminuicao de velocidade esta concentrado aqui
		{
			slowDuration -= GameController.deltaTime; 
			if(slowDuration < 0)
			{
				magicSlowEffect = 0;
				magicallySlowed = false;
			}
		}	
	}

	public void SetNextState(MonsterStates n)
	{
		nextState = n;
	}

	void ScaleMonster()
	{
		monsterDamage = monsterDamage + (scalableDamage * (toughnessInNumber - 1));
		maxLife = maxLife + scalableMaxLife * (toughnessInNumber - 1);
		currentLife = maxLife;
		speed += scalableSpeed * (toughnessInNumber - 1);
		lifeRegen = lifeRegen + (int)(scalableLifeRegen * (toughnessInNumber - 1));
		armor = armor + (int)(scalableArmor * (toughnessInNumber - 1));
	}

	public bool CheckIfCurrentOrNextState(MonsterStates m)
	{
		if (m == currentState || m == nextState)
			return true;
		return false;
	}

	// verifica se o Z mudou por algum motivo (acontece principalmente pq o charcontroller eh uma capsula e vira uma rampa as vezes)
	protected void FixZ() 
	{
		if(transform.position.z != originalZ)
			transform.position = new Vector3(transform.position.x,transform.position.y,originalZ);
	}

	protected void AbsorbLife()
	{
		if(currentLife == maxLife)
			return;
		GameObject temp = Instantiate(gameController.lifeAbsorb,GameObject.Find ("lifeAbsorbPosition").transform.position,Quaternion.Euler (0,0,0)) as GameObject;
		temp.GetComponent<GoToTarget> ().SetTarget (this.transform,2,this.transform,"RechargeLife");
		gameController.addUpdatableGameObject (temp);
	}

	public void RechargeLife()
	{
		if(currentLife == maxLife)
			return;
		GameObject temp = Instantiate(floatingDamage,transform.position+transform.right+transform.up,Quaternion.Euler (0,0,0)) as GameObject;
		temp.SendMessage ("SetRegen",GetLifeValueToReachMax());
		currentLife = maxLife;
	}

	public void SetInvulnerability(bool i)
	{
		invulnerable = i;
	}

	public float GetLifeValueToReachMax()
	{
		return maxLife - currentLife;
	}

	float GetHittableArea()
	{
		float area;
		if(GetComponent<SphereCollider> ())
			area = Mathf.PI * Mathf.Pow (GetComponent<SphereCollider> ().radius * transform.localScale.x,2);
		else
			if(GetComponent<BoxCollider> ())
				area = (GetComponent<BoxCollider> ().size.x * GetComponent<BoxCollider> ().size.y);
			else
				if(GetComponent<CapsuleCollider> ())
					area = Mathf.PI * Mathf.Pow (GetComponent<CapsuleCollider> ().radius * transform.localScale.x,2);
				else
					area = 1;
		return area;
	}

	public Vector3 GetMonsterCenter()
	{
		Vector3 center = Vector3.zero;
		if(GetComponent<SphereCollider> ())
			center = GetComponent<SphereCollider> ().center;
		else
			if(GetComponent<CapsuleCollider> ())
				center = GetComponent<CapsuleCollider> ().center;
		    else
			    if(GetComponent<BoxCollider> ())
				    center = GetComponent<BoxCollider> ().center;
        center = new Vector3(center.y * transform.localScale.y, -center.x * transform.localScale.x, -center.z);
        return transform.position - center;
	}

	public void Bleeding(float b)
	{
		bleeding = b;
	}

	public void GetAllMaterials()
	{
		materialStarting = new Material[listOfAllSprites.Length];
		for (int i = 0; i < listOfAllSprites.Length; i++) {
			materialStarting [i] = listOfAllSprites [i].material;
		}
	}

	public IEnumerator ChangeMaterialForHit()
	{
		for (int i = 0; i < listOfAllSprites.Length; i++) {
			listOfAllSprites[i].material = gameController.materialForHit;
		}
		yield return new WaitForSeconds(gameController.monsterBlinkTimeWhenHit);
		for (int i = 0; i < listOfAllSprites.Length; i++) {
			listOfAllSprites[i].material = materialStarting[i];
		}
		if(doublechanceLastWeapon)
			Hit (doublechanceLastWeapon.damage,doublechanceLastWeapon.IsWeaponCrush(),doublechanceLastWeapon);
		yield return null;
	}

	public float GetCooldownModifier()
	{
		return 1 + skills.GetCoffee ();
	}

	public void UpgradeMonster(float upgrade)
	{
		maxLife = (int)((float)maxLife * (1 + upgrade));
		currentLife = (int)(currentLife * (1 + upgrade));
		monsterDamage = monsterDamage * (1 + upgrade);
		transform.localScale = new Vector3(transform.localScale.x * (1 + upgrade),
		                                   transform.localScale.y * (1 + upgrade),
		                                   transform.localScale.z);

	}

	public void OneHitDeath()
	{
		if(CheckIfCurrentOrNextState (MonsterStates.Death) || monsterType == monsterTypes.tarrasque || monsterType == monsterTypes.grimreaperScythe)
			return;
		currentLife = -1;
		nextState = MonsterStates.Death;
		deathCountDown = 0.5f;
		gameObject.layer = 13;
	}

    public void Agressive()
    {
        if (shadow)
            shadow.color = agressiveShadow;
    }

    public void Passive()
    {
        if (shadow)
            shadow.color = passiveShadow;
    }

    /*IEnumerator WaitAndPrint() {

		print("WaitAndPrint " + Time.time);
	}*/

}
