using UnityEngine;
using System.Collections;
using System.Collections.Generic;


#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0649 // is never assigned to, and will always have its default value 

[Prefab("PlayerController")]
public class PlayerController : Singleton<PlayerController>
{

	public enum movimentationTests
	{
		stiffnonsense,
		acceleration,
		friction
	}

	public movimentationTests move = movimentationTests.stiffnonsense;
	Vector2 velocity;
	float normalizedHorizontalSpeed, normalizedVerticalSpeed;
	public float smoothedMovementFactor = 5;
	public float norm;
	public LerpEquationTypes movementLerp;
	public float accel,friction,maxSpeed;
	public enum playerControllerStates{
		VOID,
		normal,
		stun,
		pause
	}
	
	public enum playerMovimentationStates
	{
		walking,
		dodging,
		rooted,
		charging
	}

	[Range(0, 5)]
	public int strenght = 3;
	float baseStrenght = 0.1f;
	float strenghtIncrease = 0.1f;
	float originalBaseStrenght;
	[Range(0, 5)]
	public int speed = 3;
	public float baseSpeed = 15;
	float speedIncrease = 2;
	float originalBaseSpeed;
	[Range(0, 5)]
	public int cooldown = 3;
	float baseCooldown = 0;
	float cooldownIncrease = 0.05f;

	[Range(0, 5)]
	public int maxLife = 3;
	public const int lifeContainerSize = 13;
	float currentLife;
	float totalLife;
	public float blinkDuration = 1;
	public playerControllerStates nextState;
	public playerControllerStates saveState;
	public static float angle;
	float savedAngle;
	public AudioClip changeWeaponOK, changeWeaponError, weaponPickup, skillPickup, chargeSound, beingHitSound;
	public GameObject floatingDamage;
	public WeaponController.WeaponSpecial[] currentWeaponEquipped = new WeaponController.WeaponSpecial [2];
	public WeaponController.WeaponSpecial currentDefenseEquipped;
	GameObject slowFX;
	private bool movementUp, movementDown, movementLeft, movementRight;
	float originalZ;

	private playerControllerStates currentState = playerControllerStates.VOID;
	private int totalMovementDirectionsPressed; //number of keys pressed for walking
	private GameController gameController; 
	private SkillController skills;
	float mainSpeed, currentSpeed;
	float dodgeSpeed, chargeSpeed;
	private CursorController currentCursor; // Mouse Appearence and position on screen
	float[] currentAttackSpeed = new float[2];// current AttackSpeed for left 0 and right 1 hand
	float currentDefenseSpeed;
	private WeaponController [] weaponSlots = new WeaponController [3];
	private InterfaceController UI;
	private bool walkLockLeft, walkLockRight, walkLockUp, walkLockDown;
	CharacterController controller;
	TextMesh life;
	GameObject PlayerVisual;
	Animator animator;
	string [] HandID = new string [3]; 
	bool thereIsATarrasque;
	float timeDistortion = 0;
	float saveSpeed;
	float currentDodge;
	playerMovimentationStates movementState;
	PlayerHitController playerHit;
	Transform damageOutput;
	public Sprite ogreup,ogreside;
	Puppet2D_GlobalControl puppet2d;
	GameObject [] weaponInUse = new GameObject[3];
	int [] oneSecond = new int[3];
	float stunDuration;
	Transform directionLooking;
	int crushSide = 0;
	int AOESide = 1;
	int defenseSide = 2;
	int originalLayer, passThroughLayer; //layer normal e layer para passar por obstaculos
	PushController pushController;
	TargetAimController targetAim;
	//CRUSH////////////////////////////////////////////////////////////////////////////////////////////////

	//Move To Kill////////////////////////////////////////////////////////////////////////////////////////////////
	public enum movetokill
	{
		moving,
		ready
	}
	movetokill movetokillState = movetokill.ready;
	float distanceMovedToKill = 0;
	Vector3 lastPosition;

	//Reaping Hook////////////////////////////////////////////////////////////////////////////////////////////////
	public enum reapinghook
	{
		ready,
		cooldown
	}
	reapinghook reapinghookState = reapinghook.ready;
	public GameObject reapinghookVisualAid;
	GameObject currentreapinghookVisualAid;

	//Soul Charges////////////////////////////////////////////////////////////////////////////////////////////////
	public enum soulcharges
	{
		ready,
		cooldown
	}
	soulcharges soulchargesState = soulcharges.ready;
	int soulChargeCurrentHitCount = 0; 
	string soulChargeTargetName;
	//Threeshot////////////////////////////////////////////////////////////////////////////////////////////////
	enum threeshot
	{
		first,
		second,
		third
	}
	threeshot shotCount = threeshot.first;
	float timeToCooldown,threeshotErrorTime;
	int hitCount = 0;
	string threeshotMonsterBeingHittedName;

	//Home Missile////////////////////////////////////////////////////////////////////////////////////////////////
	public enum homemissile
	{
		ready,
		charge,
		cooldown
	}
	homemissile homemissileState = homemissile.ready;
	
	//Kendo////////////////////////////////////////////////////////////////////////////////////////////////
	public enum kendo
	{
		ready,
		charge,
		hold,
		cooldown
	}
	kendo kendoState = kendo.ready;
	float kendoCharge;

	//Mouse Breaker////////////////////////////////////////////////////////////////////////////////////////////////
	public enum mousebreaker
	{
		ready,
		going,
		hold,
		firing,
		cooldown
	}
	mousebreaker mousebreakerState = mousebreaker.ready;
	
	//Proton Cannon////////////////////////////////////////////////////////////////////////////////////////////////
	public enum protoncannon
	{
		ready,
		firing,
		cooldown
	}
	protoncannon protoncannonState = protoncannon.ready;
	
	//Void Ray////////////////////////////////////////////////////////////////////////////////////////////////
	public enum voidray
	{
		ready,
		firing,
		hold,
		cooldown
	}
	voidray voidrayState = voidray.ready;
	Transform spawnPos;

	//Sky Laser////////////////////////////////////////////////////////////////////////////////////////////////
	public enum skylaser
	{
		ready,
		charge,
		cooldown
	}
	skylaser skylaserState = skylaser.ready;


	//AOE////////////////////////////////////////////////////////////////////////////////////////////////

	//Boomerang////////////////////////////////////////////////////////////////////////////////////////////////
	public enum boomerang
	{
		ready,
		going,
		hold,
		returning,
		cooldown
	}
	boomerang boomerangState = boomerang.ready;

	//Death Rain////////////////////////////////////////////////////////////////////////////////////////////////
	public enum deathrain
	{
		ready,
		firing,
		cooldown
	}
	deathrain deathrainState = deathrain.ready;

	//Fire Wave////////////////////////////////////////////////////////////////////////////////////////////////
	public enum firewave
	{
		ready,
		waiting,
		cooldown
	}
	firewave firewaveState = firewave.ready;
	
	//Frag Grenade////////////////////////////////////////////////////////////////////////////////////////////////
	public enum fraggrenade
	{
		ready,
		cooldown
	}
	fraggrenade fraggrenadeState = fraggrenade.ready;

	//Shotgun////////////////////////////////////////////////////////////////////////////////////////////////
	public enum shotgun
	{
		ready,
		hold,
		waiting,
		cooldown
	}
	shotgun shotgunState = shotgun.ready;

	//Energy mines////////////////////////////////////////////////////////////////////////////////////////////////
	public enum energymines
	{
		firstmineofroom,
		deploy,
		cooldown
	}
	energymines energyminesState = energymines.firstmineofroom;

	//Chain Lightning////////////////////////////////////////////////////////////////////////////////////////////////
	public enum chainlightning
	{
		ready,
		waiting,
		cooldown
	}
	chainlightning chainlightningState = chainlightning.ready;

	//Frozen Orb////////////////////////////////////////////////////////////////////////////////////////////////
	public enum frozenorb
	{
		ready,
		hold,
		cooldown
	}
	frozenorb frozenorbState = frozenorb.ready;

	//Rumble ult////////////////////////////////////////////////////////////////////////////////////////////////
	public enum rumbleult
	{
		ready,
		hold,
		cooldown
	}
	rumbleult rumbleultState = rumbleult.ready;

	//Remote BomberMine////////////////////////////////////////////////////////////////////////////////////////////////
	public enum remotebombermine
	{
		firstmineofroom,
		deploy,
		cooldown
	}
	remotebombermine remotebombermineState = remotebombermine.firstmineofroom;

	//CONTROL////////////////////////////////////////////////////////////////////////////////////////////////

	//Disable Jumble////////////////////////////////////////////////////////////////////////////////////////////////
	public enum disablejumble
	{
		ready,
		cooldown
	}
	disablejumble disablejumbleState = disablejumble.ready;

	//Grab Chain////////////////////////////////////////////////////////////////////////////////////////////////
	public enum grabchain
	{
		ready,
		going,
		firing,
		cooldown
	}
	grabchain grabchainState = grabchain.ready;
	//bool chainReleased = false;
	bool chainToTheWall = false;
	//Jump////////////////////////////////////////////////////////////////////////////////////////////////
	public enum jump
	{
		ready,
		delay,
		going,
		cooldown
	}
	jump jumpState = jump.ready;
	Vector3 goalPosition, goalDirection;
	float lastDistanceToGoal;
	int currentChargingIsRight;

	//Portal Gun////////////////////////////////////////////////////////////////////////////////////////////////
	public enum portalgun
	{
		ready,
		going,
		cooldown
	}
	portalgun portalgunState = portalgun.ready;
	bool portalOnPlayer = false;

	//Time Bubble////////////////////////////////////////////////////////////////////////////////////////////////
	public enum timebubble
	{
		ready,
		firing,
		cooldown
	}
	timebubble timebubbleState = timebubble.ready;


	//Mega Dodge////////////////////////////////////////////////////////////////////////////////////////////////
	public enum megadodge
	{
		ready,
		delay,
		going,
		cooldown
	}
	megadodge megadodgeState = megadodge.ready;
	Vector2 lastMovementDirection;

	//Kake Bushin////////////////////////////////////////////////////////////////////////////////////////////////
	public enum kakebushin
	{
		ready,
		firing,
		cooldown
	}
	kakebushin kakebushinState = kakebushin.ready;


	void Start()
	{
		life = GameObject.Find("lifeBar").transform.FindChild("Text").GetComponent<TextMesh>();
		directionLooking = transform.FindChild ("directionLooking");
		spawnPos = transform.FindChild ("spawnpos");
		Vector3 temp = spawnPos.position;
		temp.z = GameController.ProjectilesPositionZ;
		spawnPos.position = temp;
		damageOutput = GameObject.Find("damageOutput").transform;
		controller = GetComponent<CharacterController>();
		PlayerVisual = transform.FindChild("PlayerVisual").gameObject;
		puppet2d = PlayerVisual.transform.FindChild ("Base").transform.FindChild ("Global_CTRL").GetComponent<Puppet2D_GlobalControl> ();
		animator = PlayerVisual.GetComponent<Animator> ();
		pushController = GetComponent<PushController> ();
		targetAim = transform.FindChild("TargetAim").GetComponent<TargetAimController> ();
		UI = InterfaceController.Instance;
		UI = InterfaceController.Instance;
		gameController = GameController.Instance;
		currentCursor = CursorController.Instance;
		skills = (SkillController) FindObjectOfType(typeof(SkillController));
		playerHit = PlayerHitController.Instance;
		slowFX = transform.Find("slowFX").gameObject;
		slowFX.SetActive (false);
		if(gameController==null){
			Debug.LogError("gameController Not found");
		}
		originalZ = transform.position.z;
		oneSecond[0] = 0;
		oneSecond[1] = 0;
		oneSecond [2] = 0;
		originalLayer = gameObject.layer;
		passThroughLayer = 1;
		originalBaseSpeed = baseSpeed;
		baseSpeed += speed * speedIncrease;
		mainSpeed = baseSpeed; 
		currentLife = (float)((maxLife + 1) * lifeContainerSize);
		totalLife = currentLife;
		life.text = currentLife.ToString ();
		baseCooldown = 1 - (float)cooldown * cooldownIncrease;
		originalBaseStrenght = baseStrenght;
		baseStrenght = baseStrenght + strenght * strenghtIncrease;
		transform.Rotate (0, 0, 90);
		nextState = playerControllerStates.normal;	
		UI.StartLife(maxLife);
	}
	

	public void Restart()
	{
		nextState = playerControllerStates.normal;
	}

	public void ChangeRoom(Vector3 doorPosition)
	{
		transform.position = new Vector3(doorPosition.x,doorPosition.y,transform.position.z);
		Reset ();
		skills.ResetHitenMitsurugi ();
		skills.ResetLiveandLearnShootsCount ();
		skills.ResetOnlyOncePerRoom ();
		skills.ResetTempHitCount ();
		skills.RollD20 ();
		skills.ResetHPRegeneration ();
	}

	// Update is called once per frame
	public void UpdatePlayerController() {
		if(!StartState())
			UpdateState();
	}
	
	bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
			case playerControllerStates.normal:			
				
				//PoolForWeaponEquipped();
				break;		
		}
		return true;
	}
	
	void UpdateState()
	{	
		
		pushController.UpdatePushController ();
		switch(currentState)
		{
		case playerControllerStates.normal:	
			ResolveHand (crushSide);
			ResolveHand (AOESide);
			ResolveDefense ();
			Interaction ();	
			Movement ();	
			Pause ();
			targetAim.UpdateTargetAimController ();
			if(transform.position.z != originalZ)
				transform.position = new Vector3(transform.position.x,transform.position.y,originalZ);
			break;
		
		case playerControllerStates.stun:
			stunDuration -= GameController.deltaTime;
			if(stunDuration < 0)
			{
				animator.enabled = true;
				nextState = playerControllerStates.normal;
			}
			break;
		}		
	}
	
	void ResolveHand(int isRight)
	{
		switch(currentWeaponEquipped[isRight])
		{
		case WeaponController.WeaponSpecial.threeshot:			
			ResolveThreeShot(isRight);
			break;
			
		case WeaponController.WeaponSpecial.boomerang:
			ResolveBoomerang(isRight);
			break;
			
		case WeaponController.WeaponSpecial.reapinghook:
			ResolveReapingHook(isRight);
			break;

		case WeaponController.WeaponSpecial.voidray:
			ResolveVoidRay(isRight);
			break;

		case WeaponController.WeaponSpecial.firewave:
			ResolveFireWave(isRight);
			break;

		case WeaponController.WeaponSpecial.fraggrenade:
			ResolveFragGrenade (isRight);
			break;

		case WeaponController.WeaponSpecial.chainlightning:
			ResolveChainLightning (isRight);
			break;

		case WeaponController.WeaponSpecial.mousebreaker:
			ResolveMouseBreaker(isRight);
			break;

		case WeaponController.WeaponSpecial.kendo:
			ResolveKendo(isRight);
			break;

		case WeaponController.WeaponSpecial.skylaser:
			ResolveSkyLaser(isRight);
			break;

		case WeaponController.WeaponSpecial.protoncannon:
			ResolveProtonCannon(isRight);
			break;

		case WeaponController.WeaponSpecial.soulcharges:
			ResolveSoulCharges(isRight);
			break;

		case WeaponController.WeaponSpecial.homemissile:
			ResolveHomeMissile(isRight);
			break;

		case WeaponController.WeaponSpecial.shotgun:
			ResolveShotgun(isRight);
			break;

		case WeaponController.WeaponSpecial.energymines:
			ResolveEnergyMines(isRight);
			break;

		case WeaponController.WeaponSpecial.frozenorb:
			ResolveFrozenOrb(isRight);
			break;

		case WeaponController.WeaponSpecial.rumbleult:
			ResolveRumbleUlt(isRight);
			break;

		case WeaponController.WeaponSpecial.remotebombermine:
			ResolveRemoteBomberMine(isRight);
			break;

		case WeaponController.WeaponSpecial.movetokill:
			ResolveMoveToKill(isRight);
			break;

		case WeaponController.WeaponSpecial.deathrain:
			ResolveDeathRain(isRight);
			break;
		}	
	}

	void ResolveDefense()
	{
		switch(currentDefenseEquipped)
		{
		case WeaponController.WeaponSpecial.grabchain:		
			ResolveGrabChain();
			break;

		case WeaponController.WeaponSpecial.megadodge:
			ResolveMegaDodge();
			break;			
			
		case WeaponController.WeaponSpecial.jump:
			ResolveJump ();
			break;
			
		case WeaponController.WeaponSpecial.timebubble:
			ResolveTimeBubble ();
			break;

		case WeaponController.WeaponSpecial.disablejumble:
			ResolveDisableJumble ();
			break;

		case WeaponController.WeaponSpecial.portalgun:
			ResolvePortalGun ();
			break;

		case WeaponController.WeaponSpecial.kakebushin:
			ResolveKakeBushin ();
			break;
		}


	}	
	
	//Le no teclado as teclas de movimentação
	void Movement()
	{
		slowTime -= GameController.deltaTime;
		if(slowTime < 0 && slowFX.activeSelf)
		{
			slowFX.SetActive (false);
			slowedValue = 0;
		}
		switch(movementState)
		{
		case playerMovimentationStates.walking:
			RotateChar();
			totalMovementDirectionsPressed = 0;
			movementLeft = false;
			movementDown = false;
			movementRight = false;
			movementUp = false;
			MovementInteraction();
			WalkResolve();
			//com 3 teclas pressionadas não se movimenta
			if(totalMovementDirectionsPressed < 3)
			{
				animator.SetBool ("walk",true);
			}
			if(totalMovementDirectionsPressed == 0)
			{
				animator.SetBool ("walk",false);
			}
			break;
			
		case playerMovimentationStates.dodging:
			controller.Move(new Vector3(lastMovementDirection.x * GameController.deltaTime,lastMovementDirection.y * GameController.deltaTime,0));
			if(lastMovementDirection.x < 0)
			{				
				puppet2d.flip = true;
			}
			else
				puppet2d.flip = false;
			break;
						
		case playerMovimentationStates.rooted:
			animator.SetBool ("walk",false);
			break;
			
		case playerMovimentationStates.charging:
			velocity = new Vector2(goalDirection.x * chargeSpeed * GameController.deltaTime, goalDirection.y * chargeSpeed * GameController.deltaTime).normalized;
			controller.Move(velocity);
			if(currentDefenseEquipped == WeaponController.WeaponSpecial.jump && Vector2.Distance (transform.position,goalPosition) < weaponSlots[defenseSide].LifeTime)
			{
				animator.SetBool("land",true);
			}		
			if(Vector2.Distance (transform.position,goalPosition) < 0.15f || Vector2.Distance (transform.position,goalPosition)>= lastDistanceToGoal)
				StopCharge();
			lastDistanceToGoal = Vector2.Distance (transform.position,goalPosition);
			break;
		}
	}

	void StopCharge()
	{
		movementState = playerMovimentationStates.walking;
		gameObject.layer = originalLayer;
		playerHit.GetComponent<Collider>().enabled = true;
		switch(currentDefenseEquipped)
		{
		case WeaponController.WeaponSpecial.jump:
			jumpState = jump.cooldown;
			currentDefenseSpeed = weaponSlots[defenseSide].CoolDown *  GetCooldownDefense();
			animator.SetBool("land",false);
			break;
		case WeaponController.WeaponSpecial.grabchain:
			animator.SetBool ("hook",false);
			weaponInUse[defenseSide].SendMessage("DestroyItSelf");
			break;
		}
	}

	//verificar se o jogador esta apertando algum dos botões de movimentação
	void MovementInteraction()
	{
		normalizedHorizontalSpeed = 0;
		normalizedVerticalSpeed = 0;
		if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
		{			
			if(walkLockLeft)
				movementLeft = false;
			else
			{
				totalMovementDirectionsPressed++;
				movementLeft = true;
				normalizedHorizontalSpeed = -1;
			}
		}
		if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
		{
			if(walkLockDown)
				movementDown = false;
			else
			{
				movementDown = true;
				totalMovementDirectionsPressed++;
				normalizedVerticalSpeed = 1;
			}			
		}
		if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
		{
			if(walkLockRight)
				movementRight = false;
			else
			{
				movementRight = true;
				totalMovementDirectionsPressed++;
				normalizedHorizontalSpeed = 1;
			}
		}
		if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
		{
			if(walkLockUp)
				movementUp = false;
			else
			{
				movementUp = true;
				totalMovementDirectionsPressed++;
				normalizedVerticalSpeed = -1;
			}			
		}
		switch(move)
		{
		case movimentationTests.stiffnonsense:
			currentSpeed = mainSpeed * GetHindrancesSpeed();
			velocity = new Vector2(normalizedVerticalSpeed,normalizedHorizontalSpeed);
			velocity = velocity.normalized * currentSpeed;
			break;
		case movimentationTests.acceleration:
			velocity.y = movementLerp.Lerp (velocity.x, normalizedHorizontalSpeed * mainSpeed * GetHindrancesSpeed(), GameController.deltaTime * smoothedMovementFactor );
			velocity.x = movementLerp.Lerp (velocity.y, normalizedVerticalSpeed * mainSpeed * GetHindrancesSpeed(), GameController.deltaTime * smoothedMovementFactor );
			if((movementUp || movementDown) && (movementRight || movementLeft))
				velocity *= norm;
			break;
		case movimentationTests.friction:	
			velocity = new Vector2(velocity.x + normalizedVerticalSpeed  * accel,velocity.y + normalizedHorizontalSpeed* accel);
			if(velocity.x > 0)
			{
				if(velocity.x - friction < 0)
					velocity.x = 0;
				else
					velocity.x -= friction;
			}
			else
			{
				if(velocity.x + friction > 0)
					velocity.x = 0;
				else
					velocity.x += friction;
			}
			if(velocity.y > 0)
			{
				if(velocity.y - friction < 0)
					velocity.y = 0;
				else
					velocity.y -= friction;
			}
			else
			{
				if(velocity.y + friction > 0)
					velocity.y = 0;
				else
					velocity.y += friction;
			}
			if(velocity.x > mainSpeed * GetHindrancesSpeed())
				velocity.x = mainSpeed * GetHindrancesSpeed();
			if(velocity.x < -mainSpeed * GetHindrancesSpeed())
				velocity.x = -mainSpeed * GetHindrancesSpeed();
			if(velocity.y > mainSpeed * GetHindrancesSpeed())
				velocity.y = mainSpeed * GetHindrancesSpeed();
			if(velocity.y < -mainSpeed * GetHindrancesSpeed())
				velocity.y = -mainSpeed * GetHindrancesSpeed();
			if((movementUp || movementDown) && (movementRight || movementLeft))
				velocity *= norm;
			break;
		}
	}

	//Resolve a movimentação mudando a posição do personagem usando o CharacterController
	void WalkResolve()
	{
		if (movementLeft && movementRight || movementUp && movementDown)
			return;
		controller.Move(velocity * GameController.deltaTime);
		velocity = controller.velocity;
		lastMovementDirection = velocity.normalized;
//		if(movementLeft)
//		{
//			if(totalMovementDirectionsPressed > 1)
//			{
//				if(movementRight) // left + right
//					return;
//				else
//				{
//					if(movementUp)	//diagonal left * up
//					{							
//						lastMovementDirection = new Vector2(-1,1);
//						return;
//					}
//					else //diagonal left + down
//					{						
//						lastMovementDirection = new Vector2(-1,-1);
//						return;
//					}
//				}
//			}
//			else//left 
//			{				
//				lastMovementDirection = new Vector2(-1,0);
//				return;
//			}
//		}
//		if(movementRight)
//		{
//			if(totalMovementDirectionsPressed > 1)
//			{
//				if(movementUp)	//diagonal right * up
//				{
//					lastMovementDirection = new Vector2(1,1);
//					return;
//				}
//				else //diagonal right + down
//				{
//					lastMovementDirection = new Vector2(1,-1);
//					return;
//				}
//			}
//			else //right
//			{
//				lastMovementDirection = new Vector2(1,0);
//				return;
//			}
//		}
//		if(movementUp) //up
//		{
//			if(totalMovementDirectionsPressed > 1)
//			{
//				return;
//			}
//			else
//			{
//				lastMovementDirection = new Vector2(0,1);
//				return;
//			}
//		}
//		if(movementDown) //down
//		{
//			lastMovementDirection = new Vector2(0,-1);
//			return;
//		}
	}

	void CheckInvert (Vector2 direction)
	{
		switch((int)direction.y)
		{
		case 1:
			if(!puppet2d.flip)
				animator.SetBool ("foward",true);
			else
				animator.SetBool ("foward",false);
			break;
		case -1:
			if (puppet2d.flip)
				animator.SetBool ("foward", true);
			else
				animator.SetBool ("foward", false);
			break;
		}
	}

	void DesalocateHand(int isRight)
	{
		gameController.removeWeapon(HandID[isRight]);
		currentAttackSpeed[isRight] = -1;			
		HandID[isRight] = "";
	}

	void Interaction()
	{
		/*if(Input.GetKeyDown(KeyCode.E))
		{
			gameController.PauseInventory();
			HandID[0] = "";
			HandID[1] = "";
			return;
		}*/
	}
			
	public void UpdateWeaponSlot(WeaponController[] newSlotList)
	{
		weaponSlots = newSlotList;	
	}
	
	public void UpdateSkill(SkillController currentSkill)
	{

	}

	void Pause()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			gameController.PauseGame();
			print ("Pause");
			return;
		}	
	}

	public float tempAngleCalc;
	// calcula a rotação do personagem
	void RotateChar()
	{
		//calcula o angulo do mouse pro jogador
		//tempAngleCalc = (-Mathf.Atan2((spawnPos.position.x - currentCursor.currentPosition.x), 
		 //                             (spawnPos.position.y - currentCursor.currentPosition.y)) * Mathf.Rad2Deg);	
		tempAngleCalc = Mathf.Atan2 (currentCursor.currentPosition.y - spawnPos.position.y, currentCursor.currentPosition.x - spawnPos.position.x) * Mathf.Rad2Deg;
		directionLooking.eulerAngles = new Vector3(0,0,tempAngleCalc);
		angle = tempAngleCalc;
		savedAngle = angle;
		if(angle < 0)//left
		{
			puppet2d.flip = true;
		}
		else//right
		{			
			puppet2d.flip = false;
		}
		CheckInvert(lastMovementDirection);
	}

	public WeaponController[] GetSlotList()
	{
		return weaponSlots;
	}
	
	void PlaySound(AudioClip clip, bool continous, int isRight)
	{
		if(continous)
		{
			if(isRight == 1)
			{
				gameController.RightWeaponAudioSource.clip = clip;
				gameController.RightWeaponAudioSource.loop = true;
				gameController.RightWeaponAudioSource.Play();
			}
			else
			{
				gameController.LeftWeaponAudioSource.clip = clip;
				gameController.LeftWeaponAudioSource.loop = true;
				gameController.LeftWeaponAudioSource.Play();
			}
		}
		else
		{
			if(isRight == 1)
				gameController.RightWeaponAudioSource.PlayOneShot(clip);
			else
				gameController.LeftWeaponAudioSource.PlayOneShot(clip);	
		}
	}
	
	void StopSound(int isRight)
	{
		if(isRight == 1)
		{
			gameController.RightWeaponAudioSource.Stop();
		}
		else
		{
			gameController.LeftWeaponAudioSource.Stop();
		}		
	}
	
	public void EnterTimeDistortion(float rate)
	{
		timeDistortion = rate / 100;
	}
	
	public void LeaveTimeDistortion()
	{
		timeDistortion = 0;
	}
	
	public void HitPlayer(float damage)
	{
		if(skills.UseTemp ())
			return;
		currentLife -= damage;
		if(currentLife <= 0)
		{
			Died();
		}
		else
		{
			UI.LoseLife(((int)currentLife/13),(int)currentLife%13);
			gameController.PlayerAudioSource.PlayOneShot(beingHitSound);
			GameObject temp = Instantiate(floatingDamage,damageOutput.position,Quaternion.identity) as GameObject;	
			temp.SendMessage ("SetDamage",damage);
			temp.SendMessage ("SetPlayerAsTarget",true);
			//life.text = currentLife.ToString();
		}
		skills.UseAdrenaline ();
		skills.StartOnlyOncePerRoom ();
	}
	int lastTimeTouchedAWall = 0;
	void OnControllerColliderHit(ControllerColliderHit hit) 
	{
		if(movementState == playerMovimentationStates.dodging)
		{
			if(lastTimeTouchedAWall > 0)
				return;
			if(Mathf.Abs (hit.moveDirection.x) > Mathf.Abs (hit.moveDirection.y))
			{
				lastMovementDirection.x = -lastMovementDirection.x;
				lastTimeTouchedAWall = 10;
			}
			else
			{
				lastMovementDirection.y = -lastMovementDirection.y;
				lastTimeTouchedAWall = 10;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{		
		switch(other.transform.tag)
		{
		case "Room":
			other.GetComponent<Renderer>().enabled = false;
			break;			
		}
	}
	int insideObjectCount = 0;
	void OnTriggerStay(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Unmovable:
		case Tags.Lvl2Obstacle:
		case Tags.Lvl3Obstacle:
		case Tags.Wall:	
			insideObjectCount++;
			if(insideObjectCount > 5)
			{
				insideObjectCount = 0;
				GetOutOfObject();
			}
			break;
		}
	}

	void GetOutOfObject()
	{        
        float radius = transform.GetComponent<CharacterController>().radius;
		Ray ray;
		RaycastHit [] hits;
		int layerMask = 136192;
		int attempt = 0;
		float area = 0.2f;
		bool outside = true;
		Vector3 temp = new Vector3(transform.position.x,transform.position.y,0);
		Vector3 upTemp = temp + new Vector3(0,0,-10);
		ray = new Ray(upTemp,temp - upTemp);
		hits = Physics.SphereCastAll(ray,radius,1000,layerMask);
		if(hits.Length == 0)
			outside= false;
		else
		{
			for (int i = 0; i < hits.Length; i++) {
				switch(hits[i].collider.transform.tag)
				{
				case Tags.Unmovable:
				case Tags.Lvl2Obstacle:
				case Tags.Lvl3Obstacle:
				case Tags.Wall:	
					outside = false;
					break;
				}
			}
		}
		while(!outside)
		{
			temp = new Vector3(transform.position.x + Random.Range (-area,area),transform.position.y + Random.Range (-area,area),0);
			upTemp = temp + new Vector3(0,0,-10);
			ray = new Ray(upTemp,temp - upTemp);
			hits = Physics.SphereCastAll(ray,radius,1000,layerMask);
			if(hits.Length != 0)
			{
				bool failed = false;;
				for (int i = 0; i < hits.Length; i++) {
					switch(hits[i].collider.transform.tag)
					{
					case Tags.Unmovable:
					case Tags.Lvl2Obstacle:
					case Tags.Lvl3Obstacle:
					case Tags.Wall:	
						failed = true;
						break;
					}
				}
				if(!failed)
				{
					iTween.MoveTo(gameObject,new Vector3(temp.x,temp.y,transform.position.z),0.4f);
					//print (area);
					return;
				}
			}
			attempt++;
			if(attempt > 5)
			{
				attempt = 0;
				area+=0.1f;
			}
		}
	}

	public bool isCurrentStateFight()
	{
		if(HandID[0] != "" || HandID[1] != "")
			return true;
		else
			return false;
	}		

	public playerMovimentationStates GetplayerMovimentationStates()
	{
		return movementState;	
	}
	
	public playerControllerStates GetplayerControllerStates()
	{
		return currentState;	
	}
	
	public Vector2 GetCurrentSpeed()
	{
		return velocity;
	}
	float slowedValue, slowTime, rainSlow;
	public float GetHindrancesSpeed()
	{
		float h = 1 - timeDistortion * skills.GetUnstoppable () - (slowTime > 0 ? slowedValue * skills.GetUnstoppable () : 0) - rainSlow * skills.GetUnstoppable ();
		return h < 0.1f ? 0.1f : h;
	}

	public void Raining(float r)
	{
		rainSlow = r;
	}

	public void SlowDown(float sv, float st)
	{
		if(slowTime < 0)
			slowFX.SetActive (true);
		if(st > slowTime)
			slowTime = st;
		if(sv > slowedValue)
			slowedValue = sv;
	}

	public void ResolveMoveToKill(int isRight)
	{
		switch(movetokillState)
		{
		case movetokill.moving:
			if(Vector2.Distance (lastPosition,transform.position) > weaponSlots[isRight].CoolDown)
			{
				distanceMovedToKill += Vector2.Distance (lastPosition,transform.position) * TarrasqueCooldownMultiplier();
				lastPosition = transform.position;
			}
			distanceMovedToKill += GetCooldownTickForThisFrame() * 0.1f;
			UI.InterfaceCharge(isRight,((weaponSlots[isRight].AttackSpeed * GetCooldownCrush()) - distanceMovedToKill) / (weaponSlots[isRight].AttackSpeed * GetCooldownCrush()));
			if(distanceMovedToKill > (weaponSlots[isRight].AttackSpeed * GetCooldownCrush()))
			{
				UI.InterfaceCharged(isRight);
				UI.InterfaceShootNow(isRight,1.1f);
				movetokillState = movetokill.ready;
				distanceMovedToKill = 0;
			}
			break;
		case movetokill.ready:
			distanceMovedToKill += Vector2.Distance (lastPosition,transform.position);
			lastPosition = transform.position;
			if(CheckForMouseButtonPress(isRight))
			{
				movetokillState = movetokill.moving;
				distanceMovedToKill = 0;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);	
				UI.FinishCooldown (isRight);
			}
			break;				
		}
	}

	void ResolveReapingHook(int isRight)
	{				
		currentAttackSpeed[isRight] -= reapinghookState == reapinghook.cooldown ? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(reapinghookState)
		{
		case reapinghook.ready:
			SetReapingHookVisualAidPosition(isRight);
			if(CheckForMouseButtonPress(isRight))
			{		
				reapinghookState = reapinghook.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownCrush();
				Destroy (currentreapinghookVisualAid);
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;		
		case reapinghook.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownCrush ()));
			if(currentAttackSpeed[isRight] < 0)
			{
				reapinghookState = reapinghook.ready;
				CreateReapingHookVisualAidPosition();
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public void ResolveSoulCharges(int isRight)
	{				
		currentAttackSpeed[isRight] -= soulchargesState == soulcharges.cooldown? GetCooldownTickForThisFrame() : GameController.deltaTime;		
		switch(soulchargesState)
		{
		case soulcharges.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				soulchargesState = soulcharges.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;
			
		case soulcharges.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight]);
			if(currentAttackSpeed[isRight] < 0)
			{
				soulchargesState = soulcharges.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public int GetandIncreaseSoulCount(string monsterName)
	{
		if (soulChargeTargetName == monsterName)
			soulChargeCurrentHitCount++;
		else
			ResetSoulChargeCount ();
		soulChargeTargetName = monsterName;
		return soulChargeCurrentHitCount;
	}

	public int GetSoulCount()
	{
		return soulChargeCurrentHitCount;
	}

	public void ResetSoulChargeCount()
	{
		soulChargeCurrentHitCount = 0;
		soulChargeTargetName = string.Empty;
	}

	void ResolveThreeShot(int isRight)
	{
		currentAttackSpeed[isRight] -= shotCount == threeshot.first? GetCooldownTickForThisFrame() : GameController.deltaTime;		
		switch(shotCount)
		{
		case threeshot.first:			
			if(currentAttackSpeed[isRight] < 0)
			{
				UI.FinishCooldown(isRight);
				if(CheckForMouseButtonPress(isRight))
				{			
					shotCount = threeshot.second;
					hitCount = 0;
					threeshotErrorTime = weaponSlots[isRight].auxVariable;
					currentAttackSpeed[isRight] = weaponSlots[isRight].AttackSpeed-threeshotErrorTime/2;
					weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
					UI.InterfaceTimingRing(currentAttackSpeed[isRight]);
				}
			}
			else
			{
				UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownCrush ()));
			}
			break;
		case threeshot.second:
			if(currentAttackSpeed[isRight] < 0)
			{
				UI.InterfaceShootNow(isRight,threeshotErrorTime+currentAttackSpeed[isRight]);
				if(currentAttackSpeed[isRight] < -threeshotErrorTime)
				{
					shotCount = threeshot.first;
					currentAttackSpeed[isRight] += weaponSlots[isRight].CoolDown * GetCooldownCrush ();
					UI.InterfaceTimingRingStop();
					return;
				}
				if(Input.GetMouseButtonDown(isRight))
				{
					shotCount = threeshot.third;
					currentAttackSpeed[isRight] = weaponSlots[isRight].AttackSpeed-threeshotErrorTime/2;
					InstantiateWeapon(isRight);
					weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
					UI.InterfaceTimingRing(currentAttackSpeed[isRight]);
				}
			}
			else
			{
				UI.InterfaceHold(isRight,currentAttackSpeed[isRight]);
				if(Input.GetMouseButtonDown(isRight))
				{
					//TODO: PlaySound(COMBO CANCELADO) e aumentar o cooldown na interface;
					shotCount = threeshot.first;
					currentAttackSpeed[isRight] += weaponSlots[isRight].CoolDown * GetCooldownCrush ();
					UI.InterfaceTimingRingStop();
					return;
				}
			}
			break;
		case threeshot.third:
			if(currentAttackSpeed[isRight] < 0)
			{
				UI.InterfaceShootNow(isRight,threeshotErrorTime+currentAttackSpeed[isRight]);
				if(currentAttackSpeed[isRight] < -threeshotErrorTime)
				{
					shotCount = threeshot.first;
					currentAttackSpeed[isRight] += weaponSlots[isRight].CoolDown * GetCooldownCrush ();
					UI.InterfaceTimingRingStop();
					return;
				}
				if(Input.GetMouseButtonDown(isRight))
				{
					shotCount = threeshot.first;
					currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownCrush ();
					InstantiateWeapon(isRight);
					if(hitCount == 2)
						weaponInUse[isRight].SendMessage("ArmThirdShot",threeshotMonsterBeingHittedName);
					weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				}
			}
			else
			{
				UI.InterfaceHold(isRight,currentAttackSpeed[isRight]);
				if(Input.GetMouseButtonDown(isRight))
				{
					//TODO: PlaySound(COMBO CANCELADO) e aumentar o cooldown na interface;
					shotCount = threeshot.first;
					currentAttackSpeed[isRight] += weaponSlots[isRight].CoolDown * GetCooldownCrush ();
					UI.InterfaceTimingRingStop();
					return;
				}
			}			
			break;
		}
	}

	float timeronstage = 0;
	
	void ResolveBoomerang(int isRight)
	{
		switch(boomerangState)
		{
		case boomerang.ready:			
			if(CheckForMouseButtonPress(isRight))
			{		
				boomerangState = boomerang.going;
				currentAttackSpeed[isRight] = weaponSlots[isRight].LifeTime;
				timeronstage = 0;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				UI.UpdateCooldown(isRight,0);
			}
			break;
		case boomerang.going:
			currentAttackSpeed[isRight] -= GameController.deltaTime;
			timeronstage += GameController.deltaTime;
			if(currentAttackSpeed[isRight] < 0)
			{
				currentAttackSpeed[isRight] = weaponSlots[isRight].AttackSpeed;
				oneSecond[isRight] = (int)currentAttackSpeed[isRight];
				ReturnBoomerang(isRight);
			}
			break;
		case boomerang.returning:	
			if(currentAttackSpeed[isRight] > 0)
			{
				currentAttackSpeed[isRight] -= GameController.deltaTime;
				UI.InterfaceBoomerangGoing(isRight,currentAttackSpeed[isRight]);
				if(currentAttackSpeed[isRight] < 0 && weaponInUse[isRight])
					weaponInUse[isRight].SendMessage("BoomerangReturn");
			}
			else
			{
				if(!weaponInUse[isRight])
				{
					currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE ();
					boomerangState = boomerang.cooldown;
				}
			}		
			timeronstage += GameController.deltaTime;
			break;
		case boomerang.cooldown: //usado somente para resolver o caso do Puck engolir projetil
			currentAttackSpeed[isRight] -= GetCooldownTickForThisFrame();
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				boomerangState = boomerang.ready;
				UI.FinishCooldown(isRight);
				//print (timeronstage);
			}
			break;
		}
	}
	
	void ReturnBoomerang(int isRight)
	{
		boomerangState = boomerang.returning;
		weaponInUse[isRight].SendMessage("BoomerangReturn");
		currentAttackSpeed[isRight] = -1;
	}

	public void ResolveDeathRain(int isRight)
	{
		switch(deathrainState)
		{
		case deathrain.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				deathrainState = deathrain.firing;
				weaponInUse[isRight].transform.rotation = Quaternion.identity;
				currentAttackSpeed[isRight] = weaponSlots[isRight].LifeTime;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				UI.UpdateCooldown(isRight,0);
			}
			break;	
		case deathrain.firing:
			currentAttackSpeed[isRight] -= GameController.deltaTime;
			if(currentAttackSpeed[isRight] < 0)
			{
				deathrainState = deathrain.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE ();
			}
			break;
		case deathrain.cooldown:
			currentAttackSpeed[isRight] -= GetCooldownTickForThisFrame();
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				deathrainState = deathrain.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	void ResolveFireWave(int isRight)
	{
		timeronstage += GameController.deltaTime;				
		switch(firewaveState)
		{
		case firewave.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				firewaveState = firewave.waiting;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE ();
				timeronstage = 0;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				UI.UpdateCooldown(isRight,0);
			}
			break;	
		case firewave.waiting:
			break;
		case firewave.cooldown:
			currentAttackSpeed[isRight] -= GetCooldownTickForThisFrame();
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				firewaveState = firewave.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}	
	}

	public void FireWaveOver(int isRight)
	{
		firewaveState = firewave.cooldown;
		currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
		//print (timeronstage);
	}
		
	void ResolveFragGrenade(int isRight)
	{
		currentAttackSpeed[isRight] -= fraggrenadeState == fraggrenade.cooldown ? GetCooldownTickForThisFrame() : GameController.deltaTime;		
		switch(fraggrenadeState)
		{
		case fraggrenade.ready:
			if(CheckForMouseButtonPress(isRight))
			{
				fraggrenadeState = fraggrenade.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				UI.UpdateCooldown(isRight,0);
			}
			break;	
		case fraggrenade.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				fraggrenadeState = fraggrenade.ready;
				UI.FinishCooldown(isRight);
				//print (timeronstage);
			}
			break;
		}	
	}

	public void ResolveShotgun(int isRight)
	{
		currentAttackSpeed[isRight] -= shotgunState == shotgun.cooldown? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(shotgunState)
		{
		case shotgun.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				shotgunState = shotgun.hold;
				currentAttackSpeed[isRight] = 1;
				oneSecond[isRight] = 1;
				timeronstage =0;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;		
		case shotgun.hold:
			if(Input.GetMouseButtonUp(isRight))
			{
				weaponInUse[isRight].SendMessage("StartShotgun");
				shotgunState = shotgun.waiting;
				UI.UpdateCooldown(isRight,0);
			}
			timeronstage += GameController.deltaTime;
			break;
		case shotgun.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				shotgunState = shotgun.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public void StopShotgun(float cooldown)
	{
		currentAttackSpeed[AOESide] = cooldown * GetCooldownAOE();
		shotgunState = shotgun.cooldown;
	}

	public void ResolveEnergyMines(int isRight)
	{
		currentAttackSpeed[isRight] -= energyminesState == energymines.cooldown? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(energyminesState)
		{
		case energymines.firstmineofroom:
			if(CheckForMouseButtonPress(isRight))
			{		
				energyminesState = energymines.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
				oneSecond[isRight] = 1;
				UI.UpdateCooldown(isRight,0);
				timeronstage =0;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				gameController.OnPlayerShooting (weaponInUse[isRight]);
			}
			break;		
		case energymines.deploy:
			if(Input.GetMouseButton(isRight))
			{
				weaponInUse[isRight].SendMessage("DeployMine");
				energyminesState = energymines.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
				UI.UpdateCooldown(isRight,0);
				if(Random.value < weaponSlots[isRight].AttackSpeed)
					gameController.OnPlayerShooting (weaponInUse[isRight]);
			}
			timeronstage += GameController.deltaTime;
			break;
		case energymines.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				energyminesState = energymines.deploy;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}
	int bombNumber;
	public void ResolveRemoteBomberMine(int isRight)
	{
		currentAttackSpeed[isRight] -= remotebombermineState == remotebombermine.cooldown? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(remotebombermineState)
		{
		case remotebombermine.firstmineofroom:
			if(CheckForMouseButtonPress(isRight))
			{		
				remotebombermineState = remotebombermine.cooldown;
				oneSecond[isRight] = 1;
				UI.UpdateCooldown(isRight,0);
				timeronstage =0;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				bombNumber++;
			}
			break;		
		case remotebombermine.deploy:
			if(Input.GetMouseButtonDown(isRight))
			{
				if(bombNumber == weaponSlots[isRight].ProjectileSpeed)
				{
					weaponInUse[isRight].SendMessage("ExplodeMines");
					remotebombermineState = remotebombermine.cooldown;
					bombNumber = 0;
					currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
					UI.UpdateCooldown(isRight,0);
					gameController.OnPlayerShooting (weaponInUse[isRight]);
				}
				else
				{
					weaponInUse[isRight].SendMessage("DeployMine");
					bombNumber++;
				}
			}
			timeronstage += GameController.deltaTime;
			break;
		case remotebombermine.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				remotebombermineState = remotebombermine.deploy;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	void ResolveChainLightning(int isRight)
	{
		currentAttackSpeed[isRight] -= chainlightningState == chainlightning.cooldown ? GetCooldownTickForThisFrame() : GameController.deltaTime;		
		switch(chainlightningState)
		{
		case chainlightning.ready:
			if(CheckForMouseButtonPress(isRight))
			{
				chainlightningState = chainlightning.waiting;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
				UI.UpdateCooldown(isRight,0);
			}
			break;	
		case chainlightning.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				chainlightningState = chainlightning.ready;
				UI.FinishCooldown(isRight);
				//print (timeronstage);
			}
			break;
		}	
	}

	public void ChainLightningFinished()
	{
		chainlightningState = chainlightning.cooldown;
		currentAttackSpeed[AOESide] = weaponSlots[AOESide].CoolDown * GetCooldownAOE();
	}

	public void ResolveFrozenOrb(int isRight)
	{
		currentAttackSpeed[isRight] -= frozenorbState == frozenorb.cooldown? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(frozenorbState)
		{
		case frozenorb.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				frozenorbState = frozenorb.hold;
				currentAttackSpeed[isRight] = weaponSlots[isRight].LifeTime;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;		
		case frozenorb.hold:
			if(currentAttackSpeed[isRight] < 0)
			{
				frozenorbState = frozenorb.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
				UI.UpdateCooldown(isRight,0);
			}
			timeronstage += GameController.deltaTime;
			break;
		case frozenorb.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				frozenorbState = frozenorb.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public void ResolveRumbleUlt(int isRight)
	{
		currentAttackSpeed[isRight] -= rumbleultState == rumbleult.cooldown? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(rumbleultState)
		{
		case rumbleult.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				rumbleultState = rumbleult.hold;
				currentAttackSpeed[isRight] = weaponSlots[isRight].auxVariable;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;		
		case rumbleult.hold:
			if(Input.GetMouseButtonUp(isRight) || currentAttackSpeed[isRight] < 0)
			{
				weaponInUse[isRight].SendMessage("ActivateRumbleUlt");
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE();
				rumbleultState = rumbleult.cooldown;
				UI.UpdateCooldown(isRight,0);
			}
			timeronstage += GameController.deltaTime;
			break;
		case rumbleult.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight] / (weaponSlots[isRight].CoolDown * GetCooldownAOE()));
			if(currentAttackSpeed[isRight] < 0)
			{
				rumbleultState = rumbleult.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	void ResolveDisableJumble()
	{
		currentDefenseSpeed -= disablejumbleState == disablejumble.cooldown? GetCooldownTickForThisFrameDefenseSkill () : GameController.deltaTime;		
		switch(disablejumbleState)
		{
		case disablejumble.ready:
			if(CheckForDefenseButtonPress())
			{		
				disablejumbleState = disablejumble.cooldown;
				currentDefenseSpeed = weaponSlots[defenseSide].CoolDown * GetCooldownDefense();
				weaponInUse[defenseSide].SendMessage("DisableJumbleOn",currentCursor.currentFocusGameObject);
			}
			break;	
		case disablejumble.cooldown:
			UI.UpdateCooldown(defenseSide,currentDefenseSpeed / (weaponSlots[defenseSide].CoolDown * GetCooldownDefense()));
			if(currentDefenseSpeed < 0)
			{
				disablejumbleState = disablejumble.ready;
				UI.FinishCooldown(defenseSide);
				HandID[defenseSide] = "";
			}
			break;
		}	
	}

	public void UnrootPlayer()
	{
		if(portalOnPlayer)
			return;
		movementState = playerMovimentationStates.walking;
	}
	
	public void ResolveGrabChain()
	{				
		currentDefenseSpeed -= grabchainState == grabchain.cooldown ? GetCooldownTickForThisFrameDefenseSkill () : GameController.deltaTime;
		switch(grabchainState)
		{
		case grabchain.ready:
			if(CheckForDefenseButtonPress())
			{		
				grabchainState = grabchain.going;
				//chainReleased = false;
				chainToTheWall = false;
			}
			break;
		case grabchain.going:
			break;
		/*case grabchain.hold:
			if(Input.GetButtonUp("Defense"))
			{
				chainReleased = true;
			}
			if(chainReleased)
			{
				ReleaseChain();
			}
			UI.InterfaceGrabChain (currentDefenseSpeed);
			if(currentDefenseSpeed < 0)
			{
				chainReleased = true;
			}
			break;*/
		case grabchain.firing:
			break;
		case grabchain.cooldown:
			UI.UpdateCooldown(defenseSide,currentDefenseSpeed / (weaponSlots[defenseSide].CoolDown * GetCooldownDefense()));
			if(currentDefenseSpeed < 0)
			{
				grabchainState = grabchain.ready;
				UI.FinishCooldown(defenseSide);
			}
			break;
		}
	}
	
	void ReleaseChain()
	{
		grabchainState = grabchain.firing;
		weaponInUse[defenseSide].SendMessage ("ActivateChain");
		if(chainToTheWall)
		{
			movementState = playerMovimentationStates.charging;
			animator.SetBool ("hook",true);
			chargeSpeed = weaponSlots[defenseSide].LifeTime;

			goalPosition = weaponInUse[defenseSide].transform.position;
			goalPosition.z = transform.position.z;
			goalDirection = goalPosition - transform.position;
			goalDirection.Normalize ();

			int layerMask = 132096;
			Vector3 upTemp = goalPosition + new Vector3(0,0,-100);
			Ray ray = new Ray(upTemp,goalPosition - upTemp);
			Debug.DrawRay(upTemp,goalPosition - upTemp,Color.white,5);
			float radius = controller.radius;
			while(Physics.SphereCast(ray,radius,1000,layerMask))
			{
				//print ();
				goalPosition -= goalDirection; 
				upTemp = goalPosition + new Vector3(0,0,-100);
				ray = new Ray(upTemp,goalPosition - upTemp);
				Debug.DrawRay(upTemp,goalPosition - upTemp,Color.white,5);
			}	
			lastDistanceToGoal = Vector2.Distance (transform.position,goalPosition);
			if(goalPosition.x > transform.position.x)
				puppet2d.flip = false;
			else
				puppet2d.flip = true;

			gameObject.layer = passThroughLayer;
			playerHit.GetComponent<Collider>().enabled = false;
		}
	}
	
	public void GrabChainMonster()
	{
		//grabchainState = grabchain.hold;
		currentDefenseSpeed = weaponSlots[defenseSide].AttackSpeed;
		ReleaseChain ();
	}
	
	public void GrabChainWall()
	{
		chainToTheWall = true;
		//grabchainState = grabchain.hold;
		currentDefenseSpeed = weaponSlots[defenseSide].AttackSpeed;
		ReleaseChain ();
	}
	
	public void ChainCanceled()
	{
		grabchainState = grabchain.cooldown;
		currentDefenseSpeed = weaponSlots[defenseSide].CoolDown * GetCooldownDefense();
	}

	public void ResolveMegaDodge()
	{
		currentDefenseSpeed -= megadodgeState == megadodge.cooldown ? GetCooldownTickForThisFrameDefenseSkill () : GameController.deltaTime;
		switch(megadodgeState)
		{
		case megadodge.ready:
			if(CheckForDefenseButtonPress())
			{		
				megadodgeState = megadodge.delay;
				currentDefenseSpeed = weaponSlots[defenseSide].AttackSpeed;
				animator.SetTrigger("dodge");
			}
			break;
		case megadodge.delay:
			if(currentDefenseSpeed < 0)
			{
				megadodgeState = megadodge.going;
				movementState = playerMovimentationStates.dodging;
				dodgeSpeed = weaponSlots[defenseSide].ProjectileSpeed;
				currentDefenseSpeed = weaponSlots[defenseSide].LifeTime;
				gameObject.layer = passThroughLayer;
				playerHit.GetComponent<Collider>().enabled = false;
				lastMovementDirection.Normalize ();
				lastMovementDirection.x = dodgeSpeed * lastMovementDirection.x;
				lastMovementDirection.y = dodgeSpeed * lastMovementDirection.y;
			}
			break;
		case megadodge.going:
			lastTimeTouchedAWall--;
			if(currentDefenseSpeed < 0 && !CheckCollisionWithFurniture())
			{
				megadodgeState = megadodge.cooldown;
				movementState = playerMovimentationStates.walking;
				currentDefenseSpeed = weaponSlots[defenseSide].CoolDown * GetCooldownDefense ();
				dodgeSpeed = 1;
				gameObject.layer = originalLayer;
				playerHit.GetComponent<Collider>().enabled = true;
			}
			break;		
		case megadodge.cooldown:
			UI.UpdateCooldown(defenseSide,currentDefenseSpeed / (weaponSlots[defenseSide].CoolDown * GetCooldownDefense()));
			if(currentDefenseSpeed < 0)
			{
				megadodgeState = megadodge.ready;
				UI.FinishCooldown(defenseSide);
			}
			break;
		}
	}

	public void ResolveJump()
	{				
		currentDefenseSpeed -=jumpState == jump.cooldown ? GetCooldownTickForThisFrameDefenseSkill () : GameController.deltaTime;
		switch(jumpState)
		{
		case jump.ready:
			if(CheckForDefenseButtonJump())
			{		
				jumpState = jump.delay;
				currentDefenseSpeed = weaponSlots[defenseSide].AttackSpeed;
				movementState = playerMovimentationStates.rooted;
				//if(Vector2.Distance (transform.position,goalPosition) < weaponSlots[defenseSide].LifeTime*2)
					animator.SetTrigger ("jumpsimple");
				//else
				//	animator.SetTrigger ("jump");
				chargeSpeed = weaponSlots[defenseSide].ProjectileSpeed;
				goalPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				goalPosition.z = transform.position.z;
				goalDirection = goalPosition - transform.position;
				goalDirection.Normalize ();
				lastDistanceToGoal = Vector2.Distance (transform.position,goalPosition);
			}
			break;
		case jump.delay:
			if(currentDefenseSpeed < 0)
			{
				jumpState = jump.going;
				movementState = playerMovimentationStates.charging;
				gameObject.layer = passThroughLayer;
				//playerHit.collider.enabled = false;
			}
			break;		
		case jump.going:
			break;		
		case jump.cooldown:
			UI.UpdateCooldown(defenseSide,currentDefenseSpeed / (weaponSlots[defenseSide].CoolDown * GetCooldownDefense()));
			if(currentDefenseSpeed < 0)
			{
				jumpState = jump.ready;
				UI.FinishCooldown(defenseSide);
			}
			break;
		}
	}

	void ResolvePortalGun()
	{
		currentDefenseSpeed -= portalgunState == portalgun.cooldown ? GetCooldownTickForThisFrameDefenseSkill () : GameController.deltaTime;		
		switch(portalgunState)
		{
		case portalgun.ready:
			if(CheckForDefenseButtonPress())
			{		
				portalgunState = portalgun.going;
				currentDefenseSpeed = weaponSlots[defenseSide].AttackSpeed;
				weaponInUse[defenseSide].SendMessage("PortalGunOn",currentCursor.currentFocusGameObject);
			}
			break;
		case portalgun.going:
			/*UI.InterfacePortalGun(defenseSide,currentDefenseSpeed);
			if(Input.GetMouseButtonUp(isRight) || currentAttackSpeed[isRight] < 0)
			{
				portalgunState = portalgun.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * baseCooldown;
				weaponInUse[isRight].SendMessage("ActivatePortalGun");
			}
			break;	*/
		case portalgun.cooldown:
			UI.UpdateCooldown(defenseSide,currentDefenseSpeed / (weaponSlots[defenseSide].CoolDown * GetCooldownDefense()));
			if(currentDefenseSpeed < 0)
			{
				portalgunState = portalgun.ready;
				UI.FinishCooldown(defenseSide);
			}
			break;
		}	
	}
	
	public void PortalGunFailed()
	{
		if(portalOnPlayer)
		{
			if(movementState == playerMovimentationStates.rooted)
			{
				movementState = playerMovimentationStates.walking;
			}
		}
		portalOnPlayer = false;
	}
	
	public void Teleport(Vector3 pos)
	{
		if(movementState == playerMovimentationStates.rooted)
		{
			movementState = playerMovimentationStates.walking;
		}
		pos.z = transform.position.z;
		transform.position = pos;
		portalOnPlayer = false;
	}

	void ResolveTimeBubble()
	{
		currentDefenseSpeed -= timebubbleState == timebubble.cooldown ? GetCooldownTickForThisFrameDefenseSkill () : GameController.deltaTime;
		switch(timebubbleState)
		{
		case timebubble.ready:
			if(CheckForDefenseButtonPress())
			{		
				timebubbleState = timebubble.firing;
				currentDefenseSpeed = weaponSlots[defenseSide].LifeTime;
				UI.UpdateCooldown(defenseSide,0);
			}
			break;
		case timebubble.firing:
			//UI.InterfaceTimeBubble(defenseSide,currentDefenseSpeed);
			if(currentDefenseSpeed < 0)
			{
				timebubbleState = timebubble.cooldown;
				currentDefenseSpeed = weaponSlots[defenseSide].CoolDown * GetCooldownDefense ();
			}
			break;	
		case timebubble.cooldown:
			UI.UpdateCooldown(defenseSide,currentDefenseSpeed / (weaponSlots[defenseSide].CoolDown * GetCooldownDefense()));
			if(currentDefenseSpeed < 0)
			{
				timebubbleState = timebubble.ready;
				UI.FinishCooldown(defenseSide);
			}
			break;
		}	
	}

	void ResolveKakeBushin()
	{
		currentDefenseSpeed -= kakebushinState == kakebushin.cooldown ? GetCooldownTickForThisFrameDefenseSkill () : GameController.deltaTime;
		switch(kakebushinState)
		{
		case kakebushin.ready:
			if(CheckForDefenseButtonPress())
			{		
				kakebushinState = kakebushin.firing;
				currentDefenseSpeed = weaponSlots[defenseSide].LifeTime;
			}
			break;
		case kakebushin.firing:
			break;	
		case kakebushin.cooldown:
			UI.UpdateCooldown(defenseSide,currentDefenseSpeed / (weaponSlots[defenseSide].CoolDown * GetCooldownDefense()));
			if(currentDefenseSpeed < 0)
			{
				kakebushinState = kakebushin.ready;
				UI.FinishCooldown(defenseSide);
			}
			break;
		}	
	}



	public void ResolveHomeMissile(int isRight)
	{				
		currentAttackSpeed[isRight] -= homemissileState == homemissile.cooldown ? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(homemissileState)
		{
		case homemissile.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				homemissileState = homemissile.charge;
				currentAttackSpeed[isRight] = 1;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;
		case homemissile.charge:
			UI.InterfaceCharge (isRight,currentAttackSpeed[isRight]);
			if(currentAttackSpeed[isRight] < 0)
				currentAttackSpeed[isRight] = 1;
			if(Input.GetMouseButtonUp(isRight))
			{
				homemissileState = homemissile.cooldown;
				weaponInUse[isRight].SendMessage("MouseReleased");
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownCrush();
			}
			break;
		case homemissile.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight]/(weaponSlots[isRight].CoolDown * GetCooldownCrush()));
			if(currentAttackSpeed[isRight] < 0)
			{
				homemissileState = homemissile.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public void HomeMissileAccidentalHit()
	{
		homemissileState = homemissile.cooldown;
		currentAttackSpeed[crushSide] = weaponSlots[crushSide].CoolDown * GetCooldownCrush();
	}

	public void ResolveKendo(int isRight)
	{				
		currentAttackSpeed[isRight] -= kendoState == kendo.cooldown ? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(kendoState)
		{
		case kendo.ready:
			if(Input.GetMouseButton(isRight))
			{		
				kendoState = kendo.charge;
				currentAttackSpeed[isRight] = 0;
				oneSecond[isRight] = 0;
				kendoCharge = weaponSlots[isRight].GetComponent<global::kendo>().GetChargeTime();
			}
			break;
		case kendo.charge:
			if(-currentAttackSpeed[isRight] > kendoCharge)
			{
				kendoState = kendo.hold;
				currentAttackSpeed[isRight] = weaponSlots[isRight].GetComponent<global::kendo>().GetHoldTime();
				UI.InterfaceCharged (isRight);
			}
			else
				UI.InterfaceCharge (isRight,1-(-currentAttackSpeed[isRight])/kendoCharge);
			if(Input.GetMouseButtonUp(isRight))
			{
				InstantiateWeapon(isRight);
				if(-currentAttackSpeed[isRight] > kendoCharge)
					weaponInUse[isRight].SendMessage("StartKendo",kendoCharge);
				else
					weaponInUse[isRight].SendMessage("StartKendo",-currentAttackSpeed[isRight]);
				kendoState = kendo.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownCrush();
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;
		case kendo.hold:
			UI.InterfaceShootNow (isRight,currentAttackSpeed[isRight]);
			if(Input.GetMouseButtonUp(isRight) || currentAttackSpeed[isRight] < 0)
			{
				InstantiateWeapon(isRight);
				weaponInUse[isRight].SendMessage("StartKendo",kendoCharge);
				kendoState = kendo.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownCrush();
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;
		case kendo.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight]/(weaponSlots[isRight].CoolDown * GetCooldownCrush()));
			if(currentAttackSpeed[isRight] < 0)
			{
				kendoState = kendo.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public void ResolveSkyLaser(int isRight)
	{				
		currentAttackSpeed[isRight] -= skylaserState == skylaser.cooldown ? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(skylaserState)
		{
		case skylaser.ready:
			if(CheckForMouseButtonPress(isRight))
			{		
				skylaserState = skylaser.charge;
				currentAttackSpeed[isRight] = weaponSlots[isRight].AttackSpeed;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;
		case skylaser.charge:
			UI.InterfaceCharge (isRight,currentAttackSpeed[isRight]/weaponSlots[isRight].AttackSpeed);
			if(currentAttackSpeed[isRight] < 0)
			{
				skylaserState = skylaser.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownCrush();
			}
			break;		
		case skylaser.cooldown:
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight]/(weaponSlots[isRight].CoolDown * GetCooldownCrush()));
			if(currentAttackSpeed[isRight] < 0)
			{
				skylaserState = skylaser.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public void MissedSkyLaser()
	{
		skylaserState = skylaser.cooldown;
		currentAttackSpeed[crushSide] = weaponSlots[crushSide].CoolDown * GetCooldownCrush();
	}

	public void ResolveMouseBreaker(int isRight)
	{			
		switch(mousebreakerState)
		{
		case mousebreaker.ready:
			if(Input.GetMouseButton(isRight))
			{		
				mousebreakerState = mousebreaker.going;
				currentAttackSpeed[isRight] = weaponSlots[isRight].auxVariable-1;
			}
			break;
		case mousebreaker.going:
			if(Input.GetMouseButtonDown(isRight))
				currentAttackSpeed[isRight]--;
			UI.InterfaceMouseBreaker(isRight,(int)currentAttackSpeed[isRight]);
			UI.InterfaceCharge (isRight,(currentAttackSpeed[isRight])/(weaponSlots[isRight].auxVariable));
			if(currentAttackSpeed[isRight] <= 0)
			{
				mousebreakerState = mousebreaker.hold;
				currentAttackSpeed[isRight] = weaponSlots[isRight].LifeTime;
				UI.InterfaceTimingRing(currentAttackSpeed[isRight]);
			}
			break;
		case mousebreaker.hold:
			currentAttackSpeed[isRight] -= GameController.deltaTime;
			if(currentAttackSpeed[isRight] < 0)
			{
				mousebreakerState = mousebreaker.firing;
				currentAttackSpeed[isRight] = weaponSlots[isRight].AttackSpeed;
				UI.InterfaceCharged (isRight);
			}
			break;
		case mousebreaker.firing:
			currentAttackSpeed[isRight] -= GameController.deltaTime;
			UI.InterfaceShootNow (isRight,currentAttackSpeed[isRight]);
			if(Input.GetMouseButtonDown(isRight) || currentAttackSpeed[isRight] < 0)
			{
				InstantiateWeapon(isRight);
				mousebreakerState = mousebreaker.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownCrush();
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;	
		case mousebreaker.cooldown:
			currentAttackSpeed[isRight] -= GetCooldownTickForThisFrame();
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight]/(weaponSlots[isRight].CoolDown * GetCooldownCrush()));
			if(currentAttackSpeed[isRight] < 0)
			{
				mousebreakerState = mousebreaker.ready;
				UI.FinishCooldown(isRight);
			}
			break;
		}
	}

	public void ResolveProtonCannon(int isRight)
	{				
		switch(protoncannonState)
		{
		case protoncannon.ready:
			currentAttackSpeed[isRight] += GameController.deltaTime;
			UI.InterfaceProtonCannon(isRight,currentAttackSpeed[isRight]);
			if(CheckForMouseButtonPress(isRight))
			{		
				protoncannonState = protoncannon.firing;
				weaponInUse[isRight].SendMessage ("StartCannon",currentAttackSpeed[isRight]);
				currentAttackSpeed[isRight] = weaponSlots[isRight].AttackSpeed;
				movementState = playerMovimentationStates.rooted;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;
		case protoncannon.firing:
			currentAttackSpeed[isRight] -= GameController.deltaTime;
			if(currentAttackSpeed[isRight] < 0)
			{
				protoncannonState = protoncannon.cooldown;
				currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownAOE ();
				movementState = playerMovimentationStates.walking;
			}
			break;
		case protoncannon.cooldown:
			currentAttackSpeed[isRight] -= GetCooldownTickForThisFrame();
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight]/(weaponSlots[isRight].CoolDown * GetCooldownAOE ()));
			if(currentAttackSpeed[isRight] < 0)
			{
				protoncannonState = protoncannon.ready;
				UI.FinishCooldown(isRight);
				currentAttackSpeed[isRight] = 0;
			}
			break;
		}
	}

	void ResolveVoidRay(int isRight)
	{
		currentAttackSpeed[isRight] -= voidrayState == voidray.cooldown ? GetCooldownTickForThisFrame() : GameController.deltaTime;
		switch(voidrayState)
		{
		case voidray.ready:	
			if(CheckForMouseButtonPress(isRight))
			{		
				voidrayState = voidray.firing;
				weaponInUse[isRight].SendMessage("SetStrenght",baseStrenght);
			}
			break;
		case voidray.firing:
			if(Input.GetMouseButtonUp(isRight))
				StopVoidRay(true);
			break;
		case voidray.cooldown:	
			UI.UpdateCooldown(isRight,currentAttackSpeed[isRight]/(weaponSlots[isRight].CoolDown * GetCooldownCrush()));
			if(currentAttackSpeed[isRight] < 0)
			{
				voidrayState = voidray.ready;
				UI.FinishCooldown(isRight);
			}	
			break;			
		}
	}

	void StopVoidRay(bool playerStopped)
	{
		voidrayState = voidray.cooldown;
		weaponInUse[crushSide].SendMessage("VoidRayStop");
		if(playerStopped)
			currentAttackSpeed[crushSide] = weaponSlots[crushSide].CoolDown * GetCooldownCrush() * 0.25f;
		else
			currentAttackSpeed[crushSide] = weaponSlots[crushSide].CoolDown * GetCooldownCrush();
	}

	bool CheckForMouseButtonPress(int isRight)
	{
		if(Input.GetMouseButton(isRight))
		{	
			InstantiateWeapon(isRight);
			oneSecond[isRight] = 0;
			return true;
		}
		HandID[isRight] = "";
		return false;
	}

	bool CheckForDefenseButtonPress()
	{
		if(Input.GetButtonDown("Defense"))
		{	
			InstantiateWeapon(defenseSide);
			return true;
		}
		HandID[defenseSide] = "";
		return false;
	}

	bool CheckForMouseButtonPressOnMonster(int isRight)
	{
		if(Input.GetMouseButtonDown(isRight))
		{		
			if(currentCursor.currentFocusTag != Tags.Monster || 
			   currentCursor.currentFocusGameObject.GetComponent<MonsterGeneric>().knockbackType == MonsterGeneric.KnockbackType.hulk)
			{
				return Denied(isRight);
			}
			InstantiateWeapon(isRight);
			oneSecond[isRight] = 0;
			return true;
		}
		HandID[isRight] = "";
		return false;
	}

	bool CheckForMouseButtonPressOnSelf(int isRight)
	{
		if(Input.GetMouseButtonDown(isRight))
		{		
			if(currentCursor.currentFocusTag != "PlayerHit")
			{
				return Denied(isRight);
			}
			InstantiateWeapon(isRight);
			oneSecond[isRight] = 0;
			return true;
		}
		HandID[isRight] = "";
		return false;
	}

	bool CheckForDefenseButtonJump()
	{
		if(Input.GetButtonDown("Defense"))
		{
			//Ray ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			RaycastHit [] hits;
			int layerMask = 136192;
			float radius = controller.radius/2;
			goalPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			goalPosition.z = transform.position.z;
			goalDirection = goalPosition - transform.position;
			goalDirection.Normalize ();
			Vector3 temp = goalPosition;
			Vector3 upTemp = goalPosition + new Vector3(0,0,-10);
			Ray ray = new Ray(upTemp,temp - upTemp);
			hits = Physics.SphereCastAll(ray,radius,1000,layerMask);
			if(hits.Length == 0)
			{
				//TODO: PlaySound(OUTOFMANA);
				HandID[defenseSide] = "";
				return false;
			}
			else
			{
				for (int i = 0; i < hits.Length; i++) {
					switch(hits[i].collider.transform.tag)
					{
					case Tags.Unmovable:
					case Tags.Lvl2Obstacle:
					case Tags.Lvl3Obstacle:
					case Tags.Wall:	
						HandID[defenseSide] = "";
						return false;
					}
				}
			}
			InstantiateWeapon(defenseSide);
			return true;
		}
		HandID[defenseSide] = "";
		return false;
	}

	bool CheckCollisionWithFurniture()
	{
		int layerMask = 1024;
		Vector3 upTemp = transform.position + new Vector3(0,0,-10);
		Ray ray = new Ray(upTemp,transform.position - upTemp);
		if(Physics.SphereCast(ray,controller.radius,1000,layerMask))
			return true;
		else
			return false;
	}
	
	public void PoolForWeaponEquipped()
	{
		//print (weaponSlots[0][0].weaponName);
		if(weaponSlots[crushSide])
		{
			currentWeaponEquipped[crushSide] = weaponSlots[crushSide].GetWeaponSpecial();
			currentAttackSpeed[crushSide] = 0;
		}
		else
			currentWeaponEquipped[crushSide] = WeaponController.WeaponSpecial.none;		
		if(weaponSlots[AOESide])
		{
			currentWeaponEquipped[AOESide] = weaponSlots[AOESide].GetWeaponSpecial();
			currentAttackSpeed[AOESide] = 0;
		}
		else
			currentWeaponEquipped[AOESide] = WeaponController.WeaponSpecial.none;
		if(weaponSlots[defenseSide])
		{
			currentDefenseEquipped = weaponSlots[defenseSide].GetWeaponSpecial();
			currentDefenseSpeed = 0;
		}
		else
			currentDefenseEquipped = WeaponController.WeaponSpecial.none;
		/*if(currentWeaponEquipped[0] == WeaponController.WeaponSpecial.reapinghook || currentWeaponEquipped[1] == WeaponController.WeaponSpecial.reapinghook)
		{
			CreateReapingHookVisualAidPosition();
			SetReapingHookVisualAidPosition(currentWeaponEquipped[1] == WeaponController.WeaponSpecial.reapinghook ? 1 : 0);
		}*/
	}

	void InstantiateWeapon(int isRight)
	{	
		//TODO: trocar direction facing por arte
		Vector3 spawnposition;
		float projectileAngle=0;
		WeaponController.WeaponSpecial weapon = (isRight == 2) ? currentDefenseEquipped : currentWeaponEquipped [isRight];
		switch (weapon) 
		{
			//'Feet' weapons
		default:
			spawnposition = (currentCursor.currentPosition - spawnPos.position).normalized * 1.5f;
			spawnposition.z = 0;
			spawnposition = spawnPos.position + spawnposition;
			break;
			//'Mouse' weapons
		case WeaponController.WeaponSpecial.skylaser:
		case WeaponController.WeaponSpecial.rumbleult:
			spawnposition = currentCursor.currentPosition;
			spawnposition.z = spawnPos.position.z;
			break;
		}		
		projectileAngle =  Mathf.Atan2 (currentCursor.currentPosition.y - spawnposition.y, currentCursor.currentPosition.x - spawnposition.x) * Mathf.Rad2Deg;
		GameObject tempWeapon;
		switch(weapon)
		{
		default:
			tempWeapon = Instantiate(weaponSlots[isRight].gameObject,spawnposition,Quaternion.Euler(0,0,90)) as GameObject;			
			break;		
		case WeaponController.WeaponSpecial.grabchain:
			projectileAngle =  Mathf.Atan2 (currentCursor.currentPosition.y - spawnposition.y, currentCursor.currentPosition.x - spawnposition.x) * Mathf.Rad2Deg;
			tempWeapon = Instantiate(weaponSlots[isRight].gameObject,spawnposition,Quaternion.Euler(new Vector3(0,0,projectileAngle))) as GameObject;
			break;
		case WeaponController.WeaponSpecial.protoncannon:
		case WeaponController.WeaponSpecial.boomerang:
			tempWeapon = Instantiate(weaponSlots[isRight].gameObject,spawnposition,Quaternion.Euler(new Vector3(0,0,angle))) as GameObject;
			break;			
		case WeaponController.WeaponSpecial.kendo:	
			tempWeapon = Instantiate(weaponSlots[isRight].gameObject,spawnposition,Quaternion.Euler(new Vector3(0,0,projectileAngle))) as GameObject;
			break;
		case WeaponController.WeaponSpecial.voidray:
		case WeaponController.WeaponSpecial.shotgun:
			tempWeapon = Instantiate(weaponSlots[isRight].gameObject,spawnPos.position,Quaternion.Euler(new Vector3(0,0,angle+90))) as GameObject;
			tempWeapon.transform.parent = directionLooking;
			break;			
		}
		if(isRight == 2)//defense
		{
			skills.OnPlayerDefending ();		
		}
		else
		{
			animator.SetTrigger ("shoot");
			gameController.OnPlayerShooting (tempWeapon);
		}
		weaponInUse[isRight] = tempWeapon;
		tempWeapon.SendMessage("SetSide",isRight);		
		tempWeapon.SendMessage("SetDirection",currentCursor.currentPosition-spawnposition);	
		HandID[isRight] = tempWeapon.name;
		gameController.addWeapon(tempWeapon);
	}

	bool Denied(int isRight)
	{
		//TODO: PlaySound(OUTOFMANA);
		HandID[isRight] = "";
		return false;
	}

	public void Stunned(float duration)
	{	
		nextState = playerControllerStates.stun;
		stunDuration = duration * skills.GetUnstoppable ();
		animator.enabled = false;
	}
	
	public void Reset()
	{
		playerHit.GetComponent<Collider>().enabled = true;
		GameObject.Find("CameraControl").SendMessage ("ResetCamera");
		if(movementState == playerMovimentationStates.charging)
			StopCharge();
		movementState = playerMovimentationStates.walking;
		currentAttackSpeed[0] = 0;
		currentAttackSpeed[1] = 0;
		HandID[0] = "";
		HandID[1] = "";
		shotCount = threeshot.first;
		hitCount = 0;
		boomerangState = boomerang.ready;
		if(reapinghookState == reapinghook.ready)
			Destroy (currentreapinghookVisualAid);
		reapinghookState = reapinghook.cooldown;
		disablejumbleState = disablejumble.ready;
		voidrayState = voidray.ready;
		timebubbleState = timebubble.ready;
		firewaveState = firewave.ready;
		fraggrenadeState = fraggrenade.ready;
		portalgunState = portalgun.ready;
		jumpState = jump.ready;
		grabchainState = grabchain.ready;
		mousebreakerState = mousebreaker.ready;
		kendoState = kendo.ready;
		protoncannonState = protoncannon.ready;
		soulchargesState = soulcharges.ready;
		soulChargeCurrentHitCount = 0;
		shotgunState = shotgun.ready;
		homemissileState = homemissile.ready;
		movetokillState = movetokill.moving; 
		energyminesState = energymines.firstmineofroom;
		chainlightningState = chainlightning.ready;
		rumbleultState = rumbleult.ready;
		frozenorbState = frozenorb.ready;
		remotebombermineState = remotebombermine.firstmineofroom;
		bombNumber = 0;
		lastPosition = transform.position;
		distanceMovedToKill = 0;
		deathrainState = deathrain.ready;
		UI.ResetInterface (0);
		UI.ResetInterface (1);
		if(weaponInUse[0])
		{
			weaponInUse[0].SendMessage ("DestroyItSelf");
			weaponInUse[0] = null;
		}
		if(weaponInUse[1])
		{
			weaponInUse[1].SendMessage ("DestroyItSelf");
			weaponInUse[1] = null;
		}
		GameObject [] remainingWeapons = GameObject.FindGameObjectsWithTag (Tags.Weapon);
		for (int i = 0; i < remainingWeapons.Length; i++) {
			gameController.removeWeapon(remainingWeapons[i]);
			Destroy(remainingWeapons[i]);
		}
		PoolForWeaponEquipped();
		//UI.CleanWeapons();
	}

	public void ThreeShotHit(string name)
	{
		switch(hitCount)
		{
		case 0:
			threeshotMonsterBeingHittedName = name;
			hitCount++;
			break;
		case 1:
			if(threeshotMonsterBeingHittedName == name)
			{
				hitCount++;
				if(shotCount == threeshot.first)
				{
					if(weaponInUse[crushSide])
						weaponInUse[crushSide].SendMessage("ArmThirdShot",threeshotMonsterBeingHittedName);
				}
			}
			else
				hitCount = 0;
			break;
		case 2:
			hitCount = 0;
			break;
		}
	}
	
	public void ThreeShotMiss()
	{
		hitCount = 0;
	}

	void CreateReapingHookVisualAidPosition()
	{
		currentreapinghookVisualAid = Instantiate (reapinghookVisualAid,transform.position,Quaternion.identity) as GameObject;
	}

	void SetReapingHookVisualAidPosition(int isRight)
	{
		Vector3 spawnposition = (currentCursor.currentPosition - spawnPos.position);
		spawnposition.z = 0;
		spawnposition = spawnposition.normalized;
		spawnposition = spawnPos.position + spawnposition * (weaponSlots [isRight].ProjectileSpeed + 1);
		currentreapinghookVisualAid.transform.position = spawnposition;
	}

	public void DisableJumbleOff(int isRight)
	{
		disablejumbleState = disablejumble.cooldown;
		currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownDefense ();
		HandID[isRight] = "";
	}

	public void PortalGunOff(int isRight)
	{
		portalgunState = portalgun.cooldown;
		portalOnPlayer = false;
		currentAttackSpeed[isRight] = weaponSlots[isRight].CoolDown * GetCooldownDefense ();
		HandID[isRight] = "";
	}

	public void SinkSwallow()
	{
		switch(currentWeaponEquipped[1])
		{
		case WeaponController.WeaponSpecial.boomerang:
			boomerangState = boomerang.cooldown;
			currentAttackSpeed[1] = weaponSlots[1].CoolDown * GetCooldownAOE ();
			break;

		case WeaponController.WeaponSpecial.fraggrenade:
			fraggrenadeState = fraggrenade.cooldown;
			currentAttackSpeed[1] = weaponSlots[1].CoolDown * GetCooldownAOE ();
			break;
		}

	}

	public void IncreaseStrenght()
	{
		strenght++;
		baseStrenght = originalBaseStrenght + strenght * strenghtIncrease;
	}

	public void IncreaseSpeed()
	{
		speed++;
		baseSpeed = originalBaseSpeed + speed * speedIncrease;
	}

	public void IncreaseLifeContainer()
	{
		currentLife += lifeContainerSize;
		maxLife++;
		life.text = currentLife.ToString ();
		totalLife = (float)((maxLife + 1) * lifeContainerSize);
		UI.IncreaseLife ();
	}

	public float GetCurrentLifePercentage()
	{
		return currentLife / totalLife;
	}
	int currentRoom;
	float lifeRecoverInCurrentRoom;
	public void LifeRecover(float l)
	{
        int remainder = (int)currentLife % 13;//cant recover more then the current life container!
        print(remainder);
        if (currentLife >= totalLife || remainder == 0)
			return;
        /*
                if (currentRoom == StageController.Instance.GetCurrentRoom())
                {
                    lifeRecoverInCurrentRoom += l;
                    if (lifeRecoverInCurrentRoom > 3)
                        return;
                }
                else
                {
                    currentRoom = StageController.Instance.GetCurrentRoom();
                    lifeRecoverInCurrentRoom = 0;
                } 
        */
        if (currentLife < 13)
            remainder = 13 - (int)currentLife;   
        if (l > remainder)
            l = remainder;
        currentLife += l;
		GameObject temp = Instantiate(floatingDamage,transform.position,Quaternion.Euler (0,0,0)) as GameObject;	
		temp.SendMessage ("SetRegen",l);
		if(currentLife > totalLife)
			currentLife = totalLife;	
		life.text = currentLife.ToString ();
		UI.GainLife(((int)currentLife/13),((int)currentLife%13)+1);
	}

	float GetCooldownTickForThisFrame()//implementei essa opcao para variaveis poderem acelerar/desacelarar o cooldown em tempo real (principalmente por culpa do Tarrasque)
	{
		return GameController.deltaTime * TarrasqueCooldownMultiplier();
	}

	float GetCooldownTickForThisFrameDefenseSkill()//implementei essa opcao para variaveis poderem acelerar/desacelarar o cooldown em tempo real
	{
		return GameController.deltaTime;
	}

	float GetCooldownCrush()
	{
		float aux = baseCooldown - (skills.GetLiveandLearn () + skills.GetTheWayofTheWarrior ());
		return aux < 0.1f ?  0.1f : aux;
	}

	float GetCooldownAOE()
	{
		float aux = baseCooldown - (skills.GetTheWayofTheMage() + skills.GetD20() +  skills.GetAntiZerg ());
		return aux < 0.1f ?  0.1f : aux;
	}

	float GetCooldownDefense()
	{
		float aux = baseCooldown - (skills.GetTheWayofTheShield() + skills.GetOnlyOncePerRoom ());
		return aux < 0.1f ?  0.1f : aux;
	}

	public float GetMainSpeed()
	{
		return mainSpeed;
	}

	float TarrasqueCooldownMultiplier()
	{
		if(thereIsATarrasque)
		{
			float distanceFromTarrasque = Vector2.Distance(TheTarrasque.position,transform.position);
			if(distanceFromTarrasque < auraStartingRange)
				return 1;
			if(distanceFromTarrasque > auraMaxRange)
				return auraMinCooldownDecrease;
			return 1 - ((distanceFromTarrasque - auraStartingRange) * auraIncrements);
		}
		else
			return 1;
	}
	Transform TheTarrasque;
	float auraStartingRange = 10;
	float auraMaxRange = 50;
	float auraMinCooldownDecrease = 0.3f;
	float auraIncrements;
	public void ATarrasqueIsBorn(Transform tarrasque, float startingRange, float maxRange, float minCooldownDecrease)
	{
		thereIsATarrasque = true;
		TheTarrasque = tarrasque;
		auraStartingRange = startingRange;
		auraMaxRange = maxRange;
		auraMinCooldownDecrease = minCooldownDecrease;
		auraIncrements = (1 - auraMinCooldownDecrease) / (auraMaxRange - auraStartingRange);
	}

	public void ATarrasqueIsKilled()
	{
		thereIsATarrasque = false;
	}

	public void OneHitDeath()
	{
		currentLife = -1;
		Died ();
	}

	public void Died()
	{
		playerHit.GetComponent<Collider>().enabled = false;
		animator.SetTrigger ("die");
		transform.FindChild ("PlayerVisual").GetComponent<Animator> ().SetBool ("hit", false);
		gameController.GameOver ();
	}

	public void SetWalkDirectionLock(MonsterGeneric.directions direction)
	{
		switch(direction)
		{
		case MonsterGeneric.directions.U:
			walkLockUp = true;
			break;
		case MonsterGeneric.directions.D:
			walkLockDown = true;
			break;
		case MonsterGeneric.directions.R:
			walkLockRight = true;
			break;
		case MonsterGeneric.directions.L:
			walkLockLeft = true;
			break;
		}
	}

	public void ResetWalkDirectionLock()
	{
		walkLockUp = false;
		walkLockDown = false;
		walkLockRight = false;
		walkLockLeft = false;
	}

	public GameObject dynamicGridPlayer;
}