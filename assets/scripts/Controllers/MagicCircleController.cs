using UnityEngine;
using System.Collections;

public class MagicCircleController : MonoBehaviour {

	public GameObject souls;
	public float timer;
	public Transform position;
	float countdown = 0;
	// Use this for initialization
	void Start () {
		timer = Random.Range(timer-1,timer+4);
		countdown = timer;
	}
	
	// Update is called once per frame
	void Update () {
		countdown -= GameController.deltaTime;
		if(countdown<0)
		{
			GameObject temp= Instantiate(souls,position.position,Quaternion.identity) as GameObject;
			temp.transform.parent = this.transform;
			countdown = timer;
		}
	}
}
