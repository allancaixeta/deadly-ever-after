using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillController : MonoBehaviour {
	
	public enum Skills{
		notassigned,
		Fasterbang,
		Stronger,
		CrushingRage,
		LockON,
		LastBreath,
		Growing,
		Piercing,
		Accurate,
		Knockback,
		Burn,
		Cold,
		LiveandLearn, 
		LifeSteal,
		Thewayofthewarrior,
		Dragonslayer,
		Beginnersluck,
		Hitenmitsurugi,
		Antiheavy,
		Peacemaker,
		Finalblow,
		Revenge,
		crush22,
		Fasterboom,
		BattleMage,
		Fury,
		Biggerwiderlarger,
		Sneakattack,
		Repel,
		Freeze,
		Thewayofthemage,
		Sizematters,
		Blindness,
		HPRegeneration, 
		Poison,
		Bleed,
		Armageddon,
		D20,
		AntiZerg,
		Fightingthebigguy,
		Doublechance,
		Flawless,
		Combo,
		aoe21,
		aoe22,
		Speed,
		Accel,
		Adrenaline,
		Coffee,
		Thewayoftheshield,
		Debuff,
		Unstoppable,
		Armorless,
		LoveandPeace,
		EZMode,
		RealDebuff,
		DFENCE,
		Shield,
		Parry,
		Slowmo,
		Blinky,
		Temp,
		Extralife,
		Onlyonceperroom,
		Perfect,
		Recover,
		Spiky,
		Collector
	}

	public enum LevelUpMode{
		normal,
		AliceChallenge
	}


	PlayerController playercontroller;
	GameController gameController;
	private Skills[] skills = new Skills[10]; 
	Skills acquiredCrush, acquiredAOE, acquiredDefense, acquiredAliceCrush, acquiredAliceAOE, acquiredAliceDefense;
	public string [] skillsDescription;
	public Sprite [] skillsAppearance;

	//CRUSH
	//
	//
	//

	public int crushingrage = 0;
	public float crushingrageDamageIncrease = 5;

	public int growing = 0;
	public float growingSizeIncrease = 0.25f;

	public int accurate = 0;
	public float accurateStunIncrease = 1.5f;

	public int knockback = 0;
	public float knockbackIncrease = 5;
	
	public int burn = 0;
	public int burnDamageIncrease = 3;
	private List<MonsterGeneric> listOfMonstersBurning;
	private List<float> listOfMonstersBurningCountdownToHit;
	private List<int> listOfMonstersBurningCycle;
	
	public int cold = 0;
	public float coldSlowIncrease = 0.1f;

	public int liveandLearn = 0;
	public float liveandLearnCooldownDecrease = 0.05f;
	float shootsExecutedInTheCurrentRoom;
	
	public int lifesteal = 0;
	public float lifestealPercentageIncrease = 0.01f;

	public int hitenmitsurugi = 0;
	public float hitenmitsurugiDamageIncrease = 5;
	public float hitenmitsurugiTimerToActivate = 20;
	float currentHitenmitsurugiTimerToActivate, savedHitenMitsurugiTimer;

	public int finalblow = 0;
	public float finalblowLiveLeft = 0.1f;
	
	public int revenge = 0;

	//AOE
	//
	//
	//

	public int battlemage = 0;
	public float battlemageDamageIncrease = 1;

	public int biggerwiderlarger = 0;
	public float biggerwiderlargerSizeIncrease = 0.25f;

	public int thewayofthemage = 0;
	public float thewayofthemageCooldownDecrease = 0.05f;
	
	public int sizematters = 0;
	public float sizemattersDamageIncrease = 0.25f;
	
	public int hpregeneration = 0;
	public float hpregenerationIncrease = 0.2f;
	private List<string> listOfMonstersAlreadyHitted;

	public int poison = 0;
	public int poisonDamageIncrease = 1;
	private List<MonsterGeneric> listOfMonstersPoisoned;
	private List<float> listOfMonstersPoisonedCountdownToHit;

	public int armageddon = 0;
	public int armageddonDamageIncrease = 1;
	
	public int antizerg = 0;
	public float antizergCooldownDecrease = 0.05f;
	
	public int doublechance = 0;
	public float doublechanceChanceIncrease = 0.1f;
	
	public int flawless = 0;
	
	public int combo = 0;

	//DEFENSE
	//
	//
	//

	public int speed = 0;
	public float speedIncrease = 1;

	public int accel = 0;
	public float accelCooldownDecrease = 0.15f;

	public int coffee = 0;
	public float coffeeCooldownDecrease = 0.25f;

	public int debuff = 0;
	public float debuffLifeDecrease = 0.1f;

	public int dfence = 0;
	public int dfenceArmorIncrease = 1;

	public int temp = 0;
	public float tempHitQuantityAbsorbed = 1;
	int tempHitCount = 0;
	
	public int perfect = 0;
	
	public int onlyonceperroom = 0;
	public float onlyonceperroomCooldownDecrease = 0.4f;
	bool onlyonceperroomActivated = false;
	
	public int recover = 0;
	
	public int spiky = 0;
	
	public int collector = 0;

	//SINERGIES
	int fasterbang = 0;
	public float fasterbangSpeedIncrease = 0.1f;
	
	int stronger = 0;
	
	int lockON = 0;
	public float lockONDamageIncrease = 0.2f;
	
	int lastBreath = 0;
	public float lastBreathDamageIncrease = 0.5f;

	int piercing = 0;
	public float piercingArmorIgnore = 5;

	int thewayofthewarrior = 0;
	public float thewayofthewarriorCooldownDecrease = 0.05f;
	
	int dragonslayer = 0;
	public float dragonslayerDamageIncrease = 0.1f;
	
	int beginnersluck = 0;
	public float beginnersluckDamageIncrease = 3;
		
	int antiheavy = 0;
	public float antiheavyDamageIncrease = 0.2f;
	
	int peacemaker = 0;
	public float peacemakerDamageIncrease = 0.2f;

	int fasterboom = 0;
	public float fasterboomSpeedIncrease = 0.1f;
	
	int fury = 0;
	public float furyDamageIncrease = 0.5f;
	
	int sneakattack = 0;
	public float sneakattackStunIncrease = 1.5f;
	
	int repel = 0;
	public float repelIncrease = 5;
	
	int freeze = 0;
	public float freezeSlowIncrease = 0.1f;
	
	int bleed = 0;
	public float bleedVulnerabilityIncrease = 0.2f;
	private List<MonsterGeneric> listOfMonstersBleeding;
	private List<float> listOfMonstersBleedingCountdownToHit;
	private List<int> listOfMonstersBleedingCycle;
	
	int D20 = 0;
	public float D20ChanceIncrease = 0.05f;
	bool D20CriticalHit;
	
	int fightingthebigguy = 0;
	public float fightingthebigguyDamageIncrease = 0.25f;

	
	int adrenaline = 0;
	public float adrenalineSlowdownIncrease = 1;
	public float adrenalineTimeValue = 0.5f;
	float adrenalineCooldown = 0;
	
	int thewayoftheshield = 0;
	public float thewayoftheshieldCooldownDecrease = 0.05f;
	
	int unstoppable = 0;
	public float unstoppableReductionDecrease = 0.2f;
	
	int armorless = 0;
	public float armorlessArmorDecrease = 0.2f;
	
	int loveandpeace = 0;
	public float loveandpeaceFuryDecrease;
	
	int ezmode = 0;
	public int ezmodeQuantityDecrease = 1;
	int ezmodeUses = 0;
	
	int realdebuff = 0;
	public int realdebuffLevelDecrease = 1;
	int realdebuffUses = 0;
	
	int shield = 0;
	public float shieldInvulnerabilityIncrease = 0.5f;
	
	int parry = 0;
	public float parryTime = 0.3f;
	
	int slowmo = 0;
	public float slowmoSlowdownIncrease = 0.5f;
	public float slowmoTimeValue = 0.5f;
	float slowmoCooldown = 0;
	
	int blinky = 0;
	public float blinkyDurationIncrease = 0.5f;
	
	int extralife = 0;

	void Start()
	{
		playercontroller = PlayerController.Instance;
		gameController = GameController.Instance;
		listOfMonstersBurning = new List<MonsterGeneric>();
		listOfMonstersBurningCountdownToHit = new List<float>();
		listOfMonstersBurningCycle = new List<int>();
		listOfMonstersPoisoned = new List<MonsterGeneric>();
		listOfMonstersPoisonedCountdownToHit = new List<float>();
		listOfMonstersBleeding = new List<MonsterGeneric>();
		listOfMonstersBleedingCountdownToHit = new List<float>();
		listOfMonstersBleedingCycle = new List<int>();
		currentHitenmitsurugiTimerToActivate = hitenmitsurugiTimerToActivate;
		gameController.RegisterAsInterestedOnPlayerShooting (this.gameObject);
		for (int i = 0; i < skills.Length; i++) {
			skills[i] = Skills.notassigned;
		}
	}

	void Update()
	{
		/*foreach(Skills s in skills)
		{
			switch(s)
			{
			case Skills.Fasterbang:
				break;
			case Skills.Burn:*/
		if(GetBurn () > 0)
		{
			for (int i = 0; i < listOfMonstersBurning.Count; i++) {
				listOfMonstersBurningCountdownToHit[i] -= GameController.deltaTime;
				if(!listOfMonstersBurning[i])
				{
					listOfMonstersBurning.RemoveAt (i);
					listOfMonstersBurningCountdownToHit.RemoveAt (i);
					listOfMonstersBurningCycle.RemoveAt (i);
				}
				else
				{
					if(listOfMonstersBurningCountdownToHit[i] < 0)
					{
						listOfMonstersBurning[i].Hit (GetBurn ());
						listOfMonstersBurningCountdownToHit[i] = 1;
						listOfMonstersBurningCycle[i]++;
						if(listOfMonstersBurningCycle[i] >= 3)
						{
							listOfMonstersBurning.RemoveAt (i);
							listOfMonstersBurningCountdownToHit.RemoveAt (i);
							listOfMonstersBurningCycle.RemoveAt (i);
						}
					}
				}
			}
		}
		if (hitenmitsurugi > 0) {
			currentHitenmitsurugiTimerToActivate -= GameController.deltaTime;
		}
		if(GetPoison () > 0)
		{
			for (int i = 0; i < listOfMonstersPoisoned.Count; i++) {
				listOfMonstersPoisonedCountdownToHit[i] -= GameController.deltaTime;
				if(!listOfMonstersPoisoned[i])
				{
					listOfMonstersPoisoned.RemoveAt (i);
					listOfMonstersPoisonedCountdownToHit.RemoveAt (i);
				}
				else
				{
					if(listOfMonstersPoisonedCountdownToHit[i] < 0)
					{
						listOfMonstersPoisoned[i].Hit (GetPoison ());
						listOfMonstersPoisonedCountdownToHit[i] = 1;
					}
				}
			}
		}
		if(GetBleed () > 0)
		{
			for (int i = 0; i < listOfMonstersBleeding.Count; i++) {
				listOfMonstersBleedingCountdownToHit[i] -= GameController.deltaTime;
				if(!listOfMonstersBleeding[i])
				{
					listOfMonstersBleeding.RemoveAt (i);
					listOfMonstersBleedingCountdownToHit.RemoveAt (i);
					listOfMonstersBleedingCycle.RemoveAt (i);
				}
				else
				{
					if(listOfMonstersBleedingCountdownToHit[i] < 0)
					{
						//TODO: efeito especial de monstro sangrando
						listOfMonstersBleedingCountdownToHit[i] = 1;
						listOfMonstersBleedingCycle[i]++;
						if(listOfMonstersBleedingCycle[i] >= 3)
						{
							listOfMonstersBleeding[i].Bleeding(0);
							listOfMonstersBleeding.RemoveAt (i);
							listOfMonstersBleedingCountdownToHit.RemoveAt (i);
							listOfMonstersBleedingCycle.RemoveAt (i);
						}
					}
				}
			}
		}
		if(adrenalineCooldown > 0)
		{
			adrenalineCooldown -= GameController.deltaTime;
			if(adrenalineCooldown < 0)
				Time.timeScale = 1;
		}
		if(slowmoCooldown > 0)
		{
			slowmoCooldown -= GameController.deltaTime;
			if(slowmoCooldown < 0)
				Time.timeScale = 1;
		}

				/*break;
			}
		}*/
	}

	//CRUSH
	//
	//
	//

	public void IncreaseSkillCrush(Skills s, LevelUpMode levelup)
	{
		print (s);
		switch(levelup)
		{
		case LevelUpMode.normal:
		default:
			switch(s)
			{			
			case Skills.CrushingRage:
				if(crushingrage == 0)
				{
					acquiredCrush = Skills.CrushingRage;					
				}
				crushingrage++;
				break;			
			case Skills.Growing:
				if(growing == 0)
				{
					acquiredCrush = Skills.Growing;
					
				}
				growing++;
				break;
			
			case Skills.Accurate:
				if(accurate == 0)
				{
					acquiredCrush = Skills.Accurate;
					
				}
				accurate++;
				break;
			case Skills.Knockback:
				if(knockback == 0)
				{
					acquiredCrush = Skills.Knockback;
					
				}
				knockback++;
				break;
			case Skills.Burn:
				if(burn == 0)
				{
					acquiredCrush = Skills.Burn;
					
				}
				burn++;
				break;
			case Skills.Cold:
				if(cold == 0)
				{
					acquiredCrush = Skills.Cold;
					
				}
				cold++;
				break;
			case Skills.LiveandLearn:
				if(liveandLearn == 0)
				{
					acquiredCrush = Skills.LiveandLearn;
					
				}
				liveandLearn++;
				break;
			case Skills.LifeSteal:
				if(lifesteal == 0)
				{
					acquiredCrush = Skills.LifeSteal;
					
				}
				lifesteal++;
				break;		
			
			case Skills.Hitenmitsurugi:
				if(hitenmitsurugi == 0)
				{
					acquiredCrush = Skills.Hitenmitsurugi;					
				}
				hitenmitsurugi++;
				break;	
			
			case Skills.Finalblow:
				if(finalblow == 0)
				{
					acquiredCrush = Skills.Finalblow;					
				}
				finalblow++;
				break;
			case Skills.Revenge:
				if(revenge == 0)
				{
					acquiredCrush = Skills.Revenge;					
				}
				revenge++;
				break;
			}

			break;
		case LevelUpMode.AliceChallenge:
			break;
		}
	}

	public void IncreaseSkillAOE(Skills s, LevelUpMode levelup)
	{
		print (s);
		switch(levelup)
		{
		case LevelUpMode.normal:
		default:
			switch(s)
			{		
			case Skills.BattleMage:
				if(battlemage == 0)
				{
					acquiredAOE = Skills.BattleMage;
					
				}
				battlemage++;
				break;			
			case Skills.Biggerwiderlarger:
				if(biggerwiderlarger == 0)
				{
					acquiredAOE = Skills.Biggerwiderlarger;
					
				}
				biggerwiderlarger++;
				break;
			
			case Skills.Thewayofthemage:
				if(thewayofthemage == 0)
				{
					acquiredAOE = Skills.Thewayofthemage;
					
				}
				thewayofthemage++;
				break;
			case Skills.Sizematters:
				if(sizematters == 0)
				{
					acquiredAOE = Skills.Sizematters;
					
				}
				sizematters++;
				break;
			
			case Skills.HPRegeneration:
				if(hpregeneration == 0)
				{
					acquiredAOE = Skills.HPRegeneration;
					
				}
				hpregeneration++;
				break;
			case Skills.Poison:
				if(poison == 0)
				{
					acquiredAOE = Skills.Poison;
					
				}
				poison++;
				break;
			
			case Skills.Armageddon:
				if(armageddon == 0)
				{
					acquiredAOE = Skills.Armageddon;
					
				}
				armageddon++;
				break;
			
			case Skills.AntiZerg:
				if(antizerg == 0)
				{
					acquiredAOE = Skills.AntiZerg;
					
				}
				antizerg++;
				break;
			
			case Skills.Doublechance:
				if(doublechance == 0)
				{
					acquiredAOE = Skills.Doublechance;
					
				}
				doublechance++;
				break;
			case Skills.Flawless:
				if(flawless == 0)
				{
					acquiredAOE = Skills.Flawless;
					
				}
				flawless++;
				break;
			case Skills.Combo:
				if(combo == 0)
				{
					acquiredAOE = Skills.Combo;
					
				}
				combo++;
				break;
			}
			break;
		case LevelUpMode.AliceChallenge:
			break;
		}
	}

	public void IncreaseSkillDefense(Skills s, LevelUpMode levelup)
	{
		print (s);
		switch(levelup)
		{
		case LevelUpMode.normal:
		default:
			switch (s) {
			case Skills.Speed:
				if(speed == 0)
				{
					acquiredDefense = Skills.Speed;					
				}
				speed++;
				playercontroller.IncreaseSpeed ();
				break;
			case Skills.Accel:
				if(accel == 0)
				{
					acquiredDefense = Skills.Accel;
					
				}
				accel++;
				break;
			
			case Skills.Coffee:
				if(coffee == 0)
				{
					acquiredDefense = Skills.Coffee;
					
				}
				coffee++;
				break;
			
			case Skills.Debuff:
				if(debuff == 0)
				{
					acquiredDefense = Skills.Debuff;
					
				}
				debuff++;
				break;
			
			case Skills.DFENCE:
				if(dfence == 0)
				{
					acquiredDefense = Skills.DFENCE;
					
				}
				dfence++;
				break;
			
			case Skills.Temp:
				if(temp == 0)
				{
					acquiredDefense = Skills.Temp;
					
				}
				temp++;
				break;
			
			case Skills.Onlyonceperroom:
				if(onlyonceperroom == 0)
				{
					acquiredDefense = Skills.Onlyonceperroom;
					
				}
				onlyonceperroom++;
				break;
			case Skills.Perfect:
				if(perfect == 0)
				{
					acquiredDefense = Skills.Perfect;					
				}
				perfect++;
				break;
			case Skills.Recover:
				if(recover == 0)
				{
					acquiredDefense = Skills.Recover;
					
				}
				recover++;
				break;
			case Skills.Spiky:
				if(spiky == 0)
				{
					acquiredDefense = Skills.Spiky;					
				}
				spiky++;
				break;
			case Skills.Collector:
				if(collector == 0)
				{
					acquiredDefense = Skills.Collector;					
				}
				collector++;
				break;
			}
			break;
		case LevelUpMode.AliceChallenge:
			break;
		}
	}

	void SetSinergy(Skills s)
	{

		switch(s)
		{
		case Skills.Stronger:
			if(stronger == 0)
			{
				acquiredCrush = Skills.Stronger;					
			}
			stronger++;
			playercontroller.IncreaseStrenght ();
			break;
		case Skills.LockON:
			if(lockON == 0)
			{
				acquiredCrush = Skills.LockON;
				
			}
			lockON++;
			break;
		case Skills.LastBreath:
			if(lastBreath == 0)
			{
				acquiredCrush = Skills.LastBreath;
				
			}
			lastBreath++;
			break;
		case Skills.Piercing:
			if(piercing == 0)
			{
				acquiredCrush = Skills.Piercing;
				
			}
			piercing++;
			break;
		case Skills.Thewayofthewarrior:
			if(thewayofthewarrior == 0)
			{
				acquiredCrush = Skills.Thewayofthewarrior;
				
			}
			thewayofthewarrior++;
			break;
		case Skills.Dragonslayer:
			if(dragonslayer == 0)
			{
				acquiredCrush = Skills.Dragonslayer;
				
			}
			dragonslayer++;
			break;
		case Skills.Beginnersluck:
			if(beginnersluck == 0)
			{
				acquiredCrush = Skills.Beginnersluck;
				
			}
			beginnersluck++;
			break;
		case Skills.Antiheavy:
			if(antiheavy == 0)
			{
				acquiredCrush = Skills.Antiheavy;
				
			}
			antiheavy++;
			break;
		case Skills.Peacemaker:
			if(peacemaker == 0)
			{
				acquiredCrush = Skills.Peacemaker;
				
			}
			peacemaker++;
			break;
		case Skills.Sneakattack:
			if(sneakattack == 0)
			{
				acquiredAOE = Skills.Sneakattack;
				
			}
			sneakattack++;
			break;
		case Skills.Repel:
			if(repel == 0)
			{
				acquiredAOE = Skills.Repel;				
			}
			repel++;
			break;
		case Skills.Freeze:
			if(freeze == 0)
			{
				acquiredAOE = Skills.Freeze;				
			}
			freeze++;
			break;
		case Skills.Blindness:
			break;
		case Skills.Bleed:
			if(bleed == 0)
			{
				acquiredAOE = Skills.Bleed;				
			}
			bleed++;
			break;
		case Skills.D20:
			if(D20 == 0)
			{
				acquiredAOE = Skills.D20;				
			}
			D20++;
			break;
		case Skills.Fightingthebigguy:
			if(fightingthebigguy == 0)
			{
				acquiredAOE = Skills.Fightingthebigguy;				
			}
			fightingthebigguy++;
			break;
		case Skills.Adrenaline:
			if(adrenaline == 0)
			{
				acquiredDefense = Skills.Adrenaline;				
			}
			adrenaline++;
			break;
		case Skills.Thewayoftheshield:
			if(thewayoftheshield == 0)
			{
				acquiredDefense = Skills.Thewayoftheshield;				
			}
			thewayoftheshield++;
			break;
		case Skills.Unstoppable:
			if(unstoppable == 0)
			{
				acquiredDefense = Skills.Unstoppable;				
			}
			unstoppable++;
			break;
		case Skills.Armorless:
			if(armorless == 0)
			{
				acquiredDefense = Skills.Armorless;				
			}
			armorless++;
			break;
		case Skills.LoveandPeace:
			if(loveandpeace == 0)
			{
				acquiredDefense = Skills.LoveandPeace;				
			}
			loveandpeace++;
			break;
		case Skills.EZMode:
			if(ezmode == 0)
			{
				acquiredDefense = Skills.EZMode;				
			}
			ezmode++;
			break;
		case Skills.RealDebuff:
			if(realdebuff == 0)
			{
				acquiredDefense = Skills.RealDebuff;				
			}
			realdebuff++;
			break;
		case Skills.Shield:
			if(shield == 0)
			{
				acquiredDefense = Skills.Shield;			
			}
			shield++;
			break;			
		case Skills.Slowmo:
			if(slowmo == 0)
			{
				acquiredDefense = Skills.Slowmo;				
			}
			slowmo++;
			break;
		case Skills.Blinky:
			if(blinky == 0)
			{
				acquiredDefense = Skills.Blinky;				
			}
			blinky++;
			break;
		case Skills.Extralife:
			if(extralife == 0)
			{
				acquiredDefense = Skills.Extralife;				
			}
			extralife++;
			playercontroller.IncreaseLifeContainer();
			break;
		}
	}

	public Skills RaffleSkillCrush(LevelUpMode levelup)
	{
		if (levelup == LevelUpMode.normal && acquiredCrush != Skills.notassigned)
			return acquiredCrush;
		if (levelup == LevelUpMode.AliceChallenge && acquiredAliceCrush != Skills.notassigned)
			return acquiredAliceCrush;
		int raffle = Random.Range (0,11);
		Skills skillToReturn;
		switch(raffle)
		{
		case 0:
			skillToReturn = Skills.CrushingRage;
			break;
		case 1:
			//skillToReturn = Skills.Growing;
			return RaffleSkillCrush (levelup);
		case 2:
			skillToReturn = Skills.Accurate;
			break;
		case 3: 
			skillToReturn = Skills.Knockback;
			break;
		case 4:
			skillToReturn = Skills.Burn;
			break;
		case 5:
			skillToReturn = Skills.Cold;			
			break;
		case 6:
			skillToReturn = Skills.LiveandLearn;
			break;
		case 7:
			skillToReturn = Skills.LifeSteal;			
			break;
		case 8:		
			skillToReturn = Skills.Hitenmitsurugi;			
			break;
		case 9:
			skillToReturn = Skills.Finalblow;
			break;
		case 10:
			//skillToReturn = Skills.Revenge;		
			return RaffleSkillCrush (levelup);
		default:
			skillToReturn = RaffleSkillCrush(levelup);
			break;
		}	
		if (levelup == LevelUpMode.normal && acquiredAliceCrush == skillToReturn || levelup == LevelUpMode.AliceChallenge && acquiredCrush != skillToReturn)
			return RaffleSkillCrush (levelup);
		else
			return skillToReturn;
	}

	public Skills RaffleSkillAOE(LevelUpMode levelup)
	{
		if (levelup == LevelUpMode.normal && acquiredAOE != Skills.notassigned)
			return acquiredAOE;
		if (levelup == LevelUpMode.AliceChallenge && acquiredAliceAOE != Skills.notassigned)
			return acquiredAliceAOE;
		int raffle = Random.Range (0,11);
		Skills skillToReturn;
		switch(raffle)
		{
		case 0:
			skillToReturn = Skills.BattleMage;
			break;
		case 1:
			//skillToReturn = Skills.Biggerwiderlarger;
			return RaffleSkillAOE (levelup);
		case 2:
			skillToReturn = Skills.Thewayofthemage;
			break;
		case 3: 
			skillToReturn = Skills.Sizematters;
			break;
		case 4:
			skillToReturn = Skills.HPRegeneration;
			break;
		case 5:
			skillToReturn = Skills.Poison;			
			break;
		case 6:
			skillToReturn = Skills.Armageddon;
			break;
		case 7:
			skillToReturn = Skills.AntiZerg;			
			break;
		case 8:		
			skillToReturn = Skills.Doublechance;	
			break;
		case 9:
			//skillToReturn = Skills.Flawless;
			return RaffleSkillAOE (levelup);
			break;
		case 10:
			//skillToReturn = Skills.Combo;		
			return RaffleSkillAOE (levelup);
		default:
			skillToReturn = RaffleSkillAOE(levelup);
			break;
		}	
		if (levelup == LevelUpMode.normal && acquiredAliceAOE == skillToReturn || levelup == LevelUpMode.AliceChallenge && acquiredAOE != skillToReturn)
			return RaffleSkillAOE(levelup);
		else
			return skillToReturn;
	}

	public Skills RaffleSkillDefense(LevelUpMode levelup)
	{
		if (levelup == LevelUpMode.normal && acquiredDefense != Skills.notassigned)
			return acquiredDefense;
		if (levelup == LevelUpMode.AliceChallenge && acquiredAliceDefense != Skills.notassigned)
			return acquiredAliceDefense;
		int raffle = Random.Range (0,11);
		Skills skillToReturn;
		switch(raffle)
		{
		case 0:
			skillToReturn = Skills.Speed;
			break;
		case 1:
			skillToReturn = Skills.Accel;
			break;
		case 2:
			skillToReturn = Skills.Coffee;
			break;
		case 3: 
			skillToReturn = Skills.Debuff;
			break;
		case 4:
			return RaffleSkillDefense (levelup);
			break;
		case 5:
			skillToReturn = Skills.Temp;			
			break;
		case 6:
			//skillToReturn = Skills.Perfect;
			return RaffleSkillDefense (levelup);
		case 7:
			skillToReturn = Skills.Onlyonceperroom;			
			break;
		case 8:		
			//skillToReturn = Skills.Recover;			
			return RaffleSkillDefense (levelup);
			break;
		case 9:
			//skillToReturn = Skills.Spiky;
			return RaffleSkillDefense (levelup);
		case 10:
			//skillToReturn = Skills.Collector;		
			return RaffleSkillDefense (levelup);
		default:
			skillToReturn = RaffleSkillDefense(levelup);
			break;
		}	
		if (levelup == LevelUpMode.normal && acquiredAliceDefense == skillToReturn || levelup == LevelUpMode.AliceChallenge && acquiredDefense != skillToReturn)
			return RaffleSkillDefense(levelup);
		else
			return skillToReturn;
	}

	public string GetSkillDescription(Skills s)
	{
		switch(s)
		{
		default:
		//Crush
		case Skills.CrushingRage:
			return SkillLevelDescriptionToBe(crushingrage) + ": " + skillsDescription[0];
		case Skills.Growing:
			return SkillLevelDescriptionToBe(growing) + ": " + skillsDescription[1];
		case Skills.Accurate:
			return SkillLevelDescriptionToBe(accurate) + ": " + skillsDescription[2];
		case Skills.Knockback:
			return SkillLevelDescriptionToBe(burn) + ": " + skillsDescription[3];
		case Skills.Burn:
			return SkillLevelDescriptionToBe(burn) + ": " + skillsDescription[4];
		case Skills.Cold:
			return SkillLevelDescriptionToBe(liveandLearn) + ": " + skillsDescription[5];
		case Skills.LiveandLearn:
			return SkillLevelDescriptionToBe(liveandLearn) + ": " + skillsDescription[6];
		case Skills.LifeSteal:
			return SkillLevelDescriptionToBe(lifesteal) + ": " + skillsDescription[7];
		case Skills.Hitenmitsurugi:
			return SkillLevelDescriptionToBe(hitenmitsurugi) + ": " + skillsDescription[8];
		case Skills.Finalblow:
			return SkillLevelDescriptionToBe(finalblow) + ": " + skillsDescription[9];
		case Skills.Revenge:
			return SkillLevelDescriptionToBe(revenge) + ": " + skillsDescription[10];
		//AOE
		case Skills.BattleMage:
			return SkillLevelDescriptionToBe(battlemage) + ": " + skillsDescription[11];
		case Skills.Biggerwiderlarger:
			return SkillLevelDescriptionToBe(biggerwiderlarger) + ": " + skillsDescription[12];
		case Skills.Thewayofthemage:
			return SkillLevelDescriptionToBe(thewayofthemage) + ": " + skillsDescription[13];		
		case Skills.Sizematters:
			return SkillLevelDescriptionToBe(sizematters) + ": " + skillsDescription[14];
		case Skills.HPRegeneration:
			return SkillLevelDescriptionToBe(hpregeneration) + ": " + skillsDescription[15];
		case Skills.Poison:
			return SkillLevelDescriptionToBe(poison) + ": " + skillsDescription[16];
		case Skills.Armageddon:
			return SkillLevelDescriptionToBe(armageddon) + ": " + skillsDescription[17];
		case Skills.AntiZerg:
			return SkillLevelDescriptionToBe(antizerg) + ": " + skillsDescription[18];
		case Skills.Doublechance:
			return SkillLevelDescriptionToBe(doublechance) + ": " + skillsDescription[19];
		case Skills.Flawless:
			return SkillLevelDescriptionToBe(flawless) + ": " + skillsDescription[20];
		case Skills.Combo:
			return SkillLevelDescriptionToBe(combo) + ": " + skillsDescription[21];
		//Defense
		case Skills.Speed:
			return SkillLevelDescriptionToBe(speed) + ": " + skillsDescription[22];		
		case Skills.Accel:
			return SkillLevelDescriptionToBe(accel) + ": " + skillsDescription[23];
		case Skills.Coffee:
			return SkillLevelDescriptionToBe(coffee) + ": " + skillsDescription[24];
		case Skills.Debuff:
			return SkillLevelDescriptionToBe(debuff) + ": " + skillsDescription[25];
		case Skills.DFENCE:
			return SkillLevelDescriptionToBe(dfence) + ": " + skillsDescription[26];
		case Skills.Temp:
			return SkillLevelDescriptionToBe(temp) + ": " + skillsDescription[27];
		case Skills.Perfect:
			return SkillLevelDescriptionToBe(perfect) + ": " + skillsDescription[28];
		case Skills.Onlyonceperroom:
			return SkillLevelDescriptionToBe(onlyonceperroom) + ": " + skillsDescription[29];
		case Skills.Recover:
			return SkillLevelDescriptionToBe(recover) + ": " + skillsDescription[30];
		case Skills.Spiky:
			return SkillLevelDescriptionToBe(spiky) + ": " + skillsDescription[31];
		case Skills.Collector:
			return SkillLevelDescriptionToBe(collector) + ": " + skillsDescription[32];
		}
	}

	string SkillLevelDescriptionToBe(int skilllevel)
	{
		switch(skilllevel)
		{
		case 0:		
			return "Basic";
		case 1: 
			return "Advanced";
		case 2:		
			return "Expert";
		case 3:
		default:
			return "Max";
		}
	}

	public Sprite GetSkillAppearance(Skills s)
	{
		switch(s)
		{
			//Crush
		case Skills.CrushingRage:
			return skillsAppearance[0];
		case Skills.Growing:
			return skillsAppearance[1];
		case Skills.Accurate:
			return skillsAppearance[2];
		case Skills.Knockback:
			return skillsAppearance[3];
		case Skills.Burn:
			return skillsAppearance[4];
		case Skills.Cold:
			return skillsAppearance[5];
		case Skills.LiveandLearn:
			return skillsAppearance[6];
		case Skills.LifeSteal:
			return skillsAppearance[7];
		case Skills.Hitenmitsurugi:
			return skillsAppearance[8];
		case Skills.Finalblow:
			return skillsAppearance[9];
		case Skills.Revenge:
			return skillsAppearance[10];
			//AOE
		case Skills.BattleMage:
			return skillsAppearance[11];
		case Skills.Biggerwiderlarger:
			return skillsAppearance[12];
		case Skills.Thewayofthemage:
			return skillsAppearance[13];		
		case Skills.Sizematters:
			return skillsAppearance[14];
		case Skills.HPRegeneration:
			return skillsAppearance[15];
		case Skills.Poison:
			return skillsAppearance[16];
		case Skills.Armageddon:
			return skillsAppearance[17];
		case Skills.AntiZerg:
			return skillsAppearance[18];
		case Skills.Doublechance:
			return skillsAppearance[19];
		case Skills.Flawless:
			return skillsAppearance[20];
		case Skills.Combo:
			return skillsAppearance[21];
			//Defense
		case Skills.Speed:
			return skillsAppearance[22];
		case Skills.Accel:
			return skillsAppearance[23];
		case Skills.Coffee:
			return skillsAppearance[24];
		case Skills.Debuff:
			return skillsAppearance[25];
		case Skills.DFENCE:
			return skillsAppearance[26];
		case Skills.Temp:
			return skillsAppearance[27];
		case Skills.Perfect:
			return skillsAppearance[28];
		case Skills.Onlyonceperroom:
			return skillsAppearance[29];
		case Skills.Recover:
			return skillsAppearance[30];
		case Skills.Spiky:
			return skillsAppearance[31];
		case Skills.Collector:
			return skillsAppearance[32];	
		default:
			return skillsAppearance[0];
		}
	}

	public bool IsMaxLevel(Skills s)
	{
		switch(s)
		{
		default:
			//Crush
		case Skills.CrushingRage:
			return SkillLevelDescriptionToBe(crushingrage) == "Max" ? true : false;
		case Skills.Growing:
			return SkillLevelDescriptionToBe(growing) == "Max" ? true : false;
		case Skills.Accurate:
			return SkillLevelDescriptionToBe(accurate) == "Max" ? true : false;
		case Skills.Knockback:
			return SkillLevelDescriptionToBe(knockback) == "Max" ? true : false;
		case Skills.Burn:
			return SkillLevelDescriptionToBe(burn) == "Max" ? true : false;
		case Skills.Cold:
			return SkillLevelDescriptionToBe(cold) == "Max" ? true : false;
		case Skills.LiveandLearn:
			return SkillLevelDescriptionToBe(liveandLearn) == "Max" ? true : false;
		case Skills.LifeSteal:
			return SkillLevelDescriptionToBe(lifesteal) == "Max" ? true : false;
		case Skills.Hitenmitsurugi:
			return SkillLevelDescriptionToBe(hitenmitsurugi) == "Max" ? true : false;
		case Skills.Finalblow:
			return SkillLevelDescriptionToBe(finalblow) == "Max" ? true : false;
		case Skills.Revenge:
			return SkillLevelDescriptionToBe(revenge) == "Max" ? true : false;
			//AOE
		case Skills.BattleMage:
			return SkillLevelDescriptionToBe(battlemage) == "Max" ? true : false;
		case Skills.Biggerwiderlarger:
			return SkillLevelDescriptionToBe(biggerwiderlarger) == "Max" ? true : false;
		case Skills.Thewayofthemage:
			return SkillLevelDescriptionToBe(thewayofthemage) == "Max" ? true : false;		
		case Skills.Sizematters:
			return SkillLevelDescriptionToBe(sizematters) == "Max" ? true : false;
		case Skills.HPRegeneration:
			return SkillLevelDescriptionToBe(hpregeneration) == "Max" ? true : false;
		case Skills.Poison:
			return SkillLevelDescriptionToBe(poison) == "Max" ? true : false;
		case Skills.Armageddon:
			return SkillLevelDescriptionToBe(armageddon) == "Max" ? true : false;
		case Skills.AntiZerg:
			return SkillLevelDescriptionToBe(antizerg) == "Max" ? true : false;
		case Skills.Doublechance:
			return SkillLevelDescriptionToBe(doublechance) == "Max" ? true : false;
		case Skills.Flawless:
			return SkillLevelDescriptionToBe(flawless) == "Max" ? true : false;
		case Skills.Combo:
			return SkillLevelDescriptionToBe(combo) == "Max" ? true : false;
			//Defense
		case Skills.Speed:
			return SkillLevelDescriptionToBe(speed) == "Max" ? true : false;		
		case Skills.Accel:
			return SkillLevelDescriptionToBe(accel) == "Max" ? true : false;
		case Skills.Coffee:
			return SkillLevelDescriptionToBe(coffee) == "Max" ? true : false;
		case Skills.Debuff:
			return SkillLevelDescriptionToBe(debuff) == "Max" ? true : false;
		case Skills.DFENCE:
			return SkillLevelDescriptionToBe(dfence) == "Max" ? true : false;
		case Skills.Temp:
			return SkillLevelDescriptionToBe(temp) == "Max" ? true : false;
		case Skills.Perfect:
			return SkillLevelDescriptionToBe(perfect) == "Max" ? true : false;
		case Skills.Onlyonceperroom:
			return SkillLevelDescriptionToBe(onlyonceperroom) == "Max" ? true : false;
		case Skills.Recover:
			return SkillLevelDescriptionToBe(recover) == "Max" ? true : false;
		case Skills.Spiky:
			return SkillLevelDescriptionToBe(spiky) == "Max" ? true : false;
		case Skills.Collector:
			return SkillLevelDescriptionToBe(collector) == "Max" ? true : false;
		}
	}

	public float GetFasterBang()
	{
		return fasterbang * fasterbangSpeedIncrease;
	}

	public float GetCrushingRage()
	{
		return crushingrage * crushingrageDamageIncrease;
	}

	public float GetLockON()
	{
		return lockON * lockONDamageIncrease;
	}
	
	public float GetLastBreath()
	{
		return lastBreath * lastBreathDamageIncrease;
	}
	
	public float GetGrowing()
	{
		return growing * growingSizeIncrease;
	}
	
	public int GetPiercing()
	{
		return piercing;
	}

	public float GetAccurate()
	{
		return accurate * accurateStunIncrease;
	}

	public float GetKnockback()
	{
		return knockback * knockbackIncrease;
	}
	
	public int GetBurn()
	{
		return burn * burnDamageIncrease;
	}

	public void RegisterAsBurningMonster(MonsterGeneric m)
	{
		if (!m)
			return;
		for (int i = 0; i < listOfMonstersBurning.Count; i++) {
			if(listOfMonstersBurning[i].name == m.name)
				return;
		}
		listOfMonstersBurning.Add (m);
		listOfMonstersBurningCountdownToHit.Add (1);
		listOfMonstersBurningCycle.Add (0);
	}

	public float GetCold()
	{
		return cold * coldSlowIncrease;
	}
	
	public float GetLiveandLearn()
	{
		float aux = liveandLearn * liveandLearnCooldownDecrease * shootsExecutedInTheCurrentRoom;
		return aux > (0.15f * liveandLearn) ? (0.15f * liveandLearn) : aux;
	}

	public void ResetLiveandLearnShootsCount()
	{
		shootsExecutedInTheCurrentRoom = 0;
	}

	public float GetLifeSteal()
	{
		return lifesteal * lifestealPercentageIncrease;
	}

	public float GetDragonslayer()
	{
		return dragonslayer * dragonslayerDamageIncrease;
	}
	
	public float GetTheWayofTheWarrior()
	{
		return thewayofthewarrior * thewayofthewarriorCooldownDecrease;
	}
	
	public float GetBeginnersLuck()
	{
		return (shootsExecutedInTheCurrentRoom == 1 && beginnersluck > 0)? beginnersluck * beginnersluckDamageIncrease : 1;
	}
	
	public float GetHitenMitsurugi()
	{
		if(savedHitenMitsurugiTimer < 0)
		{
			return  hitenmitsurugi > 0 ? hitenmitsurugi * hitenmitsurugiDamageIncrease : 1;
		}
		else
			return 1;
	}

	public void ResetHitenMitsurugi()
	{
		savedHitenMitsurugiTimer = 1;
		currentHitenmitsurugiTimerToActivate = hitenmitsurugiTimerToActivate;
	}

	public float GetAntiheavy(float armor)
	{
		float damageIncrease = antiheavy * antiheavyDamageIncrease * armor;
		if(damageIncrease > 0.3f)
		   return 0.3f;
		else
			return damageIncrease;
	}

	public float GetPeacemaker()
	{
		return peacemaker * peacemakerDamageIncrease;
	}

	public float GetFinalblow()
	{
		return finalblow * finalblowLiveLeft;
	}

	//AOE
	//
	//
	//



	public float GetFasterBoom()
	{
		return fasterboom * fasterboomSpeedIncrease;
	}

	public float GetBattleMage()
	{
		return battlemage * battlemageDamageIncrease;
	}
	
	public float GetFury()
	{
		return fury * furyDamageIncrease;
	}
	
	public float GetBiggerwiderlarger()
	{
		return biggerwiderlarger * biggerwiderlargerSizeIncrease;
	}
	
	public float GetSneakattack()
	{
		return sneakattack * sneakattackStunIncrease;
	}

	public float GetRepel()
	{
		return repel * repelIncrease;
	}
	
	public float GetFreeze()
	{
		return freeze * freezeSlowIncrease;
	}
	
	public float GetTheWayofTheMage()
	{
		return thewayofthemage * thewayofthemageCooldownDecrease;
	}
	
	public float GetSizeMatters()
	{
		return sizematters * sizemattersDamageIncrease;
	}

	public float GetHPRegeneration(string monsterName)
	{
        if (hpregeneration == 0)
            return 0;
		for (int i = 0; i < listOfMonstersAlreadyHitted.Count; i++) {
			if(listOfMonstersAlreadyHitted[i] == monsterName)
				return 0;
		}
		listOfMonstersAlreadyHitted.Add (monsterName);
		float seed = Random.value;
		if (hpregeneration * hpregenerationIncrease > 1.01f)
			return (seed <= (hpregeneration * hpregenerationIncrease - 1)) ? 2 : 1;
		else
			return (seed <= hpregeneration * hpregenerationIncrease) ? 1 : 0;
	}

	public void ResetHPRegeneration()
	{
		listOfMonstersAlreadyHitted = new List<string>();
	}
	
	public int GetPoison()
	{
		return poison * poisonDamageIncrease;
	}
	
	public void RegisterAsPoisonedMonster(MonsterGeneric m)
	{
		if (!m)
			return;
		for (int i = 0; i < listOfMonstersPoisoned.Count; i++) {
			if(!listOfMonstersPoisoned[i])
			{
				listOfMonstersPoisoned.RemoveAt (i);
				listOfMonstersPoisonedCountdownToHit.RemoveAt (i);
			}
			if(listOfMonstersPoisoned[i].name == m.name)
				return;
		}
		listOfMonstersPoisoned.Add (m);
		listOfMonstersPoisonedCountdownToHit.Add (0);
	}
	
	public float GetBleed()
	{
		return bleed * bleedVulnerabilityIncrease;
	}
	
	public void RegisterAsBleedingMonster(MonsterGeneric m)
	{
		if (!m)
			return;
		for (int i = 0; i < listOfMonstersBleeding.Count; i++) {
			if(listOfMonstersBleeding[i].name == m.name)
			{
				listOfMonstersBleedingCycle[i] = 0;
				return;
			}
		}
		m.Bleeding (GetBleed ());
		listOfMonstersBleeding.Add(m);
		listOfMonstersBleedingCountdownToHit.Add(1);
		listOfMonstersBleedingCycle.Add(0);
	}
	
	public int GetArmageddon()
	{
		return armageddon * armageddonDamageIncrease;
	}
	
	public float GetD20()
	{
		return D20CriticalHit ? 1 : 0;
	}

	public void RollD20()
	{
		if (Random.value < D20 * D20ChanceIncrease)
			D20CriticalHit = true;
		else
			D20CriticalHit = false;
	}
	
	public float GetAntiZerg()
	{
		float aux = antizerg * antizergCooldownDecrease * gameController.CountNumberOfMonstersInTheRoom();
		return aux > 0.25f ? 0.25f : aux;
	}

	public float GetFightingthebigguy()
	{
		return fightingthebigguy * fightingthebigguyDamageIncrease;
	}

	public float GetDoublechance()
	{
		return doublechance * doublechanceChanceIncrease;
	}
	/*
	public void IncreaseSkill()
	{
		stronger++;
	}
	
	public float Get()
	{
		return stronger * stronger;
	}

*/

	float rain, snow;
	public void StartRaining(float r)
	{
		rain = r;
	}

	public void StopRaining()
	{
		rain = 0;
	}

	public void StartSnow(float s)
	{
		snow = s;
	}
	
	public void StopSnow()
	{
		snow = 0;
	}

	public float GetAccel(bool isProjectile)
	{
		float r = (accel * accelCooldownDecrease) + (isProjectile ? 0 : rain) + snow;
		return r > 0.9f ? 0.9f : r;
	}

	public float GetAdrenaline()
	{
		return adrenaline * adrenalineSlowdownIncrease;
	}

	public void UseAdrenaline()
	{
		if(adrenaline > 0)
		{
			adrenalineCooldown = GetAdrenaline ();
			Time.timeScale = adrenalineTimeValue;
		}
	}

	public float GetCoffee()
	{
		return coffee * coffeeCooldownDecrease;
	}

	public float GetTheWayofTheShield()
	{
		return thewayoftheshield * thewayoftheshieldCooldownDecrease;
	}

	public float GetDebuff()
	{
		return 1 - debuff * debuffLifeDecrease;
	}

	public float GetUnstoppable()
	{
		return 1 - unstoppable * unstoppableReductionDecrease;
	}

	public float GetArmorless()
	{
		return 1 - armorless * armorlessArmorDecrease;
	}

	public float GetLoveandPeace()
	{
		if(loveandpeace <= 0)
			return 1;
		else
			return (1 + loveandpeace) * loveandpeaceFuryDecrease;
	}

	public int GetEZMode()
	{
		if(ezmodeUses >= (ezmode * ezmodeQuantityDecrease))
			return 0;
		else
		{
			ezmodeUses++;
			return 1;
		}
	}

	public float GetEZModeUses()
	{
		return ezmodeUses - (ezmode * ezmodeQuantityDecrease);
	}

	public void ResetEZMode()
	{
		ezmodeUses = 0;
	}

	public int GetRealDebuff()
	{
		if(realdebuffUses >= (realdebuff * realdebuffLevelDecrease))
			return 0;
		else
		{
			realdebuffUses++;
			return 1;
		}
	}
	
	public void ResetRealDebuff()
	{
		realdebuffUses = 0;
	}

	public int GetDFENCE()
	{
		return dfence * dfenceArmorIncrease;
	}

	public float GetShield()
	{
		return shield * shieldInvulnerabilityIncrease;
	}

	public float GetSlowmo()
	{
		return slowmo * slowmoSlowdownIncrease;
	}
	
	public void UseSlowmo()
	{
		if(slowmo > 0)
		{
			slowmoCooldown = GetSlowmo ();
			Time.timeScale = slowmoTimeValue;
		}
	}

	public float GetBlinky()
	{
		return blinky * blinkyDurationIncrease;
	}

	public bool UseTemp()
	{
		if(tempHitCount >= temp * tempHitQuantityAbsorbed)
			return false;
		tempHitCount++;
		return true;
	}

	public void ResetTempHitCount()
	{
		tempHitCount = 0;
	}

	public float GetOnlyOncePerRoom()
	{
		if(onlyonceperroomActivated)
			return onlyonceperroom * onlyonceperroomCooldownDecrease;
		else
			return 0;
	}

	public void StartOnlyOncePerRoom()
	{
		onlyonceperroomActivated = true;
	}

	public void ResetOnlyOncePerRoom()
	{
		onlyonceperroomActivated = false;
	}

	public void OnPlayerShooting(GameObject weapon)
	{
		shootsExecutedInTheCurrentRoom++;
		if(weapon.GetComponent<WeaponController>().IsWeaponCrush ())
	    {
			savedHitenMitsurugiTimer = currentHitenmitsurugiTimerToActivate;
			currentHitenmitsurugiTimerToActivate = hitenmitsurugiTimerToActivate;
		}
		else
		{
			if(armageddon > 0)
				gameController.HitAllMonster (GetArmageddon());
			RollD20();
		}
	}

	public void OnPlayerDefending()
	{
		if(GetShield () > 0 || parry > 0)
		{
			PlayerHitController playerHitController = ((PlayerHitController) FindObjectOfType(typeof(PlayerHitController)));
			if(GetShield () > 0)
				playerHitController.SendMessage ("SetcurrentBlinkDuration",GetShield ());
			if(parry > 0)
				playerHitController.SendMessage ("SetParry",parryTime);
		}
		UseSlowmo ();
	}

	public void LevelUp()
	{
		InterfaceController.Instance.LevelUp ();
	}
}
