using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {
	
	public enum buttonTarget{
		UI,
		gamecontroller
	}
	
	public enum parameterType{
		NoParameter,
		Int,
		Float,
		String,
		Bool
	}
	
	public enum buttonType{
		Press,
		Switch
	}
	
	public buttonType TypeOfButton;
	public parameterType typeOfParameter;
	public buttonTarget target = buttonTarget.UI;
	public string message;
	public int intParameter;
	public float floatParamenter;
	public string stringParameter;
	public bool boolParameter;
	
	//Material pra usar se o tipo do button for Press
	public Material MaterialNormal;
	public Material MaterialPressed;
	
	//Material pra usar se o tipo do button for Switch
	public Material MaterialOn;
	public Material MaterialOff;
	
	//Time to remain pressed
	public float UnpressDelay = 0.13f;
	
	//Timer to count how long ago the button has been pressed and then released
	private float delay = 0.0f;
	bool switchOn = true;
	private GameController gameController;
	InterfaceController UI;
	// Use this for initialization
	void Start () {
		gameObject.tag = "Button";
		gameController = GameController.Instance;
		UI = InterfaceController.Instance;
	}
	
	// Update is called once per frame
	public void UpdateButtonController (){			
		switch(TypeOfButton)
		{
		case buttonType.Press:
			UpdateButtonPress();
			break;
		case buttonType.Switch:
			UpdateButtonSwitch();
			break;
		}
	}
	
	public void UpdateButtonPress (){	
		if(delay > 0)
		{
			delay -= GameController.deltaTime;
			if(delay < 0)
			{
				delay = 0;
				this.GetComponent<Renderer>().material = MaterialNormal;
				switch(target)
				{
				case buttonTarget.gamecontroller:					
				default:		
					SendThisMessage(typeOfParameter,gameController.gameObject);
					break;
				case buttonTarget.UI:
					SendThisMessage(typeOfParameter,UI.gameObject);
					break;
				}
			}
		}
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			int layermask = 1000;
			if(Physics.Raycast(ray, out hit, 10000, layermask)){
				if(hit.collider.transform == this.transform)
				{
					delay = UnpressDelay;
					this.GetComponent<Renderer>().material = MaterialPressed;				
				}
			}
		}
	}
	
	public void UpdateButtonSwitch()
	{
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			int layermask = 1000;
			if(Physics.Raycast(ray, out hit, 10000, layermask)){
				if(hit.collider.transform != this.transform)
					return;
				switchOn = !switchOn;
				if(switchOn)
					this.GetComponent<Renderer>().material = MaterialOn;
				else
					this.GetComponent<Renderer>().material = MaterialOff;
				switch(target)
				{
				case buttonTarget.gamecontroller:					
				default:		
					SendThisMessage(typeOfParameter,gameController.gameObject);
					break;
				case buttonTarget.UI:
					SendThisMessage(typeOfParameter,UI.gameObject);
					break;
				}
			}
		}
	}
	
	void SendThisMessage(parameterType paramType, GameObject currentTarget)
	{		
		switch(typeOfParameter)
		{
		case parameterType.NoParameter:
		default:
			currentTarget.SendMessage(message);
			break;
		case parameterType.Int:
			currentTarget.SendMessage(message,intParameter);
			break;
		case parameterType.Float:
			currentTarget.SendMessage(message,floatParamenter);
			break;
		case parameterType.String:
			currentTarget.SendMessage(message,stringParameter);
			break;
		case parameterType.Bool:
			currentTarget.SendMessage(message,boolParameter);
			break;
		}		
	}
		
	void OnEnable()
	{
		if(TypeOfButton == buttonType.Press)
			this.GetComponent<Renderer>().material = MaterialNormal;
	}
	
	public void SwitchMaterial(bool on)
	{
		switchOn = on;
		if(switchOn)
			this.GetComponent<Renderer>().material = MaterialOn;
		else
			this.GetComponent<Renderer>().material = MaterialOff;
	}
}
