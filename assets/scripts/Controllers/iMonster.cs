﻿using UnityEngine;
using System.Collections;



public interface iMonster {
	void SetToughness(int level);
	void DestroyItSelf();
}



public class MnstMsgs {
	public const string SetToughness = "SetToughness";
	public const string DestroyItself = "DestroyItSelf";
	public const string SetDirection = "SetDirection";
	public const string SetLifeTime = "SetLifeTime";
}