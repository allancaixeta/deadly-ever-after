using UnityEngine;
using System.Collections;

public class SimpleSwitchDualButton : MonoBehaviour {
	
	//Material pra usar se o tipo do button for Press
	public Material MaterialOn; // On == down == pressed
	public Material MaterialOff;// Of == up == unpressed
		
	//Objeto alvo para onde voce vai mandar uma mensagem quando o button for apertado
	public GameObject target;
	
	//Nome da função a ser chamada quando o button for apertado
	public string message1;
	public string message2;
	Texture original;
	public int valor;
	// Use this for initialization
	void Start () {
		//gameObject.tag = "Button";  O ideal é que a interfaces esteja em um layer e tag unico dela
		original = MaterialOff.mainTexture;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			int layermask = 1000;
			if(Physics.Raycast(ray, out hit, 10000, layermask)){
				if(hit.collider.transform != this.transform)
					return;
				//this.renderer.material = MaterialOn;
				target.SendMessage(message1,valor);

			}
		}
		if(Input.GetMouseButtonDown(1))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			int layermask = 1000;
			if(Physics.Raycast(ray, out hit, 10000, layermask)){
				if(hit.collider.transform != this.transform)
					return;
				//this.renderer.material = MaterialOn;
				target.SendMessage(message2,valor);
			}
		}
	}
	
	public void TurnOff()
	{
		this.GetComponent<Renderer>().material = MaterialOff;
	}

	public void TurnOn()
	{
		this.GetComponent<Renderer>().material = MaterialOn;
	}

	public void UpAppearence(Texture2D t)
	{
		MaterialOn.mainTexture = t;
	}
	
	public void DownAppearence(Texture2D t)
	{
		MaterialOff.mainTexture = t;
	}

	void OnDestroy()
	{
		MaterialOff.mainTexture = original;
	}
	
}
