using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public struct LifeCards
{
	public int currentCard;
	public int currentSlot;
	public bool losingLife;
}

#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.

[Prefab("InterfaceController")]
public class InterfaceController : Singleton<InterfaceController>
{	
	
	public enum interfaceControllerStates{
		VOID,
		menu,
		stage,
		pause,
        tutorial
	}
	
	public enum menuStates{
		main_menu,
		pause_menu,
		options_menu,
		gamma_menu,
		inventory_menu,
	}
	
	public int weaponInterfaceDisplayCapacity, skillInterfaceDisplayCapacity, monsterInterfaceDisplayCapacity;
	
	public interfaceControllerStates nextState = interfaceControllerStates.stage;
	public interfaceControllerStates saveState;
	public Material transparent, leftslot, rightslot, defenseslot;
	
	public Material emptySlot;
	public AudioClip DoubleWeaponErrorSound;
	public Renderer greenWeaponBarCrushHUD, grayWeaponBarCrushHUD, greenWeaponBarAOEHUD, grayWeaponBarAOEHUD, greenWeaponBarDefenseHUD, grayWeaponBarDefenseHUD;
	public Animator ringCrush, ringAOE, ringDefense;

	private interfaceControllerStates currentState = interfaceControllerStates.VOID;
	interfaceControllerStates lastState;
	private List<WeaponController> weaponList;
	List<LifeCards> lifeListToConsume;
	private SkillController skillcontroller;
	PlayerController player;
	GameController gameController;
	private Transform initialPosWeapon, finalPosWeapon, initialPosSkill, finalPosSkill;
	GameObject mainMenu, weaponInterface, skillInterface, slotInterface, pauseMenu, menuOptions, GammaSLIDER, switchCountDown, HUD, tabelaacertos, restart;
	// Use this for initialization
	GameObject []tempListSkill;
	GameObject []tempListWeapon;
	int tempListSize;
	WeaponController [] newSlotList = new WeaponController [3];

	CursorController currentCursor;
	float saveGamma;
	menuStates currentMenuState;
	Vector2 FillBarTileMatrix;	
	public TextMesh weaponRight, weaponLeft, weaponRight2, weaponLeft2, weaponRight3, weaponLeft3, weaponDefense;
	public Text nomes, level, dano;
	public GameObject skillsSelection;
	public Image skillCrush, skillAOE, skillDefense;
	SkillController.Skills potentialSkillCrush, potentialSkillAOE, potentialSkillDefense;   
	GameObject [][]rooms = new GameObject[8][];
	GameObject selectedCurrentRoom, treasureRoom, bossRoom;
	Vector2 []roomPosition = new Vector2[12];
	int treasureRoomNumber, bossRoomNumber;

	Animator []lifeSlots = new Animator[10];
	Animator []lifeCards = new Animator[13];
	int currentLifeSlot, currentLifeCard;

	public Text skilldescription;
	string []skillsDescriptions = new string[3];

	const float shootNowBlinkTime = 1;

	public Transform timingRing;
	public SpriteRenderer wrongTiming;
    GameObject mapInterface, weaponSlot;
	void Awake () {
		gameController = GameController.Instance;
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		currentCursor = CursorController.Instance;
		weaponList = new List<WeaponController>();
		lifeListToConsume = new List<LifeCards>();
		skillcontroller = (SkillController) FindObjectOfType(typeof(SkillController));
        mainMenu = GameObject.Find("menu");
        mainMenu.SetActive(false);
        pauseMenu = GameObject.Find("pauseMenu");
			
		menuOptions = GameObject.Find("menuOptions");
	
		GammaSLIDER = GameObject.Find("GammaSLIDER");

		switchCountDown = GameObject.Find("switchCountDown");

		HUD = GameObject.Find("HUD");

		if(weaponInterfaceDisplayCapacity < 1)
			weaponInterfaceDisplayCapacity = 28;
		if(skillInterfaceDisplayCapacity < 1)
			skillInterfaceDisplayCapacity = 14;
		if(monsterInterfaceDisplayCapacity < 1)
			monsterInterfaceDisplayCapacity = 21;
		for (int i = 0; i < 8; i++) {
			rooms[i] = new GameObject[8];
		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				string s = "r"+(i+1).ToString()+(j+1).ToString();
				rooms[i][j] = GameObject.Find(s);
			}	
		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				rooms[i][j].SetActive (false);
			}	
		}
		selectedCurrentRoom = GameObject.Find("selected");
		treasureRoom = GameObject.Find("treasureRoom");
		bossRoom = GameObject.Find("bossRoom");
		treasureRoom.SetActive (false);
		bossRoom.SetActive (false);

		tabelaacertos = GameObject.Find("tabelaacertos");
		tabelaacertos.SetActive (false);
        restart = GameObject.Find("restart");
        restart.SetActive(false);

        nomes.text = "";
		level.text = "";
		dano.text = "";

		for (int i = 0; i < 10; i++) {
			lifeSlots[i] = GameObject.Find("lifeslot"+(i+1).ToString()).GetComponent<Animator>();
		}

		for (int i = 0; i < 13; i++) {
			lifeCards[i] = GameObject.Find("lifepoint"+(i+1).ToString()).GetComponent<Animator>();
		}

	}
	//chamada apos o jogo iniciar e os objetos serem carregados
	//desativa objetos que precisam ser desativados
	public void GameStarted()
	{
		pauseMenu.SetActive (false);
		menuOptions.SetActive (false);
		GammaSLIDER.SetActive(false);
		//HUD.SetActive(false);
	}
	
	// Update is called once per frame
	public void UpdateInterfaceController () {
		if(!StartState())
			UpdateState();
	}
	
	bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
            case interfaceControllerStates.menu:
                mainMenu.SetActive(true);
                pauseMenu.SetActive(false);
                menuOptions.SetActive(false);
                GammaSLIDER.SetActive(false);
                break;

            case interfaceControllerStates.stage:			
			    CleanWeapons();
			    HUD.SetActive(true);
                mainMenu.SetActive(false);
                break;

		    case interfaceControllerStates.pause:			
			    saveState = lastState;
			    pauseMenu.SetActive(true);
			    HUD.SetActive(false);
			    currentMenuState = menuStates.pause_menu;
			    break;

            case interfaceControllerStates.tutorial:
                pauseMenu.SetActive(false);
                menuOptions.SetActive(false);
                GammaSLIDER.SetActive(false);
                HUD.SetActive(false);
                break;
		}
		return true;
	}
	public float aimXadjust, aimYadjust;
	void UpdateState()
	{
		switch(currentState)
		{
		case interfaceControllerStates.stage:
			/*consuming life list*/
			if(lifeListToConsume.Count > 0)
			{
				if(!consumingLifeList)
				{
					if(lifeListToConsume[0].losingLife)
						StartCoroutine (AnimateLoseLife(lifeListToConsume[0].currentSlot,lifeListToConsume[0].currentCard));
					else
						StartCoroutine (AnimateGainingLife(lifeListToConsume[0].currentSlot,lifeListToConsume[0].currentCard));
				}
			}


			break;
			
		case interfaceControllerStates.pause:			
			
			break;
		}
	}
	
	public void AddWeapon(WeaponController weapon)
	{
		weaponList.Add(weapon);
	}
	
	void MountInterface()
	{
		/*MountSlotList();
		MountSkillList();
		MountWeaponList();	*/	
		//MountEquipList();
	}

	public WeaponController GetWeapon(int n)
	{
		return weaponList[n];	
	}
	
	public void OpenOptions()
	{		
		menuOptions.SetActive(true);
		currentMenuState = menuStates.options_menu;
	}
	
	public void OpenGamma()
	{		
		this.saveGamma = GammaSLIDER.transform.FindChild("GuiSLIDER").GetComponent<GUISlider>().GammaCorrection;
		GammaSLIDER.SetActive(true);
		currentMenuState = menuStates.gamma_menu;
	}
	
	public void CloseOptions()
	{
		menuOptions.SetActive(false);
		currentMenuState = menuStates.pause_menu;
	}
	
	public void CloseGammaOptions(bool OK)
	{
		if(OK)
			gameController.SetGamma();
		else
		{
			GammaSLIDER.transform.FindChild("GuiSLIDER").GetComponent<GUISlider>().GammaCorrection = saveGamma;
			gameController.SetGamma(saveGamma);
		}
		GammaSLIDER.SetActive(false);	
		currentMenuState = menuStates.options_menu;
	}
		
	public void OpenHUD()
	{		
		HUD.SetActive(true);
	}
	
	public void CloseHUD()
	{		
		HUD.SetActive(false);
	}
	
	public void SwitchMaterial(string switcher, bool on)
	{
		switch(switcher)
		{		
		case "countDown":
			switchCountDown.SendMessage("SwitchMaterial",on);
			break;
		}
	}
	
	public menuStates GetMenuState()
	{
		return currentMenuState;	
	}
	
	public void Unpause()
	{
		pauseMenu.SetActive(false);	
	}

	public void StartLife(int numberOfSlots)
	{
		for (int i = 0; i < lifeSlots.Length; i++) {
			if(i < numberOfSlots)
				lifeSlots[i].SetTrigger ("On");
			else
				lifeSlots[i].SetTrigger ("Off");
		}
		currentLifeSlot = numberOfSlots;
		currentLifeCard = PlayerController.lifeContainerSize;
	}

	public void IncreaseLife()
	{
		lifeSlots[currentLifeSlot].SetTrigger ("On");
		currentLifeSlot++;
	}

	bool consumingLifeList = false;
	public void LoseLife(int currentSlot, int currentCard)
	{
		LifeCards lc;
		lc.currentSlot = currentSlot;
		lc.currentCard = currentCard;
		lc.losingLife = true;
		lifeListToConsume.Add (lc);
	}

	public IEnumerator AnimateLoseLife(int currentSlot, int currentCard)
	{
		consumingLifeList = true;
		//Debug.Log ("Consuming: "+currentSlot.ToString ()+" "+currentCard.ToString());
		if(currentSlot < currentLifeSlot || currentCard == 0)
		{
			for (int i = currentLifeCard; i > 0; i--) {
				lifeCards[i-1].SetBool ("showing",false);
				yield return new WaitForSeconds(0.1f);
			}
			if(currentCard == 0)
			{
				currentSlot--;
				currentCard = PlayerController.lifeContainerSize;
			}
			for (int i = currentLifeSlot; i > currentSlot; i--) {
				lifeSlots[i-1].SetTrigger ("Off");
			}

			for (int i = 1; i <= currentCard; i++)
			{
				lifeCards[i-1].SetTrigger ("recover");
				yield return new WaitForSeconds(0.1f);
				lifeCards[i-1].SetBool ("showing",true);
			}
			currentLifeSlot = currentSlot;
		}
		else
		{
			for (int i = currentLifeCard; i > currentCard; i--) {
				lifeCards[i-1].SetBool ("showing",false);
				yield return new WaitForSeconds(0.14f);
			}						
		}
		currentLifeCard = currentCard;
		lifeListToConsume.RemoveAt (0);
		consumingLifeList = false;
		yield return null;
	}

	public void GainLife(int currentSlot, int currentCard)
	{
		LifeCards lc;
		lc.currentSlot = currentSlot;
		lc.currentCard = currentCard;
		lc.losingLife = false;
		lifeListToConsume.Add (lc);
	}

	public IEnumerator AnimateGainingLife(int currentSlot, int currentCard)
	{
		consumingLifeList = true;
		Debug.Log ("Consuming: "+currentSlot.ToString ()+" "+currentCard.ToString());
		if(currentSlot > currentLifeSlot || currentCard == 0)
		{
			for (int i = currentLifeCard; i > 0; i--) {
				lifeCards[i-1].SetBool ("showing",false);
				yield return new WaitForSeconds(0.1f);
			}
			if(currentCard == 0)
			{
				currentSlot++;
				currentCard = PlayerController.lifeContainerSize;
			}
			for (int i = currentLifeSlot; i > currentSlot; i--) {
				lifeSlots[i-1].SetTrigger ("On");
			}
			
			for (int i = 1; i <= currentCard; i++)
			{
				lifeCards[i-1].SetTrigger ("recover");
				yield return new WaitForSeconds(0.1f);
				lifeCards[i-1].SetBool ("showing",true);
			}
			currentLifeSlot = currentSlot;
		}
		else
		{
			for (int i = currentLifeCard; i > currentCard; i--) {
				lifeCards[i-1].SetBool ("showing",true);
				yield return new WaitForSeconds(0.14f);
			}						
		}
		currentLifeCard = currentCard;
		lifeListToConsume.RemoveAt (0);
		consumingLifeList = false;
		yield return null;
	}

	public void UpdateCooldown(int side, float cooldown)
	{		
		switch(side)
		{
		default:
		case 0:
			weaponLeft.text = "";			
			grayWeaponBarCrushHUD.gameObject.SetActive (true);
			grayWeaponBarCrushHUD.material.SetFloat("_Cutoff",1-cooldown);			
			greenWeaponBarCrushHUD.gameObject.SetActive (false);
			ringCrush.SetBool ("cooldown",true);
			timingRing.gameObject.SetActive (false);
			break;
		case  1:
			weaponRight.text = "";			
			grayWeaponBarAOEHUD.gameObject.SetActive (true);
			grayWeaponBarAOEHUD.material.SetFloat("_Cutoff",1-cooldown);
			greenWeaponBarAOEHUD.gameObject.SetActive (false);
			ringAOE.SetBool ("cooldown",true);
			break;
		case 2:
			weaponDefense.text = "";
			grayWeaponBarDefenseHUD.gameObject.SetActive (true);
			grayWeaponBarDefenseHUD.material.SetFloat("_Cutoff",1-cooldown);
			greenWeaponBarDefenseHUD.gameObject.SetActive (false);
			ringDefense.SetBool ("cooldown",true);
			break;
		}
	}

	public void InterfaceCharge(int side, float charge)
	{
		charge = Mathf.Clamp01 (charge);
		if(charge == 0)
			return;
		if(side == 0)
		{			
			greenWeaponBarCrushHUD.gameObject.SetActive (true);
			greenWeaponBarCrushHUD.material.SetFloat("_Cutoff",charge);
			weaponLeft2.text = "";
		}
		else
		{
			weaponRight2.text = "";
		}
	}

	public void InterfaceCharged(int side)
	{
		if(side == 0)
		{
			ringCrush.SetTrigger ("glow");
		}
		else
		{
			ringAOE.SetTrigger ("glow");
		}
	}

	public void InterfaceTimingRing(float time)
	{
		timingRing.gameObject.SetActive (true);
		timingRing.localScale = Vector3.one * 2;
		iTween.ScaleTo (timingRing.gameObject, Vector3.one, time);
	}

	public void InterfaceTimingRingStop()
	{
		timingRing.gameObject.SetActive (false);
		wrongTiming.transform.localScale = Vector3.zero;
		iTween.ScaleTo (wrongTiming.gameObject, Vector3.one * 3, 1);
		iTween.ColorTo (wrongTiming.gameObject, Color.clear, 1);
	}

	public void InterfaceShootNow(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft.text = "Time to Shoot: " + attackspeed.ToString ("0.0");
			if(attackspeed > shootNowBlinkTime)
			{
				ringCrush.SetBool ("blink",false);
			}
			else
			{
				ringCrush.SetBool ("blink",true);
			}
		}
		else
		{
			weaponRight.text = "Time to Shoot: " + attackspeed.ToString ("0.0");
			if(attackspeed > shootNowBlinkTime)
			{
				ringAOE.SetBool ("blink",false);
			}
			else
			{
				ringAOE.SetBool ("blink",true);
			}
		}
	}

	public void FinishCooldown(int side)
	{
		switch(side)
		{
		default:
		case 0:
			ringCrush.SetTrigger ("normal");
			ringCrush.SetBool ("cooldown",false);
			break;
		case 1:
			ringAOE.SetTrigger ("normal");
			ringAOE.SetBool ("cooldown",false);
			break;
		case 2:
			ringDefense.SetTrigger ("normal");
			ringDefense.SetBool ("cooldown",false);
			break;
		}
		ResetInterface (side);
	}

	public void ResetInterface(int side)
	{
		switch(side)
		{
		default:
		case 0:
			weaponLeft.text = "";
			weaponLeft2.text = "";
			weaponLeft3.text = "";			
			grayWeaponBarCrushHUD.material.SetFloat("_Cutoff",1);
			grayWeaponBarCrushHUD.gameObject.SetActive (false);			
			greenWeaponBarCrushHUD.material.SetFloat("_Cutoff",1);
			ringCrush.SetTrigger ("normal");
			break;
		case 1:
			weaponRight.text = "";
			weaponRight2.text = "";
			weaponRight3.text = "";			
			grayWeaponBarAOEHUD.material.SetFloat("_Cutoff",1);
			grayWeaponBarAOEHUD.gameObject.SetActive (false);			
			greenWeaponBarAOEHUD.material.SetFloat("_Cutoff",1);
			ringAOE.SetTrigger ("normal");
			break;
		case 2:
			weaponDefense.text = "";
			grayWeaponBarDefenseHUD.material.SetFloat("_Cutoff",1);
			grayWeaponBarDefenseHUD.gameObject.SetActive (false);
			greenWeaponBarDefenseHUD.material.SetFloat("_Cutoff",1);
			ringDefense.SetTrigger ("normal");
			break;
		}
	}

	public void InterfaceThreeShot(int side, float threeshotErrorTime)
	{
		if(side == 0)
		{
			weaponLeft2.text = "Combo: " + threeshotErrorTime.ToString ("0.0");
		}
		else
		{
			weaponRight2.text = "Combo: " + threeshotErrorTime.ToString ("0.0");
		}
	}
	
	public void InterfaceBoomerangGoing(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft.text = "Going: " + attackspeed.ToString ("0.0");
		}
		else
		{
			weaponRight.text = "Going: " + attackspeed.ToString ("0.0");
		}
	}
	
	public void InterfaceHold(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft2.text = "Hold: " + attackspeed.ToString ("0.0");
		}
		else
		{
			weaponRight2.text = "Hold: " + attackspeed.ToString ("0.0");
		}
	}
	
	public void InterfaceSwing(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft2.text = "AttackSpeed: " + attackspeed.ToString ("0.0");
		}
		else
		{
			weaponRight2.text = "AttackSpeed: " + attackspeed.ToString ("0.0");
		}
	}

	public void InterfaceVoidRay(int side, float maxHold, float currentHold)
	{
		if(side == 0)
		{
			weaponLeft2.text = "Max Hold: " + maxHold.ToString ("0.0");
			weaponLeft3.text = "Current Hold: " + currentHold.ToString ("0.0");
		}
		else
		{
			weaponRight2.text = "Max Hold: " + maxHold.ToString ("0.0");
			weaponRight3.text = "Current Hold: " + currentHold.ToString ("0.0");
		}
	}

	public void VoidRayChargeBar(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft.text = "Carga: " + (attackspeed).ToString ("0.0");
		}
		else
		{
			weaponRight.text = "Carga: " + (attackspeed).ToString ("0.0");
		}
	}

	public void InterfacePortalGun(int side, float attackspeed)
	{
		weaponDefense.text = "Teleport: " + attackspeed.ToString ("0.0");
	}

	public void InterfaceTimeBubble(int side, float attackspeed)
	{
		weaponDefense.text = "Duration: " + attackspeed.ToString ("0.0");
	}

	public void InterfaceHammerStun(int side, float attackspeed)
	{
		weaponDefense.text = "Stun: " + attackspeed.ToString ("0.0");
	}

	public void InterfaceGrabChain(float attackspeed)
	{
		weaponDefense.text = "Holding Limit: " + attackspeed.ToString ("0.0");
	}

	public void InterfaceMouseBreaker(int side, int attackspeed)
	{
		if(side == 0)
		{
			weaponLeft.text = "Clicks: " + attackspeed.ToString ();
		}
		else
		{
			weaponRight.text = "Clicks: " + attackspeed.ToString ();
		}
	}

	public void InterfaceMouseBreakerHold(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft.text = "Hold: " + attackspeed.ToString ("0.0");
		}
		else
		{
			weaponRight.text = "Hold: " + attackspeed.ToString ("0.0");
		}
	}



	public void InterfaceProtonCannon(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft.text = "Damage: " + attackspeed.ToString ("0.0");
		}
		else
		{
			weaponRight.text = "Damage: " + attackspeed.ToString ("0.0");
		}
	}

	public void SoulChargeCount(int side, int count)
	{
		if(side == 0)
		{
			weaponLeft.text = "Souls: " + count.ToString ();
		}
		else
		{
			weaponRight.text = "Souls: " + count.ToString ();
		}
	}

	public void SoulChargeFury(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft2.text = "Fury timer: " + attackspeed.ToString ("0.0");
		}
		else
		{
			weaponRight2.text = "Fury timer: " + attackspeed.ToString ("0.0");
		}
	}

	public void InterfaceHomeMissile(int side, float attackspeed)
	{
		if(side == 0)
		{
			weaponLeft2.text = "Damage: " + attackspeed.ToString ("0.0");
		}
		else
		{
			weaponRight2.text = "Damage: " + attackspeed.ToString ("0.0");
		}
	}


	
	public void CleanWeapons()
	{
		weaponList = new List<WeaponController>();
		newSlotList = new WeaponController [3];
		SetWeapons ();
	}

	public void SetWeapons()
	{
		newSlotList[0] = gameController.GetCrush();
		newSlotList [1] = gameController.GetAOE ();
		newSlotList[2] = gameController.GetDefense();
		player.UpdateWeaponSlot(newSlotList);
	}	
	
	public void VisualWeapon(int n, Texture2D m, Texture2D t)
	{
		/*UIWeapon[n].renderer.material.mainTexture = m;
		UIWeapon[n].GetComponent<SimpleSwitchDualButton>().MaterialOn.mainTexture = t;
		UIWeapon[n].GetComponent<SimpleSwitchDualButton>().MaterialOff.mainTexture = m;*/
	}

	public void MiniMapFirstRoom(int x, int y)
	{
		roomPosition[0] = new Vector2(x,y);
		rooms[x-1][y-1].SetActive (true);
		selectedCurrentRoom.transform.position = rooms[x-1][y-1].transform.position + Vector3.back *0.1f;  
		treasureRoom.SetActive (false);
		bossRoom.SetActive (false);
	}

    /*public void SetRoomPosFromDoor(int roomNumber, int doorSide, int roomDestination)
	{
		roomDestination--;//pega o numero da sala e diminue 1 para entrar na posiçao certa do vetor
		if(roomPosition[roomDestination] != Vector2.zero)
			return;
		int x = (int)roomPosition[roomNumber].x;
		int y = (int)roomPosition[roomNumber].y;
        print("Sala:" + roomNumber + "(" + x +" "+ y +")");        
        switch (doorSide)
		{
		default:
		case 1:
			x--;
			break;
		case 2:
			y--;
			break;
		case 3:
			x++;
			break;
		case 4:
			y++;
			break;
		}
		roomPosition[roomDestination] = new Vector2(x,y);
        if(roomDestination == 8)
        {
            print(x);
            print(y);
        }
	}*/

    public void SetRoomPos(int roomNumber, float x, float y)
    {
        roomPosition[roomNumber] = new Vector2(x, y);
    }


    public void ChangeSelectedRoom(int nextRoomNumber)
	{
		int x = (int)roomPosition[nextRoomNumber].x;
		int y = (int)roomPosition[nextRoomNumber].y;
		selectedCurrentRoom.transform.position = rooms[x-1][y-1].transform.position + Vector3.back *0.1f; 

	}

	public void SetNeighboursRooms(int roomDestination)
	{
		roomDestination--;//pega o numero da sala e diminue 1 para entrar na posiçao certa do vetor
		int x = (int)roomPosition[roomDestination].x;
		int y = (int)roomPosition[roomDestination].y;
		rooms[x-1][y-1].SetActive (true);
		if(roomDestination == treasureRoomNumber)
		{
			treasureRoom.transform.position = rooms[x-1][y-1].transform.position + Vector3.back *0.1f; 
			treasureRoom.SetActive (true);
		}
		else if(roomDestination == bossRoomNumber)
			{
				bossRoom.transform.position = rooms[x-1][y-1].transform.position + Vector3.back *0.1f; 
				bossRoom.SetActive (true);
			}
	}

	public void SetTreasureRoom(int roomNumber)
	{
		treasureRoomNumber = roomNumber;
	}

	public void SetBossRoom(int roomNumber)
	{
		bossRoomNumber = roomNumber;
	}

	public void OnPlayerHit(string n, string l, string d)
	{
		nomes.text += n + '\n';
		level.text += l + '\n';
		dano.text += d + '\n';
	}

	public void GameOver()
	{
		tabelaacertos.SetActive (true);
        restart.SetActive(true);
        HUD.SetActive (false);
	}

	public void LevelUp()
	{
		//levelUp.SetActive (true);
		skillsSelection.SetActive (true);
		potentialSkillCrush = skillcontroller.RaffleSkillCrush(SkillController.LevelUpMode.normal);
		if(potentialSkillCrush == SkillController.Skills.notassigned || skillcontroller.IsMaxLevel(potentialSkillCrush))
		{
			skillCrush.gameObject.SetActive (false);
		}
		else
		{	
			skillCrush.gameObject.SetActive (true);
			//skillCrush.SendMessage ("SetDescription",skillcontroller.GetSkillDescription (potentialSkillCrush));
			skillsDescriptions[0] = skillcontroller.GetSkillDescription (potentialSkillCrush);
			skillCrush.sprite = skillcontroller.GetSkillAppearance (potentialSkillCrush);
		}	
		potentialSkillAOE = skillcontroller.RaffleSkillAOE(SkillController.LevelUpMode.normal);
		if(potentialSkillAOE == SkillController.Skills.notassigned || skillcontroller.IsMaxLevel(potentialSkillAOE))
		{
			skillAOE.gameObject.SetActive (false);
		}
		else
		{
			//skillAOE.SendMessage ("SetDescription",skillcontroller.GetSkillDescription (potentialSkillAOE));
			skillsDescriptions[1] = skillcontroller.GetSkillDescription (potentialSkillAOE);
			skillAOE.sprite = skillcontroller.GetSkillAppearance (potentialSkillAOE);
		}
		potentialSkillDefense = skillcontroller.RaffleSkillDefense(SkillController.LevelUpMode.normal);
		if(potentialSkillDefense == SkillController.Skills.notassigned || skillcontroller.IsMaxLevel(potentialSkillDefense))
		{
			skillDefense.gameObject.SetActive (false);
		}
		else
		{
			//skillDefense.SendMessage ("SetDescription",skillcontroller.GetSkillDescription (potentialSkillDefense));
			skillsDescriptions[2] = skillcontroller.GetSkillDescription (potentialSkillDefense);
			skillDefense.sprite = skillcontroller.GetSkillAppearance (potentialSkillDefense);
		}
		/*Debug.Log (potentialSkillCrush);
		Debug.Log (potentialSkillAOE);
		Debug.Log (potentialSkillDefense);*/
	}

	public void ChooseSkill(int s)
	{
		switch(s)
		{
		case 0:
			skillcontroller.IncreaseSkillCrush (potentialSkillCrush,SkillController.LevelUpMode.normal);
			break;
		case 1:
			skillcontroller.IncreaseSkillAOE (potentialSkillAOE,SkillController.LevelUpMode.normal);
			break;
		case 2:
			skillcontroller.IncreaseSkillDefense (potentialSkillDefense,SkillController.LevelUpMode.normal);
			break;
		}
		skillsSelection.SetActive (false);
	}

	public void SetSkillDescription(int s)
	{
		skilldescription.text  = skillsDescriptions[s];
	}

	public void CancelSkillSelection()
	{
		skillsSelection.SetActive (false);
	}

	public void ResizeHUD(float s)
	{
		HUD.transform.localScale = Vector3.one * s;
	}

    public void TutorialTurnOnLife()
    {
        HUD.SetActive(true);
        mapInterface = HUD.transform.Find("mapInterface").gameObject;
        weaponSlot = HUD.transform.Find("weaponSlot").gameObject;
        mapInterface.SetActive(false);
        weaponSlot.SetActive(false);
    }

    public void TutorialTurnOnCrushWeapon()
    {   
        weaponSlot.SetActive(true);
        weaponSlot.transform.Find("GUI_Cooldown_Rings/GUIringAOE").gameObject.SetActive(false);
        weaponSlot.transform.Find("GUI_Cooldown_Rings/GUIringdefense").gameObject.SetActive(false);
        CleanWeapons();
        player.Reset();
    }

    public void TutorialTurnOnAOEWeapon()
    {
        weaponSlot.SetActive(true);
        weaponSlot.transform.Find("GUI_Cooldown_Rings/GUIringAOE").gameObject.SetActive(true);
        CleanWeapons();
        player.Reset();
    }

    public void TutorialTurnOnMap()
    {
        mapInterface.SetActive(true);
    }
}