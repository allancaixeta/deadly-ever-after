﻿using UnityEngine;
using System.Collections;

public abstract class BossRoomController : RoomController {

	protected PlayerController player;

	public enum bossState{
		wait,
		start,
		fight,
		finish
	}

	public enum monsterPhase
	{
		phase1=0,
		phase2=1,
		phase3=2
	}

	protected bossState currentState, lastState;
	public bossState nextState;
	protected monsterPhase phase;
	// Update is called once per frame
	public abstract bool StartState ();
	
	public abstract void UpdateState();	

	// Update is called once per frame
	public void UpdateBossRoom ()
	{
		if(!StartState())
			UpdateState();
	}

	public void SetStage(StageController.StageNumber s)
	{
		currentStage = s;
	}

	public void SetPhase(monsterPhase p)
	{
		phase = p;
	}
}
