﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public abstract class EventController : MonoBehaviour {

	protected bool dontUsePositiveEffect;

	public enum eventlvl
	{
		one,
		two,
		three
	}	

	public enum sides
	{
		left = 0,
		right = 1,
		both = 2,
		ontop =3
	}

	public string eventName;
	public eventlvl level = eventlvl.one;
	public float damage = 1;
	public int monsterDamage = 5;

	protected Transform cenarioLimits;
	protected RoomController room;
	protected SkillController skills;

	public void StartEvent (StageController.StageNumber stage) {
		room = (RoomController) FindObjectOfType(typeof(RoomController));	
		skills = (SkillController) FindObjectOfType(typeof(SkillController));
		cenarioLimits = room.GetCenarioLImits ();
		SpecificEventStart (stage);
	}

	public abstract void SpecificEventStart(StageController.StageNumber stage);

	public void StopEvent()
	{
		if(this)
			Destroy (this.gameObject);
	}

	public void DestroyItSelf() //also remove from gamecontroller eventList
	{
		GameController gameController = GameController.Instance;	
		gameController.removeEvent (this.gameObject);
	}

	public void UpdateEvent () {
		SpecificEventUpdate ();
	}

	public abstract void SpecificEventUpdate();

	public void SetdontUsePositiveEffect()
	{
		dontUsePositiveEffect = true;
	}

	public float GetCooldownModifier()
	{
		return 1 + skills.GetCoffee ();
	}
}
