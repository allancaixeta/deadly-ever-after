﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PushController : MonoBehaviour {

	public float pushTime= 0.2f;
	public float pushPower = 1;
	public float mediumsPushDivision = 0.75f;
	public float heaviesPushDivision = 0.5f;

	Vector2 velocity;
	List<MonsterGeneric> listOfMonstersPushing;
	List<float> listOfMonstersCountdown;
	List<Vector2> listOfPushDirections;

	// Use this for initialization
	void Awake () {
		listOfMonstersPushing = new List<MonsterGeneric>();
		listOfMonstersCountdown = new List<float>();
		listOfPushDirections = new List<Vector2>();
	}
	
	// Update is called once per frame
	public void UpdatePushController () {
		for (int i = 0; i < listOfMonstersPushing.Count; i++) {
			listOfMonstersCountdown[i] -= GameController.deltaTime;
			if(!listOfMonstersPushing[i])
			{
				Remove (i);
			}
			else
			{				
				if(listOfMonstersCountdown[i] < 0 || listOfMonstersPushing[i].CheckIfCurrentOrNextState(MonsterStates.Knockback))
				{
					listOfMonstersPushing[i].Pushed (false);
					Remove (i);
				}
				else
				{
					switch(listOfMonstersPushing [i].knockbackType)
					{
					case MonsterGeneric.KnockbackType.light:
						velocity = listOfPushDirections[i] * GameController.deltaTime * pushPower;
						break;
					case MonsterGeneric.KnockbackType.medium:
						velocity = listOfPushDirections[i] * GameController.deltaTime * pushPower * mediumsPushDivision;
						break;
					case MonsterGeneric.KnockbackType.heavy:
						velocity = listOfPushDirections[i] * GameController.deltaTime * pushPower * heaviesPushDivision;
						break;
					}
					listOfMonstersPushing [i].GetComponent<CharacterController>().Move(velocity); 
				}
			}
		}
	}
	
	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if(hit.collider.tag == Tags.Monster)
		{
			MonsterGeneric monster = hit.collider.GetComponent<MonsterGeneric>();
			if(monster.knockbackType != MonsterGeneric.KnockbackType.hulk)
			{
				for (int i = 0; i < listOfMonstersPushing.Count; i++) 
				{
					if (listOfMonstersPushing [i] == monster)
						return;
				}
				listOfMonstersPushing.Add(monster);
				listOfMonstersCountdown.Add(pushTime);
				monster.Pushed (true);
				Vector2 pushDir = monster.transform.position - transform.position;
				pushDir.Normalize();
				listOfPushDirections.Add (pushDir);
			}		
		}
	}

	void Remove(int i)
	{
		listOfMonstersPushing.RemoveAt (i);
		listOfMonstersCountdown.RemoveAt (i);
		listOfPushDirections.RemoveAt (i);
	}
}