using UnityEngine;
using System.Collections;

[Prefab("CursorController")]
public class CursorController : Singleton<CursorController>
{
	
	public enum cursorControllerStates{
		VOID,
		normal, 
		dragging_weapon,
		pause
	}
	
	public float speed;
	public cursorControllerStates nextState = cursorControllerStates.normal;		
	public Material normal_cursor,no_cursor,select_cursor,hand,activate_cursor,attack_cursor, floor_item, interface_item,
	interface_item_select;	
	public string currentFocusTag;
	public bool activateOn = false;
	public GameObject currentFocusGameObject;
	public Vector3 currentPosition;
	private cursorControllerStates currentState = cursorControllerStates.VOID;	

	GuideController weaponGuide, skillGuide, monsterGuide;
	string lastTagViewed, lastItemName, lastWeaponName, lastMagicName, lastMonsterName;
	GameObject currentOpenGuide, currentDraggedObject;
	InterfaceController UI;
	Vector3 currentPosition2;
	Transform topView,cameraTransform;
	void Awake()
	{
		//UI = InterfaceController.Instance;
		cameraTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
		topView = cameraTransform.transform.FindChild("TopView");
		
	}
	
	// Use this for initialization
	void Start () {
		gameObject.tag = "Cursor";
	}
	
	// Atualizar a aparencia e posição do mouse
	public void UpdateCursorController() {	
		
		if(!StartState())
			UpdateState();
	}
	
	bool StartState()
	{
		if(currentState == nextState)
			return false;
		currentState = nextState;
		switch(currentState)
		{
			case cursorControllerStates.normal:
				//currentOpenGuide.SetActive(false);
				break;
			
			case cursorControllerStates.pause:	
				break;
		}	
		return true;
	}
	
	void UpdateState()
	{
		switch(currentState)
		{
		case cursorControllerStates.normal:	
		case cursorControllerStates.pause:
			currentPosition2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);			
			//currentPosition2.y -= 4;
			//currentPosition2.z += 4;
			this.transform.position = currentPosition2;
			activateOn = false;
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			int layerMask = 1 << 12;
			if(Physics.Raycast(ray, out hit, 10000,layerMask)){
				currentPosition = hit.point; 
			}
			else
			{
				currentPosition = currentPosition2 + (topView.position - cameraTransform.position);
			}
			int layerMask2 = 378881;
			if(Physics.Raycast(ray, out hit, 10000,layerMask2)){
				currentFocusTag = hit.collider.transform.tag;
				currentFocusGameObject = hit.collider.transform.gameObject;
				/*switch(currentFocusTag)
				{
				case "Button":
				case "Item":
					renderer.material = select_cursor;
					break;
				case "PlayerHit":
				case "Player":
					renderer.material = no_cursor;
					break;
				case Tags.Monster:
					renderer.material = attack_cursor;
					break;
				case "UISkill":
				case "UISlotSkill":
					if(lastTagViewed == currentFocusTag && lastItemName == currentFocusGameObject.name)
						return;
					if(lastTagViewed != currentFocusTag)
						currentOpenGuide.SetActive(false);
					renderer.material = interface_item;
					skillGuide.gameObject.SetActive(true);
					skillGuide.SetSkillData(currentFocusGameObject.GetComponent<SkillController>());
					lastItemName = currentFocusGameObject.name;
					currentOpenGuide = skillGuide.gameObject;
					break;
				case "UIWeapon":					
					if(lastTagViewed == currentFocusTag && lastWeaponName == currentFocusGameObject.name
						|| currentFocusGameObject.renderer.material.name == "transparent (Instance)")
						return;
					if(lastTagViewed != currentFocusTag)
						currentOpenGuide.SetActive(false);
					renderer.material = interface_item;
					//weaponGuide.gameObject.SetActive(true);
					/*if(currentFocusTag == "UIWeapon")
						weaponGuide.SetWeaponData(UI.GetWeapon(int.Parse(currentFocusGameObject.name)));
					else
						weaponGuide.SetWeaponData(UI.GetWeapon(currentFocusGameObject.name));
					lastWeaponName = currentFocusGameObject.name;
					//currentOpenGuide = weaponGuide.gameObject;
					break;
				case "UISlotWeapon":					
					if(lastTagViewed == currentFocusTag && lastWeaponName == currentFocusGameObject.name
						|| currentFocusGameObject.renderer.material.name == "transparent (Instance)")
						return;
					if(lastTagViewed != currentFocusTag)
						currentOpenGuide.SetActive(false);
					renderer.material = interface_item_select;
					//weaponGuide.gameObject.SetActive(true);
					/*if(currentFocusTag == "UIWeapon")
						weaponGuide.SetWeaponData(UI.GetWeapon(int.Parse(currentFocusGameObject.name)));
					else
						weaponGuide.SetWeaponData(UI.GetWeapon(currentFocusGameObject.name));
					lastWeaponName = currentFocusGameObject.name;
					//currentOpenGuide = weaponGuide.gameObject;
					break;
				case "UIMonster":
					if(lastTagViewed == currentFocusTag && lastMonsterName == currentFocusGameObject.name)
						return;
					if(lastTagViewed != currentFocusTag)
						currentOpenGuide.SetActive(false);
					renderer.material = interface_item;
					monsterGuide.gameObject.SetActive(true);
					monsterGuide.SetMonsterData(currentFocusGameObject.GetComponent<MonsterGeneric>());
					lastMonsterName = currentFocusGameObject.name;
					currentOpenGuide = monsterGuide.gameObject;
					break;		
				default:
					renderer.material = normal_cursor;
					if(lastTagViewed != currentFocusTag)
						currentOpenGuide.SetActive(false);
					break;
				}
				lastTagViewed = currentFocusTag;*/
			}
			else
			{
				GetComponent<Renderer>().material = normal_cursor;
				currentFocusTag = "NOCOLLIDER";
			}
			break;
		
		case cursorControllerStates.dragging_weapon:
			currentPosition2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);			
			/*currentPosition2.y -= 4;
			currentPosition2.z += 4;*/
			this.transform.position = currentPosition2;
			break;
		}
	}

	public Vector3 GetMousePosition()
	{
		currentPosition2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);			
		//currentPosition2.y -= 4;
		//currentPosition2.z += 4;
		return currentPosition2 + (topView.position - cameraTransform.position);
	}

	/*DEPRECATED
	public void AttachDraggedWeapon(GameObject draggedOne)
	{
		currentDraggedObject = draggedOne;
		nextState = cursorControllerStates.dragging_weapon;
	}
	
	//true se o drag and drop deu certo, false se deu errado
	public bool DetachDraggedWeapon()
	{
		nextState = cursorControllerStates.pause;
		Ray ray;
		RaycastHit hit;
		ray = Camera.main.ScreenPointToRay((Input.mousePosition));
		int layerMask = 1 << 8;
		layerMask = ~layerMask;
		if(Physics.Raycast(ray, out hit, 10000,layerMask)){
			currentFocusTag = hit.transform.tag;
			currentFocusGameObject = hit.collider.gameObject;
			switch(currentFocusTag)
			{
			case "UISlotWeapon":				
				UI.SwitchInterfaceSlotWeapon(int.Parse(currentDraggedObject.name),
				int.Parse(currentFocusGameObject.name.Substring(4,1)),
				int.Parse(currentFocusGameObject.name.Substring(5,1)));
				break;
			case "UISlotSkill":
				UI.SwitchInterfaceSlotSkill(int.Parse(currentDraggedObject.name));
				break;
			default: // undo drag and drop
				return false;
			}
		}
		return true;
	}
*/
}