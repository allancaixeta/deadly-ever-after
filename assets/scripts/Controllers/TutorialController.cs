﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
class Tut
{
    public bool tutorialCompleted = false;
}

public class TutorialController : MonoBehaviour {

    public WeaponController tutorialCrushWeapon, tutorialAOEWeapon;
    public GameObject door1, door2, door3;
    public float distanceToUnlockDoor = 100;
    private CameraFollow cameraFollow;
    private PlayerController playerController;
    private GameController gameController;
    private InterfaceController UI;
    private CursorController currentCursor;
    private List<MonsterGeneric> tutorialMonsterList;
    tutorialStages TutorialStages = tutorialStages.movimentation;
    bool firstFrame = true;
    public enum tutorialStages
    {
        movimentation,
        life,
        crush,
        AOE,
        lineOfSight,
        obstacles
    }

    // Use this for initialization
    void Start () {
        
        
    }
	
	// Update is called once per frame
	void Update () {
        if(firstFrame)
        {
            cameraFollow = (CameraFollow)FindObjectOfType(typeof(CameraFollow));
            playerController = PlayerController.Instance;
            gameController = GameController.Instance;
            UI = InterfaceController.Instance;
            currentCursor = CursorController.Instance;

            Cursor.visible = true;
            playerController.nextState = PlayerController.playerControllerStates.normal;
            cameraFollow.ResetCamera();
            UI.nextState = InterfaceController.interfaceControllerStates.tutorial;
            UI.UpdateInterfaceController();

            GameObject[] tempMonsterVect = GameObject.FindGameObjectsWithTag(Tags.Monster);
            tutorialMonsterList = new List<MonsterGeneric>();
            for (int i = 0; i < tempMonsterVect.Length; i++)
            {
                tutorialMonsterList.Add(tempMonsterVect[i].GetComponent<MonsterGeneric>());
            }
            door1.SetActive(false);
            door2.SetActive(false);
            door3.SetActive(false);
            firstFrame = false;
        }
        else
        {
            playerController.UpdatePlayerController();
            currentCursor.UpdateCursorController();
            UI.UpdateInterfaceController();
            cameraFollow.UpdateCamera();
            UpdateMonsterList();
            switch (TutorialStages)
            {
                case tutorialStages.movimentation:
                    if (Vector2.Distance(playerController.transform.position, GameObject.Find("tutorialPhase2").transform.position) < 5)
                    {
                        TutorialStages = tutorialStages.life;
                        UI.TutorialTurnOnLife();
                    }
                    break;
                case tutorialStages.life:
                    if (Vector2.Distance(playerController.transform.position, GameObject.Find("tutorialPhase3").transform.position) < 4)
                    {
                        TutorialStages = tutorialStages.crush;
                        UI.AddWeapon(tutorialCrushWeapon);
                        UI.VisualWeapon(0, tutorialCrushWeapon.weaponAppearenceUP, tutorialCrushWeapon.weaponAppearenceDown);
                        gameController.SetCrush(tutorialCrushWeapon);
                        UI.TutorialTurnOnCrushWeapon();
                        playerController.transform.position = GameObject.Find("tutorialStagesLineOfSight").transform.position;
                        cameraFollow.ResetCamera();
                    }
                    break;
                case tutorialStages.crush:
                    if (Vector2.Distance(playerController.transform.position, GameObject.Find("tutorialPhase4").transform.position) < 1)
                    {
                        TutorialStages = tutorialStages.AOE;
                        UI.AddWeapon(tutorialAOEWeapon);
                        UI.VisualWeapon(1, tutorialAOEWeapon.weaponAppearenceUP, tutorialAOEWeapon.weaponAppearenceDown);
                        gameController.SetAOE(tutorialAOEWeapon);
                        UI.TutorialTurnOnAOEWeapon();
                    }
                    break;
                case tutorialStages.AOE:
                    if (!door1.activeSelf)
                    { 
                        door1.SetActive(true);
                        for (int i = 0; i < tutorialMonsterList.Count; i++)
                        {
                            if (tutorialMonsterList[i] && Vector2.Distance(tutorialMonsterList[i].transform.position, playerController.transform.position) < distanceToUnlockDoor)
                                door1.SetActive(false);
                        }
                    }
                    else
                    {
                        if (Vector2.Distance(playerController.transform.position, GameObject.Find("tutorialPhase5").transform.position) < 3)
                        {
                            TutorialStages = tutorialStages.lineOfSight;
                            playerController.transform.position = GameObject.Find("tutorialStagesWeapons").transform.position;
                            cameraFollow.ResetCamera();
                        }
                    }
                    break;
                case tutorialStages.lineOfSight:
                    if (!door2.activeSelf)
                    {
                        door2.SetActive(true);
                        for (int i = 0; i < tutorialMonsterList.Count; i++)
                        {
                            if (tutorialMonsterList[i] && Vector2.Distance(tutorialMonsterList[i].transform.position, playerController.transform.position) < distanceToUnlockDoor)
                                door2.SetActive(false);
                        }
                    }
                    else
                    {
                        if (Vector2.Distance(playerController.transform.position, GameObject.Find("tutorialPhase6").transform.position) < 3)
                        {
                            TutorialStages = tutorialStages.obstacles;
                            playerController.transform.position = GameObject.Find("tutorialStagesObstacles").transform.position;
                            cameraFollow.ResetCamera();
                        }
                    }
                    break;
                case tutorialStages.obstacles:
                    if (!door3.activeSelf)
                    {
                        door3.SetActive(true);
                        for (int i = 0; i < tutorialMonsterList.Count; i++)
                        {
                            if (tutorialMonsterList[i] && Vector2.Distance(tutorialMonsterList[i].transform.position, playerController.transform.position) < distanceToUnlockDoor)
                                door3.SetActive(false);
                        }
                    }
                    else
                    {
                        if (Vector2.Distance(playerController.transform.position, GameObject.Find("tutorialPhase7").transform.position) < 3)
                        {
                            gameController.TutorialOver();
                        }
                    }
                    break;
            }
        }
    }

    public void UpdateMonsterList()
    {
        if (tutorialMonsterList == null || tutorialMonsterList.Count == 0)
        {
            return;
        }        
        for (int i = 0; i < tutorialMonsterList.Count; i++)
        {
            if (tutorialMonsterList[i] == null)
                tutorialMonsterList.RemoveAt(i);
            else
                tutorialMonsterList[i].UpdateMonsterGeneric();
        }
    }
}
