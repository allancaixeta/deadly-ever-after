using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShadowController : MonoBehaviour {
		
	public float shadowY;
	public Vector3 shadowSize;

	protected Transform m_Shadow;
	protected float m_OriginalY;
	protected Vector3 m_OriginalSize;
	protected bool m_Passingthrough = false;

	public int adjust = 10;
	public LerpEquationTypes lerpType;

	protected enum States { None, Move, Restore }
	protected States m_State;
	protected float m_Elapsed;
	protected const float M_Duration = 0.1f;
	protected Vector3 m_MovStartPos;
	protected Vector3 m_MovStartScale;

	protected Vector3 normalPos;
	protected Vector3 shadowPos;

	void Start () {
		m_Shadow = transform.FindChild("shadow");
		m_OriginalY = m_Shadow.localPosition.y;
		m_OriginalSize = m_Shadow.localScale;
		m_Elapsed = 0.0f;
		normalPos = new Vector3(m_Shadow.localPosition.x, m_OriginalY, m_Shadow.localPosition.z);
		shadowPos = new Vector3(m_Shadow.localPosition.x, shadowY, m_Shadow.localPosition.z);
	}



	public void Update() {
		if(m_State != States.None) {
			float factor = m_Elapsed / M_Duration;
			factor = lerpType.GetEquationTransformedFactor(factor);
		
			Vector3 localPos, localScale;

			if(m_State == States.Move) {
				localPos = Vector3.Lerp(m_MovStartPos, shadowPos, factor);
				localScale = Vector3.Lerp(m_MovStartScale, shadowSize, factor);
			} else {
				localPos = Vector3.Lerp(m_MovStartPos, normalPos, factor);
				localScale = Vector3.Lerp(m_MovStartScale, m_OriginalSize, factor);
			}

			m_Shadow.localPosition = localPos;
			m_Shadow.localScale = localScale;

			if(factor >= 1.0f) {
				m_State = States.None;
			} else {
				m_Elapsed += Time.deltaTime;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == Tags.Lvl2Obstacle)
		{
			if(m_Shadow.localPosition != shadowPos)
			{
				m_State = States.Move;
				m_Elapsed = 0.0f;
				m_Shadow.GetComponent<DepthControl>().SetAdjust (adjust);						
				m_MovStartScale = m_Shadow.localScale;
				m_MovStartPos = m_Shadow.localPosition;
			}
			else
			{
				m_Passingthrough = true;
			}
		}

	}

	void OnTriggerExit(Collider other)
	{
		if(other.transform.tag == Tags.Lvl2Obstacle)
		{
			if(m_Passingthrough)
			{
				m_Passingthrough = false;
				return;
			}
			m_State = States.Restore;
			m_Elapsed = 0.0f;
			m_Shadow.GetComponent<DepthControl>().SetAdjust (-adjust);
			m_Passingthrough = false;
			m_MovStartScale = m_Shadow.localScale;
			m_MovStartPos = m_Shadow.localPosition;
		}
	}
}