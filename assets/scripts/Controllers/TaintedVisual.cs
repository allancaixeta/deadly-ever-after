﻿using UnityEngine;
using System.Collections;

public class TaintedVisual : MonoBehaviour {

	public Material material;
	public string[] exceptions;
	bool isAnException;
	// Use this for initialization
	void Start () {
		SpriteRenderer[] allSprites = gameObject.GetComponentsInChildren<SpriteRenderer>(true);
		foreach (SpriteRenderer sprites in allSprites) {
			for (int i = 0; i < exceptions.Length; i++) {
				if(exceptions[i] == sprites.name)
				{
					isAnException = true;
				}
			}
			if(!isAnException)
				sprites.material = material;
			isAnException = false;
		}
		SkinnedMeshRenderer[] allMeshes = gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true);
		foreach (SkinnedMeshRenderer sprites in allMeshes) {
			for (int i = 0; i < exceptions.Length; i++) {
				if(exceptions[i] == sprites.name)
				{
					isAnException = true;
				}
			}
			if(!isAnException)
				sprites.material.color = material.color;
			isAnException = false;
		}
		gameObject.SendMessage ("GetAllMaterials");
	}
}
