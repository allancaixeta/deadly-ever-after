using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class WeaponController : MonoBehaviour {
	
	public enum weaponControllerStates{
		VOID,
		start,
		normal,
		finish,
		pause
	}

	public string weaponName;
	public int damage;
	public float AttackSpeed = 1;//tempo de "recarga" da arma
	public int ProjectileSpeed = 1;
	public AudioClip audioClip;
	public float LifeTime = 2; // tempo de vida do projetil
	public float CoolDown = 1;
	public float auxVariable;
	public float knockBack = 0;
	public weaponControllerStates nextState = weaponControllerStates.start;		
	public string UniqueID;
	public Texture2D weaponAppearenceUP, weaponAppearenceDown;	

	public GameObject onHitEffect;
	protected weaponControllerStates saveState;	
	protected weaponControllerStates lastState;
	protected weaponControllerStates currentState = weaponControllerStates.VOID;
	protected float countDown;
	protected GameController gameController;
	protected float currentHitTime;
	protected bool isLeft;
	protected List<string> monsterHitListID;
	protected List<float> monsterHitListDuration;
	protected CursorController currentCursor;
	protected GameObject player;	
	protected bool isRight;
	protected InterfaceController UI;
	protected float timeDistortion = 1;
	protected Transform weaponVisual;
	protected Material normalMaterial;
	protected Vector3 direction;
	protected float playerStrenght;
	protected SkillController skills;
	public enum WeaponSpecial
	{
		none=0,
		threeshot=1,
		reapinghook=2,
		voidray=3,
		mousebreaker=4,
		kendo=5,
		movetokill=6,
		homemissile=7,
		soulcharges=8,
		skylaser=9,
		bouncyball=19,
		boomerang=21,
		firewave=22,
		fraggrenade=23,
		shotgun=24,
		deathrain=25,
		protoncannon=26,
		energymines=27,
		chainlightning=28,
		remotebombermine=29,
		frozenorb=30,
		rumbleult=31,
		disablejumble=41,
		timebubble=42,
		portalgun=43,
		jump=44,
		grabchain=45,
		megadodge=46,
		kakebushin=47,
		onetimedamage=61,
		constantdamage=62
	}
	bool weaponIsCrush;
	public WeaponSpecial weaponSpecial;
	Vector3 colliderCenter;
	//Transform cenarioLimits;
	// Use this for initialization
	void Awake(){
		if(gameObject.tag != "UIWeapon" && gameObject.tag != "UISlotWeapon")
			gameObject.tag = Tags.Weapon;
		gameController = GameController.Instance;
		currentCursor =  GameObject.FindGameObjectWithTag("Cursor").GetComponent<CursorController>();
		skills = (SkillController) FindObjectOfType(typeof(SkillController));
		//GetWeaponParameters();
		countDown = LifeTime;
		currentHitTime = AttackSpeed;
		player = GameObject.FindGameObjectWithTag("Player");
		System.TimeSpan now;
		now = System.DateTime.Now - new System.DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
		UniqueID = weaponName + (now.TotalSeconds*100000).ToString();
		if(gameObject.tag != "UISlotWeapon")
			gameObject.name = UniqueID;
		monsterHitListID = new List<string>();
		monsterHitListDuration = new List<float>();
		weaponVisual = transform.FindChild("WeaponVisual");	
		//normalMaterial = weaponVisual.renderer.material;
		//cenarioLimits = GameObject.Find("CenarioLimits").transform;
		SpecificWeaponAwake();
		switch(weaponSpecial)
		{
		default:
			weaponIsCrush = false;
			break;
		case WeaponSpecial.threeshot:
		case WeaponSpecial.homemissile:
		case WeaponSpecial.kendo:
		case WeaponSpecial.voidray:
		case WeaponSpecial.mousebreaker:
		case WeaponSpecial.movetokill:
		case WeaponSpecial.reapinghook:
		case WeaponSpecial.bouncyball:
		case WeaponSpecial.soulcharges:
		case WeaponSpecial.skylaser:
			weaponIsCrush = true;
			break;
		}
		if(GetComponent<SphereCollider>())
		{
			colliderCenter = GetComponent<SphereCollider>().center;
			return;
		}
		if(GetComponent<CapsuleCollider>())
		{
			colliderCenter = GetComponent<CapsuleCollider>().center;
			return;
		}
		if(GetComponent<BoxCollider>())
		{
			colliderCenter = GetComponent<BoxCollider>().center;
			return;
		}
	} 
	
	public abstract void SpecificWeaponAwake();
	
	// Update is called once per frame
	public void UpdateWeaponController(){
		
		if(!StartState())
			UpdateState();
	}
	
	protected abstract bool StartState();
	
	protected abstract void UpdateState();
			
	public void SetDirection(Vector3 v)
	{
		v.z = 0;
		direction = v.normalized;
	}

	protected void MoveForward()
	{
		//transform.Translate(new Vector3(1,0,0)* GameController.deltaTime * ProjectileSpeed * timeDistortion);
		Moving ();
		DecreaseLifeTime();
	}
	
	protected void DecreaseLifeTime()
	{
		countDown -= GameController.deltaTime;
		if(countDown < 0)
		{		
			DestroyItSelf();		
		}				
	}

	protected void Moving()
	{
		//transform.Translate(new Vector3(1,0,0)* GameController.deltaTime * ProjectileSpeed * timeDistortion);
		float speed = ProjectileSpeed;
		transform.Translate(direction * GameController.deltaTime * (speed) * timeDistortion,Space.World);
	}

	public abstract void DestroyItSelf();
	
	protected abstract void OnTriggerEnter(Collider other);	
	
	protected abstract void CheckForHit(MonsterGeneric monster, string monsterID, Vector3 position);
	
	protected bool OnMonsterHit(string monsterID, float speed)
	{
		for (int i = 0; i < monsterHitListID.Count; i++){
			if(monsterHitListID[i] == monsterID)
				return true;
		}
		monsterHitListID.Add(monsterID);
		monsterHitListDuration.Add(speed);	
		return false;
	}

	protected void OnTriggerStay(Collider other)
	{
		switch(other.transform.tag)
		{
		case Tags.Unmovable:
		case Tags.Lvl2Obstacle:
		case Tags.Lvl3Obstacle:
		case Tags.Wall:	
		case Tags.Monster:
		case Tags.BossMonster:
			OnTriggerEnter(other);
			break;
		}
	}
	
	protected abstract void OnTriggerExit(Collider other);
	
	protected bool HitMonster(int dmg, MonsterGeneric monster, Vector3 position)
	{
		return monster.Hit(dmg,weaponIsCrush, this);
	}

	public void SetUniqueID(string id)
	{
		UniqueID = id;	
	}
	
	protected void DecreaseMonsterHitListDuration()
	{
		for (int i = 0; i < monsterHitListDuration.Count; i++) {
			monsterHitListDuration[i] -= GameController.deltaTime  * timeDistortion;
			if(monsterHitListDuration[i] < 0)
			{
				monsterHitListID.RemoveAt(i);
				monsterHitListDuration.RemoveAt(i);
			}
		}
	}
	
	public void SetSide(int right)
	{
		this.isRight = right == 1 ? true : false;
		this.isLeft = !isRight;
	}
	
	protected void PlaySound(AudioClip clip, bool continous)
	{
		if(continous)
		{
			if(isRight)
			{
				gameController.RightWeaponAudioSource.clip = clip;
				gameController.RightWeaponAudioSource.loop = true;
				gameController.RightWeaponAudioSource.Play();
			}
			else
			{
				gameController.LeftWeaponAudioSource.clip = clip;
				gameController.LeftWeaponAudioSource.loop = true;
				gameController.LeftWeaponAudioSource.Play();
			}
		}
		else
		{
			if(isRight)
				gameController.RightWeaponAudioSource.PlayOneShot(clip);
			else
				gameController.LeftWeaponAudioSource.PlayOneShot(clip);	
		}
	}

	public void CheckIfInsideRoom()
	{
		Vector3 upTemp = transform.position + colliderCenter + new Vector3(0,0,-30);
		Ray ray = new Ray(upTemp,transform.position + colliderCenter - upTemp);
		RaycastHit hit;
		int layerMask = 4096;
		if(!Physics.Raycast (ray,out hit, 100, layerMask)){			
			nextState = weaponControllerStates.finish;
		}
	}

	public bool CheckIfInsideRoomandReturn()
	{
		Vector3 upTemp = transform.position + colliderCenter + new Vector3(0,0,-30);
		Ray ray = new Ray(upTemp,transform.position + colliderCenter - upTemp);
		RaycastHit hit;
		int layerMask = 4096;
		if(!Physics.Raycast (ray,out hit, 100, layerMask)){			
			return true;
		}
		return false;
	}

	public void SetStrenght(float strenght)
	{
		playerStrenght = strenght;
		damage = (int)(damage * (1+playerStrenght));
		if(weaponIsCrush)
		{
			if(weaponSpecial == WeaponSpecial.threeshot)
				damage = (int)(damage * (1+skills.GetCrushingRage()/3));
			else
				damage = (int)(damage * (1+skills.GetCrushingRage()));
			switch(currentCursor.currentFocusTag)
			{
			case Tags.Monster:
			case Tags.BossMonster:
				damage = (int)(damage *  (1+skills.GetLockON ()));
				break;
			}
			damage = (int)(damage *  (1+(skills.GetLastBreath ()*(1-player.GetComponent<PlayerController>().GetCurrentLifePercentage()))));
			damage = (int)(damage *  skills.GetBeginnersLuck ());
			damage = (int)(damage * skills.GetHitenMitsurugi ());
			switch(weaponSpecial)
			{
			default:
			case WeaponSpecial.kendo:
				transform.localScale = transform.localScale * (1+skills.GetGrowing ()); 
				//Scale Shuriken Particles Values
				ParticleSystem[] systems;
				systems = transform.GetComponentsInChildren<ParticleSystem>(true);				
				foreach(ParticleSystem ps in systems)
				{
					ScaleParticleValues(ps, 1+skills.GetGrowing (), this.gameObject);
				}
				break;
			}
		}
		else
		{
			if(weaponSpecial == WeaponSpecial.boomerang)
				damage = (int)(damage * (1+skills.GetBattleMage()/2));
			else
				damage = (int)(damage * (1+ skills.GetBattleMage()));
			damage = (int)(damage * (1+(skills.GetFury ()*(1-player.GetComponent<PlayerController>().GetCurrentLifePercentage()))));
			switch(weaponSpecial)
			{
			default:
			case WeaponSpecial.boomerang:
				transform.localScale = transform.localScale * (1+skills.GetBiggerwiderlarger ()); 
				//Scale Shuriken Particles Values
				ParticleSystem[] systems;
				systems = transform.GetComponentsInChildren<ParticleSystem>(true);				
				foreach(ParticleSystem ps in systems)
				{
					ScaleParticleValues(ps, 1+skills.GetBiggerwiderlarger (), this.gameObject);
				}
				break;
			}
		}
	}

	public abstract void EnterTimeDistortion(float rate);
	
	public abstract void LeaveTimeDistortion();
		
	public float GetDPS()
	{
		return 1/Mathf.Abs(AttackSpeed) * (float)damage;
	}	
	
	public WeaponSpecial GetWeaponSpecial()
	{
		return weaponSpecial;	
	}
		
	public abstract void EnterSink();

	//Scale System
	private void ScaleParticleValues(ParticleSystem ps, float ScalingValue, GameObject parent)
	{
				//Particle System
				ps.startSize *= ScalingValue;
				ps.gravityModifier *= ScalingValue;
				if (ps.startSpeed > 0.01f)
						ps.startSpeed *= ScalingValue;
				if (ps.gameObject != parent)
						ps.transform.localPosition *= ScalingValue;
		
				/*SerializedObject psSerial = new SerializedObject(ps);
		
		//Scale Emission Rate if set on Distance
		if(psSerial.FindProperty("EmissionModule.enabled").boolValue && psSerial.FindProperty("EmissionModule.m_Type").intValue == 1)
		{
			psSerial.FindProperty("EmissionModule.rate.scalar").floatValue /= ScalingValue;
		}
		
		//Scale Size By Speed Module
		if(psSerial.FindProperty("SizeBySpeedModule.enabled").boolValue)
		{
			psSerial.FindProperty("SizeBySpeedModule.range.x").floatValue *= ScalingValue;
			psSerial.FindProperty("SizeBySpeedModule.range.y").floatValue *= ScalingValue;
		}
		
		//Scale Velocity Module
		if(psSerial.FindProperty("VelocityModule.enabled").boolValue)
		{
			psSerial.FindProperty("VelocityModule.x.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("VelocityModule.x.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("VelocityModule.x.maxCurve").animationCurveValue);
			psSerial.FindProperty("VelocityModule.y.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("VelocityModule.y.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("VelocityModule.y.maxCurve").animationCurveValue);
			psSerial.FindProperty("VelocityModule.z.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("VelocityModule.z.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("VelocityModule.z.maxCurve").animationCurveValue);
		}
		
		//Scale Limit Velocity Module
		if(psSerial.FindProperty("ClampVelocityModule.enabled").boolValue)
		{
			psSerial.FindProperty("ClampVelocityModule.x.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.x.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.x.maxCurve").animationCurveValue);
			psSerial.FindProperty("ClampVelocityModule.y.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.y.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.y.maxCurve").animationCurveValue);
			psSerial.FindProperty("ClampVelocityModule.z.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.z.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.z.maxCurve").animationCurveValue);
			
			psSerial.FindProperty("ClampVelocityModule.magnitude.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.magnitude.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ClampVelocityModule.magnitude.maxCurve").animationCurveValue);
		}
		
		//Scale Force Module
		if(psSerial.FindProperty("ForceModule.enabled").boolValue)
		{
			psSerial.FindProperty("ForceModule.x.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ForceModule.x.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ForceModule.x.maxCurve").animationCurveValue);
			psSerial.FindProperty("ForceModule.y.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ForceModule.y.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ForceModule.y.maxCurve").animationCurveValue);
			psSerial.FindProperty("ForceModule.z.scalar").floatValue *= ScalingValue;
			IterateKeys(psSerial.FindProperty("ForceModule.z.minCurve").animationCurveValue);
			IterateKeys(psSerial.FindProperty("ForceModule.z.maxCurve").animationCurveValue);
		}
		
		//Scale Shape Module
		if(psSerial.FindProperty("ShapeModule.enabled").boolValue)
		{
			psSerial.FindProperty("ShapeModule.boxX").floatValue *= ScalingValue;
			psSerial.FindProperty("ShapeModule.boxY").floatValue *= ScalingValue;
			psSerial.FindProperty("ShapeModule.boxZ").floatValue *= ScalingValue;
			psSerial.FindProperty("ShapeModule.radius").floatValue *= ScalingValue;
			
			//Create a new scaled Mesh if there is a Mesh reference
			//(ShapeModule.type 6 == Mesh)
			if(psSerial.FindProperty("ShapeModule.type").intValue == 6)
			{
				//Unity 4+ : changing the Transform scale will affect the shape Mesh
				ps.transform.localScale = ps.transform.localScale * ScalingValue;
				EditorUtility.SetDirty(ps);
			}
		}
		
		//Apply Modified Properties
		psSerial.ApplyModifiedProperties();
	}
	
	//Iterate and Scale Keys (Animation Curve)
	private void IterateKeys(AnimationCurve curve)
	{
		for(int i = 0; i < curve.keys.Length; i++)
		{
			curve.keys[i].value *= ScalingValue;
		}
	}*/
	}

	public bool IsWeaponCrush()
	{
		return weaponIsCrush;
	}

}