using UnityEngine;
using System.Collections;

public class TimeDistortionDetector : MonoBehaviour {
	
	public enum type
	{
		everything,
		monsters,
		weapons,
		creatures,
		player
	}
	public type currentType;
	public float DistortionRate = 1;
	// Use this for initialization
	void Start () {
	}
	
	void OnTriggerEnter(Collider other)
	{
		switch(currentType)
		{
		case type.everything:	
			other.SendMessage("EnterTimeDistortion",DistortionRate, SendMessageOptions.DontRequireReceiver);
			break;
		case type.monsters:
			if(other.tag == Tags.Monster)
				other.SendMessage("EnterTimeDistortion",DistortionRate);
			break;
		case type.creatures:
			if(other.tag == "Creature")
				other.SendMessage("EnterTimeDistortion",DistortionRate);
			break;
		case type.player:
			if(other.tag == "PlayerHit")
				other.SendMessage("EnterTimeDistortion",DistortionRate);
			break;
		case type.weapons:
			if(other.tag == Tags.Weapon)
				other.SendMessage("EnterTimeDistortion",DistortionRate);
			break;
		}
	}
	
	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter(other);
	}
	
	void OnTriggerExit(Collider other)
	{		
		switch(currentType)
		{
		case type.everything:	
			other.SendMessage("LeaveTimeDistortion",DistortionRate, SendMessageOptions.DontRequireReceiver);
			break;
		case type.monsters:
			if(other.tag == Tags.Monster)
				other.SendMessage("LeaveTimeDistortion",DistortionRate);
			break;
		case type.creatures:
			if(other.tag == "Creature")
				other.SendMessage("LeaveTimeDistortion",DistortionRate);
			break;
		case type.player:
			if(other.tag == "Player")
				other.SendMessage("LeaveTimeDistortion",DistortionRate);
			break;
		case type.weapons:
			if(other.tag == Tags.Weapon)
				other.SendMessage("LeaveTimeDistortion",DistortionRate);
			break;
		}
	}
}