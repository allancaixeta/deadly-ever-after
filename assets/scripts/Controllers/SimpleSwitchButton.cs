using UnityEngine;
using System.Collections;

public class SimpleSwitchButton : MonoBehaviour {
	
	//Material pra usar se o tipo do button for Press
	public Material MaterialOn;
	public Material MaterialOff;
		
	//Objeto alvo para onde voce vai mandar uma mensagem quando o button for apertado
	public GameObject target;
	
	//Nome da função a ser chamada quando o button for apertado
	public string message;
	//bool switchOn = false;
	
	public int valor;
	// Use this for initialization
	void Start () {
		//gameObject.tag = "Button";  O ideal é que a interfaces esteja em um layer e tag unico dela
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			int layermask = 1000;
			if(Physics.Raycast(ray, out hit, 10000, layermask)){
				if(hit.collider.transform != this.transform)
					return;
				this.GetComponent<Renderer>().material = MaterialOn;
				target.SendMessage(message,valor);
			}
		}
	}
	
	public void TurnOff()
	{
		this.GetComponent<Renderer>().material = MaterialOff;
	}
}
