﻿using UnityEngine;
using System.Collections;

public class TargetAimController : MonoBehaviour {

	public float maxReach = 50;
	public LerpEquationTypes lerp;
	public float sphereCastRadius = 1;
	public float lightningSize = 1; //grossura do raio
	int lightningRealSize; //grossura real do raio
	public float timeToEaseTheUse = 5;
	float currentTimeEaseTheUse;
	public float speedUpVariable = 0.2f;
	float loopsOfEaseToUse = 0;
	Transform lightningTarget, finalTarget;
	float defaultDistance;
	enum targetType{
		nothing,
		monster,
		objects		
	}
	targetType targetControl = targetType.nothing;
	float targetDistance;
	Transform target;
	float tempDistance;
	float oneOverLightningSize;
	int lightningLength;
	ParticleSystem.Particle[] particles;
	ParticleSystem particleSystem;
	Vector3 voidrayDirection;
	GameObject monsterTarget;
	Ray ray;
	RaycastHit hit;
	CursorController currentCursor;
	PlayerController player;
	void Start()
	{
		particleSystem = transform.FindChild("Visual").GetComponent<ParticleSystem>();
		lightningTarget = transform.FindChild("lightningTarget");
		finalTarget = transform.FindChild ("FinalTarget");	
		targetDistance = maxReach;
		defaultDistance = targetDistance;
		tempDistance = targetDistance;		
		finalTarget.localPosition = new Vector3(defaultDistance,0,0);
		currentTimeEaseTheUse = timeToEaseTheUse;
		RecreateLightningParticles();				
		LightningVoidRayGun();
		currentCursor = CursorController.Instance;
		player = PlayerController.Instance;
		transform.position = new Vector3 (transform.position.x, transform.position.y,GameController.ProjectilesPositionZ);
	}



	public void UpdateTargetAimController()
	{
		transform.rotation = Quaternion.Euler (0, 0, Mathf.Atan2 (currentCursor.currentPosition.y - player.transform.position.y, currentCursor.currentPosition.x - player.transform.position.x) * Mathf.Rad2Deg);
		currentTimeEaseTheUse -= GameController.deltaTime;
		if(currentTimeEaseTheUse < 0)
		{
			loopsOfEaseToUse += speedUpVariable;
			currentTimeEaseTheUse = timeToEaseTheUse;
		}
		voidrayDirection = finalTarget.position - transform.position;
		voidrayDirection.z = 0;
		ray = new Ray(transform.position,voidrayDirection);
		int layerMask = 2321408;
		if(Physics.SphereCast(ray, sphereCastRadius, out hit, Vector2.Distance (finalTarget.position,this.transform.position)+0.5f, layerMask)){
			if(hit.collider.name == finalTarget.name)
			{
				if(targetControl == targetType.nothing)
				{
					target = finalTarget;
					targetDistance = defaultDistance;							
				}
				else
				{
					targetControl = targetType.nothing;
				}
			}
			else
			{						
				target = hit.collider.transform;
				targetDistance = hit.distance;
				if(target.tag == Tags.Monster)
				{	
					if(targetControl == targetType.monster)
					{
						if(monsterTarget)
						{
							if(monsterTarget.name != hit.collider.name)
							{
								monsterTarget = hit.collider.gameObject;
							}
						}
						else
						{
							monsterTarget = hit.collider.gameObject;
						}
					}
					else
					{
						targetControl = targetType.monster;
						monsterTarget = hit.collider.gameObject;
					}
				}
				else
				{
					if(targetControl != targetType.objects)
					{
						targetControl = targetType.objects;
					}
				}
			}
		}
		else 
		{
			Debug.LogError("Mira com defeito!");
		}
		LightningVoidRayGun();	

	}

	void LightningVoidRayGun()
	{
		lightningTarget.localPosition = new Vector3((targetDistance + sphereCastRadius),0,0);
		if(Mathf.Abs(tempDistance-targetDistance) > 1)
		{
			RecreateLightningParticles();	
			tempDistance = targetDistance;
		}
		for (int i=0; i < lightningLength; i++)
		{
			Vector3 position = Vector3.Lerp(this.transform.position, lightningTarget.position, oneOverLightningSize * (float)i);
			particles[i].position = position;
			switch(targetControl)
			{
			case targetType.monster:
				particles[i].color = Color.red;
				break;
			case targetType.objects:
			default:
			case targetType.nothing:
				particles[i].color = Color.white;
				break;
			}
			particles[i].remainingLifetime = 1;
		}
		particleSystem.SetParticles(particles,lightningLength);		
	}

	void RecreateLightningParticles()
	{
		lightningRealSize = (int)(targetDistance * 4 * lightningSize);
		oneOverLightningSize = 1f / (float)lightningRealSize;		
		particles = new ParticleSystem.Particle[lightningRealSize];
		particleSystem.enableEmission = false;	
		particleSystem.Emit(lightningRealSize);
		lightningLength = particleSystem.GetParticles(particles);
	}
}