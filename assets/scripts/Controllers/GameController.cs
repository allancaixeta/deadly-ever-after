using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using GameDataEditor;

[Prefab("GameController", true)]
public class GameController : Singleton<GameController>
{
	
	public enum gameControllerStates{
		VOID,
        tutorial,
		intro1,
		intro2,
		menu,
		stage1,
		stage2,
		stage3,
		stage4,
		stage5,
		stage6,
		stage7,
		stage8,
		stage9,
		stage10,
		initializeScene,
		normal,
		pause,
		locked,
		gameover
	}
	
	public Resolution resolution;
	public float aspectRatio;
	public gameControllerStates nextState;
	public AudioClip interfaceError;
	public AudioSource RightWeaponAudioSource, LeftWeaponAudioSource, MonsterAudioSource, PlayerAudioSource, MusicAudioSource;
    int tutorialOver;
   

    public enum categories
	{
		crush,
		aoe,
		defense
	}
	
	private gameControllerStates currentState = gameControllerStates.VOID;
	private PlayerController playerController;
	private CursorController currentCursor;
	private InterfaceController UI;
	private CameraFollow cameraFollow;
	private List<WeaponController> weaponList;
	private List<MonsterGeneric> monsterList;
	private List<EventController> eventList;
	private List<GameObject> updatablesGameObjects;
	private List<string> recentlyJoined;
	private List<float> recentlyJoinedTime;
	private List<GameObject> listOfInterestedsInPlayerShooting;
	private List<ButtonController> buttonList;
	StageController stageController;
	private gameControllerStates saveState, lastState;	
	float GammaCorrection;
	bool openUI = false;
	//TODO: set this to true
	bool countDownAfterPause = false;
	PlayerHitController playerHit;
	public TextAsset datafile;	
	enum SceneType{
		stage,
		house,
		intro,
		menu
	}
	
	public static float deltaTime;
	public SpinTop spintop;
	public Pinocchio pinocchio;
	public Hydra hydra;
	public BombermanExplosion hydraExplosion;
	public Beanstalk beanstalk;
	public FireHairlessMule firehairlessmule;
	public Puck puck;
	public GameObject chosenMonster;
	public PrototipoController prototipo;
	public static float ProjectilesPositionZ= -5;
	public static float FlightPositionY= -4.5f;
	public static float InitialMaxDesyncTime = 2;
	public static float minPlayerDistanceToSpawn = 10;
	public Material materialForHit, materialForBurn, materialForPoison;
	public float monsterBlinkTimeWhenHit = 0.2f;
	public GameObject lifeAbsorb, shieldAbsorb;
	public int darkTaintedNumber = 3;
	public GameObject GetLifeAb()
	{
		return lifeAbsorb;
	}


	void Awake()
	{
		resolution = Screen.currentResolution;
		aspectRatio = resolution.width/resolution.height;
		playerController = PlayerController.Instance;
		playerHit = PlayerHitController.Instance;
		currentCursor = CursorController.Instance;	
		UI = InterfaceController.Instance;
		stageController = (StageController) FindObjectOfType(typeof(StageController));
		cameraFollow = (CameraFollow) FindObjectOfType(typeof(CameraFollow));
		RightWeaponAudioSource = GameObject.Find("RightWeaponAudioSource").GetComponent<AudioSource>();
		LeftWeaponAudioSource = GameObject.Find("LeftWeaponAudioSource").GetComponent<AudioSource>();
		MonsterAudioSource = GameObject.Find("MonsterAudioSource").GetComponent<AudioSource>();
		PlayerAudioSource = GameObject.Find("PlayerAudioSource").GetComponent<AudioSource>();
		MusicAudioSource = GameObject.Find("MusicAudioSource").GetComponent<AudioSource>();
		weaponList = new List<WeaponController>();
		monsterList = new List<MonsterGeneric>();
		eventList = new List<EventController> ();
		updatablesGameObjects = new List<GameObject> ();
		buttonList = new List<ButtonController>();	
		recentlyJoined = new List<string>();
		recentlyJoinedTime = new List<float>();
		ReadSaveFile();
		AssembleCrowd2List ();
		listOfInterestedsInPlayerShooting = new List<GameObject>();
		GDEDataManager.Init("gde_data");		
        nextState = gameControllerStates.intro1;
	}
    ReadConfig file;

    void ReadSaveFile()
	{
		//TODO: read save file and set options value, gamma value, save file
		file = new ReadConfig(datafile);
		countDownAfterPause = file.GetInt("CountDownAfterPause")==1 ? true : false;
		float volume = file.GetFloat("SFXSound");		
		LeftWeaponAudioSource.volume = volume;
		RightWeaponAudioSource.volume = volume;
		MonsterAudioSource.volume = volume;
		PlayerAudioSource.volume = volume;
		volume = file.GetFloat("MusicSound");
		MusicAudioSource.volume = volume;
        //tutorialOver = file.GetInt("Tutorial");
        if (!Application.isEditor && PlayerPrefs.HasKey("Tutorial"))
            tutorialOver = PlayerPrefs.GetInt("Tutorial");
        SetGamma(file.GetFloat("GammaCorrection"));
	}
	
    public void TutorialOver()
    {
        PlayerPrefs.SetInt("Tutorial",1);
        PlayerPrefs.Save();
        nextState = gameControllerStates.menu;
        SceneManager.LoadScene("MainGame");                
    }

	// Update is called once per frame
	void Update () {
		if(!StartState())
			UpdateState();	
	}
	
	bool StartState()
	{
		lastState = currentState;
		if(currentState == nextState || (nextState == gameControllerStates.pause))
			return false;
		if((nextState == saveState && currentState == gameControllerStates.pause))
		{
			currentState = nextState;
			return false;
		}
		currentState = nextState;			
		switch(currentState)
		{
		    case gameControllerStates.intro1:                
                Scene scene = SceneManager.GetActiveScene();
                switch(scene.name)
                {

                    case "Tutorial":
                        if (tutorialOver == 1)
                        {
                            TutorialOver();
                        }
                        else
                            nextState = gameControllerStates.tutorial;
                        break;

                    default:
                        nextState = gameControllerStates.menu;
                        break;
                }               
                break;
			
		    case gameControllerStates.intro2:
			    ChangeScene("demo");
			    break;
			
		    case gameControllerStates.initializeScene:	
			    cameraFollow.ResetCamera ();
			    SceneChanged(SceneType.stage);
			    UI.GameStarted();
			    break;

            case gameControllerStates.tutorial:
                Time.timeScale = 1;
                break;

            case gameControllerStates.menu:
                ChangeUIState(InterfaceController.interfaceControllerStates.menu);
                Time.timeScale = 1;
                break;

            case gameControllerStates.stage1:	
		    case gameControllerStates.stage2:
		    case gameControllerStates.stage3:
		    case gameControllerStates.stage4:
		    case gameControllerStates.stage5:
		    case gameControllerStates.stage6:
                Time.timeScale = 1;
                Cursor.visible = true;
			    stageController.SwitchStage(StageController.StageNumber.stage1);
			    playerController.nextState = PlayerController.playerControllerStates.normal;
			    ChangeUIState(InterfaceController.interfaceControllerStates.stage);
			    cameraFollow.ResetCamera ();
			    //stageController.SwitchStage(GetStageNumber(currentState));
			    break;
			
		    case gameControllerStates.pause:
			    saveState = lastState;
			    currentCursor.nextState = CursorController.cursorControllerStates.pause;
			    ChangeUIState(InterfaceController.interfaceControllerStates.pause);
			    playerHit.gameObject.SetActive(false);
			    if(monsterList != null)
			    {
				    for (int i = 0; i < monsterList.Count; i++) {
					    monsterList[i].Pause();
				    }
			    }
			    PlayerAudioSource.Stop();
			    RightWeaponAudioSource.Stop();
			    LeftWeaponAudioSource.Stop();
			    MonsterAudioSource.Stop();
			    switch(saveState)
			    {
			    case gameControllerStates.stage1:
			    case gameControllerStates.stage2:
			    case gameControllerStates.stage3:
			    case gameControllerStates.stage4:
			    case gameControllerStates.stage5:
			    case gameControllerStates.stage6:
				    StageUpdate();
				    break;				
			    }			
			    break;
			
		    case gameControllerStates.gameover:
			    break;				
		}
		return true;
	}

	bool locked = false;

	void UpdateState()
	{	
		if(Input.GetKeyDown(KeyCode.P))
		{
			locked = !locked;	
		}
        if (Input.GetKey(KeyCode.Escape))
            ExitGame();
		if(locked)
		{			
			return;	
		}
		if(Time.deltaTime > 0.33f)
			deltaTime = 0.33f;
		else
			deltaTime = Time.deltaTime;
		switch(currentState)
		{			
		    case gameControllerStates.intro1:
			    currentCursor.UpdateCursorController();
			    UpdateList(buttonList);
			    break;
			
		    case gameControllerStates.intro2:
			    nextState = gameControllerStates.initializeScene;
			    break;

		    case gameControllerStates.initializeScene:	
			    break;

            case gameControllerStates.tutorial:
                UpdateList(weaponList);
                break;

            case gameControllerStates.stage1:	
		    case gameControllerStates.stage2:
		    case gameControllerStates.stage3:
		    case gameControllerStates.stage4:
		    case gameControllerStates.stage5:
		    case gameControllerStates.stage6:
			    if(Input.GetKeyDown(KeyCode.O))
			    {
				    if(Time.timeScale == 1)
					    Time.timeScale = timeScaletoTest;
				    else
					    Time.timeScale = 1;
			    }
			    timeCountInCurrentRoom -= GameController.deltaTime;
			    StageUpdate();
			    HandleHydras();
			    break;	
			
		    case gameControllerStates.pause:
			    currentCursor.UpdateCursorController();
			    UI.UpdateInterfaceController();
			    UpdateList(buttonList);
			    if(Input.GetKeyDown(KeyCode.Escape))
			    {
				    switch(UI.GetMenuState())
				    {
				    case InterfaceController.menuStates.pause_menu:
				    case InterfaceController.menuStates.inventory_menu:
					    this.UnPausing();
					    break;
				    case InterfaceController.menuStates.gamma_menu:
					    UI.CloseGammaOptions(false);
					    break;
				    case InterfaceController.menuStates.options_menu:
					    UI.CloseOptions();
					    break;
				    }								
				    return;
			    }
			    if(Input.GetKeyDown(KeyCode.E) && UI.GetMenuState() == InterfaceController.menuStates.inventory_menu)
			    {
				    this.UnPausing();
			    }
			    break;			
		    case gameControllerStates.gameover:
			    cameraFollow.UpdateCamera ();
			    break;
		}
	}
	float timeCountInCurrentRoom;
	void StageUpdate()
	{		
		stageController.UpdateStageController();
		playerController.UpdatePlayerController();
		currentCursor.UpdateCursorController();
		UI.UpdateInterfaceController ();
		cameraFollow.UpdateCamera ();
		UpdateList(weaponList);
		UpdateList(monsterList);
		UpdateList (eventList);
		UpdateList (updatablesGameObjects);
	}
	
	StageController.StageNumber GetStageNumber(gameControllerStates state)
	{
		switch(state)
		{
		case gameControllerStates.stage1:
		default:
			return StageController.StageNumber.stage1;
		case gameControllerStates.stage2:
			return StageController.StageNumber.stage2;
		case gameControllerStates.stage3:
			return StageController.StageNumber.stage3;
		case gameControllerStates.stage4:
			return StageController.StageNumber.stage4;
		case gameControllerStates.stage5:
			return StageController.StageNumber.stage5;
		case gameControllerStates.stage6:
			return StageController.StageNumber.stage6;
		}
	}		

	public void ChangeScene(string nextScene)
	{
		Application.LoadLevel(nextScene);
	}
	
	void SceneChanged(SceneType type)
	{
		RenderSettings.ambientLight = new Color(this.GammaCorrection, this.GammaCorrection, this.GammaCorrection, 1.0f);
		weaponList = new List<WeaponController>();
		monsterList = new List<MonsterGeneric>();
		buttonList = new List<ButtonController>();	
		//GetMonstersInScene();
		GetButtonsInScene();
	}
	
	void GetMonstersInScene()
	{
		GameObject [] tempMonsterVect = GameObject.FindGameObjectsWithTag(Tags.Monster);
		for (int i = 0; i < tempMonsterVect.Length; i++) {
			for (int j = i+1; j < tempMonsterVect.Length; j++) {
				//print (tempMonsterVect[i].name);
				if(tempMonsterVect[i].name == tempMonsterVect[j].name)
				{
					Debug.LogWarning ("equal names");
					tempMonsterVect[i].name = "M" + (double.Parse(tempMonsterVect[i].name.Substring(1,15)) + 
						Random.Range(666,66666)).ToString();
					tempMonsterVect[i].SendMessage("SetUniqueID",tempMonsterVect[i].name);
				}
			} 
			addMonster(tempMonsterVect[i]);
		}
	}
	
	void GetButtonsInScene()
	{
		GameObject [] tempButtonVect = GameObject.FindGameObjectsWithTag("Button");
		for (int i = 0; i < tempButtonVect.Length; i++) {
			addButton(tempButtonVect[i]);
		}	
	}

	public void UpdateList(List<WeaponController> list)
	{
		if(list == null)
			return;
		for (int i = 0; i < list.Count; i++) {
            if (list[i] == null)
                list.RemoveAt(i);
            else
                list[i].UpdateWeaponController();
		}
	}
		
	public void UpdateList(List<MonsterGeneric> list)
	{
		if(list == null || list.Count == 0)
		{
			if(timeCountInCurrentRoom < 0)			
				stageController.RoomCleared ();
			return;
		}
		if(list.Count == 1)
		{
			if(list[0].category == MonsterGeneric.monsterCategory.miniboss)
			{
				stageController.RoomCleared ();
				list[0].DestroyItSelf();
				return;
			}
		}
		bool onlyProjectileLeft = true;
		if(stageController.GetCurrentRoom() == 11)
			onlyProjectileLeft = false;
		else
		{
			for (int i = 0; i < list.Count; i++) {
				if(list[i].GetMonsterType() != MonsterGeneric.monsterTypes.projectile)
					onlyProjectileLeft = false;
			}
		}
		if (onlyProjectileLeft)
		{
			stageController.RoomCleared ();
			for (int i = 0; i < list.Count; i++) {
				list[i].DestroyItSelf();
			}
			return;
		}
		for (int i = 0; i < list.Count; i++) {
            if (list[i] == null)
                list.RemoveAt(i);
            else
                list[i].UpdateMonsterGeneric();
		}
	}

	public void UpdateList(List<EventController> list)
	{
		if(list == null)
			return;
		for (int i = 0; i < list.Count; i++) {
            if (list[i] == null)
                list.RemoveAt(i);
            else
                list[i].UpdateEvent();
		}
	}

	public void UpdateList(List<GameObject> list)
	{
		if(list == null)
			return;
		for (int i = 0; i < list.Count; i++) {
            if (list[i] == null)
                list.RemoveAt(i);
			else
                list[i].SendMessage ("UpdateMeGameController");
		}
	}

	public void UpdateList(List<ButtonController> list)
	{
		if(list == null)
			return;
		for (int i = 0; i < list.Count; i++) {
            if (list[i] == null)
                list.RemoveAt(i);
            else
                list[i].UpdateButtonController();
		}
	}
	
	public void addWeapon(GameObject w)
	{
		for (int i = 0; i < weaponList.Count; i++) {
			if(w.name == weaponList[i].name)
			{
				//Debug.LogWarning ("equal names");
				w.name = weaponList[i].weaponName + weaponList.Count.ToString ();
				w.SendMessage("SetUniqueID",w.name);
			}
		} 
		weaponList.Add(w.GetComponent<WeaponController>());	
	}	
		
	public bool removeWeapon(GameObject w)
	{
		return weaponList.Remove(w.GetComponent<WeaponController>());
	}
	
	public void removeWeapon(string ID)
	{	
		for (int i = 0; i < weaponList.Count; i++) {
			if(weaponList[i].UniqueID == ID)
			{
				WeaponController w = weaponList[i];
				weaponList.RemoveAt(i);
				w.DestroyItSelf();
				return;
			}
		}
	}
	
	public void addMonster(GameObject m)
	{	
		for (int i = 0; i < monsterList.Count; i++) {
			if(m.name == monsterList[i].name)
			{
				m.name = "M" + monsterList.Count.ToString(); //(double.Parse(m.name.Substring(1,15)) + Random.Range(666,66666)).ToString();
				m.SendMessage("SetUniqueID",m.name);
			}
		} 
		monsterList.Add(m.GetComponent<MonsterGeneric>());	
	}	
		
	public bool removeMonster(GameObject m)
	{
		return monsterList.Remove(m.GetComponent<MonsterGeneric>());
	}
	
	public void addButton(ButtonController b)
	{	
		buttonList.Add(b);	
	}
	
	public void addButton(GameObject b)
	{		
		buttonList.Add(b.GetComponent<ButtonController>());	
	}

	public void addEvent(GameObject e)
	{	
		eventList.Add(e.GetComponent<EventController>());	
	}	

	public void removeEvent(GameObject e)
	{
		eventList.Remove(e.GetComponent<EventController>());
		Destroy (e);
	}

	public void ClearEventsInRoom()
	{
		GameObject temp;
		if(eventList == null)
			return;
		while(eventList.Count != 0)
		{
			temp = eventList[0].gameObject;
			eventList.Remove (eventList[0]);
			Destroy (temp);
		}
	}


	public void addUpdatableGameObject(GameObject u)
	{	
		updatablesGameObjects.Add(u);	
	}	
	
	public void removeUpdatableGameObject(GameObject u)
	{
		updatablesGameObjects.Remove(u);
		Destroy (u);
	}

	public void removeUpdatableGameObjectWithouthDestroy(GameObject u)
	{
		updatablesGameObjects.Remove(u);
	}

	public void PauseGame()
	{
		nextState = gameControllerStates.pause;	
		openUI = false;			
	}
	
	public void UnPausing()
	{
		if(openUI)
			playerController.PoolForWeaponEquipped();
		openUI = false;
		if(monsterList != null)
		{
			for (int i = 0; i < monsterList.Count; i++) {
				monsterList[i].UnPause();
			}
		}
		UI.Unpause();
		UnPauseGame();
		return;				
	}
	
	public void UnPauseGame()
	{		
		nextState = saveState;	
		currentCursor.nextState = CursorController.cursorControllerStates.normal;
		ChangeUIState(UI.saveState);
		playerHit.gameObject.SetActive(true);
	}
	
	public void PauseInventory()
	{
		nextState = gameControllerStates.pause;	
		openUI = true;
	}
	
	void ChangeUIState(InterfaceController.interfaceControllerStates state)
	{
		UI.nextState = state;
		UI.UpdateInterfaceController();	
	}
	
	public void BeginGame()
	{
		nextState = gameControllerStates.intro2;				
	}
	
	public void SetGamma()
	{
		GUISlider slider = GameObject.Find("GammaSLIDER").transform.FindChild("GuiSLIDER").GetComponent<GUISlider>();
		this.GammaCorrection = slider.GammaCorrection;
		RenderSettings.ambientLight = new Color(GammaCorrection, GammaCorrection, GammaCorrection, 1.0f);
	}

	public void SetGamma(float savedGamma)
	{
		GammaCorrection = savedGamma;
		RenderSettings.ambientLight = new Color(GammaCorrection, GammaCorrection, GammaCorrection, 1.0f);
	}
	
	public void ExitGame()
	{
		Application.Quit();	
	}
	
	public void SwitchSFX(float vol)
	{
		PlayerAudioSource.volume = vol;	
		RightWeaponAudioSource.volume = vol;
		LeftWeaponAudioSource.volume = vol;
		MonsterAudioSource.volume = vol;		
		//inventory.SwitchMaterial("SFX",mute);
	}
	
	public void SwitchSFXs()
	{
		PlayerAudioSource.mute = !PlayerAudioSource.mute;	
		RightWeaponAudioSource.mute = !RightWeaponAudioSource.mute;
		LeftWeaponAudioSource.mute = !LeftWeaponAudioSource.mute;
		MonsterAudioSource.mute = !MonsterAudioSource.mute;
	}
	
	public void SwitchMusic(float vol)
	{
	 	MusicAudioSource.volume = vol;	
		//inventory.SwitchMaterial("music",mute);
	}
	
	public void SwitchMusics()
	{
		MusicAudioSource.mute = !MusicAudioSource.mute;
	}

	public void SwitchCountDown(bool on)
	{
	 	countDownAfterPause = on;	
		UI.SwitchMaterial("countDown",on);
	}
	
	public void SwitchCountDowns()
	{
		countDownAfterPause = !countDownAfterPause;
	}
	
	public void PlayErrorSound()
	{
		PlayerAudioSource.PlayOneShot(interfaceError);	
	}
			
	public void GameOver()
	{
		if(currentState == gameControllerStates.gameover)
			return;
		nextState = gameControllerStates.gameover;
		cameraFollow.FocusPlayer ();
		Time.timeScale = 0.3f;
	}

	public void GameOverScreen()
	{
		cameraFollow.ResetCamera ();
		KillThemAll ();
		stageController.RoomCleared ();
		//Application.LoadLevel("gameover");
		playerController.transform.position = new Vector3(50,50,0);
		playerController.nextState = PlayerController.playerControllerStates.VOID;
		UI.GameOver();
	}

	public void StartGame()
	{
		ChangeScene("arena");
		for (int i = 0; i < monsterList.Count; i++) {
			Destroy(monsterList[i].gameObject);
		}
		monsterList = new List<MonsterGeneric>();
		//prototipo.gameObject.SetActive(true);	
		if(weaponList == null)
			return;
		for (int i = 0; i < weaponList.Count; i++) {
			weaponList[i].DestroyItSelf();
		}
		playerController.Reset();
		nextState = gameControllerStates.stage1;
	}

	public void StartTestHardcore()
	{
		StartTestNormal ();
		stageController.SetHardcore ();
	}

	public void StartTestNormal()
	{
		for (int i = 0; i < monsterList.Count; i++) {
			Destroy(monsterList[i].gameObject);
		}
		monsterList = new List<MonsterGeneric>();
		//prototipo.gameObject.SetActive(true);	
		if(weaponList == null)
			return;
		for (int i = 0; i < weaponList.Count; i++) {
			weaponList[i].DestroyItSelf();
		}
		playerController.Reset();
		cameraFollow.ResetCamera ();
		nextState = gameControllerStates.stage1;
	}

    public void StartTestTutorial()
    {
        nextState = gameControllerStates.tutorial;
        SceneManager.LoadScene("Tutorial");
    }

    public void Restart()
    {
        for (int i = 0; i < monsterList.Count; i++)
        {
            Destroy(monsterList[i].gameObject);
        }
        monsterList = new List<MonsterGeneric>();
        //prototipo.gameObject.SetActive(true);	
        if (weaponList == null)
            return;
        for (int i = 0; i < weaponList.Count; i++)
        {
            weaponList[i].DestroyItSelf();
        }
        playerController.Reset();
        cameraFollow.ResetCamera();
        //GameObject.Find("menu").SetActive(false);
        nextState = gameControllerStates.menu;
        SceneManager.LoadScene("MainGame");
    }

    public GameObject []monsterChaser;
	public MonsterTainted []chaserTainted;
	public GameObject []monsterCrowd;
	GameObject []monsterCrowdCategory2;
	public MonsterTainted []crowdTainted;
	public GameObject []monsterShooter;
	public MonsterTainted []shooterTainted;
	public GameObject []monsterMuscle;
	public MonsterTainted []muscleTainted;

	public WeaponController []weaponAOE;
	public WeaponController []weaponDefense;
	public WeaponController []weaponCrush;	
	WeaponController AOE,crush,defense;

	public MonsterGeneric.monsterCategory GetCategoryofMonsterChosen()
	{
		return chosenMonster.GetComponent<MonsterGeneric>().category;
	}
	
	public WeaponController GetAOE()
	{
		return AOE;
	}
	
	public void SetAOE(int n)
	{
		AOE = weaponAOE[n];
	}

    public void SetAOE(WeaponController n)
    {
        AOE = n;
    }

    public WeaponController GetCrush()
	{
		return crush;
	}
	
	public void SetCrush(int n)
	{
		crush = weaponCrush[n];
    }

    public void SetCrush(WeaponController n)
    {
        crush = n;
    }

    public WeaponController GetDefense()
	{
		return defense;
	}
	
	public void SetDefense(int n)
	{
		defense = weaponDefense[n];
	}
		
	public void JoinTwoHydras(GameObject m1, GameObject m2)
	{		
		for (int i = 0; i < recentlyJoined.Count; i++) {
			if(recentlyJoined[i] == m1.name || recentlyJoined[i] == m2.name)			
				return;
		}
		if (m1.GetComponent<MonsterGeneric>().CheckIfCurrentOrNextState(MonsterStates.Death) || m2.GetComponent<MonsterGeneric>().CheckIfCurrentOrNextState(MonsterStates.Death))
			return;
		recentlyJoined.Add (m1.name);
		recentlyJoined.Add (m2.name);
		recentlyJoinedTime.Add(3);
		recentlyJoinedTime.Add(3);		
		Vector3 v = m1.transform.position;
		m2.transform.position = new Vector3 (m2.transform.position.x,v.y,m2.transform.position.z);
		m1.SendMessage ("AnimateJoinTwoHydras",m1.transform.position.x > m2.transform.position.x ? true:false);
		m1.SendMessage ("InstantiateJoinedHydra");
		m2.SendMessage ("AnimateJoinTwoHydras",m2.transform.position.x > m1.transform.position.x ? true:false);
		return;
	}

	public void ExplodeTwoHydras(GameObject m1, GameObject m2, GameObject explosion)
	{		
		for (int i = 0; i < recentlyJoined.Count; i++) {
			if(recentlyJoined[i] == m1.name || recentlyJoined[i] == m2.name)			
				return;
		}
		recentlyJoined.Add (m1.name);
		recentlyJoined.Add (m2.name);
		recentlyJoinedTime.Add(3);
		recentlyJoinedTime.Add(3);		
		Vector3 position = new Vector3(Mathf.Abs(m1.transform.position.x - m2.transform.position.x)/2 + m1.transform.position.x ,
		                               Mathf.Abs(m1.transform.position.y - m2.transform.position.y)/2 + m1.transform.position.y, 1.5f);
		MonsterGeneric.toughness toughness = m1.GetComponent<MonsterGeneric>().monsterToughness;
		GameObject temp = Instantiate(explosion.gameObject,position,Quaternion.identity) as GameObject;
		temp.SendMessage (MnstMsgs.SetToughness,toughness);
		temp.GetComponent<MonsterGeneric>().nextState = MonsterStates.Start;
		addMonster(temp);
		return;
	}
	
	void HandleHydras()
	{
		for (int i = 0; i < recentlyJoinedTime.Count; i++) {
			recentlyJoinedTime[i] -= GameController.deltaTime;
			if(recentlyJoinedTime[i] < 0)
			{
				recentlyJoined.RemoveAt(i);
				recentlyJoinedTime.RemoveAt(i);
			}
		}	
	}

	public void StartMonsters()
	{
		timeCountInCurrentRoom = 1;
		for (int i = 0; i < monsterList.Count; i++) {
			monsterList[i].nextState = MonsterStates.Start;
		}
	}

	public GameObject GetChaserMonster(bool tainted)
	{
		int monsterChosen = Random.Range (0,monsterChaser.Length);
		if (tainted) 
		{
			return chaserTainted[monsterChosen].monsterPrefab[Random.Range(0,chaserTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterChaser[monsterChosen];
	}

	public GameObject GetChaser(int monsterChosen, bool tainted)
	{
		if (tainted) 
		{
			return chaserTainted[monsterChosen].monsterPrefab[stageController.GetRoom ().IsThereADarkness () ? darkTaintedNumber : Random.Range(0,chaserTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterChaser[monsterChosen];
	}

	public GameObject GetCCMonster(bool tainted)
	{
		int monsterChosen = Random.Range (0,monsterCrowd.Length);
		if (tainted) 
		{
			return crowdTainted[monsterChosen].monsterPrefab[Random.Range(0,crowdTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterCrowd[monsterChosen];
	}

	public GameObject GetCC(int monsterChosen, bool tainted)
	{
		if (tainted) 
		{
			return crowdTainted[monsterChosen].monsterPrefab[stageController.GetRoom ().IsThereADarkness () ? darkTaintedNumber : Random.Range(0,crowdTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterCrowd[monsterChosen];
	}

	public GameObject GetShooterMonster(bool tainted)
	{
		int monsterChosen = Random.Range (0,monsterShooter.Length);
		if (tainted) 
		{
			return shooterTainted[monsterChosen].monsterPrefab[Random.Range(0,shooterTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterShooter[monsterChosen];
	}

	public GameObject GetShooter(int monsterChosen, bool tainted)
	{
		if (tainted) 
		{
			return shooterTainted[monsterChosen].monsterPrefab[stageController.GetRoom ().IsThereADarkness () ? darkTaintedNumber : Random.Range(0,shooterTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterShooter[monsterChosen];
	}

	public GameObject GetMuscleMonster(bool tainted)
	{
		int monsterChosen = Random.Range (0,monsterMuscle.Length);
		if (tainted) 
		{
			return muscleTainted[monsterChosen].monsterPrefab[Random.Range(0,muscleTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterMuscle[monsterChosen];
	}

	public GameObject GetMuscle(int monsterChosen, bool tainted)
	{
		if (tainted) 
		{
			return muscleTainted[monsterChosen].monsterPrefab[stageController.GetRoom ().IsThereADarkness () ? darkTaintedNumber : Random.Range(0,muscleTainted[monsterChosen].monsterPrefab.Length)];
		}
		else
			return monsterMuscle[monsterChosen];
	}

	public GameObject GetChaserTainted(int monsterChosen, int tainted)
	{
		return chaserTainted[monsterChosen].monsterPrefab[tainted];
	}

	public GameObject GetCCTainted(int monsterChosen, int tainted)
	{
		return crowdTainted[monsterChosen].monsterPrefab[tainted];
	}

	public GameObject GetShooterTainted(int monsterChosen, int tainted)
	{
		return shooterTainted[monsterChosen].monsterPrefab[tainted];
	}

	public GameObject GetMuscleTainted(int monsterChosen, int tainted)
	{
		return muscleTainted[monsterChosen].monsterPrefab[tainted];
	}

	//FUncao para montar uma lista de todos os monstros do tipo Crowd2 do jogo (incluindo os que sao tainted)
	void AssembleCrowd2List()
	{
		List<GameObject> monsterOfCrowd = new List<GameObject>();
		for (int i = 0; i < monsterCrowd.Length; i++) {
			if(monsterCrowd[i].GetComponent<MonsterGeneric>().GetMonsterCategory() == MonsterGeneric.monsterCategory.crowd2)
				monsterOfCrowd.Add (monsterCrowd[i]);
			if(monsterCrowd[i].GetComponent<Hydra>())
			{
				//checa se o filho eh crowd2
				if(monsterCrowd[i].GetComponent<Hydra>().smallerCopy.GetComponent<MonsterGeneric>().GetMonsterCategory()== MonsterGeneric.monsterCategory.crowd2)
					monsterOfCrowd.Add (monsterCrowd[i].GetComponent<Hydra>().smallerCopy);
				//checa se o neto eh crowd2
				if(monsterCrowd[i].GetComponent<Hydra>().smallerCopy.GetComponent<Hydra>().smallerCopy.GetComponent<MonsterGeneric>().GetMonsterCategory()== MonsterGeneric.monsterCategory.crowd2)
					monsterOfCrowd.Add (monsterCrowd[i].GetComponent<Hydra>().smallerCopy.GetComponent<Hydra>().smallerCopy);
			}
			if(monsterCrowd[i].GetComponent<Swan>())
			{
				if(monsterCrowd[i].GetComponent<Swan>().swanduck.GetComponent<MonsterGeneric>().GetMonsterCategory()== MonsterGeneric.monsterCategory.crowd2)
					monsterOfCrowd.Add (monsterCrowd[i].GetComponent<Swan>().swanduck);
			}
		}
		for (int i = 0; i < chaserTainted.Length; i++) {
			for (int j = 0; j < chaserTainted[i].monsterPrefab.Length; j++) {
				if(chaserTainted[i].monsterPrefab[j].GetComponent<MonsterGeneric>().GetMonsterCategory() == MonsterGeneric.monsterCategory.crowd2) 
					monsterOfCrowd.Add (chaserTainted[i].monsterPrefab[j]);
			}
		}
		for (int i = 0; i < crowdTainted.Length; i++) {
			for (int j = 0; j < crowdTainted[i].monsterPrefab.Length; j++) {
				if(crowdTainted[i].monsterPrefab[j].GetComponent<MonsterGeneric>().GetMonsterCategory() == MonsterGeneric.monsterCategory.crowd2) 
					monsterOfCrowd.Add (crowdTainted[i].monsterPrefab[j]);
				if(crowdTainted[i].monsterPrefab[j].GetComponent<Hydra>())
				{
					if(crowdTainted[i].monsterPrefab[j].GetComponent<Hydra>().smallerCopy)
					{
						//checa se o filho eh crowd2
						if(crowdTainted[i].monsterPrefab[j].GetComponent<Hydra>().smallerCopy.GetComponent<MonsterGeneric>().GetMonsterCategory() == MonsterGeneric.monsterCategory.crowd2)
							monsterOfCrowd.Add (crowdTainted[i].monsterPrefab[j].GetComponent<Hydra>().smallerCopy);
						//checa se o neto eh crowd2
						if(crowdTainted[i].monsterPrefab[j].GetComponent<Hydra>().smallerCopy.GetComponent<Hydra>().smallerCopy.GetComponent<MonsterGeneric>().GetMonsterCategory()== MonsterGeneric.monsterCategory.crowd2)
							monsterOfCrowd.Add (crowdTainted[i].monsterPrefab[j].GetComponent<Hydra>().smallerCopy.GetComponent<Hydra>().smallerCopy);
					}
				}
				if(crowdTainted[i].monsterPrefab[j].GetComponent<Swan>())
				{
					if(crowdTainted[i].monsterPrefab[j].GetComponent<Swan>().swanduck)
					{
						//checa se o filho eh crowd2
						if(crowdTainted[i].monsterPrefab[j].GetComponent<Swan>().swanduck.GetComponent<MonsterGeneric>().GetMonsterCategory() == MonsterGeneric.monsterCategory.crowd2)
							monsterOfCrowd.Add (crowdTainted[i].monsterPrefab[j].GetComponent<Swan>().swanduck);
					}
				}
			}
		}
		for (int i = 0; i < shooterTainted.Length; i++) {
			for (int j = 0; j < shooterTainted[i].monsterPrefab.Length; j++) {
				if(shooterTainted[i].monsterPrefab[j].GetComponent<MonsterGeneric>().GetMonsterCategory() == MonsterGeneric.monsterCategory.crowd2) 
					monsterOfCrowd.Add (shooterTainted[i].monsterPrefab[j]);
			}
		}
		for (int i = 0; i < muscleTainted.Length; i++) {
			for (int j = 0; j < muscleTainted[i].monsterPrefab.Length; j++) {
				if(muscleTainted[i].monsterPrefab[j].GetComponent<MonsterGeneric>().GetMonsterCategory() == MonsterGeneric.monsterCategory.crowd2) 
					monsterOfCrowd.Add (muscleTainted[i].monsterPrefab[j]);
			}
		}

		monsterCrowdCategory2 = new GameObject[monsterOfCrowd.Count];
		for (int i = 0; i < monsterOfCrowd.Count; i++) {
			monsterCrowdCategory2[i] = monsterOfCrowd[i];
		}
	}

	public List<Transform> SummonACrowd(Vector2 minSpot, Vector2 maxSpot)
	{
		//int currentMonsterCategory = 2;//chaser,crowd,shooter,muscle
		//int monsterNumberinArray = Random.Range(0,gameController.monsterCrowd.Length);
		List<Transform> monsterList = new List<Transform>();
		GameObject monster = monsterCrowdCategory2[Random.Range(0,monsterCrowdCategory2.Length)];
		int monterQuantity = Random.Range (monster.GetComponent<MonsterGeneric>().monsterMin,monster.GetComponent<MonsterGeneric>().monsterMax+1);
		for (int k = 0; k < monterQuantity; k++) {
			Vector2 spot = Vector2.one;
			spot = new Vector2(Random.Range (minSpot.x,maxSpot.x),Random.Range (minSpot.y,maxSpot.y));
			while(Vector2.Distance (spot,playerController.transform.position) < GameController.minPlayerDistanceToSpawn)
				spot = new Vector2(Random.Range (minSpot.x,maxSpot.x),Random.Range (minSpot.y,maxSpot.y));
			Vector3 position = new Vector3(spot.x,spot.y,1.5f);
			GameObject temp;
			temp = Instantiate(monster,position,Quaternion.identity) as GameObject;
			int tough = stageController.GetToughness ();
			if(monster.GetComponent<MonsterGeneric>().isTainted)
				tough++;
			if(tough > 5)
				tough = 5;
			if(tough < 1)
				tough = 1;
			temp.SendMessage (MnstMsgs.SetToughness,tough);
			//addMonster(temp);
			//temp.GetComponent<MonsterGeneric>().nextState = MonsterGeneric.monsterStates.VOID;
			temp.GetComponent<CharacterController>().enabled = false;
			monsterList.Add (temp.transform);
		}
		return monsterList;
	}

	public void AddMonster(MonsterGeneric.monsterCategory category,int toughness, int monsternumber, int taint)
	{
		GameObject temp, pref;
		switch(category)
		{
		default:
		case MonsterGeneric.monsterCategory.chaser:
			if(taint == 0)
				pref = monsterChaser[monsternumber];
			else
				pref = GetChaserTainted(monsternumber,taint-1);
			break;
		case MonsterGeneric.monsterCategory.crowd1:
		case MonsterGeneric.monsterCategory.crowd2:
			if(taint == 0)
				pref = monsterCrowd[monsternumber];
			else
				pref = GetCCTainted(monsternumber,taint-1);
			break;
		case MonsterGeneric.monsterCategory.shooter:
			if(taint == 0)
				pref = monsterShooter[monsternumber];
			else
				pref = GetShooterTainted(monsternumber,taint-1);
			break;
		case MonsterGeneric.monsterCategory.muscle:
			if(taint == 0)
				pref = monsterMuscle[monsternumber];
			else
				pref = GetMuscleTainted(monsternumber,taint-1);
			break;
		}
		temp = Instantiate(pref,new Vector3(50,50,1.5f),Quaternion.identity) as GameObject;
		temp.SendMessage (MnstMsgs.SetToughness,toughness);
		temp.GetComponent<MonsterGeneric> ().nextState = MonsterStates.Start;
		addMonster (temp);	
	}

	public void AddEvent(int eventNumber)
	{
		GameObject temp, pref;
		pref = GetComponent<EventList>().listOfMiniBoss[eventNumber];
		temp = Instantiate(pref,new Vector3(50,50,1.5f),Quaternion.identity) as GameObject;
		temp.GetComponent<EventController>().StartEvent(StageController.StageNumber.stage1);
		addEvent (temp);
	}

	public float timeScaletoTest = 0.1f;

	public TextMesh lvl;
	public int level;

	public void AddLevel()
	{
		level++;
		if(level > 5)
			level = 5;
		lvl.text = level.ToString();
	}
	
	public void SubtractLevel()
	{
		level--;
		if(level < 1)
			level = 1;
		lvl.text = level.ToString();
	}

	public TextMesh tainted;
	public int taint;
	
	public void AddTainted()
	{
		taint++;
		if(taint > 4)
			taint = 4;
		tainted.text = taint.ToString();
	}
	
	public void SubtractTainted()
	{
		taint--;
		if(taint < 0)
			taint = 0;
		tainted.text = taint.ToString();
	}

	public void KillThemAll()
	{
		for (int i = 0; i < monsterList.Count; i++) {
			monsterList[i].Hit (10000);
		}
	}

	public void RegisterAsInterestedOnPlayerShooting(GameObject g)
	{
		listOfInterestedsInPlayerShooting.Add (g);
	}

	public void UnregisterAsInterestedOnPlayerShooting(GameObject g)
	{
		listOfInterestedsInPlayerShooting.Remove(g);
	}

	public void ClearListofInterestedOnPlayerShooting()
	{
		listOfInterestedsInPlayerShooting = new List<GameObject> ();
	}

	public void OnPlayerShooting(GameObject weapon)
	{
		for (int i = 0; i < listOfInterestedsInPlayerShooting.Count; i++) {
			if(listOfInterestedsInPlayerShooting[i])
				listOfInterestedsInPlayerShooting[i].SendMessage ("OnPlayerShooting",weapon);
		}
	}

	public void HitAllMonster(int damage)
	{
		for (int i = 0; i < monsterList.Count; i++) {
			monsterList[i].Hit (damage);
		}
	}

	public float CountNumberOfMonstersInTheRoom()
	{
		float count = 0;
		for (int i = 0; i < monsterList.Count; i++) {
			switch(monsterList[i].GetMonsterType ())
			{
			default: 
				count++;
				break;
			case MonsterGeneric.monsterTypes.boss:
			case MonsterGeneric.monsterTypes.hydraexplosion:
			case MonsterGeneric.monsterTypes.projectile:
			case MonsterGeneric.monsterTypes.separatepiece:
				break;
			}
		}
		return count;
	}

    public float CountNumberOfMonstersInTheRoom(MonsterGeneric.monsterCategory category)
    {
        float count = 0;
        for (int i = 0; i < monsterList.Count; i++)
        {
            if (monsterList[i].GetMonsterCategory() == category)
                count++;            
        }
        return count;
    }

    public GameObject FindNearestMonster(Vector3 weaponPos, float minimumDistance)
	{
		int closerMonster = -1;
		float currentMinDistance = minimumDistance;
		float calculatedDistance;
		if(monsterList == null || monsterList.Count == 0)
			return null;
		for (int i = 0; i < monsterList.Count; i++) {
			calculatedDistance = Vector2.Distance(weaponPos,monsterList[i].transform.position);
			if(calculatedDistance < currentMinDistance)
			{
				Ray ray;
				RaycastHit hit;
				Vector3 currentDirection = monsterList[i].transform.position - weaponPos;
				ray = new Ray(weaponPos,currentDirection);
				int layerMask = 3072;
				if(Physics.Raycast(ray, out hit, 10000,layerMask)){
					if(hit.collider.name == monsterList[i].name)
						closerMonster = i;
				}				
			}
		}
		if(closerMonster == -1)
			return null;
		else
			return monsterList[closerMonster].gameObject;
	}

	public Transform FindClosestMonster(Vector3 position, string excludeThisOne)
	{
		if(monsterList == null || monsterList.Count == 0)
			return null;
		
		Transform closestMonster = monsterList[0].transform;
		if(monsterList[0].name == excludeThisOne)
			closestMonster = monsterList[1].transform;
		for (int i = 0; i < monsterList.Count; i++) {
			if(Vector2.Distance (monsterList[i].transform.position,position) < Vector2.Distance (closestMonster.position,position))
			{
				if(monsterList[i].name != excludeThisOne && monsterList[i].GetMonsterType () != MonsterGeneric.monsterTypes.projectile 
				   && monsterList[i].GetMonsterType () != MonsterGeneric.monsterTypes.hydraexplosion)
					closestMonster = monsterList[i].transform;
			}
		}
		return closestMonster;
	}

	public List<Transform> FindAllMonsterWithinDistance(Vector3 position, float minimumDistance)
	{
		if(monsterList == null || monsterList.Count == 0)
			return null;

		List<Transform> closeMonstersList = new List<Transform>();

		for (int i = 0; i < monsterList.Count; i++) {
			if(Vector2.Distance (monsterList[i].transform.position,position) < minimumDistance)
			{
				if(monsterList[i].GetMonsterType () != MonsterGeneric.monsterTypes.projectile && monsterList[i].GetMonsterType () != MonsterGeneric.monsterTypes.hydraexplosion)
					closeMonstersList.Add (monsterList[i].transform);
			}
		}
		return closeMonstersList;
	}

	public List<MonsterGeneric> GetMonsterList()
	{
		return monsterList;
	}

}