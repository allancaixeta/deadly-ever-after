using UnityEngine;

public class SetRenderQueue : MonoBehaviour {
    
	public int renderOrder;
    
    protected void Awake() {
		GetComponent<Renderer>().sortingLayerName = "UI";
		GetComponent<Renderer>().sortingOrder = renderOrder;
    }
}