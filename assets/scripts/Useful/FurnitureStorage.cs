﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class furnituredefinition{
	public GameObject prefab;
	public furniture.typeOfFurniture furnituretype;
}

public class FurnitureStorage : MonoBehaviour {

	public furnituredefinition [] allAnimalFurnitures;
	public furnituredefinition [] allBodyFurnitures;
	public furnituredefinition [] allFireFurnitures;
	public furnituredefinition [] allMiscFurnitures;
	public furnituredefinition [] allPlantFurnitures;
	public furnituredefinition [] allStoneFurnitures;
	public furnituredefinition [] allSupernaturalFurnitures;
	public furnituredefinition [] allUtFurnitures;
	public furnituredefinition [] allWoodFurnitures;

	int chosenOne;
	List<furnituredefinition> allOfThisFurniture;

	public GameObject GetFurniturePrefab(furniture.typeOfFurniture type)
	{
		switch(type)
        { 
        //animal
        case furniture.typeOfFurniture.crow:       
            allOfThisFurniture = new List<furnituredefinition>();
            for (int i = 0; i < allAnimalFurnitures.Length; i++)
            {
                if (allAnimalFurnitures[i].furnituretype == type)
                    allOfThisFurniture.Add(allAnimalFurnitures[i]);
            }
            chosenOne = Random.Range(0, allOfThisFurniture.Count);
            return allOfThisFurniture[chosenOne].prefab;

        //bodies
        case furniture.typeOfFurniture.bonesH:
		case furniture.typeOfFurniture.bonesV:
        case furniture.typeOfFurniture.bodiesH:
        case furniture.typeOfFurniture.bodiesV:
            allOfThisFurniture = new List<furnituredefinition>();
			for (int i = 0; i < allBodyFurnitures.Length; i++) {
				if(allBodyFurnitures[i].furnituretype == type)
					allOfThisFurniture.Add(allBodyFurnitures[i]);
			}
			chosenOne = Random.Range (0,allOfThisFurniture.Count);
			return allOfThisFurniture[chosenOne].prefab;
		default:
            //fire
        case furniture.typeOfFurniture.fire:
		case furniture.typeOfFurniture.torch:
        case furniture.typeOfFurniture.hallowen:
			allOfThisFurniture = new List<furnituredefinition>();
			for (int i = 0; i < allFireFurnitures.Length; i++) {
				if(allFireFurnitures[i].furnituretype == type)
					allOfThisFurniture.Add(allFireFurnitures[i]);
			}
			chosenOne = Random.Range (0,allOfThisFurniture.Count);
			return allOfThisFurniture[chosenOne].prefab;
        //stone
		case furniture.typeOfFurniture.grave:
		case furniture.typeOfFurniture.stonealtarH:
		case furniture.typeOfFurniture.stonealtarV:
		case furniture.typeOfFurniture.stonecolumn:
		case furniture.typeOfFurniture.religiousstatue:
			allOfThisFurniture = new List<furnituredefinition>();
			for (int i = 0; i < allStoneFurnitures.Length; i++) {
				if(allStoneFurnitures[i].furnituretype == type)
					allOfThisFurniture.Add(allStoneFurnitures[i]);
			}
			chosenOne = Random.Range (0,allOfThisFurniture.Count);
			return allOfThisFurniture[chosenOne].prefab;
        //supernatural
        case furniture.typeOfFurniture.cristalball:
		case  furniture.typeOfFurniture.magiccircle:
			allOfThisFurniture = new List<furnituredefinition>();
			for (int i = 0; i < allSupernaturalFurnitures.Length; i++) {
				if(allSupernaturalFurnitures[i].furnituretype == type)
					allOfThisFurniture.Add(allSupernaturalFurnitures[i]);
			}
			chosenOne = Random.Range (0,allOfThisFurniture.Count);
			return allOfThisFurniture[chosenOne].prefab;
        //util
        case furniture.typeOfFurniture.road:
        case furniture.typeOfFurniture.pileH:
        case furniture.typeOfFurniture.pileV:
        case furniture.typeOfFurniture.groceryH:
        case furniture.typeOfFurniture.groceryV:
        case furniture.typeOfFurniture.jailH:
        case furniture.typeOfFurniture.jailV:
            case furniture.typeOfFurniture.target:
        case furniture.typeOfFurniture.ironchain:
			allOfThisFurniture = new List<furnituredefinition>();
			for (int i = 0; i < allUtFurnitures.Length; i++) {
				if(allUtFurnitures[i].furnituretype == type)
					allOfThisFurniture.Add(allUtFurnitures[i]);
			}
			chosenOne = Random.Range (0,allOfThisFurniture.Count);
			return allOfThisFurniture[chosenOne].prefab;
        //wood
		case furniture.typeOfFurniture.woodchairH:
		case furniture.typeOfFurniture.woodchairV:
		case furniture.typeOfFurniture.chair:
        case furniture.typeOfFurniture.barrel:
        case furniture.typeOfFurniture.box:
        case furniture.typeOfFurniture.bed:
        case furniture.typeOfFurniture.fenceH:
        case furniture.typeOfFurniture.fenceV:
        case furniture.typeOfFurniture.tortureH:
        case furniture.typeOfFurniture.tortureV:
            case furniture.typeOfFurniture.dinnertable:
        case furniture.typeOfFurniture.table:
        case furniture.typeOfFurniture.cask:
            allOfThisFurniture = new List<furnituredefinition>();
			for (int i = 0; i < allWoodFurnitures.Length; i++) {
				if(allWoodFurnitures[i].furnituretype == type)
					allOfThisFurniture.Add(allWoodFurnitures[i]);
			}
			chosenOne = Random.Range (0,allOfThisFurniture.Count);
			return allOfThisFurniture[chosenOne].prefab;
        //plant
		case furniture.typeOfFurniture.plantlabiryinth:
        case furniture.typeOfFurniture.tree:
        case furniture.typeOfFurniture.straw:
        case furniture.typeOfFurniture.pumpkin:
        case furniture.typeOfFurniture.flowerH:
        case furniture.typeOfFurniture.flowerV:
            allOfThisFurniture = new List<furnituredefinition>();
			for (int i = 0; i < allPlantFurnitures.Length; i++) {
				if(allPlantFurnitures[i].furnituretype == type)
					allOfThisFurniture.Add(allPlantFurnitures[i]);
			}
			chosenOne = Random.Range (0,allOfThisFurniture.Count);
			return allOfThisFurniture[chosenOne].prefab;
		}
	}
}
