﻿using UnityEngine;
using System.Collections;

public class JumpToBoss : MonoBehaviour {

	StageController stage;
	GameController gameController;
	// Use this for initialization
	void Awake () {
		stage = (StageController) FindObjectOfType(typeof(StageController));	
		gameController = GameController.Instance;	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			if(GetComponent<Collider>().Raycast (ray, out hit, 1000)){
				gameController.KillThemAll();
				stage.RoomCleared ();
				stage.MoveToRoom(11,0);
			}
		}
	}
}
