﻿using UnityEngine;
using System.Collections;
using GameDataEditor;

[System.Serializable]
public class FourDoors
{
	public StageController.door []doors = new StageController.door[4];
}

public class StageConfiguration : MonoBehaviour{
    public string mapName;
	public FourDoors []room = new FourDoors[12];	
   [System.NonSerialized]
    public Vector2[] allRooms = new Vector2[12];
    /*doorSide{
	 *  0 nao tem porta
		1 left
		2 up
		3 right
		4 down
	}*/

    GDEMapData map;

    void Start()
    {
        LoadStageConfiguration();
    }

    public void LoadStageConfiguration()
    {
        LoadAllRoomsFromtheGDE();
        for (int i = 0; i < 12; i++)//analyze room number and attributes each door to a destination
        {
            for (int j = 0; j < 12; j++)
            {
                if (allRooms[i].x - 1 == allRooms[j].x && allRooms[i].y == allRooms[j].y)
                {
                    room[i].doors[0].doorSide = 1; //left door
                    room[i].doors[0].doorDestinationRoom = j + 1;
                }
                if (allRooms[i].x == allRooms[j].x && allRooms[i].y - 1 == allRooms[j].y)
                {
                    room[i].doors[1].doorSide = 2; //up door
                    room[i].doors[1].doorDestinationRoom = j + 1;
                }
                if (allRooms[i].x + 1 == allRooms[j].x && allRooms[i].y == allRooms[j].y)
                {
                    room[i].doors[2].doorSide = 3; //right door
                    room[i].doors[2].doorDestinationRoom = j + 1;
                }
                if (allRooms[i].x == allRooms[j].x && allRooms[i].y + 1 == allRooms[j].y)
                {
                    room[i].doors[3].doorSide = 4; //down door
                    room[i].doors[3].doorDestinationRoom = j + 1;
                }
            }
        }
    }

    void LoadAllRoomsFromtheGDE()
    {
        GDEDataManager.DataDictionary.TryGetCustom(mapName, out map);
        allRooms[0].x = int.Parse(map.Room1.Substring(0, 1));
        allRooms[0].y = int.Parse(map.Room1.Substring(1, 1));
        allRooms[1].x = int.Parse(map.Room2.Substring(0, 1));
        allRooms[1].y = int.Parse(map.Room2.Substring(1, 1));
        allRooms[2].x = int.Parse(map.Room3.Substring(0, 1));
        allRooms[2].y = int.Parse(map.Room3.Substring(1, 1));
        allRooms[3].x = int.Parse(map.Room4.Substring(0, 1));
        allRooms[3].y = int.Parse(map.Room4.Substring(1, 1));
        allRooms[4].x = int.Parse(map.Room5.Substring(0, 1));
        allRooms[4].y = int.Parse(map.Room5.Substring(1, 1));
        allRooms[5].x = int.Parse(map.Room6.Substring(0, 1));
        allRooms[5].y = int.Parse(map.Room6.Substring(1, 1));
        allRooms[6].x = int.Parse(map.Room7.Substring(0, 1));
        allRooms[6].y = int.Parse(map.Room7.Substring(1, 1));
        allRooms[7].x = int.Parse(map.Room8.Substring(0, 1));
        allRooms[7].y = int.Parse(map.Room8.Substring(1, 1));
        allRooms[8].x = int.Parse(map.Room9.Substring(0, 1));
        allRooms[8].y = int.Parse(map.Room9.Substring(1, 1));
        allRooms[9].x = int.Parse(map.Room10.Substring(0, 1));
        allRooms[9].y = int.Parse(map.Room10.Substring(1, 1));
        allRooms[10].x =int.Parse(map.Treasure.Substring(0, 1));
        allRooms[10].y = int.Parse(map.Treasure.Substring(1, 1));
        allRooms[11].x = int.Parse(map.Boss.Substring(0, 1));
        allRooms[11].y = int.Parse(map.Boss.Substring(1, 1));        
    }
}
