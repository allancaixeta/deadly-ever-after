﻿using UnityEngine;
using System.Collections;

#pragma warning disable 0414 // private field assigned but not used.

public class HideShowInterface : MonoBehaviour {

	public enum state
	{
		hide,
		show
	}
	state currentState = state.hide;
	public GameObject pulled;
	Ray ray;
	RaycastHit hit;
	// Use this for initialization
	void Start () {
		pulled.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		switch(currentState)
		{
		case state.hide:
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			if (GetComponent<Collider>().Raycast (ray, out hit, 100)) {	
				currentState = state.show;
				GetComponent<Collider>().enabled = false;
				GetComponent<Renderer>().enabled = false;
				pulled.SetActive (true);
			}
			break;
		case state.show:
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			if (!pulled.GetComponent<Collider>().Raycast (ray, out hit, 100)) {	
				currentState = state.hide;
				GetComponent<Collider>().enabled = true;
				GetComponent<Renderer>().enabled = true;
				pulled.SetActive (false);
			}
			break;
		}

	}
}
