﻿using UnityEngine;
using System.Collections;

public class AddMonsterButton : MonoBehaviour {

	//Objeto alvo para onde voce vai mandar uma mensagem quando o button for apertado
	GameController target;
	
	//Nome da função a ser chamada quando o button for apertado
	public MonsterGeneric.monsterCategory category;
	public MonsterGeneric.toughness toughness;
	public int monsterNumber;
	PrototipoController prototipo;
	// Use this for initialization
	void Start () {
		//gameObject.tag = "Button";  O ideal é que a interfaces esteja em um layer e tag unico dela
		target = (GameController) FindObjectOfType(typeof(GameController));
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
		{
			Ray ray;
			RaycastHit hit;
			ray = Camera.main.ScreenPointToRay((Input.mousePosition));
			if(GetComponent<Collider>().Raycast (ray, out hit, 1000)){
				target.AddMonster(category,target.level,monsterNumber,target.taint);
			}
		}
	}
}
