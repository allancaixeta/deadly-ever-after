﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DepthControl : MonoBehaviour {

	public enum Type
	{
		furniture=0,
		monster=1,
		character=2,
		FX=3,
		UI=4,
		tallfurniture=5
	}
	Vector3 position;
	public Type currentType;
	public Transform parent;
	Transform player;
	Renderer[] allSprites;
	ParticleSystem[] allParticles;
	bool container;
	int lastParentPosition, currentParentY, newSortingOrder;
	public bool onTop, onBottom, backGround;
	public bool onlyForInitialization;
	public bool dontUseParentPosition;
	public float adjustY = 0;
	float remainder;
	Color currentColor;
	SpriteRenderer spriteRenderer;
	// Use this for initialization
	void Start () {
		if(parent == null)
			parent = transform.parent;
		player = GameObject.FindGameObjectWithTag (Tags.Player).transform;
		if (parent == null || dontUseParentPosition) 
		{
			parent = transform;
			currentParentY = (int)(parent.position.x + adjustY);
		}
		else
		{			
			currentParentY = (int)(parent.position.x + adjustY);
			remainder = parent.position.x - (float)currentParentY;
			transform.position = new Vector3(transform.position.x,transform.position.y,remainder);
		}
		lastParentPosition = currentParentY;
		string layerName;
		if(backGround)
			layerName = "Background";
		else
			layerName = "Everything";
		switch(currentType)
		{
		case Type.furniture:
		case Type.tallfurniture:
			if(backGround)
				GetComponent<Renderer>().sortingLayerName = "Background";
			else
				GetComponent<Renderer>().sortingLayerName = "Everything";
			GetComponent<Renderer>().sortingOrder = currentParentY + getHeight();
			spriteRenderer = GetComponent<SpriteRenderer>();
			break;
		case Type.monster:
		case Type.character:
			allSprites = gameObject.GetComponentsInChildren<Renderer>(true);
			List<Renderer> listToSelectSpritesWithSelfControl =  new List<Renderer>();
			List<GameObject> listofEverySpriteWithDepthControl =  new List<GameObject>();
			foreach (Renderer sr in allSprites) {
				if (sr.tag != Tags.IgnoreDepth && !sr.GetComponent<Puppet2D_HiddenBone> () && !sr.GetComponent<ParticleSystem> ()) 
				{
					if (sr.GetComponent<SpriteRenderer> () && sr.GetComponent<SpriteRenderer> ().sprite && sr.gameObject.activeSelf) 
					{
						Sprite s = sr.GetComponent<SpriteRenderer> ().sprite;
						if(s.name != "BoneScaled" && s.name != "splineControl" && s.name != "BoneNoJoint" && s.name != "BoneJoint" &&
							s.name != "IKControl" && s.name != "parentControl" && s.name != "orientControl")
							listToSelectSpritesWithSelfControl.Add (sr);
					}
					else
						listToSelectSpritesWithSelfControl.Add (sr);
				}
				if(sr.GetComponent<DepthControl>() && sr.transform != this.transform)
					listofEverySpriteWithDepthControl.Add(sr.gameObject);
			}
			for (int i = 0; i < listofEverySpriteWithDepthControl.Count; i++) {
				Renderer [] tempAllDescendantSprites = listofEverySpriteWithDepthControl[i].gameObject.GetComponentsInChildren<Renderer>(true);
				foreach (Renderer sr in tempAllDescendantSprites) 
				{
					listToSelectSpritesWithSelfControl.Remove (sr);
				}				
			}
			allSprites = new Renderer[listToSelectSpritesWithSelfControl.Count];
			for (int i = 0; i < listToSelectSpritesWithSelfControl.Count; i++) {
				allSprites[i] = listToSelectSpritesWithSelfControl[i];
			}
			foreach (Renderer sprites in allSprites) {
				sprites.sortingLayerName = layerName;
				sprites.sortingOrder = currentParentY + getHeight();
			}
			break;
		case Type.FX:
			allParticles = gameObject.GetComponentsInChildren<ParticleSystem>(true);
			List<ParticleSystem> listToSelectParticlesWithSelfControl =  new List<ParticleSystem>();
			List<GameObject> listofEveryoneWithDepthControl =  new List<GameObject>();
			foreach (ParticleSystem p in allParticles) {
				listToSelectParticlesWithSelfControl.Add (p);
				if((p.GetComponent<DepthControl>() || p.tag == "IgnoreDepth") && p.transform != this.transform)
					listofEveryoneWithDepthControl.Add(p.gameObject);
			}
			for (int i = 0; i < listofEveryoneWithDepthControl.Count; i++) {
				ParticleSystem [] tempAllDescendantParticles = listofEveryoneWithDepthControl[i].gameObject.GetComponentsInChildren<ParticleSystem>(true);
				foreach (ParticleSystem p in tempAllDescendantParticles) 
				{
					listToSelectParticlesWithSelfControl.Remove (p);
				}				
			}
			allParticles = new ParticleSystem[listToSelectParticlesWithSelfControl.Count];
			for (int i = 0; i < listToSelectParticlesWithSelfControl.Count; i++) {
				allParticles[i] = listToSelectParticlesWithSelfControl[i];
			}

			foreach (ParticleSystem p in allParticles) {
				p.GetComponent<Renderer>().sortingLayerName = layerName;
				p.GetComponent<Renderer>().sortingOrder = currentParentY + getHeight();
			}
			break;
		case Type.UI:
			GetComponent<Renderer>().sortingLayerName = "UI";
			break;
		}
		if(onlyForInitialization)
			this.enabled = false;
	}



	// Update is called once per frame
	void Update () {
		currentParentY = (int)(parent.position.x - adjustY);
		newSortingOrder = currentParentY + getHeight();
		if(lastParentPosition != currentParentY)
		{
			switch(currentType)
			{
			case Type.furniture:
			case Type.tallfurniture:
				GetComponent<Renderer>().sortingOrder = newSortingOrder;
				break;
			case Type.monster:
			case Type.character:
				if(allSprites[0].sortingOrder != newSortingOrder)
				{
					foreach (Renderer sprites in allSprites) {
						sprites.sortingOrder = newSortingOrder;
					}
				}
				break;
			case Type.FX:
				if(allParticles[0].GetComponent<Renderer>().sortingOrder != newSortingOrder)
				{
					foreach (ParticleSystem p in allParticles) {
						p.GetComponent<Renderer>().sortingOrder = newSortingOrder;
					}
				}
				break;
			}
		}
		lastParentPosition = currentParentY;
		if(currentType == Type.tallfurniture)
			CheckPlayerHeight();
	}

	int getHeight()
	{
		return (onTop ? 10 : 0) + (onBottom ? -1 : 0);
	}

	void CheckPlayerHeight()
	{
		if(!spriteRenderer)
			return;
		currentColor = spriteRenderer.color;
		if(player.position.x < currentParentY)
			currentColor.a = 0.7f;
		else
			currentColor.a = 1f;
		iTween.ColorTo (this.gameObject, currentColor, 0.3f);
	}

	public void SetAdjust(float a)
	{
		adjustY += a;
	}
}