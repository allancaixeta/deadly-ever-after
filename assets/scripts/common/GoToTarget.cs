﻿using UnityEngine;
using System.Collections;

public class GoToTarget : MonoBehaviour {

	bool staticTarget = false;
	Transform target, finalizationEventTarget;
	Vector3 targetPosition;
	float currentTimeToReachTarget,totalTimeToReachTarget;
	string message;
	GameController gameController;
	bool targetIsaMonster, messageSent;
	Vector3 bornPosition;
	void Awake()
	{
		gameController = GameController.Instance;
		bornPosition = transform.position;
	}

	public void UpdateMeGameController()
	{
		if(!staticTarget)
		{
			if(!target)
			{
				CallFinalizationEvent();
				return;
			}
			if(!messageSent)
			{
				currentTimeToReachTarget -= GameController.deltaTime;
				if(!targetIsaMonster)
					transform.position = Vector3.Lerp (target.position,bornPosition,currentTimeToReachTarget/totalTimeToReachTarget);
				else
					transform.position = Vector3.Lerp (target.GetComponent<MonsterGeneric>().GetMonsterCenter (),bornPosition,currentTimeToReachTarget/totalTimeToReachTarget);
				if(currentTimeToReachTarget < 0)
					CallFinalizationEvent();
			}
		}
		else
		{
			currentTimeToReachTarget -= GameController.deltaTime;
			transform.position = Vector3.Lerp (targetPosition,bornPosition,currentTimeToReachTarget/totalTimeToReachTarget);
			if(currentTimeToReachTarget < 0)
				CallFinalizationEvent();
		}
	}

	public void SetTarget(Transform target, float timeToReachTarget, Transform finalizationEventTarget, string message)
	{
		this.target = target;
		if(this.target.GetComponent<MonsterGeneric>())
		{
			targetIsaMonster = true;
			iTween.ColorFrom (GameObject.Find ("lifeBar"),Color.red, 1f);
		}
		this.totalTimeToReachTarget = timeToReachTarget;
		currentTimeToReachTarget = this.totalTimeToReachTarget;
		this.finalizationEventTarget = finalizationEventTarget;
		this.message = message;
	}

	public void SetTarget(Vector3 target, float timeToReachTarget)
	{
		targetPosition = target;
		this.totalTimeToReachTarget = timeToReachTarget;
		currentTimeToReachTarget = this.totalTimeToReachTarget;
		staticTarget = true;
	}

	protected void CallFinalizationEvent()
	{
		if(finalizationEventTarget)
			finalizationEventTarget.SendMessage (message,SendMessageOptions.DontRequireReceiver);
		gameController.removeUpdatableGameObject (this.gameObject);
		messageSent = true;
	}
}