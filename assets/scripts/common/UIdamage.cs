using UnityEngine;
using System.Collections;

public class UIdamage : MonoBehaviour {
	
	bool beginexecution =false;
	float countdown;
	public float lifeTime = 2;
	public Font redFont;
	public Material redFontMaterial;
	public Font greenFont;
	public Material greenFontMaterial;
	// Update is called once per frame
	void Awake()
	{
		transform.Rotate (0,0,90);
	}

	void Update () {
		if(beginexecution)
		{
			countdown-=GameController.deltaTime;
			if(countdown<0)
				Destroy(gameObject);
		}
	}
	
	public void SetDamage(float damage)
	{
		gameObject.GetComponent<TextMesh>().text = damage.ToString ("0.0");
		Vector3 v = new Vector3 (Random.Range (-1.5f,1.5f),Random.Range (0,2f),0);
		iTween.MoveTo(gameObject,transform.position+v,lifeTime);
		beginexecution = true;
		countdown = lifeTime;
	}
	
	public void SetRegen(float regen)
	{
		gameObject.GetComponent<TextMesh>().color = Color.green;
		gameObject.GetComponent<TextMesh>().text = "+" + regen.ToString ("0.0");
		iTween.MoveTo(gameObject,transform.position+Vector3.back,lifeTime);
		beginexecution = true;
		countdown = lifeTime;
	}
	
	public void SetPlayerAsTarget(bool playerAsTarget)
	{
		gameObject.GetComponent<TextMesh>().font = redFont;
		GetComponent<Renderer>().material = redFontMaterial;
	}
	
	public void SetManaRegen(int regen)
	{
		gameObject.GetComponent<TextMesh>().text = "+" + regen.ToString ();
		iTween.MoveTo(gameObject,transform.position+Vector3.back,lifeTime);
		beginexecution = true;
		countdown = lifeTime;
	}
	
	public void SetManaLoose(int regen)
	{
		gameObject.GetComponent<TextMesh>().text = "-" + regen.ToString ();
		iTween.MoveTo(gameObject,transform.position+Vector3.back,lifeTime);
		beginexecution = true;
		countdown = lifeTime;
	}
}
