using UnityEngine;
using System.Collections;
 
public class GUISlider : MonoBehaviour {
   
	public enum Type{
		gamma,
		shadowrays,
		SFX,
		music
	}
	
	public float minValue, maxValue;   
	public Type currentType;
	
	/*gamma*/
	public float GammaCorrection = 0.28f;
	/**/
	
	/*SFX*/
	public float SFXValue = 5;
	/**/
	
	/*Music*/
	public float musicValue = 5;
	/**/
	
	/*Music*/
	public int shadowsRays = 1000;
	/**/
	
	enum States{
		unpressed,
		pressed
	}
	States currentState;
	Transform bar;
	float currentValue;
	TextMesh textValue;
	float amplitude;
	GameController gameController;
	void Start()
	{
		gameController = GameController.Instance;
		SFXValue = gameController.PlayerAudioSource.volume;
		musicValue = gameController.MusicAudioSource.volume;
		GammaCorrection = RenderSettings.ambientLight.r;
		bar = transform.FindChild("bar");
		textValue = bar.FindChild("Value").GetComponent<TextMesh>();
		if(minValue > maxValue)
		{
			float temp = maxValue;
			maxValue = minValue;
			minValue = temp;
		}
		amplitude = maxValue - minValue;		
		switch(currentType)
		{
		case Type.gamma:
			currentValue = GammaCorrection;
			break;
		case Type.SFX:
			currentValue = SFXValue;
			break;
		case Type.music:
			currentValue = musicValue;
			break;
		case Type.shadowrays:
			currentValue = shadowsRays;
			break;
		}
		float x = (currentValue / maxValue) * (GetComponent<Collider>().bounds.max.x - GetComponent<Collider>().bounds.min.x) + GetComponent<Collider>().bounds.min.x; 
		bar.position = new Vector3(x,bar.position.y,bar.position.z);
		UpdateValue();
	}
	
    void Update() {
		switch(currentState)
		{
		case States.unpressed:
			if(Input.GetMouseButtonDown(0))
			{
				Ray ray;
				RaycastHit hit;
				ray = Camera.main.ScreenPointToRay((Input.mousePosition));
				if(GetComponent<Collider>().Raycast(ray, out hit, 10000)){
					if(hit.collider.transform.tag == "Slider")
					{
						currentState = States.pressed;
					}
				}
			}
			break;
		case States.pressed:
			float x = Camera.main.ScreenToWorldPoint((Input.mousePosition)).x;
			if(x > GetComponent<Collider>().bounds.max.x)
				x = GetComponent<Collider>().bounds.max.x;
			else if(x < GetComponent<Collider>().bounds.min.x)
					x = GetComponent<Collider>().bounds.min.x;				
			bar.position = new Vector3(x,bar.position.y,bar.position.z);
			if(!Input.GetMouseButton(0))
				currentState = States.unpressed;
			currentValue = ((x - GetComponent<Collider>().bounds.min.x) / (GetComponent<Collider>().bounds.max.x - GetComponent<Collider>().bounds.min.x)) * amplitude + minValue;
			UpdateValue();
			break;
		}
    }
	
	void UpdateValue()
	{
		switch(currentType)
		{
		case Type.gamma:
			GammaCorrection = currentValue;
			textValue.text = (GammaCorrection * 16.66f).ToString("0");
			RenderSettings.ambientLight = new Color(GammaCorrection, GammaCorrection, GammaCorrection, 1.0f);
			break;
		case Type.SFX:
			SFXValue = currentValue;
			textValue.text = (SFXValue * 10).ToString("0");
			gameController.SwitchSFX(SFXValue);
			break;
		case Type.music:
			musicValue = currentValue;
			textValue.text = (musicValue * 10).ToString("0");
			gameController.SwitchMusic(musicValue);
			break;
		}
	}
}