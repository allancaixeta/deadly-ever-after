﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Author: BytoNyte - www.bytonyte.com - Programming blog with a lot of free tutorials!
/// Attach to any game object to print out FPS information to the screen.
/// </summary>
public class FPSCounter : MonoBehaviour 
{
	public Vector2 GUIPosition = Vector2.zero;
	
	private float currentFrameTime;
	private float currentTime;
	private int frameCount;
	
	void Start() 
	{		
		Application.targetFrameRate = 1000;
		
		currentFrameTime = 0.0f;
		currentTime = 0.0f;
		frameCount = 0;
	}
	
	void Update()
	{
		currentTime += Time.deltaTime;
		
		if (currentTime > 1.0f)
		{
			currentFrameTime = 1000.0f/frameCount;
			
			currentTime = 0;
			frameCount = 0;
		}
		
		frameCount++;
	}
	
	void OnGUI()
	{
		GUI.Box(new Rect(GUIPosition.x, GUIPosition.y, 200, 25), "Current frametime: " + currentFrameTime + " ms");
	}
}