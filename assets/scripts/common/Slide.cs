﻿using UnityEngine;
using System.Collections;

public class Slide : MonoBehaviour {

	public GameObject target;
	public float speedMultiplier = 0.1f;
	public bool up;
	// Update is called once per frame
	public void SlideNow() {
		iTween.MoveTo (target, target.transform.position + (Vector3.up * speedMultiplier * (up ? -1 : 1)),1);
	}
}
