﻿using UnityEngine;
using System.Collections;

public class OnEnableInstantiate : MonoBehaviour {

	public GameObject prefab;
	public Transform parent;
	void OnEnable ()
	{
		(Instantiate (prefab, this.transform.position, this.transform.rotation) as GameObject).transform.parent = parent;
	}
}
