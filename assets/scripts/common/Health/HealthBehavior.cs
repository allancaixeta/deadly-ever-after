﻿using UnityEngine;
using System.Collections;



public class HealthBehavior : MonoBehaviour, IDamageable {
	// Life attributes
	[System.Serializable]
	public class LifeAttrs {
		public float maxLife;
		public float lifeRegen;
	}
	[SerializeField] protected LifeAttrs m_Base;
	[SerializeField] protected LifeAttrs m_Scalable;

	protected float m_CurrentLife;


	// Common use properties
	public bool IsDead { get { return(m_CurrentLife <= 0.0f); } }



	public delegate void OnDeathHandler();
	protected event OnDeathHandler OnDeathEvent;



	protected void Awake() {
		m_CurrentLife = m_Base.maxLife + m_Scalable.maxLife;
	}



	protected void OnEnable() {
		if(IsDead) {
			if(OnDeathEvent != null) OnDeathEvent();
		}
	}



	/// <summary>
	/// Applies a damage to this health. Will return true if object is or still dead
	/// after this damage (does not mean that it died from this damage).
	/// </summary>
	public bool ApplyDamage(float damage) {
		bool dead = false;

		m_CurrentLife -= damage;
		if(m_CurrentLife <= 0.0f) {
			dead = true;
			m_CurrentLife = 0.0f;
			if(OnDeathEvent != null) OnDeathEvent();
		}

		return(dead);
	}



	/// <summary>
	/// Registers a callback method to handle OnDeathEvent. If this IsDead is true and
	/// allowImmediateCall is also true, will immediately call the onDeathCallback. This
	/// method then returns IsDead value.
	/// </summary>
	public bool RegisterToDeathEvent(OnDeathHandler onDeathCallback, bool allowImmediateCall) {
		bool isDead = IsDead;

		if(isDead && allowImmediateCall) onDeathCallback();
		OnDeathEvent += onDeathCallback;

		return(isDead);
	}



	public void UnregisterFromDeathEvent(OnDeathHandler onDeathCallback) {
		OnDeathEvent -= onDeathCallback;
	}
}
