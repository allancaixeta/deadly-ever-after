﻿using UnityEngine;
using System.Collections;

public interface IDamageable {
	bool ApplyDamage(float damage);
}

public class DmgMsgs {
	public const string ApplyDamage = "ApplyDamage";
}

