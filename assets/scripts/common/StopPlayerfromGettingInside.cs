﻿using UnityEngine;
using System.Collections;

public class StopPlayerfromGettingInside : MonoBehaviour {

	public float distanceToSpherecastFromPlayer = 1;
	public float distanceToPushPlayer = 1;
	PlayerController player;
	int layerMask = 1024;
	Vector3 upTemp, direction;
	Ray ray;
	RaycastHit hit;
	float angleToPush;
	Vector3 vectorToPush;
	MonsterGeneric.directions labirynthDirection;
	bool on = false;
	Vector2 playerSpeed;
	float delayToTurnOf;
	int errorCounter;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
		upTemp = player.transform.position + new Vector3(0,0,-10);
		direction = player.transform.position - upTemp;
	}

	// Update is called once per frame
	void Update () {
        if (delayToTurnOf > 0)
		{
			delayToTurnOf -= GameController.deltaTime;
			if(delayToTurnOf < 0)
			{
				on = false;
				player.ResetWalkDirectionLock();
			}
		}
		if(on)
		{
			Vector2 playerSpeed = vectorToPush.normalized * distanceToSpherecastFromPlayer;
			upTemp = player.transform.position +  new Vector3(-playerSpeed.x,-playerSpeed.y,-10);
			ray = new Ray(upTemp,direction);
			if(Physics.SphereCast(ray,player.GetComponent<CharacterController>().radius*0.9f,out hit,15,layerMask))
			{
				//print (hit.collider.tag);
				if(hit.collider.tag == Tags.Lvl2Obstacle)
				{
					errorCounter = 0;
					switch(labirynthDirection)
					{
					default:
					case MonsterGeneric.directions.U:
						player.SetWalkDirectionLock(MonsterGeneric.directions.D);
						break;
					case MonsterGeneric.directions.D:
						player.SetWalkDirectionLock(MonsterGeneric.directions.U);
						break;
					case MonsterGeneric.directions.R:
						player.SetWalkDirectionLock(MonsterGeneric.directions.L);
						break;
					case MonsterGeneric.directions.L:
						player.SetWalkDirectionLock(MonsterGeneric.directions.R);
						break;
					}
				}	
			}
			else
			{
				errorCounter++;
				if(errorCounter > 5)
				{
					player.ResetWalkDirectionLock();
					//print ("ResetWalkDirectionLock");
				}
			}
		}
		upTemp = player.transform.position +  new Vector3(0,0,-10);
		ray = new Ray(upTemp,direction);
		if(Physics.SphereCast(ray,player.GetComponent<CharacterController>().radius,out hit,15,layerMask))
		{
			if(hit.collider.tag == Tags.Lvl2Obstacle)
			{
				player.transform.position = player.transform.position + vectorToPush * distanceToPushPlayer * GameController.deltaTime;
			}
		}
	}

	public void SwitchOnStopPlayerfromGettingInside(MonsterGeneric.directions labirynthDirection, Vector3 velocity)
	{
        this.on = true;
		vectorToPush = velocity;
		this.labirynthDirection = labirynthDirection;
		delayToTurnOf = 0.5f;
	}
	public float finalPushDistance = 3;
	public void SwitchOffStopPlayerfromGettingInside()
	{
		delayToTurnOf = 0.5f;
		upTemp = player.transform.position +  new Vector3(0,0,-10);
		ray = new Ray(upTemp,direction);
		if(Physics.SphereCast(ray,player.GetComponent<CharacterController>().radius,out hit,15,layerMask))
		{
			if(hit.collider.tag == Tags.Lvl2Obstacle)
			{
				switch(labirynthDirection)
				{
				default:
				case MonsterGeneric.directions.U:
					player.transform.position = new Vector3(player.transform.position.x,hit.collider.transform.position.y + finalPushDistance,player.transform.position.z);
					break;
				case MonsterGeneric.directions.D:
					player.transform.position = new Vector3(player.transform.position.x,hit.collider.transform.position.y - finalPushDistance,player.transform.position.z);
					break;
				case MonsterGeneric.directions.R:
					player.transform.position = new Vector3(hit.collider.transform.position.x + finalPushDistance,player.transform.position.y,player.transform.position.z);
					break;
				case MonsterGeneric.directions.L:
					player.transform.position = new Vector3(hit.collider.transform.position.x - finalPushDistance,player.transform.position.y,player.transform.position.z);
					break;
				}

			}
		}
	}
}
