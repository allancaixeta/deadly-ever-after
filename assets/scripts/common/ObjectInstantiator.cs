﻿using UnityEngine;
using System.Collections;

public class ObjectInstantiator : MonoBehaviour {

	public GameObject prefab;
	bool started = false;
	// Use this for initialization
	void Start () {
		if(!started)
		{
			GameObject temp = Instantiate (prefab,transform.position,transform.rotation) as GameObject;
			temp.GetComponent<ObjectInstantiator>().enabled = false;
			temp.transform.parent = transform.parent;
		}
		Destroy (this.gameObject);
	}

	public Transform StartNow()
	{
		GameObject temp = Instantiate (prefab,transform.position,transform.rotation) as GameObject;
		temp.GetComponent<ObjectInstantiator>().enabled = false;
		temp.transform.parent = transform.parent;
		started = true;
		return temp.transform;
	}

	public void StartandDestroy()
	{
		GameObject temp = Instantiate (prefab,transform.position,transform.rotation) as GameObject;
		temp.GetComponent<ObjectInstantiator>().enabled = false;
		temp.transform.parent = transform.parent;
		this.gameObject.SetActive (false);
		Destroy (this.gameObject);
	}
}
