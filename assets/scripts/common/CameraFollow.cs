﻿using UnityEngine;
using System.Collections;

[Prefab("CameraControl", true)]
public class CameraFollow : Singleton<CameraFollow>
{
	public float marginX;
	public float marginY;
	public float xSmooth = 8f;		// How smoothly the camera catches up with it's target movement in the x axis.
	public float ySmooth = 8f;		// How smoothly the camera catches up with it's target movement in the y axis.
	public Vector2 maxXAndY;		// The maximum x and y coordinates the camera can have.
	public Vector2 minXAndY;		// The minimum x and y coordinates the camera can have.

	public Vector2 maxXAndYinStage;		// The maximum x and y coordinates the camera can have in the stage
	public Vector2 minXAndYinStage;		// The minimum x and y coordinates the camera can have in the sate
	private Transform player;		// Reference to the player's transform.

	Vector2 tempMaxXYState, tempMinXYStage;
	bool followPlayer = true;
	bool closeFocusPlayer = false;
	float shakeTime;
	int  intensity;
	Animator animator;
	public float deathDuration;
	float currentDeathAnim;
	public LerpEquationTypes lerpType;
	float camOriginalSize;
	public float camZoomSize;
	Vector3 positionAtDeathTime;
	private GameController gameController; 
	void Awake ()
	{		
		// Setting up the reference.
		player = GameObject.FindGameObjectWithTag("Player").transform;
		animator = GetComponent<Animator>();
		tempMaxXYState = maxXAndYinStage;
		tempMinXYStage = minXAndYinStage;
		camOriginalSize = Camera.main.orthographicSize;
		gameController = GameController.Instance;
		transform.Rotate (0, 0, 90);
	}

	void Start () 
	{
	}

	bool CheckXMargin()
	{
		// Returns true if the distance between the camera and the player in the x axis is greater than the x margin.
		return Mathf.Abs(targetX - transform.position.y) < marginX;
	}


	bool CheckYMargin()
	{
		// Returns true if the distance between the camera and the player in the y axis is greater than the y margin.
		return Mathf.Abs(targetY - transform.position.x) < marginY;
	}


	public void UpdateCamera ()
	{
		if(shakeTime > 0)
		{
			shakeTime -= GameController.deltaTime;
			if(shakeTime < 0)
			{
				animator.SetBool ("shake",false);
				animator.SetBool ("shakeIntense",false);
				animator.speed = 1;
			}
		}
		if(followPlayer)
			TrackMouse();
		if(closeFocusPlayer)
		{
			currentDeathAnim -= GameController.deltaTime;
			Camera.main.orthographicSize = lerpType.Lerp (camZoomSize,camOriginalSize,currentDeathAnim / deathDuration);
			transform.position = lerpType.Lerp (player.position,positionAtDeathTime,currentDeathAnim / deathDuration);
			if(currentDeathAnim < 0)
			{
				Camera.main.orthographicSize = camOriginalSize;
				closeFocusPlayer = false;
				gameController.GameOverScreen();
			}
		}
	}

	public void ResetCamera()
	{
		targetX = Mathf.Clamp (player.position.x, minXAndYinStage.x, maxXAndYinStage.x);
		targetY = Mathf.Clamp (player.position.y, minXAndYinStage.y, maxXAndYinStage.y);		
		transform.position = new Vector3 (player.position.x, player.position.y, transform.position.z);
		transform.parent = null;
	}
	
	void TrackMouse ()
	{
		// By default the target x and y coordinates of the camera are it's current x and y coordinates.
		//float targetX = transform.position.x;
		//float targetY = transform.position.y;

		// If the player has moved beyond the x margin...
		//if(CheckXMargin())
			// ... the target x coordinate should be a Lerp between the camera's current x position and the player's current x position.
			//targetX = Mathf.Lerp(player.position.x, currentCursor.currentPosition.x, xSmooth * GameController.deltaTime);

		// If the player has moved beyond the y margin...
		//if(CheckYMargin())
			// ... the target y coordinate should be a Lerp between the camera's current y position and the player's current y position.
			//targetY = Mathf.Lerp(player.position.y, currentCursor.currentPosition.y, ySmooth * GameController.deltaTime);

		// The target x and y coordinates should not be larger than the maximum or smaller than the minimum.
		//targetX = Mathf.Clamp(targetX-player.position.x, minXAndY.x, maxXAndY.x) + player.position.x;
		//targetY = Mathf.Clamp(targetY-player.position.y, minXAndY.y, maxXAndY.y) + player.position.y;

		// Set the camera's position to the target position with the same z component.




		//Vector2 target = currentCursor.currentPosition - player.position;
		//transform.position = new Vector3 (target.x,target.y,transform.position.z);

		mouseposition = Camera.main.ScreenToViewportPoint (Input.mousePosition);

		targetX = Mathf.Lerp(player.transform.position.y - minXAndY.x, player.transform.position.y + maxXAndY.x, mouseposition.x);
		targetY = Mathf.Lerp(player.transform.position.x - minXAndY.y, player.transform.position.x + maxXAndY.y, 1 - mouseposition.y); 

		if (CheckXMargin ())
			targetX = transform.position.y;
		if (CheckYMargin ())
			targetY = transform.position.x;

		//targetX = Mathf.Clamp (targetX, minXAndYinStage.x, maxXAndYinStage.x);
		//targetY = Mathf.Clamp (targetY, minXAndYinStage.y, maxXAndYinStage.y);
		transform.position = Vector3.Lerp (transform.position, new Vector3 (targetY , targetX, transform.position.z),xSmooth * GameController.deltaTime);
	}

	Vector2 mouseposition;
	float targetX,targetY;

	public void FixPosition(bool f)
	{
		followPlayer = !f;
	}

	public void ShakeCamera(float time, int intensity)
	{
		shakeTime = time;
		animator.SetBool ("shake",true);
		animator.speed = intensity;
	}

	
	public void ShakeIntenseCamera(float time, int intensity)
	{
		shakeTime = time;
		animator.SetBool ("shakeIntense",true);
		animator.speed = intensity;
	}



	public void ChangeMargins(Vector2 maxXYStage, Vector2 MinXYStage)
	{
		maxXAndYinStage = maxXYStage;
		minXAndYinStage = MinXYStage;
	}

	public void RestoreMargins()
	{
		maxXAndYinStage = tempMaxXYState;
		minXAndYinStage = tempMinXYStage;
	}

	public void FocusPlayer()
	{
		followPlayer = false;
		closeFocusPlayer = true;
		currentDeathAnim = deathDuration;
		positionAtDeathTime = transform.position;
	}
}
