﻿using UnityEngine;
using System.Collections;

public class Testing : MonoBehaviour {

    public CharacterController c;
	void Awake () {
		print ("Script was awaked");
	}
    CapsuleCollider b;
    bool ativo;
    // Use this for initialization
    void Start () {
        c = GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update () {
        //print (Time.time);
        if (Input.GetKeyDown(KeyCode.B))
            ativo = !ativo;
         if(ativo)
            c.SimpleMove(Vector2.zero*Time.deltaTime);
	}

	void OnDestroy() {
		print("Script was destroyed");
	}
}
