﻿using UnityEngine;
using System.Collections;
//componente para checar linha de visao entre o monstro/objeto e o jogador
public class LineOfSight : MonoBehaviour {

	public bool upperVision;
	Vector3 direction;
	Vector3 origin;
	Vector3 charControllerAdjust; //ajuste para centralizar o raycast na posicao exata do character controller
	Ray ray;
	RaycastHit hit;
	int layerMask = 1049600; //somente objetos e o jogador
	Transform player;
	public bool hasLineOfSight = false;
	public float lostLineOfSightConfirmation = 0.08f; //depois de perder linha de visao testa por 0.5 segundos para confirmar que perdeu
	float currentLostLineOfSightTimer = 0;
	public GameObject currentGameobj;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player").transform;
		charControllerAdjust = GetComponent<CharacterController> ().center;
	}
	
	// Update is called once per frame
	void Update () {
		origin = transform.position;
		this.direction = player.position - origin;
		ray = new Ray(origin,direction);
		if(Physics.SphereCast (ray,0.5f,out hit, 200f, layerMask)){
			currentGameobj = hit.collider.gameObject;
			if(hit.collider.tag == "Player")
			{
				hasLineOfSight = true;
				currentLostLineOfSightTimer = lostLineOfSightConfirmation;
			}
			else
			{
				CheckLostLineOfSight();
			}
		}
		else  //SOMENTE ACONTECE ESSE ERRO QUANDO ELE ESTA DENTRO DO JOGADOR, ENTAO DESNECESSARIO MANTER ESSSA EXCECAO
		{
			if(Vector2.Distance (origin,player.transform.position) > 2)
				CheckLostLineOfSight();
			else
				Debug.LogWarning ("Raycast error, no gameobject found.");
			//Debug.DrawRay(origin,direction,Color.white,2);

		}
	}

	void CheckLostLineOfSight()
	{
		currentLostLineOfSightTimer -= GameController.deltaTime;
		if(currentLostLineOfSightTimer < 0)
		{
			if(hasLineOfSight && Vector2.Distance (player.position, origin) > 1.5f)
				hasLineOfSight = false;
		}
	}

	public Vector3 GetDirection()
	{
		return direction;
	}
}
