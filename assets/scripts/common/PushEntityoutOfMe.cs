﻿using UnityEngine;
using System.Collections;

public class PushEntityoutOfMe : MonoBehaviour {


	Vector3 pushDirection = Vector3.right;
	public float strenghtMultiplier = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetPushDirection(Vector3 dir)
	{
		pushDirection = dir;
	}

	void OnTriggerEnter(Collider other)
	{       
		if(other.transform.tag == Tags.Monster)
		{
			other.transform.GetComponent<MonsterGeneric>().Stun (0.25f);
			other.transform.Translate (pushDirection * GameController.deltaTime * strenghtMultiplier,Space.World);
		}
	}

	void OnTriggerStay(Collider other)
	{
		OnTriggerEnter(other);
	}

}
