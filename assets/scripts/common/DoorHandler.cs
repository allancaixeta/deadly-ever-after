using UnityEngine;
using System.Collections;

public class DoorHandler : MonoBehaviour {

	public int doorSide;
	public bool doorWorking = false;
	public int destinationRoom; 
	public GameObject openVisual;
	StageController stageController;
	float timer;
	void Awake()
	{
		stageController = (StageController) FindObjectOfType(typeof(StageController));
	}

	void Update()
	{
		timer -= GameController.deltaTime;
	}

	public void SetDestinationRoom(int d)
	{
		destinationRoom = d;
	}

	public void OpenDoor()
	{
		if(destinationRoom!=0)
		{
			doorWorking = true;
			openVisual.SetActive (true);
			timer = 0.1f;
		}
	}

	void OnTriggerEnter(Collider other)
	{		
		if(doorWorking && timer < 0 && other.transform.tag == "Player")
		{
			doorWorking = false;
			stageController.MoveToRoom(destinationRoom-1,NextDoorSide());
		}
	}

	int NextDoorSide()
	{
		switch(doorSide)
		{
		default:
		case 1:
			return 3;
		case 2:
			return 4;
		case 3:
			return 1;
		case 4:
			return 2;
		}
	}
}