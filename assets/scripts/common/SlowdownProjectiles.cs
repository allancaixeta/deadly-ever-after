﻿using UnityEngine;
using System.Collections;

public class SlowdownProjectiles : MonoBehaviour {

	public float speedReduction = 0.2f;
	public GameObject FX;
	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Collider>().tag == Tags.Weapon)
		{
			if(other.GetComponent<WeaponController>().ProjectileSpeed == 0)
				return;
			other.GetComponent<WeaponController>().ProjectileSpeed = (int)((float)other.GetComponent<WeaponController>().ProjectileSpeed * (1-speedReduction));
			GameObject fx = Instantiate (FX,transform.position,Quaternion.identity) as GameObject;
			fx.transform.parent = this.transform;
		}
	}

}
