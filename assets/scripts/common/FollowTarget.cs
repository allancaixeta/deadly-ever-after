﻿using UnityEngine;
using System.Collections;

public class FollowMouse : MonoBehaviour {

	CursorController cursor;

	// Use this for initialization
	void Start () {
		cursor = CursorController.Instance;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = cursor.currentPosition;
	}
}
