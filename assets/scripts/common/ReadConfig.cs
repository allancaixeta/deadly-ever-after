using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;

public class ReadConfig
{
    private Dictionary<string, string> dictionary = new Dictionary<string, string>();

	public ReadConfig(string FileName)
	{
		//Debug.Log(FileName);
		if (File.Exists(FileName))
		{
			string[] all_lines = File.ReadAllLines(FileName);
			LoadFile(all_lines);
		} else {
			Debug.LogError("Error [ReadConfig]: specified file [" + FileName + "] not found.");
		}
    }
	
	
	
	public ReadConfig(TextAsset Asset)
	{
		string[] all_lines = Asset.text.Split('\n', '\r');
		LoadFile(all_lines);
	}
	
	
	
    private void LoadFile(string[] all_lines)
    {
		dictionary.Clear();
		
        foreach (string line in all_lines)
        {
            if ((!string.IsNullOrEmpty(line)) &&
                (!line.StartsWith(";")) &&
                (!line.StartsWith("#")) &&
			    (!line.StartsWith("//")) &&
                (!line.StartsWith("'")) &&
                (line.Contains("=")))
            {
                int index = line.IndexOf('=');
                string key = line.Substring(0, index).Trim();
                string value = line.Substring(index + 1).Trim();

                if ((value.StartsWith("\"") && value.EndsWith("\"")) ||
                    (value.StartsWith("'") && value.EndsWith("'")))
                {
                    value = value.Substring(1, value.Length - 2);
                }
                dictionary.Add(key, value);
            }
        }
    }
	
	
	//Static wrappers...
	
	
	
	#region String
	
	public string GetString(string key)
	{
		return GetString(key, "");
	}
	
	public string GetString(string key, string defaultValue)
	{
		if (dictionary.ContainsKey(key))
		{
			string str = dictionary[key] as string;
			return str.Replace("\\n","\n");
		}
		return defaultValue;
	}
	
	#endregion
	
	
	
	#region Int
	
	public int GetInt(string key)
	{
		return GetInt(key, 0);
	}
	public int GetInt(string key, int defaultValue)
	{
		if (dictionary.ContainsKey(key))
		{
			return int.Parse(GetString(key));
		}
		return defaultValue;
	}
	
	#endregion
	
	
	
	#region Float
	
	public float GetFloat(string key)
	{
		return GetFloat(key, 0.0f);
	}
	
	public float GetFloat(string key, float defaultValue)
	{
		if (dictionary.ContainsKey(key))
		{
			return float.Parse(GetString(key));	
		}
		return defaultValue;
	}	
	
	#endregion
	
	
	
	#region Bool
	public bool GetBool(string key)
	{
		return GetBool(key, false);
	}
	
	public bool GetBool(string key, bool defaultValue)
	{
		if (dictionary.ContainsKey(key))
		{
			return bool.Parse(GetString(key));	
		}
		return defaultValue;
	}
	#endregion
}
