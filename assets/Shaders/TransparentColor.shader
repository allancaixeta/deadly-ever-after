// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Transparent/Color" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		
		Pass {
			Lighting Off
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform fixed4 _Color;
	
			
			
			struct Input {
				half4 vertex : POSITION;
			};
			
			struct V2F {
				half4 vertex : POSITION;
			};
			
			
			
			V2F vert(Input In) {
				V2F Out;
				Out.vertex = UnityObjectToClipPos(In.vertex);
				return(Out);
			}
			
			
			
			fixed4 frag(V2F In) : COLOR {
				return(_Color);
			}
	
			ENDCG
		}
	} 
}
