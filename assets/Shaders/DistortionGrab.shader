// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/DistortionGrab" {
    Properties {
        _NoiseTex ("Noise Texture (RG)", 2D) = "white" {}
        _WaterSpeed2 ("Water Speed", Range (0, 10)) = 0.37
    }
   SubShader
{
 Tags { "Queue" = "Transparent+10" }

   	GrabPass {
		Name "DistortionGrab"
	}

	Pass
	{
		Name "Distortion"
		Blend Off
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			struct v2f {
				float4 pos      : POSITION;
				float4 uvgrab       : TEXCOORD0;
				float2 uv       : TEXCOORD1;
				float4 screenPos    : TEXCOORD2;
			};

			v2f vert (appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);   
				#if UNITY_UV_STARTS_AT_TOP
				float scale = -1.0;
				#else
				float scale = 1.0;
				#endif
				o.uvgrab.xy = (float2(o.pos.x, o.pos.y * scale) + o.pos.w) * 0.5;
				o.uvgrab.zw = o.pos.zw;
				o.uv = v.texcoord.xy;
				return o;
			}

			sampler2D _BumpMap;
			sampler2D _GrabTexture;
			float4 _GrabTexture_TexelSize;
			float _WaterSpeed2;
			half4 frag( v2f i ) : COLOR
			{
				float2 riverUVs = IN.uv_BumpMap;
				riverUVs.y += 0.3 + _Time * _WaterSpeed2;
				float3 normal = UnpackNormal(tex2D(_BumpMap, i.uv));
		   
				float2 offset = normal.xy * 5 * _GrabTexture_TexelSize.xy;
				i.uvgrab.xy = (offset * i.uvgrab.z) + i.uvgrab.xy;
				return half4(tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(i.uvgrab)).rgb, 1);
			}
		ENDCG
	}
	}
}