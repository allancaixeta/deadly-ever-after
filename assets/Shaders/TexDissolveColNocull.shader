// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

    //  Created by Nils Daumann on 04.06.2011.
    //  Copyright (c) 2011 Nils Daumann
     
    //  Permission is hereby granted, free of charge, to any person obtaining a copy
    //  of this software and associated documentation files (the "Software"), to deal
    //  in the Software without restriction, including without limitation the rights
    //  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    //  copies of the Software, and to permit persons to whom the Software is
    //  furnished to do so, subject to the following conditions:
     
    //  The above copyright notice and this permission notice shall be included in
    //  all copies or substantial portions of the Software.
     
    //  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    //  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    //  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    //  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    //  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    //  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    //  THE SOFTWARE.
     
    //  www.slindev.com
     
    Shader "Custom/Dissolve/TexDissolveColNocull"
    {
        Properties
        {
            _Color("Dissolve Color (RGB), Range (A)", Color) = (1.0, 1.0, 1.0, 1.0)
            _AlphaDisc("Dissolve state", Range(0, 1)) = 0.5
            _MainTex("Base (RGB), Dissolve Mask (A)", 2D) = "white" {}
        }
       
        //GLSL shader used on devices rendering with OpenGL ES 2.0
        SubShader
        {
            Tags {"IgnoreProjector"="True" "RenderType"="TransparentCutout"}
            Pass
            {
                Tags {"LightMode" = "Always"}
                Cull Off
               
                GLSLPROGRAM
                    #pragma fragmentoption ARB_precision_hint_fastest
                   
                    uniform vec4 _Color;
                    uniform float _AlphaDisc;
                    uniform sampler2D _MainTex;
                    varying mediump vec2 texcoord;
                   
                    #ifdef VERTEX
                        void main()
                        {
                            gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
                            texcoord = gl_MultiTexCoord0.xy;
                        }
                    #endif
                   
                    #ifdef FRAGMENT
                        void main()
                        {
                            gl_FragColor = texture2D(_MainTex, texcoord);
                            if(gl_FragColor.a < _AlphaDisc+_Color.a)
                                gl_FragColor.rgb = _Color.rgb;
                            if(gl_FragColor.a < _AlphaDisc)
                                discard;
                        }
                    #endif
                ENDGLSL
            }
        }
       
        //CG/HLSL shader used on all other devices with shader support
        SubShader
        {
            Tags {"IgnoreProjector"="True" "RenderType"="TransparentCutout"}
            Pass
            {
                Tags {"LightMode" = "Always"}
                Cull Off
               
                CGPROGRAM
                    #pragma exclude_renderers gles
                    #pragma vertex vert
                    #pragma fragment frag
                    #pragma fragmentoption ARB_precision_hint_fastest
                   
                    uniform float4 _Color;
                    uniform float _AlphaDisc;
                    uniform sampler2D _MainTex;
                   
                    struct appdata_base
                    {
                        float4 vertex : POSITION;
                        float4 texcoord : TEXCOORD0;
                    };
                   
                    struct v2f
                    {
                        float4 pos : POSITION;
                        float2 texcoord : TEXCOORD0;
                    };
     
                    v2f vert(appdata_base v)
                    {
                        v2f o;
                       
                        o.pos = UnityObjectToClipPos(v.vertex);
                        o.texcoord.xy = v.texcoord.xy;
                       
                        return o;
                    }
                   
                    float4 frag(v2f i) : COLOR
                    {
                        half4 Color = tex2D(_MainTex, i.texcoord);
                        if(Color.a < _AlphaDisc+_Color.a)
                            Color.rgb = _Color.rgb;
                        clip(Color.a-_AlphaDisc);
                        return Color;
                    }
                ENDCG
            }
        }
       
        //Fallback for devices not supporting shaders at all
        SubShader
        {
            Tags {"IgnoreProjector"="True" "RenderType"="TransparentCutout"}
            Pass
            {
                Tags {"LightMode" = "Always"}
                Cull Off
                AlphaTest Greater [_AlphaDisc]
                SetTexture [_MainTex]
                {
                    ConstantColor [_Color]
                    Combine texture*constant, texture*constant
                }
            }
        }
    }