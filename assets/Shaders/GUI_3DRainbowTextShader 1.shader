// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "GUI/3D Rainbow Text Shader" { 
	Properties { 
	   _MainTex ("Font Texture", 2D) = "white" {}
	   _RainbowCenterPos ("Rainbow Center Pos 2D", Vector) = (0,0,0,0)
	   _RainbowSize ("Rainbow Size", float) = 30
	   _Speed ("Anim Speed", float) = 1
	} 
	
	
	
	SubShader { 
	   Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" } 
	   Lighting Off Cull Off ZWrite Off Fog { Mode Off } 
	   Blend SrcAlpha OneMinusSrcAlpha 
	   Pass { 
	      CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform half2 _RainbowCenterPos;
			uniform half _RainbowSize;
			uniform half _Speed;
	
			
			
			struct Input {
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
			};
			
			struct V2F {
				half4 vertex : POSITION;
				half4 texcoord : TEXCOORD0;
				half4 texcoord1 : TEXCOORD1;
			};
			
			
			
			V2F vert(Input In) {
				V2F Out;
				Out.vertex = UnityObjectToClipPos(In.vertex);
				Out.texcoord = In.texcoord;
				Out.texcoord1 = mul(unity_ObjectToWorld, In.vertex);
				return(Out);
			}
			
			
			
			half4 frag(V2F In) : COLOR {
				half dist = 
					fmod(
						distance(In.texcoord1.xy, _RainbowCenterPos.xy) + (_Time * _Speed),
						_RainbowSize
					)
				/ _RainbowSize;
				
				half4 finalColor = half4(0, 0, 0, 1);
				finalColor.r = clamp(max(-6.25*dist + 2.06,  6.25*dist - 4.19), 0, 1);
				finalColor.g = clamp(min( 5.88*dist       , -5.88*dist + 3.94), 0, 1);
				finalColor.b = clamp(min( 5.88*dist - 1.94, -5.88*dist + 5.88), 0, 1);
				
				finalColor.a *= tex2D(_MainTex, In.texcoord.xy).a;
				return(finalColor);
			}
	
			ENDCG
	   } 
	} 
}