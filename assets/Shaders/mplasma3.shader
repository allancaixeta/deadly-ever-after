// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/mplasma3"
{
Properties
{
_Time("Time", Float) = 0
_ResX("_ResX", Float) = 128
_ResY("_ResY", Float) = 128
}
SubShader
{
Tags {Queue = Geometry}
Pass
{
CGPROGRAM
#pragma target 3.0
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"
 
uniform float _ResX;
uniform float _ResY;
uniform float _Anim;
 
struct v2f {
float4 pos : POSITION;
float4 color : COLOR0;
float4 fragPos : COLOR1;
};
 
v2f vert (appdata_base v)
{
v2f o;
o.pos = UnityObjectToClipPos (v.vertex);
o.fragPos = o.pos;
o.color = float4 (1.0, 1.0, 1.0, 1);
return o;
}
 
half4 frag (v2f i) : COLOR {
float4 outColor = i.color;
// main code, *original shader by: ‘Plasma’ by Viktor Korsun (2011)
float x = i.fragPos.x;
float y = i.fragPos.z;
float mov0 = x+y+cos(sin(_Time/10)*2.)*100.+sin(x/100.)*1000.;
float mov1 = y / _ResY / 0.2 + _Time;
float mov2 = x / _ResX / 0.2;
float c1 = abs(sin(mov1+_Time)/2.+mov2/2.-mov1-mov2+_Time);
float c2 = abs(sin(c1+sin(mov0/1000.+_Time)+sin(y/40.+_Time)+sin((x+y)/100.)*3.));
float c3 = abs(sin(c2+cos(mov1+mov2+c2)+cos(mov2)+sin(x/1000.)));
outColor.rgba = float4(c1,c2,c3,0.5);
return outColor;
}
ENDCG
}
}
FallBack "VertexLit"
}