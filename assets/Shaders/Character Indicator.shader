    Shader "Custom/Character Indicator" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _WrapTex ("Wrap ramp (RGBA)", 2D) = "black" {}
        _IndicatorTex ("Indicator Lights (RGB)", 2D) = "white" {}
        _Indicator ("Indicator Color", Color) = (1,1,1,1)
        _Cutoff ("Alpha cutoff", Range (0,1)) = 0.0
    }
     
    SubShader {
        Tags { [COLOR="red"]"Queue" = "Geometry+1"[/COLOR] "RenderType" = "Opaque" }
       
    [COLOR="red"]       Pass {         
                Tags { "LightMode" = "Always" }
                AlphaTest Greater [_Cutoff]
                ZWrite Off
                ZTest Greater
               
                SetTexture [_MainTex] {
                    constantColor [_Indicator]
                    combine constant, texture
                }
            }[/COLOR]
     
    CGPROGRAM
    #pragma surface surf Ramp alphatest:_Cutoff
     
    uniform float4 _Color;
    uniform float4 _Indicator;
    uniform sampler2D _MainTex;
    uniform sampler2D _WrapTex;
    uniform sampler2D _IndicatorTex;
     
    half4 LightingRamp (SurfaceOutput s, half3 lightDir, half atten) {
        half NdotL = dot (s.Normal, lightDir);
        half diff = NdotL * 0.5 + 0.5;
        half3 ramp = tex2D (_WrapTex, float2(diff)).rgb;
        half4 c;
        c.rgb = s.Albedo * _LightColor0.rgb * ramp * (atten * 2);
        c.a = s.Alpha;
        return c;
    }
     
    struct Input {
        float2 uv_MainTex;
        float2 uv_BumpMap;
        float3 viewDir;
    };
     
    void surf (Input IN, inout SurfaceOutput o) {
        o.Albedo = tex2D ( _MainTex, IN.uv_MainTex).rgb * _Color;
        o.Emission = tex2D ( _IndicatorTex, IN.uv_MainTex).rgb * _Indicator * 2;
       
        half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
        o.Emission += tex2D ( _MainTex, IN.uv_MainTex).rgb * 2 * pow (rim, 3.0);
        o.Alpha = tex2D ( _MainTex, IN.uv_MainTex).a;
    }
     
    ENDCG
     
    }
     
    Fallback " Glossy", 0
     
    }