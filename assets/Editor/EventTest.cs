﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(floorhazard))]
public class EventTest : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		floorhazard myScript = (floorhazard)target;
		if(GUILayout.Button("Start Event"))
		{
			myScript.StartEvent(StageController.StageNumber.stage1);
		}
	}
}