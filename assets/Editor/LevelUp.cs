﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SkillController))]
public class LevelUp : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		SkillController myScript = (SkillController)target;
		if(GUILayout.Button("Level UP!"))
		{
			myScript.LevelUp ();
		}
	}
}